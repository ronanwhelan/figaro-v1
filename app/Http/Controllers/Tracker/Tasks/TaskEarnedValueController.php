<?php

namespace App\Http\Controllers\Tracker\Tasks;


use App\Models\Task;
use App\Models\TaskEarnedValue;
use App\Rowsys\Tracker\Classes\UpdateTaskEarnedValues;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class TaskEarnedValueController extends Controller
{

    protected $stepNames = ['gen_perc','rev_perc','re_issu_perc','s_off_perc'];
    protected $stepHrs = ['gen_hrs','rev_hrs','re_issu_hrs','s_off_hrs'];
    protected $stepTargetHrs  = ['gen_target_hrs','rev_target_hrs','re_issu_target_hrs','s_off_target_hrs'];
    protected $stepCompleteDate  = ['gen_complete_date','re_issu_complete_date','re_issu_complete_date','s_off_complete_date'];

    public function insertCurrentTasksEarned(){

        $rulesPercBreakDown = [70,0,20,10];

        $completeDate = Carbon::create(2017, 9, 1);

        for ( $i = 0; $i < 4; $i++){

            $insertCollection = collect([]);

            $stepPerc = $this->stepNames[$i];

            $stepHrs = $this->stepHrs[$i];

            $stepCompleteDate = $this->stepCompleteDate[$i];

            $stepTargetHrs = $this->stepTargetHrs[$i];

            $stepTasks = Task::where($stepPerc,'>',0)->get();

            foreach ($stepTasks as $task){

                if($task->$stepPerc  > 99){
                    $completeDate = $task->$stepCompleteDate;
                }

                $insertCollection->push([

                    'task_id' => $task->id,
                    'stage_id' => $task->stage_id,
                    'type_id' => $task->task_type_id,
                    'step_num' => $i + 1,
                    'group_owner_id' => $task->group_owner_id,
                    'begin_perc' => 0,
                    'end_perc' => $task->$stepPerc,
                    'dif_perc' => $task->$stepPerc,
                    'task_target_hrs' => $task->target_val,
                    'step_rule_perc' => $rulesPercBreakDown[$i],
                    'created_at' => $completeDate,
                    'earned_hrs' => $task->$stepHrs,
                    'step_target_hrs' => $task->$stepTargetHrs,
                    'created_by_id' => Auth::user()->id,

                ]);
            }

            TaskEarnedValue::insert(
                $insertCollection->toArray()
            );

            //return $insertCollection->all();
        }

    }
}

<?php

namespace App\Http\Controllers\Tracker\Tasks;


use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Models\Task;
use Carbon\Carbon;

class CompileTasksController extends Controller
{

    public function index(){

        $tasks = Task::where('compiled', 1)->orderBy('compiled_at','ASC')->get();

        $report = [];
        if(sizeof($tasks) > 0){
            $today = Carbon::now();
            $lastCompileGuess = Carbon::create($today->year, $today->month , $today->day ,04,00,00);
            $startTime = Task::where('compiled', 1)->where('complete', 0)->orderBy('compiled_at','ASC')->first()->compiled_at;
            $finishTime = Task::where('compiled', 1)->where('complete', 0)->where('compiled_at','<',$today)->orderBy('compiled_at','DESC')->first()->compiled_at;
            $duration = $startTime->diffInMinutes($finishTime);

            $lastCompiledAt = Carbon::now()->diffInMinutes($finishTime);
            $lastCompileDisplay = $lastCompiledAt. ' mins ago';
            if((int)$lastCompiledAt > 60){
                $lastCompiledAt = Carbon::now()->diffInHours($finishTime);
                $lastCompileDisplay = $lastCompiledAt. ' hours ago';
            }

            $compiledCount = Task::where('compiled', 1)->count();
            $notCompiled = Task::where('compiled', 0)->where('complete',0)->count();

           // $total  = Task::where('complete',0)->count();
            //$remaining = $total - $compiledCount;

            $taskWithErrorsCount = '';

            $totalComplete = 0;

            $report = [
                 $startTime,$finishTime,$duration,
                $compiledCount,$notCompiled,$lastCompileDisplay
            ];
        }


        return view('rowsys.tracker.admin.index', [
           'report' => $report,
        ]);
    }

}

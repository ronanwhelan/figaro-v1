<?php

namespace App\Http\Controllers\Tracker\StepPhases;

use App\Jobs\AddTaskStepPhases;
use App\Jobs\StepPhases\UpdateTaskStepPhasesAfterPhaseUpdated;
use App\Jobs\StepPhases\UpdateTaskPercentAndUpdateOrAddStepPhase;
use App\Models\Group;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskStepPhase;
use App\Models\TaskType;
use App\Models\TaskTypeStepPhase;
use App\Rowsys\Tracker\Classes\StepPhases\UpdateTaskStepPercentage;
use App\Rowsys\Tracker\Classes\StepPhases\UpdateTaskStepPhase;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StepPhaseController extends Controller
{
    //

    public function index()
    {

        $taskTypeStepsPhases = TaskTypeStepPhase::with('taskType', 'stage')->get();
        $stages = Stage::orderBy('name', 'ASC')->get();
        $taskTypes = TaskType::orderBy('name', 'ASC')->get();
        $groups = Group::orderBy('name', 'ASC')->get();

        return view('rowsys.tracker.step_phases.read.table.index', [
            'taskTypeStepsPhases' => $taskTypeStepsPhases,
            'stages' => $stages,
            'taskTypes' => $taskTypes,
            'groups' => $groups,
        ]);
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'stage' => 'required',//
            'category' => 'required',
            'step' => 'required',
            //'phase_1' => 'min:2',
        ]);

        $exists = TaskTypeStepPhase::where('stage_id', (int)$request->stage)
            ->where('task_type_id', (int)$request->category)
            ->where('step_number', (int)$request->step)
            ->exists();
        if ($exists) {
            return \Response::json(['message' => 'A rule already exists for this step, category and stage'], 422);
        }

        $taskTypeStepPhase = new TaskTypeStepPhase();
        $taskTypeStepPhase->stage_id = (int)$request->stage;
        $taskTypeStepPhase->task_type_id = (int)$request->category;
        $taskTypeStepPhase->step_number = (int)$request->step;

        $taskTypeStepPhase->break_down_count = (int)$request->phase_break_down_count;

        $taskTypeStepPhase->p1_owner_id = (int)$request->phase_1;
        $taskTypeStepPhase->p2_owner_id = (int)$request->phase_2;
        $taskTypeStepPhase->p3_owner_id = (int)$request->phase_3;
        $taskTypeStepPhase->p4_owner_id = (int)$request->phase_4;
        $taskTypeStepPhase->p5_owner_id = (int)$request->phase_5;
        $taskTypeStepPhase->p6_owner_id = 1;
        $taskTypeStepPhase->p7_owner_id = 1;
        $taskTypeStepPhase->p8_owner_id = 1;
        $taskTypeStepPhase->p9_owner_id = 1;
        $taskTypeStepPhase->p10_owner_id = 1;

        $count = 0;
        if ((int)$request->phase_1 > 1) {
            $count++;
        };
        if ((int)$request->phase_2 > 1) {
            $count++;
        };
        if ((int)$request->phase_3 > 1) {
            $count++;
        };
        if ((int)$request->phase_4 > 1) {
            $count++;
        };
        if ((int)$request->phase_5 > 1) {
            $count++;
        };
        $taskTypeStepPhase->phase_count = $count;
        $taskTypeStepPhase->save();
        $message = "Step Phase Added - All effected tasks will now be updated";

        $this->dispatch(new UpdateTaskPercentAndUpdateOrAddStepPhase($taskTypeStepPhase));

       // $this->dispatch(new AddTaskStepPhases($taskTypeStepPhase));

        $data = ['message' => $message];

        return $data;
    }

    public function updateForm($id)
    {

        $stepPhases = TaskTypeStepPhase::find($id);

        $taskCount = TaskStepPhase::where('task_type_step_id', $id)->count();

        $category = TaskType::find($stepPhases->task_type_id);

        $groups = Group::orderBy('name','ASC')->get();

        return view('rowsys.tracker.step_phases.update.form', [
            'groups' => $groups,
            'stepPhases' => $stepPhases,
            'taskCount' => $taskCount,
            'category' => $category,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id)
    {
        $taskTypeStepPhase = TaskTypeStepPhase::find($id);

        $taskTypeStepPhase->p1_owner_id = (int)$request->phase_1;
        $taskTypeStepPhase->p2_owner_id = (int)$request->phase_2;
        $taskTypeStepPhase->p3_owner_id = (int)$request->phase_3;
        $taskTypeStepPhase->p4_owner_id = (int)$request->phase_4;
        $taskTypeStepPhase->p5_owner_id = (int)$request->phase_5;
        $taskTypeStepPhase->p6_owner_id = 1;
        $taskTypeStepPhase->p7_owner_id = 1;
        $taskTypeStepPhase->p8_owner_id = 1;
        $taskTypeStepPhase->p9_owner_id = 1;
        $taskTypeStepPhase->p10_owner_id = 1;

        $count = 0;
        if ((int)$request->phase_1 > 1) {
            $count++;
        };
        if ((int)$request->phase_2 > 1) {
            $count++;
        };
        if ((int)$request->phase_3 > 1) {
            $count++;
        };
        if ((int)$request->phase_4 > 1) {
            $count++;
        };
        if ((int)$request->phase_5 > 1) {
            $count++;
        };
        if ((int)$request->phase_6 > 1) {
            $count++;
        };
        if ((int)$request->phase_7 > 1) {
            $count++;
        };
        if ((int)$request->phase_8 > 1) {
            $count++;
        };
        if ((int)$request->phase_9 > 1) {
            $count++;
        };
        if ((int)$request->phase_10 > 1) {
            $count++;
        };
        $taskTypeStepPhase->phase_count = $count;

        if ($count > 0) {
            $taskTypeStepPhase->break_down_count = 0;
        } else {
            $taskTypeStepPhase->break_down_count = (int)$request->step_count;
        }

        $taskTypeStepPhase->save();

        TaskStepPhase::where('task_type_step_id',$taskTypeStepPhase->id)
            ->update([
                'phase_count' => $taskTypeStepPhase->phase_count,
                'break_down_count' => $taskTypeStepPhase->break_down_count,
                'p1_owner_id' => $taskTypeStepPhase->p1_owner_id,
                'p2_owner_id' => $taskTypeStepPhase->p2_owner_id,
                'p3_owner_id' => $taskTypeStepPhase->p3_owner_id,
                'p4_owner_id' => $taskTypeStepPhase->p4_owner_id,
                'p5_owner_id' => $taskTypeStepPhase->p5_owner_id,
            ]);

        $this->dispatch(new UpdateTaskPercentAndUpdateOrAddStepPhase($taskTypeStepPhase));

        return "Updated" ;
    }


    public function ownerStats(Request $request)
    {

        $taskIds = $request->listOfTaskIds;
        //$taskSteps = TaskStepPhase::whereIn('task_id',$taskIds)->count();
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $data = [];

        foreach ($groups as $group) {

            $step2Count = TaskStepPhase::whereIn('task_id', $taskIds)
                ->where('step_number', 2)
                ->where('break_down_count', 0)
                ->where('p1_owner_id', $group->id)
                ->Where(function ($query) use ($group) {
                    $query->where('p1_owner_id', $group->id)
                        ->orWhere('p2_owner_id', $group->id)
                        ->orWhere('p3_owner_id', $group->id)
                        ->orWhere('p4_owner_id', $group->id)
                        ->orWhere('p5_owner_id', $group->id);
                })
                ->count();
            $step4Count = TaskStepPhase::whereIn('task_id', $taskIds)
                ->where('step_number', 4)
                ->where('break_down_count', 0)
                ->Where(function ($query) use ($group) {
                    $query->where('p1_owner_id', $group->id)
                        ->orWhere('p2_owner_id', $group->id)
                        ->orWhere('p3_owner_id', $group->id)
                        ->orWhere('p4_owner_id', $group->id)
                        ->orWhere('p5_owner_id', $group->id);
                })
                ->count();

            if ($step2Count > 0 || $step4Count > 0) {
                $ownerData = [$group->name, $step2Count, $step4Count];
                array_push($data, $ownerData);
            }
        }
        return $data;

    }


    //Compile all the Task Step Phases - loop through all the Task Type Step Phases
    public function compileTaskStepPhases()
    {

        /* $query  = 'DELETE FROM task_step_phases';
         $message = \DB::getPdo()->exec($query);*/

        $taskTypeStepPhases = TaskTypeStepPhase::get();

        foreach($taskTypeStepPhases as $taskTypeStepPhase){
            $this->dispatch(new UpdateTaskPercentAndUpdateOrAddStepPhase($taskTypeStepPhase));
        }

        $html = "<h1>Compiling all Task Step Phases</h1> <br>
                <button style='
                font-size: 140%;
                text-align: center;
                background-color: #808080;
                border: none;
                border-radius: 12px;
                color: white;
                padding: 15px 32px;'
                onclick='javascript:window.history.back();'>Go Back</button>";

        return $html;

    }



    //loop through the task type phases to ensure the counts line up
    public function updateTaskTypeStepPhase(){

        $taskTypeStepPhases = TaskTypeStepPhase::get();

        foreach($taskTypeStepPhases as $taskTypeStepPhase) {

            $count = 0;
            if ((int)$taskTypeStepPhase->p1_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p2_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p3_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p4_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p5_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p6_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p7_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p8_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p9_owner_id > 1) {
                $count++;
            };
            if ((int)$taskTypeStepPhase->p10_owner_id > 1) {
                $count++;
            };
            $taskTypeStepPhase->phase_count = $count;

            if ($count > 0) {
                $taskTypeStepPhase->break_down_count = 0;
            } else {
                $taskTypeStepPhase->break_down_count = (int)$taskTypeStepPhase->break_down_count;
            }

            $taskTypeStepPhase->save();

            TaskStepPhase::where('task_type_step_id',$taskTypeStepPhase->id)
                ->update([
                    'phase_count' => $taskTypeStepPhase->phase_count,
                    'break_down_count' => $taskTypeStepPhase->break_down_count,
                    'p1_owner_id' => $taskTypeStepPhase->p1_owner_id,
                    'p2_owner_id' => $taskTypeStepPhase->p2_owner_id,
                    'p3_owner_id' => $taskTypeStepPhase->p3_owner_id,
                    'p4_owner_id' => $taskTypeStepPhase->p4_owner_id,
                    'p5_owner_id' => $taskTypeStepPhase->p5_owner_id,
                ]);
        }

        return "Task Types Compiled" ;
    }






    //Update all effected tasks after a task type phase is updated.
    //The task type phase could be updated by:
    // - The breakdown count could be increased or decreased
    // - The number or reviewers could change
    // - The number and step reviewers could change
    public function oldcompileTaskStepPhases(Request $request)
    {
        $taskTypeStepPhase = TaskTypeStepPhase::find((int)$request->step_id);

        $stepNumber = $taskTypeStepPhase->step_number;
        //stepCount
        $stepCount = $taskTypeStepPhase->break_down_count;
        if ($taskTypeStepPhase->phase_count > 0) {
            $stepCount = $taskTypeStepPhase->phase_count;
        }
        //loop through tasks and update the task percentage and the task step phase details
        $tasks = Task::where('task_type_id', $taskTypeStepPhase->task_type_id)->get();

        $updateTaskStepPercentage = new UpdateTaskStepPercentage();
        $updateTaskStepPhase = new UpdateTaskStepPhase();

        foreach ($tasks as $task) {
            $percentage = $updateTaskStepPercentage->updateTaskStepPercentage($task, $stepCount, $stepNumber);
            $updateTaskStepPhase->updateOrAddStepPhase($task, $taskTypeStepPhase,$percentage);
        }

        return sizeof($tasks) . " tasks updated";
    }

}

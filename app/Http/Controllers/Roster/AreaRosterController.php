<?php

namespace App\Http\Controllers\Roster;

use App\Models\Area;
use App\Models\Roster\AreaHour;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class AreaRosterController extends Controller
{
    /* ===============================
    |           AREA ROSTER
    |================================ */

    protected $months = array('January', 'February', 'March', 'April', 'May',
        'June', 'July ', 'August', 'September', 'October', 'November', 'December'
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rowsys.roster.user.read.index', [
            //'items' => $items,
        ]);
    }


    public function showAreaRoster()
    {
        $areas = Area::get();

        $areaHoursData = AreaHour::get();

        return view('rowsys.roster.area.index', [
            'areas' => $areas,
            'months' => $this->months,
            'areaHoursData' => $areaHoursData,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::get();

        $areaHoursData = AreaHour::get();

        return view('rowsys.roster.area.create.index', [
            'areas' => $areas,
            'months' => $this->months,
            'areaHoursData' => $areaHoursData,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'area' => 'required',
            'month' => 'required',
            'hours' => 'required|integer|min:1',
        ]);

        $today = Carbon::today();
        $year = $request->year;
        $month = (int)$request->month;
        $d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $day = $d;
        $tz = 'Asia/Singapore';
        $monthDate = Carbon::createFromDate($year, $month, $day, $tz);

        $userAreaHours = new AreaHour();
        $userAreaHours->area_id = $request->area;

        $userAreaHours->month_date = $monthDate;
        $userAreaHours->year = $year;

        $userAreaHours->month_num = $month;
        $userAreaHours->month = $this->months[$month-1];
        $userAreaHours->hours = $request->hours;

        $userAreaHours->save();

        $message = 'Area hours added';
        return Redirect::back()->with('message', $message)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('rowsys.roster.area.update.form', [
            //'items' => $items,
        ]);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return " update the user roster";
    }

    //updates the hours with X-editable
    public function updateHours(Request $request,$id){
        //return $request->hours;
        $hours = (int)$request->value;
        if (is_numeric($request->value) && $hours > 0) {
            $userAreaHours = AreaHour::find($id);
            $userAreaHours->hours = $hours;
            $userAreaHours->save();
            return \Response::json([
                'status'=> 'success',
                'msg' => 'Updated'
            ], 200);
        }
        else {
            return \Response::json([
                'hours' => $request->name,
                'value' => $request->value,
                'status'=> 'error',
                'msg' => 'Value must be number'
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        AreaHour::destroy($id);
        $data = ['message'=>'Deleted '.$id];
        return $data;
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\User;


class TransferController extends Controller
{

    /**
     * Transfer to V2
     *
     * @param $email
     * @return \Illuminate\Http\Response
     */
    public function transferFromV2($email)
    {
        $user = User::where('email', $email)->first();

        if ($user) {

            \Auth::loginUsingId($user->id);

            redirect('dashboard/admin');
        }

        return redirect('login');
    }


}

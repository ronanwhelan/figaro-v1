<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rowsys.admin.tables.datatables.index')->with('message','');
    }



    public function smartDataTables()
    {
        return view('rowsys.admin.tables.datatables.index')->with('message','');
    }

    public function siteDataTables()
    {
        return view('rowsys.admin.tables.datatables.fromsite')->with('message','');
    }
    public function responsiveDataTable()
    {
        return view('rowsys.admin.tables.datatables.resp_datatable')->with('message','');
    }


}

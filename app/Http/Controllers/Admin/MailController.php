<?php

namespace App\Http\Controllers\Admin;

use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use Symfony\Component\Routing\Route;


class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd($fileList);
        return view('rowsys.admin.mail.mail', [
            //'fileList' => $fileList,
           // 'awsLink' => $awsLink,
        ]);
    }

    public function sendEmailReminder(Request $request, $id)
    {
        $user = \Auth::getUser();
        $email = 'ronanj.whelan@gmail.com';

        $total = Task::count();
        $completed = Task::where('complete', '=', 1)->count();
        $leftToDo = (int)$total - $completed;
        $percDone = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
        }

        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addWeeks(1);
        $dueWithinOneWeek = Task::where('complete', '=', 0)->where('next_td', '<', $twoWeeksFromToday)->count();

        $emails = ['ronanj.whelan@pes-international.com','ronanj.whelan@gmail.com'];

        $tasksPastDue = 12;

        $data = [$total,$percDone,$tasksPastDue,$completed,$dueWithinOneWeek];

        Mail::send('rowsys.admin.mail.emails.reminder', ['user' => $user,'data' => $data], function ($m) use ($user,$email) {
            $m->from('info@projectfigaro.com', 'Figaro Project Update');
            $m->to($email, $user->name)->subject('Update on recent events in your project');
        });

        return \Redirect::back()->with('message','Email to  ' . $user->name . ' sent');

    }



    public function sendMailToAllUsers(Request $request){

        $data = 'this is the message text';
       // $emailMainBodyText = $request->emailMainBodyText;
        //$emailSubjectText = $request->emailSubjectText;
        $fromUser = \Auth::getUser();

        $emails = ['ronanj.whelan@pes-international.com','ronanj.whelan@gmail.com'];

        Mail::queue('rowsys.admin.mail.emails.gen_to_all_users', ['fromUser' => $fromUser,'data' => $data], function($m) use ($emails)
        {
            $m->from('info@projectfigaro.com', 'Figaro Project Update');
            $m->to($emails)->subject('This is test e-mail');
        });

        return \Redirect::back()->with('message','Email  sent');
    }

}

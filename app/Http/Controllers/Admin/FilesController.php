<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\Punchlist\CompileItems;
use App\Models\ScheduleDate;
use App\Models\TaskImport;
use App\Rowsys\Admin\Files\CsvFileImporter;
use App\Rowsys\Admin\Files\ImportScheduleFromFile;
use App\Rowsys\Admin\Files\ImportTasksFromFile;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use Mockery\Exception;
use App\Rowsys\Import\TasksImport;
use App\Rowsys\Tracker\Classes\Compile;
use App\Rowsys\PunchList\Compile as itemCompile;


class FilesController extends Controller
{
    /*
     * ----------------------------------
     *           TASKS IMPORTS
     *  ----------------------------------
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $importErrors = [];
        $importLists = [];
        $scheduleLinkOverview = [];

        $numOfNewTasks = [];
        $totalNumOfNewTasks = [];

        $countOfNewTasks = 0;

        //Tasks Import Table - old table
        $numOfTasksWithNoDate = \DB::table('task_imports')->where('date_string', '')->count();
        $numOfNewAreas = \DB::select("SELECT * FROM task_imports WHERE area NOT IN (SELECT name FROM areas group by name) group by area");
        $numOfNewSystems = \DB::select("SELECT count(*) as count FROM task_imports WHERE system NOT IN (SELECT tag FROM systems)");
        $numOfNewGroups = \DB::select("SELECT * FROM task_imports WHERE task_group NOT IN (SELECT description FROM groups group by name) group by task_group");
        $numOfNewTaskTypes = \DB::select('SELECT * FROM task_imports WHERE task_type NOT IN (SELECT short_name FROM task_types group by short_name) group by task_type');

        $importErrors = [];
        //Tasks Temp- This table is created and deleted for imports
        if (Schema::hasTable('tasks_tmp')) {
            $totalNumOfNewTasks = \DB::select("SELECT count(*) as count FROM tasks_tmp");
            $scheduleLinkOverview = [];

            $importLists = [];

            $numOfNewTasks = \DB::select("SELECT count(*) as count FROM tasks_tmp WHERE number NOT IN (SELECT number FROM tasks)");
            //$countOfNewTasks = $countOfNewTasks[0]->count;
            $importErrors = TaskImport::select(\DB::raw("id,stage_id,area_id,system_id,group_id,task_type_id,group_owner_id,schedule_number,task_sub_type_id,if(schedule_number NOT IN (SELECT number FROM schedule_dates),'no','yes') as schedule_link"))
                ->where('stage_id', 0)
                ->orWhere('area_id', 0)
                ->orWhere('system_id', 0)
                ->orWhere('group_id', 0)
                ->orWhere('task_type_id', 0)
                ->orWhere('task_sub_type_id', 0)
                ->orWhere('schedule_number', '')
                ->orWhere('group_owner_id', 0)
                ->orWhereRaw('schedule_number NOT IN (SELECT number FROM schedule_dates)')
                ->get();
            //return $importErrors;
            $scheduleLinkOverview = \DB::select('SELECT id FROM tasks_tmp WHERE schedule_number NOT IN (SELECT number FROM schedule_dates group by number) ');

            if (count($importErrors) === 0) {
                $importLists = TaskImport::with('area', 'group', 'system', 'taskType', 'stage', 'scheduleNumber', 'taskSubType')->get();
            }
        }


        //return $scheduleLinkOverview;

        //SELECT * FROM task_imports WHERE task_type NOT IN (SELECT short_name FROM task_types group by short_name) group by task_type
        //$users = \DB::select('select *  from users where confirmed = ?', [1])->count();->select(DB::raw('count(*) as user_count, status')

        return view('rowsys.admin.files.tasks.import', [
            'importErrors' => $importErrors,
            'importLists' => $importLists,
            'scheduleLinkOverview' => $scheduleLinkOverview,

            'numOfNewTasks' => $countOfNewTasks,
            'totalNumOfNewTasks' => $countOfNewTasks,

            'numOfTasksWithNoDate' => $numOfTasksWithNoDate,
            'numOfNewAreas' => count($numOfNewAreas),
            'numOfNewSystems' => $numOfNewSystems[0]->count,
            'numOfNewGroups' => count($numOfNewGroups),
            'numOfNewTaskTypes' => count($numOfNewTaskTypes),
        ]);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function importTasksFromFileToImportTable(Request $request)
    {

        $file = $request->file('file');
        $filePath = '';
        $message = '';

        $this->validate($request, [
            //'file' => 'required|mimes:csv,xlsx,xls',
            'file' => 'required',
        ]);

        $validated = true;
        $extension = $file->getClientOriginalExtension();
        if (!isset($file) || $extension !== "csv" || !file_exists($file) || !is_readable($file)) {
            $validated = false;
        }
        if ($validated) {
            try {

                //Copy file to temp directory and normalize it
                $fileImporter = new CsvFileImporter();
                $file = $fileImporter->moveAndNormalize($file);
                $filePath = $file->getPathName();
                //Import the Tasks to Database
                $importTasksFromFile = new ImportTasksFromFile($filePath);
                $importTasksFromFile->makeTempTaskTable();//make it if not there already
                $importTasksFromFile->clearTempTaskTable();
                $importTasksFromFile->resetAutoIncrementNumber();
                //$importTasksFromFile->clearTempTaskTable();
                $message = $importTasksFromFile->importToTmpTableFromFile();
                $message = $message . ' Tasks are ready to be imported. Review the report below to get some stats on the imported data.';

                //$importTasksFromFile->updateBaseDateIfImportHadNoDate();

            } catch (Exception $e) {
                $message = 'Something bad happened :' . $e;
            }

        } else {
            $message = $message . ' :Validation Failed - Please check the file and try again.';
        }

        return \Redirect::back()->with('message', $message)->withInput();

    }

    //Add tasks from task_tmp to main task table
    public function addTasksToMainTableFromTempTable()
    {
        $message = 'importing';
        $importTasksFromFile = new ImportTasksFromFile('');
        $importTasksFromFile->addTasks();
        $importTasksFromFile->clearTempTaskTable();
        $compile = new Compile();
        $compile->compileAllTasks();

        return "Tasks imported. Tasks will now be compiled. This may take a few minutes.";
    }

    //
    public function importAllFromImportTable()
    {
        $message = 'importing';
        $importTasksFromFile = new ImportTasksFromFile('');
        $importTasksFromFile->updateBaseDateIfImportHadNoDate();
        //Add Areas
        $areaMessage = $importTasksFromFile->addAreas();
        //Add Systems
        $systemMessage = $importTasksFromFile->addSystems();
        //Add Task Types
        $taskTypeMessage = $importTasksFromFile->addTaskTypes();
        //Add Task Types
        $importedTasksMessage = $importTasksFromFile->addTasks();
        $message = $importedTasksMessage . ' Tasks Imported ,  ' . $areaMessage . ' new Areas added,  ' . $systemMessage . ' new Systems added,  ' . $taskTypeMessage . '  new Task Types added ';
        //Compile All Tasks
        //$importTasksFromFile->importScheduleDatesFromImportTable();
        //$importTasksFromFile->importScheduleLinkTableFromImportTable();
        $compile = new Compile();
        $compile->compileAllTasks();

        return $message;

    }

    /**
     * Old code to import tasks into table
     * @param Request $request
     * @return $this
     */
    public function old_importTasksFromFileToImportTable(Request $request)
    {
        $file = $request->file('file');
        $filePath = '';
        $message = '';

        $this->validate($request, [
            //'file' => 'required|mimes:csv,xlsx,xls',
            'file' => 'required',
        ]);
        $validated = true;
        $extension = $file->getClientOriginalExtension();
        if (!isset($file) || $extension !== "csv" || !file_exists($file) || !is_readable($file)) {

            //$validated = false;
        }
        if ($validated) {
            try {
                //Copy file to temp directory and normalize it
                $fileImporter = new CsvFileImporter();
                $file = $fileImporter->moveAndNormalize($file);
                $filePath = $file->getPathName();
                //Import the Tasks to Database
                $importTasksFromFile = new ImportTasksFromFile($filePath);
                $importTasksFromFile->clearTmpTable();
                $importTasksFromFile->clearTaskTable();
                //$importTasksFromFile->clearTestingTable();
                $message = $importTasksFromFile->importToTmpTableFromFile();
                $message = $message . ' Tasks are ready to be imported. Review the report below to get some stats on the imported data.';
                //$importTasksFromFile->updateBaseDateIfImportHadNoDate();

            } catch (Exception $e) {
                $message = 'Something bad happened :' . $e;
            }

        } else {
            $message = ' :Validation Failed - Please check the file and try again.';
        }

        return \Redirect::back()->with('message', $message)->withInput();

    }
    //*************************************************
    /*
     * ----------------------------------
     *          SCHEDULE IMPORTS
     *  ----------------------------------
     */
    public function getImportScheduleView()
    {
        $datesWithErrors = ScheduleDate::where('start_date', null)->orWhere('finish_date', null)->get();


        return view('rowsys.tracker.schedule.import.index', [
            'datesWithErrors' => $datesWithErrors,
        ]);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function importScheduleDatesFromFileToScheduleTable(Request $request)
    {
        $message = '';
        $returnedMessages = '';
        $errorData = [];
        $file = $request->file('file');

        $this->validate($request, [
            //'file' => 'required|mimes:csv,xlsx,xls',
            'file' => 'required',
        ]);

        $validated = true;
        $extension = $file->getClientOriginalExtension();
        if (!isset($file) || $extension !== "csv" || !file_exists($file) || !is_readable($file)) {
            $validated = false;
        }
        if ($validated) {
            try {
                //Copy file to temp directory and normalize it
                $fileImporter = new CsvFileImporter();
                $file = $fileImporter->moveAndNormalize($file);
                $filePath = $file->getPathName();
                $importScheduleFromFile = new ImportScheduleFromFile($filePath);

                //$importScheduleFromFile->clearScheduleTable();
                //$count  = $importScheduleFromFile->importToScheduleDatesTable();
                $returnedMessages = $returnedMessages . ' / ' . $importScheduleFromFile->deleteTempScheduleTable();
                $returnedMessages = $returnedMessages . ' / ' . $importScheduleFromFile->createTempScheduleTable();
                //$importScheduleFromFile->clearTempScheduleTable();
                $count = $importScheduleFromFile->importBasicFileToTempScheduleDatesTable();

                $errors = \DB::table('schedule_dates_tmp')->where('start_date', null)->orWhere('finish_date', null)->count();

                //$errors = ScheduleDate::where('start_date',null)->orWhere('finish_date',null)->count();
                if ($errors === 0) {
                    $message = $count . ' Dates Imported.';
                    $importScheduleFromFile->clearScheduleTable();
                    $importScheduleFromFile->copyTempTableToScheduleTable();
                    //Tasks
                    $tasksCompiler = new Compile();
                    $tasksCompiler->compileAllTasks();
                    //Items
                    $compileJob = new CompileItems();
                    $this->dispatch($compileJob);

                    //Delete the temp file - ie. the import file
                    $importScheduleFromFile->deleteTempScheduleTable();
                    $importScheduleFromFile->deleteTempImportScheduleFile();

                } else {
                    $countErrors = ScheduleDate::where('start_date', null)->orWhere('finish_date', null)->count();
                    if ($countErrors > 0) {
                        $message = $message . 'Opps! Import Failed!  There are ' . $countErrors . ' entries with the incorrect date format in the file.Please correct the date format and try again';
                    } else {
                        $message = $message . 'Something is not quite right with the File. Ensure the file is setup accordingly ';
                    }

                    $errorData = \DB::table('schedule_dates_tmp')->where('start_date', null)->orWhere('finish_date', null)->count();
                    $data = ['message' => $message, 'errorData' => $errorData];
                }
                //$importScheduleFromFile->deleteTempScheduleTable();
                //$message = $message .' Delete File ='.$importScheduleFromFile->deleteTempImportScheduleFile();


            } catch (Exception $e) {
                $message = $message . ' Something is not right  :' . $e;
            } finally {

            }

        } else {
            $message = $message . ' :Validation Failed - Please check the file and try again. File extension must be csv - Type .' . $extension . ' provided';
        }


        return redirect('/schedule/import')->with('message', $message);
        //return \Redirect::back()->with('message', $message)->withInput();
    }


    public function importTasksReport()
    {
        return view('rowsys.admin.files.tasks.import_report', [
            //'jobs' => $jobs,
        ]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importFile(Request $request, TasksImport $import)
    {

        $file = $request->file('file');

        //dd($file->getPathname());

        $taskImport = new TasksImport($request);
        $taskImport->getFile();

        $filename = $file->getClientOriginalName();
        $pub_path = public_path();
        $extension = $file->getClientOriginalExtension();

        dd($filename);


        Excel::load($request->file('file'), function ($reader) {
            $fileName = $reader->get();

            // reader methods
            // dd($fileName);


        });

        Excel::filter('chunk')->load($pub_path . '/' . $filename)->chunk(250, function ($results) {
            dd($results);
            foreach ($results as $row) {
                dd('im here');
            }
        });

        dd('im here');


        try {
            $this->validate($request, [
                //'file' => 'required|mimes:csv,xlsx,xls',
                'file' => 'required',
            ]);


            dd($file);

            dd($pub_path);

            dd($filename . ' - ' . $pub_path . ' - ' . $extension);


            $path = $file->getRealPath();


            $filename = $file->getClientOriginalName();


            $pub_path = public_path();
            $extension = $file->getClientOriginalExtension();

            $pathToFile = $pub_path . $filename;
            dd($file->getClientOriginalName());
            dd($pathToFile);

            Excel::load($pathToFile, function ($reader) {

                // reader methods

            });


            /*
                        Excel::filter('chunk')->load($pathToFile)->chunk(250, function ($results) {
                            dd($results);
                            foreach ($results as $row) {
                                dd($row);
                            }
                        });*/


        } catch (Exception $e) {
            $message = 'Something bad happened :' . $e;

            return \Redirect::back()->with('message', $message)->withInput();
        }


        $message = ' :Import in process - This may take a bit of time,  we will send a notification when it is finished ';

        return \Redirect::back()->with('message', $message)->withInput();

    }


    /**
     *
     * Available Commands
     *
     */
    public function fileCommand()
    {

        Excel::filter('chunk')->load(database_path('seeds/csv/users.csv'))->chunk(250, function ($results) {
            foreach ($results as $row) {
                $user = User::create([
                    'username' => $row->username,
                    // other fields
                ]);
            }
        });

        Excel::filter('chunk')->load('file.csv')->chunk(250, function ($results) {
            foreach ($results as $row) {
                // do stuff
            }
        });


        Excel::filter('chunk')->load('file.csv')->chunk(250, function ($results) {
            foreach ($results as $row) {
                // do stuff
            }
        });


        DB::table('users')->insert([
            ['email' => 'taylor@example.com', 'votes' => 0],
            ['email' => 'dayle@example.com', 'votes' => 0]
        ]);

        $id = DB::table('users')->insertGetId(
            ['email' => 'john@example.com', 'votes' => 0]
        );


    }


    /**
     *
     * Exports the tasks into an external file to be downloaded.
     *
     * @return string
     */
    public function exportTasksToFile(){

         $query  = "select
                    schedule_number,
                    number as task_number,
                    description as task_description,
                    status,
                    complete,
                    target_val,
                    earned_val,
                    (select name from stages where id  = stage_id) as stage,
                    (select name from areas where id  = area_id) as area,
                    (select tag from systems where id  = system_id) as system,
                    (select name from groups where id  = group_id) as task_group,
                    (select name from task_types where id  = task_type_id) as category,
                    (select name from roles where id  = group_owner_id) as owner
                    from tasks INTO OUTFILE '/tmp/tasks.txt'";

        $tasks = \DB::getPdo()->exec($query);

        return "check for file";



    }


}

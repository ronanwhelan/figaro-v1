<?php

namespace App\Http\Controllers\Admin;

use App\Models\Job;
use App\Models\JobLog;
use App\Rowsys\Admin\Logging\ImportLogFilesIntoTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use App\Jobs\CompileAllTasks;


class QueueController extends Controller
{
    /**
     * Display a listing of the resource.
     * @class View
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all();
        $numOfJobs = sizeof($jobs);
        $jobLogs = JobLog::all();
        $jobsCompileFull = JobLog::where('type', '=', 'COMPILE ALL')->orderBy('started_at', 'desc')->get();
        $jobsCompileSub = JobLog::where('type', '=', 'COMPILE SUB')->orderBy('started_at', 'desc')->get();
        $jobsRulesUpdated = JobLog::where('type', '=', 'RULE UPDATED')->orderBy('started_at', 'desc')->get();

        return view('rowsys.admin.queues.queues', [
            'jobs' => $jobs,
            'numOfJobs' => $numOfJobs,
            'jobsCompileFull' => $jobsCompileFull,
            'jobsCompileSub' => $jobsCompileSub,
            'jobsRulesUpdated' => $jobsRulesUpdated,

        ]);

        //return view('queues');
        //
    }

    /**
     * @return mixed
     * @param \Storage
     */
    public function CompileLogFiles()
    {
        $importLogFile = new ImportLogFilesIntoTable();
        $importLogFile->importLogFilesToLoggingTable();
        $importLogFile->clearLogFiles();
        $date = Carbon::now();
        return Redirect::back()->with('message', 'Logs File Compiled into Database , ' . $date . '');


    }


}

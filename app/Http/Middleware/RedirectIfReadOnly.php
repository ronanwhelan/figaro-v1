<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfReadOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() === null || $request->user()->hasReadOnlyAccess()) {
            return redirect('welcome')->with('message', 'You have only Read Access. Contact your administrator to allow access');
        }
        return $next($request);

    }
}

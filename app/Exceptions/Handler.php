<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if($e->getCode() === 'HY000'){
            $message = "<p>There is a problem with the text in the import file</p><br>";
            $button = '<FORM><INPUT Type="button" VALUE="Back" onClick="history.go(-1);return true;"></FORM>';
            return $message . '<p>ERROR '.$e->getMessage().'</p>'.$button;
        }


        if($e instanceof \Illuminate\Session\TokenMismatchException){
            //return redirect()->back()->withInput($request->except('password'))->with(['messgage' => 'Session Expired.', 'message-type' => 'danger']);
            return redirect()->back()->withInput()->with('token', csrf_token());
            //return redirect('welcome');
        }

        return parent::render($request, $e);
    }



}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BackupDataBase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'figaro:backupdatabase {name}';
    //rowsys:backupdatabase {name?}
    //rowsys:backupdatabase {name = 'Joe'} {--greeting=Hi}

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        //backup :      mysqldump novartis-asia > novartis-asia-$(date '+%Y-%m-%d_%H-%M-%S').sql -upes-user -p33sePgniS
        //restore :     mysql -udbbackup -p7479usrDb test-backup < z_dbbackups/novartis/novartis-asia-2016-05-12_03-40-32.sql

        exec("mysqldump novartis-asia > /storage/novartis-asia-$(date '+%Y-%m-%d_%H-%M-%S').sql -upes-user -p33sePgniS");

        //$this->option('greeting')
        $this->info('backing up the database' . $this->argument('name'));
        //echo 'back up database';
    }
}

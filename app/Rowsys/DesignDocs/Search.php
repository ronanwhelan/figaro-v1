<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 3/6/16
 * Time: 1:32 PM
 */

namespace App\Rowsys\DesignDocs;


class Search {

    protected $request;
    protected $query;

    function __construct(\Request $request)
    {
        $this->request = $request;
    }

    /**
     * Return the Object Query
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Return the Object Query
     * @return mixed
     */
    public function getQueryToSql()
    {
        $sql = clone $this->query;

        return $sql->toSql();
    }

    /*
     * Build and return the main query
     */
    public function buildAndReturnMainQuery()
    {
        //TODO add project id
        $query = Item::where('project_id', 1);
        $this->getMainFilterQuery($query);

        return $this->query;
    }

    /**
     * Add the users Filter to the Query
     */
    public function addUserFilterToRequest()
    {
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $selectedAreasArray = [$userFilter->filter_area_ids];
        $selectedGroupsArray = [$userFilter->filter_group_ids];
        $selectedTypesArray = [$userFilter->filter_type_ids];
        $selectedStagesArray = [$userFilter->filter_stage_ids];

        if ($userFilter->filter_area_ids !== null && $userFilter->filter_area_ids !== '') {
            $this->request['search_areas'] = explode(',', $selectedAreasArray[0]);
        }
        if ($userFilter->filter_group_ids !== null && $userFilter->filter_group_ids !== '') {
            $this->request['search_groups'] = explode(',', $selectedGroupsArray[0]);
        }
        if ($userFilter->filter_type_ids !== null && $userFilter->filter_type_ids !== '') {
            $this->request['search_types'] = explode(',', $selectedTypesArray[0]);
        }
        if ($userFilter->filter_stage_ids !== null && $userFilter->filter_stage_ids !== '') {
            $this->request['search_stages'] = explode(',', $selectedStagesArray[0]);
        }
    }

    /*
     * Get Main Search Filter $query
     * Search Filter that includes Area, System, Group, Task Type and search String
     */
    public function getMainFilterQuery(Builder $query)
    {
        // --- NUMBER TEXT FIELD---
        $search_has_task_number = $this->request->has('search_number');
        if ($search_has_task_number) {
            $searchNumber = $this->request->search_task_number;
        }
        // --- AREAS AND SYSTEMS ---
        $search_has_areas = $this->request->has('search_areas');
        if ($search_has_areas) {
            $searchAreas = $this->request->search_areas;
        }
        $search_has_systems = $this->request->has('search_systems');
        if ($search_has_systems) {
            $searchSystems = $this->request->search_systems;
        }
        // --- GROUPS AND TYPES ---
        $search_has_groups = $this->request->has('search_groups');
        if ($search_has_groups) {
            $searchGroups = $this->request->search_groups;
        }
        $search_has_types = $this->request->has('search_types');
        if ($search_has_types) {
            $searchTypes = $this->request->search_types;
        }
        // --- CATEGORY ---
        $search_has_category = $this->request->has('search_category');
        if ($search_has_category) {
            $searchCategory = $this->request->search_category;
        }

        //===========================================================

        //NUMBER AND DESCRIPTION
        if ($search_has_task_number) {
            $searchNumber = '%' . $this->request->search_number . '%';
            $query = $query->where('number', 'like', $searchNumber)
                ->orWhere('description', 'like', $searchNumber);
        }
        //AREA and SYSTEMS
        //has no areas no systems
        if (!$search_has_areas & !$search_has_systems) {
            //Default to ALL
        } else {
            //has areas but no systems
            if ($search_has_areas & !$search_has_systems) {
                $query = $query->whereIn('area_id', $searchAreas);
            } else {
                if ($search_has_systems) {
                    $query = $query->whereIn('system_id', $searchSystems);
                }
            }
        }
        //GROUPS and TYPES
        //has no groups no types
        if (!$search_has_groups & !$search_has_types) {
            //Default to ALL
        } else {
            if ($search_has_types) {
                $query = $query->whereIn('type_id', $searchTypes);
            } else {
                if ($search_has_groups) {
                    $query = $query->whereIn('group_id', $searchGroups);
                }
            }
        }
        //CATEGORIES
        if ($search_has_category) {
            $query = $query->where('category_id', $searchCategory);
        }

        $this->query = $query;

    }

    /*
    * Get Main Complete Filter $query
    * Search Filter that looks at the complete status
    */
    public function addCompleteFilterQuery()
    {
        $search_has_complete = $this->request->has('inlineRadioOptions');
        $completeSelection = $this->request->inlineRadioOptions;
        if ($search_has_complete) {
            if ($completeSelection === "all") {

            }
            if ($completeSelection === "done") {
                $this->query->where('complete', '=', 1);
            }
            if ($completeSelection === "not-done") {
                $this->query->where('complete', '=', 0);
            }

        }
    }

    /*
    * Get Time Query
    * Search Filter based ont eh time range given
    */
    public function addTimeFilterQuery()
    {
        $search_has_from_date_range = $this->request->has('search_from_date');
        $search_has_to_date_range = $this->request->has('search_from_date');
        $search_has_time = $this->request->has('search_time');
        $searchTime = (int)$this->request->search_time;

        if ($searchTime !== 6) {
            $today = Carbon::now();
            if ($search_has_time) {
                if ($searchTime === 1 || $searchTime === 0) {
                    //Default to ALL
                } else {
                    //1 Week
                    if ($searchTime === 2) {
                        $today = $today->addWeeks(1);
                    }
                    //2 weeks
                    if ($searchTime === 3) {
                        $today = $today->addWeeks(2);
                    }
                    //4 Weeks
                    if ($searchTime === 4) {
                        $today = $today->addWeeks(4);
                    }
                    //8 Weeks
                    if ($searchTime === 5) {
                        $today = $today->addWeeks(8);
                    }
                    $this->query->where('due_date', '<', $today);
                }
            }
        } elseif ($searchTime === 6) {
            if ($search_has_from_date_range || ($search_has_to_date_range)) {
                $this->query->where('due_date', '>', $this->request->search_from_date)
                    ->where('due_date', '<', $this->request->search_to_date);

            }
            if ($search_has_from_date_range) {
                // $this->query->where('next_td', '<', $this->request->search_to_date);
            }
        }
    }

    /**
     * Sort By
     */
    public function addOrderBy()
    {
        $search_has_sort_by = $this->request->has('search_order_by');
        if ($search_has_sort_by) {
            $sortBy = (int)$this->request->search_order_by;
            switch ($sortBy) {
                case 1:
                    $this->query->orderBy('due_date', 'ASC');
                    break;
                case 2:
                    $this->query->orderBy('number', 'ASC');
                    break;
                case 3:
                    $this->query->orderBy('area_id', 'ASC');
                    break;
                case 4:
                    $this->query->orderBy('system_id', 'ASC');
                    break;
                case 5:
                    $this->query->orderBy('group_id', 'ASC');
                    break;
                case 6:
                    $this->query->orderBy('type_id', 'ASC');
                    break;
                case 7:
                    $this->query->orderBy('category_id', 'ASC');
                    break;
            }
        }
        //$this->query->orderBy('number', 'ASC');
    }


}
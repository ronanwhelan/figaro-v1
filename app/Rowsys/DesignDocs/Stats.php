<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 3/6/16
 * Time: 1:31 PM
 */

namespace App\Rowsys\DesignDocs;
use Illuminate\Database\Eloquent\Builder;


class Stats {



    public function searchStats(Builder $query)
    {

        //Total
        $queryClone = clone $query;
        $total = $queryClone->count();

        //Complete and %
        $queryClone = clone $query;
        $complete = $queryClone->where('complete', '=', 1)->count();
        $completePercent = 0;
        if ($total > 0) {
            $completePercent = $complete / $total;
            if ($completePercent < 0.9) {
                $completePercent = '< 1';
            } else {
                $completePercent = round($completePercent, 2);
            }
        }
        $today = Carbon::now();
        $queryClone = clone $query;
        $pastDue = $queryClone->where('due_date', '<', $today)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $queryClone = clone $query;
        $inSevenDays = $today->addWeeks(1);
        $dueInSevenDays = $queryClone->where('due_date', '<', $inSevenDays)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $queryClone = clone $query;
        $inFourteenDays = $today->addWeeks(2);
        $dueInFourteenDays = $queryClone->where('due_date', '<', $inFourteenDays)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $queryClone = clone $query;
        $oneMonthFromToday = $today->addWeeks(4);
        $dueInOneMonth = $queryClone->where('due_date', '<', $oneMonthFromToday)->where('complete', '=', 0)->count();

        $data = [
            'total' => $total,
            'completePercent' => $completePercent,
            'complete' => $complete,
            'pastDue' => $pastDue,
            'dueInSevenDays' => $dueInSevenDays - $pastDue,
            'dueInFourteenDays' => $dueInFourteenDays - $pastDue,
            'dueInOneMonth' => $dueInOneMonth - $pastDue,
            'query' => $query->toSql(),
        ];

        return $data;
    }

}
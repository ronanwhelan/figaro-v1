<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 24/9/16
 * Time: 2:28 PM
 */

namespace app\Rowsys\Tracker\Classes;


use App\Models\Task;
use App\Models\TaskStepPhase;

class StepPhase
{


    protected $task;
    protected $stepNumber;
    protected $breakDownCount;

    /**
     *
     */
    function __construct(Task $task, $stepNumber, $breakDownCount)
    {
        $this->$task = $task;
        $this->$stepNumber = $stepNumber;
        $this->$breakDownCount = $breakDownCount;

    }

    //If the breakdown count changes the task percentage may need to be changed to fit the new breakdown
    //e.g Lets say the current  breakdown count = 3 and the percent for the task is at 33% - if the user changes
    //the breakdown count = 4 then 33% does not work - it needs to be changed to 25% or 50% to fit the new breakdown of 4
    public function updateTaskPercentageAndTaskStepPhase()
    {

        $stepPercentage = $this->getStepPercentageValue();

        if($stepPercentage === 0 || $stepPercentage === 100) {

            //No action needed

        }else{
            $stepPhaseExists = TaskStepPhase::where('task_id',$this->task->id)->exists();
            if ($stepPhaseExists) {
                //check if task percentage needs ot be updated for the new breakdown count



            } else {
                //Add new Step Phase for the task
                $this->addNewStepPhase();



            }

            //recalculate the task percentage - to ensure it fits the breakdown count
            $stepPercentage = $this->recalculateTaskStepPercentage($stepPercentage);

            //update the task
            $this->updateAndSaveTaskStepPercentage($stepPercentage);
        }


    }

    public function addNewStepPhase(){

        $taskStepPhase = new TaskStepPhase();
        $taskStepPhase->task_id = $this->task->id;
        $taskStepPhase->task_type_step_id = $this->task->task_type_id;
        $taskStepPhase->step_number = $this->stepNumber;
        $taskStepPhase->phase_count = 0;

        $taskStepPhase->p1_owner_id = $stepPhase->p1_owner_id;
        $taskStepPhase->p2_owner_id = $stepPhase->p2_owner_id;
        $taskStepPhase->p3_owner_id = $stepPhase->p3_owner_id;
        $taskStepPhase->p4_owner_id = $stepPhase->p4_owner_id;
        $taskStepPhase->p5_owner_id = $stepPhase->p5_owner_id;
        $taskStepPhase->p6_owner_id = $stepPhase->p6_owner_id;
        $taskStepPhase->p7_owner_id = $stepPhase->p7_owner_id;
        $taskStepPhase->p8_owner_id = $stepPhase->p8_owner_id;
        $taskStepPhase->p9_owner_id = $stepPhase->p9_owner_id;
        $taskStepPhase->p10_owner_id = $stepPhase->p10_owner_id;

        //Status - get the step status e.g gen, review
        $currentId = 1;
        switch ($this->stepNumber) {
            case 1:
                $status = $task->gen_perc;
                break;
            case 2:
                $status = $task->rev_perc;
                break;
            case 3:
                $status = $task->re_issu_perc;
                break;
            case 4:
                $status = $task->s_off_perc;
                break;
            default:
                $status  = 0;

        }
        $taskStepPhase->status = $status;

        //Current Id
        if($status === 100){
            $currentId = 1;
        }
        $taskStepPhase->current_owner_id = $currentId;

        //Save
        $taskStepPhase->save();

    }

    public function getStepPercentageValue(){

        $stepPercentage = 0;

        switch ($this->stepNumber) {
            case 1:
                $stepPercentage = $this->task->gen_perc;
                break;

            case 2:
                $stepPercentage = $this->task->rev_perc;
                break;

            case 3:
                $stepPercentage = $this->task->re_issu_perc;
                break;

            case 4:
                $stepPercentage = $this->task->s_off_perc;
                break;

            default:
                $stepPercentage = 0;
        }

        return   $stepPercentage;
    }


    public function recalculateTaskStepPercentage($currentTaskPercentage){

        switch ($this->breakDownCount) {
            case 4:
                $newPercentage = $this->breakDownFour($currentTaskPercentage);
                break;

            case 5:
                $newPercentage = $this->breakDownFive($currentTaskPercentage);
                break;

            case 10:
                $newPercentage = 0;
                break;

            default:
                $newPercentage = 0;
        }

        return $newPercentage;

    }

    public function breakDownFour($percentage){

        switch ($percentage) {
            case ($percentage > 0 || $percentage < 25):
                $percentage = 25;
                break;
            case ($percentage > 25 || $percentage < 50):
                $percentage = 50;
                break;

            case ($percentage > 50 || $percentage <= 100):
                $percentage = 100;
                break;

            default:
                $percentage = 0;
        }

        return $percentage;

    }

    public function breakDownFive($percentage){

        switch ($percentage) {
            case ($percentage > 0 || $percentage < 10):
            $percentage = 10;
            break;
            case ($percentage > 10 || $percentage < 20):
                $percentage = 10;
                break;
            case ($percentage > 20 || $percentage < 30):
                $percentage = 10;
                break;
            case ($percentage > 30 || $percentage < 40):
                $percentage = 10;
                break;
            case ($percentage > 40 || $percentage < 50):
                $percentage = 50;
                break;
            case ($percentage > 60 || $percentage < 70):
                $percentage = 70;
                break;
            case ($percentage > 70 || $percentage < 80):
                $percentage = 80;
                break;
            case ($percentage > 80 || $percentage < 90):
                $percentage = 90;
                break;
            case ($percentage > 90 || $percentage <=100):
                $percentage = 100;
                break;
            default:
                $percentage = 0;
        }

        return $percentage;

    }



    public function updateAndSaveTaskStepPercentage($percentage){

        switch ($this->stepNumber) {
            case 1:
                $this->task->gen_perc = $percentage;
                break;

            case 2:
                $this->task->rev_perc = $percentage;
                break;

            case 3:
                $this->task->re_issu_perc = $percentage;
                break;

            case 4:
                $this->task->s_off_perc = $percentage;
                break;

            default:
                $stepPercentage = 0;
        }

        $this->task->save();
    }




}
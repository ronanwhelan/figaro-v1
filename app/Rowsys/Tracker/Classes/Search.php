<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 30/3/16
 * Time: 3:17 PM
 */

namespace app\Rowsys\Tracker\Classes;


use App\Models\Stage;
use App\Models\Task;
use App\Models\UserFilter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Search
{

    protected $request;
    protected $query;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Return the Object Query
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Return the Object Query
     * @return mixed
     */
    public function getQueryToSql()
    {
        $sql = clone $this->query;

        return $sql->toSql();
    }

    /*
     * Build and return the main query
     */
    public function buildAndReturnMainQuery()
    {
        //TODO add project id
        $query = Task::where('project_id', 1);
        $this->getMainFilterQuery($query);

        return $this->query;
    }

    /**
     * Add the users Filter to the Query
     */
    public function addSavedSearchToRequest()
    {
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $selectedAreasArray = [$userFilter->search_area_ids];
        $selectedGroupsArray = [$userFilter->search_group_ids];
        $selectedTypesArray = [$userFilter->search_type_ids];
        $selectedStagesArray = [$userFilter->search_stage_ids];
        $selectedRolesArray = [$userFilter->search_role_ids];

        if ($userFilter->filter_area_ids !== null && $userFilter->search_area_ids !== '') {
            $this->request['search_areas'] = explode(',', $selectedAreasArray[0]);
            //$this->request->session()->put('dashBoardFilterAreas', $this->request['search_areas']);

        }
        if ($userFilter->filter_group_ids !== null && $userFilter->search_group_ids !== '') {
            $this->request['search_groups'] = explode(',', $selectedGroupsArray[0]);
            //$this->request->session()->put('dashBoardFilterGroups', $this->request['search_groups']);
        }
        if ($userFilter->filter_type_ids !== null && $userFilter->search_type_ids !== '') {
            $this->request['search_types'] = explode(',', $selectedTypesArray[0]);
            //$this->request->session()->put('dashBoardFilterTypes', $this->request['search_types']);
        }
        if ($userFilter->filter_stage_ids !== null && $userFilter->search_stage_ids !== '') {
            $this->request['search_stages'] = explode(',', $selectedStagesArray[0]);
            //$this->request->session()->put('dashBoardFilterStages', $this->request['search_stages']);
        }
        if ($userFilter->filter_role_ids !== null && $userFilter->search_role_ids !== '') {
            $this->request['search_roles'] = explode(',', $selectedRolesArray[0]);
            //$this->request->session()->put('dashBoardFilterStages', $this->request['search_roles']);
        }

    }
    /**
     * Add the users Filter to the Query
     */
    public function addUserFilter()
    {
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $selectedAreasArray = [$userFilter->filter_area_ids];
        $selectedGroupsArray = [$userFilter->filter_group_ids];
        $selectedTypesArray = [$userFilter->filter_type_ids];
        $selectedStagesArray = [$userFilter->filter_stage_ids];
        $selectedRolesArray = [$userFilter->filter_role_ids];

        if ($userFilter->filter_area_ids !== null && $userFilter->filter_area_ids !== '') {
            $this->query->whereIn('area_id', explode(',', $selectedAreasArray[0]));
        }
        if ($userFilter->filter_group_ids !== null && $userFilter->filter_group_ids !== '') {
            $this->query->whereIn('group_id', explode(',', $selectedGroupsArray[0]));
        }
        if ($userFilter->filter_type_ids !== null && $userFilter->filter_type_ids !== '') {
            $this->query->whereIn('task_type_id', explode(',', $selectedTypesArray[0]));
        }
        if ($userFilter->filter_stage_ids !== null && $userFilter->filter_stage_ids !== '') {
            $this->query->whereIn('stage_id', explode(',', $selectedStagesArray[0]));
        }
        if ($userFilter->filter_role_ids !== null && $userFilter->filter_role_ids !== '') {
            $this->query->whereIn('group_owner_id', explode(',', $selectedRolesArray[0]));

        }

    }

    /*
     * Get Main Search Filter $query
     * Search Filter that includes Area, System, Group, Task Type and search String
     */
    public function getMainFilterQuery(Builder $query)
    {
        // --- NUMBER TEXT FIELD---
        $search_has_task_number = $this->request->has('search_task_number');
        if ($search_has_task_number) {
            $searchNumber = $this->request->search_task_number;
            $this->request->session()->put('text', $searchNumber);
        }

        //Ignore filters id direct links from dashboard that do not use this filter
       //if ( !$this->request->has('flagged_tasks') && !$this->request->has('my_tasks') && !$this->request->has('my_flagged_tasks') && !$this->request->has('my_teams_tasks')) {
            if(true){

            // --- AREAS AND SYSTEMS ---
            $search_has_areas = $this->request->has('search_areas');
            if ($search_has_areas) {
                $searchAreas = $this->request->search_areas;
            }

            $search_has_systems = $this->request->has('search_systems');
            if ($search_has_systems) {
                $searchSystems = $this->request->search_systems;
            }
            // --- GROUPS AND TYPES ---
            $search_has_groups = $this->request->has('search_groups');
            if ($search_has_groups) {
                $searchGroups = $this->request->search_groups;
            }
            $search_has_types = $this->request->has('search_types');
            if ($search_has_types) {
                $searchTypes = $this->request->search_types;
            }
            // --- SUB TYPES ---
            $search_has_task_sub_type = $this->request->has('search_task_sub_type');
            if ($search_has_task_sub_type) {
                $searchSubTypes = $this->request->search_task_sub_type;
            }
            // --- STAGES ---
            $search_has_stages = $this->request->has('search_stages');
            if ($search_has_stages) {
                $searchStages = $this->request->search_stages;
            }
            // --- ROLES ---
            $search_has_roles = $this->request->has('search_roles');
            if ($search_has_roles) {
                $searchRoles = $this->request->search_roles;
            }

            $search_has_gen = $this->request->has('search_gen');
            if ($search_has_gen) {
                $searchGen = $this->request->search_gen;
            }
            $search_has_exe = $this->request->has('search_exe');
            if ($search_has_exe) {
                $searchExe = $this->request->search_exe;
            }


            //NUMBER AND DESCRIPTION

            if ($search_has_task_number) {
                $searchText = $this->request->search_task_number;
                $pieces = explode(" ", $searchText);
                $query = $query->where('number', 'LIKE', '%' . trim($pieces[0]) . '%');
                for ($i = 1; $i < sizeof($pieces); $i++) {
                    $query = $query->where('number', 'LIKE', '%' . $pieces[$i] . '%');
                }
                $query = $query->orWhere('description', 'LIKE', '%' . trim($pieces[0]) . '%');
                for ($i = 1; $i < sizeof($pieces); $i++) {
                    $query = $query->where('description', 'LIKE', '%' . trim($pieces[$i]) . '%');
                }
                $query = $query->orWhere('document_number', 'LIKE', '%' . trim($pieces[0]) . '%');
                for ($i = 1; $i < sizeof($pieces); $i++) {
                    $query = $query->where('document_number', 'LIKE', '%' . trim($pieces[$i]) . '%');
                }
                //old - $searchNumber = '%' . $this->request->search_task_number . '%';$query = $query->where('number', 'like', $searchNumber)->orWhere('description', 'like', $searchNumber);
            }

            //AREA and SYSTEMS
            //has no areas no systems
            if (!$search_has_areas & !$search_has_systems) {
                //Default to ALL
            } else {
                //has areas but no systems
                if ($search_has_areas & !$search_has_systems) {
                    //dd("im here");
                    $query = $query->whereIn('area_id', $searchAreas);

                } else {
                    if ($search_has_systems) {
                        $query = $query->whereIn('system_id', $searchSystems);
                    }
                }
            }
            //GROUPS and TYPES
            //has no groups no types
            if (!$search_has_groups & !$search_has_types) {
                //Default to ALL
            } else {
                if ($search_has_types) {
                    $query = $query->whereIn('task_type_id', $searchTypes);
                } else {
                    if ($search_has_groups) {
                        $query = $query->whereIn('group_id', $searchGroups);
                    }
                }
            }
            //Sub Types
            if ($search_has_task_sub_type) {
                $query = $query->whereIn('task_sub_type_id', $searchSubTypes);
            }

            //STAGES
            if ($search_has_stages) {
                $query = $query->whereIn('stage_id', $searchStages);
            } else {
                if ($search_has_gen & $search_has_exe) {
                    //
                } else {
                    $stages = Stage::get();
                    //has areas but no systems
                    if ($search_has_gen) {
                        $query = $query->where('stage_id', '=', $stages[0]->id);
                    }
                    //has areas and systems
                    if ($search_has_exe) {
                        $query = $query->where('stage_id', '=', $stages[1]->id);
                    }
                }
            }
            //Roles
            if ($search_has_roles) {
                $query = $query->whereIn('group_owner_id', $searchRoles);
            }
        }
        $this->query = $query;

    }

    /*
    * Get Main Complete Filter $query
    * Search Filter that looks at the complete status
    */
    public function addCompleteFilterQuery()
    {

        $search_has_complete = $this->request->has('inlineRadioOptions');
        $completeSelection = $this->request->inlineRadioOptions;
        if ($search_has_complete) {
            if ($completeSelection === "all") {
            }
            if ($completeSelection === "done") {
                $this->query->where('complete', '=', 1);
            }
            if ($completeSelection === "not-done") {
                $this->query->where('complete', '=', 0);
            }
        }
    }

    /*
    * Get Time Query
    * Search Filter based ont eh time range given
    */
    public function addTimeFilterQuery()
    {
        $search_has_from_date_range = $this->request->has('search_from_date');
        $search_has_to_date_range = $this->request->has('search_from_date');
        $search_has_time = $this->request->has('search_time');
        $searchTime = (int)$this->request->search_time;

        if ($searchTime !== 6) {
            $today = Carbon::now();
            if ($search_has_time) {
                if ($searchTime === 1 || $searchTime === 0) {
                    //Default to ALL
                } else {
                    //1 Week
                    if ($searchTime === 2) {
                        $today = $today->addWeeks(1);
                    }
                    //2 weeks
                    if ($searchTime === 3) {
                        $today = $today->addWeeks(2);
                    }
                    //4 Weeks
                    if ($searchTime === 4) {
                        $today = $today->addWeeks(4);
                    }
                    //8 Weeks
                    if ($searchTime === 5) {
                        $today = $today->addWeeks(8);
                    }
                    $this->query->where('next_td', '<', $today);
                }
            }
        } elseif ($searchTime === 6) {
            if ($search_has_from_date_range || ($search_has_to_date_range)) {
                $this->query->where('next_td', '>', $this->request->search_from_date)
                    ->where('next_td', '<', $this->request->search_to_date);

            }
            if ($search_has_from_date_range) {
                // $this->query->where('next_td', '<', $this->request->search_to_date);
            }
        }

        if ($searchTime === 5) {
            $this->addPastDue();
        }
    }

    /**
     * Sort By
     */
    public function addOrderBy()
    {
        $search_has_sort_by = $this->request->has('search_order_by');
        if ($search_has_sort_by) {
            $sortBy = (int)$this->request->search_order_by;
            switch ($sortBy) {
                case 1:
                    $this->query->orderBy('next_td', 'ASC');
                    break;
                case 2:
                    $this->query->orderBy('number', 'ASC');
                    break;
                case 3:
                    $this->query->orderBy('area_id', 'ASC');
                    break;
                case 4:
                    $this->query->orderBy('system_id', 'ASC');
                    break;
                case 5:
                    $this->query->orderBy('group_id', 'ASC');
                    break;
                case 6:
                    $this->query->orderBy('task_type_id', 'ASC');
                    break;
                case 7:
                    $this->query->orderBy('stage_id', 'ASC');
                    break;
            }
        }
        //$this->query->orderBy('number', 'ASC');
    }

    /**
     * Step Status
     */
    public function addStepStatus()
    {
        $search_has_step_status = $this->request->has('search_status');
        if ($search_has_step_status) {
            $statusChoice = (int)$this->request->search_status;
            switch ($statusChoice) {
                case 1:
                    //Review
                 /*   $this->query->where('geb_perc', '<', 100)->where('gen_applicable', '=', 1)
                        ->where('rev_perc', '=', 0)
                        ->where('re_issu_perc', '=', 0)
                        ->where('s_off_perc', '=', 0);
                 */
                    break;
                case 2:
                    //Review
                    $this->query->where('rev_perc', '<', 100)->where('rev_applicable', '=', 1)
                        ->where('re_issu_perc', '=', 0)
                        ->where('s_off_perc', '=', 0)
                        ->where(function ($query) {
                            $query->where('gen_perc', '=', 100)->where('gen_applicable', '=', 1)->orWhere('gen_applicable', 0);});
                    break;
                case 3:
                    //Sign Off / Approval
                    $this->query->where('s_off_perc', '<', 100)->where('s_off_applicable', '=', 1)
                        ->where(function ($query) {
                            $query->where('gen_perc', '=', 100)->where('gen_applicable', '=', 1)->orWhere('gen_applicable', 0);})
                        ->where(function ($query) {
                            $query->where('rev_perc', '=', 100)->where('rev_applicable', '=', 1)->orWhere('rev_applicable', 0);})
                        ->where(function ($query) {
                            $query->where('re_issu_perc', '=', 100)->where('re_issu_applicable', '=', 1)->orWhere('re_issu_applicable', 0);});
                    break;
            }
        }
    }

    /**
     * Check to see if the request has any links e.g from the dashboard
     * if so add the required query
     */
    public function checkForAnyAppLinks()
    {
        if ($this->request->has('user_filtered_tasks')) {
            $this->addUserFilter();
        }
        if ($this->request->has('dashboard_past_due_on')) {
            $this->addUserFilter();
            $this->addPastDue();
           //
        }
        if ($this->request->has('dashboard_due_in_weeks')) {
            $this->addUserFilter();
            $this->addDueInWeeks($this->request->dashboard_due_in_weeks);

        }
        if ($this->request->has('active_tasks')) {
            $this->addActiveTasksOnly();
            $this->addUserFilter();
        }
        if ($this->request->has('prep_active_tasks')) {
            $this->query->where('stage_id', '=', 1)->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100);
        }
        if ($this->request->has('exec_active_tasks')) {
            $this->query->where('stage_id', '=', 2)->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100);
        }
        if ($this->request->has('flagged_tasks')) {
            $this->query->where('flag', '=', 1);
        }

        if ($this->request->has('my_flagged_tasks')) {
            $user = Auth::user();
            $userFilter = UserFilter::where('user_id', $user->id)->first();
            if (count($userFilter) > 0) {
                $currentListString = $userFilter->task_flag_list;
                $currentListArray = explode(',', $currentListString);
                $this->query->whereIn('id', $currentListArray);
            }
        }

        $user = Auth::user();
        if ($this->request->has('my_tasks')) {
            $this->query->where('assigned_user_id', '=', $user->id);
        }
        $userRoleIds = $user->roles->lists('id')->toArray();
        if ($this->request->has('my_teams_tasks')) {
            $this->query->whereIn('group_owner_id', $userRoleIds);
        }

    }

    /**
     * Add Past Due to the Query
     */
    public function addPastDue()
    {
        $today = Carbon::now();
        $this->query->where('next_td', '<', $today);
    }

    /**
     * Add Active Tasks only  gen perc > 0
     */
    public function addActiveTasksOnly()
    {
        $this->query->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100);
    }

    /**
     * Add Past Due to the Query
     */
    public function addDueInWeeks($numOfWeeks)
    {
        $today = Carbon::now();
        $dateInTheFuture = $today->addWeeks((int)$numOfWeeks);
        $today = Carbon::now();
        $this->query->where('next_td', '>=', $today)
            ->where('next_td', '<=', $dateInTheFuture)
            ->where('complete', 0);
    }

    public function saveLastSearch()
    {
        $user = \Auth::user();
        $userFilterExists = UserFilter::where('user_id', $user->id)->exists();

        if (!$userFilterExists) {
            $userFilter = new UserFilter();
            $userFilter->user_id = $user->id;
            $userFilter->save();
        }
        $userFilterExists = UserFilter::where('user_id', $user->id)->exists();
        if ($userFilterExists) {
            $userFilter = UserFilter::where('user_id', $user->id)->first();

            // --- NUMBER TEXT FIELD---
            $userFilter->search_text = '';
            $search_has_task_number = $this->request->has('search_task_number');
            if ($search_has_task_number) {
                $searchNumber = $this->request->search_task_number;
                $userFilter->search_text = $searchNumber;
                //$this->request->session()->put('text', $searchNumber);
            }
            //Areas
            $userFilter->search_area_ids = '';
            //\Session::put('sessionAreas', []);
            $search_has_areas = $this->request->has('search_areas');
            if ($search_has_areas) {
                $searchAreas = $this->request->search_areas;
                $userFilter->search_area_ids = implode(',', $searchAreas);
                //\Session::put('sessionAreas', implode(',', $searchAreas));
            }
            //Systems
            $userFilter->search_system_ids = '';
            //\Session::put('sessionSystems', []);
            $search_has_systems = $this->request->has('search_systems');
            if ($search_has_systems) {
                $searchSystems = $this->request->search_systems;
                $userFilter->search_system_ids = implode(',', $searchSystems);
            }
            //Groups
            $userFilter->search_group_ids = '';
            $search_has_groups = $this->request->has('search_groups');
            if ($search_has_groups) {
                $searchGroups = $this->request->search_groups;
                $userFilter->search_group_ids = implode(',', $searchGroups);
                //\Session::put('sessionGroups', $searchGroups);
            }
            //Types
            $userFilter->search_type_ids = '';
            $search_has_types = $this->request->has('search_types');
            if ($search_has_types) {
                $searchTypes = $this->request->search_types;
                $userFilter->search_type_ids = implode(',', $searchTypes);
                //$this->request->session()->put('sessionTypes ', $searchTypes);
            }
            //Stages
            $stages = Stage::get();
            if ($this->request->search_gen === '1') {
                $userFilter->search_stage_ids = $stages[0]->id;
            }
            if ($this->request->search_exe === '1') {
                $userFilter->search_stage_ids = $stages[1]->id;
            }
            if ($this->request->search_gen === '1' && $this->request->search_exe === '1') {
                $userFilter->search_stage_ids = '';
            }

            //Roles
            $userFilter->search_role_ids = '';
            $search_has_roles = $this->request->has('search_roles');
            if ($search_has_roles) {
                $searchRoles = $this->request->search_roles;
                $userFilter->search_role_ids = implode(',', $searchRoles);
            }

            //Complete
            //$userFilter->search_complete = 0;
            $search_has_complete = $this->request->has('inlineRadioOptions');
            $completeSelection = $this->request->inlineRadioOptions;
            if ($search_has_complete) {
                if ($completeSelection === "done") {
                    //$userFilter->search_complete = 1;
                }
            }

            $userFilter->save();
        }

    }


}
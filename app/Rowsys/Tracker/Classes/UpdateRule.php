<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 23/2/16
 * Time: 10:33 AM
 */

namespace App\Rowsys\Tracker\Classes;

use App\Models\TaskRule;
use Illuminate\Http\Request;

class UpdateRule
{

    protected $rule;
    protected $message;
    protected $request;

    function __construct(TaskRule $rule,Request $request, $message)
    {
        $this->rule = $rule;
        $this->request = $request;
        $this->message = $message;
    }


    public function getMessage()
    {
        return $this->message;
    }

    public function updateRule()
    {
        // Buffer Days
        $this->rule->gen_ex_buffer_days = $this->request->gen_ex_buffer_days;
        $this->rule->rev_buffer_days = $this->request->review_buffer_days;
        $this->rule->re_issu_buffer_days = $this->request->re_issue_buffer_days;
        $this->rule->s_off_buffer_days = $this->request->sign_off_buffer_days;
        // Status
        $this->rule->gen_ex_status = $this->request->gen_ex_status;
        $this->rule->rev_status = $this->request->review_status;
        $this->rule->re_issu_status = $this->request->re_issue_status;
        $this->rule->s_off_status = $this->request->sign_off_status;
        // Hrs Percentage
        $this->rule->gen_ex_hrs_perc = $this->request->gen_hrs;
        $this->rule->rev_hrs_perc = $this->request->rev_hrs;
        $this->rule->re_issu_hrs_perc = $this->request->re_issu_hrs;
        $this->rule->s_off_hrs_perc = $this->request->s_off_hrs;

        $this->rule->save();

        var_dump($this->request->server);
        var_dump($this->request->request->get('name'));

        $this->message = 'Task Rule Updated';

    }


}
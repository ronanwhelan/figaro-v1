<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 19/10/17
 * Time: 5:52 PM
 */

namespace app\Rowsys\Tracker\Classes;


use App\Models\Task;
use App\Models\TaskEarnedValue;
use App\Models\TaskRule;
use Illuminate\Support\Facades\DB;

class UpdateTaskEarnedValues
{
    //so the rule or target date - so all the the values must be updated
    // if the tasks is complete then  no update required if rule changed
    // if the target hours have changes then all the

    /**
     *  Update the Hrs for each of earned values for the task being updated
     *
     * @param $task
     * @return mixed
     */
    public static function updateHrsForEarnedValue($task){

        TaskEarnedValue::where('task_id',$task->id)->update([
            'task_target_hrs' => $task->target_val
        ]);

        DB::update('update task_earned_values set earned_hrs = ((task_target_hrs * step_rule_perc) /100  * dif_perc) / 100 where task_id = ?', [$task->id]);

        return $task;
    }


    /**
     * @param $task
     * @param $rule
     */
    public static function updateRuleValuesForEarnedValue($task, $rule ){

        DB::update('update task_earned_values set step_rule_perc = ? where task_id = ? and step_num = 1', [$rule->gen_ex_hrs_perc,$task->id]);
        DB::update('update task_earned_values set step_rule_perc = ? where task_id = ? and step_num = 2', [$rule->rev_hrs_perc,$task->id]);
        DB::update('update task_earned_values set step_rule_perc = ? where task_id = ? and step_num = 3', [$rule->re_issu_hrs_perc,$task->id]);
        DB::update('update task_earned_values set step_rule_perc = ? where task_id = ? and step_num = 4', [$rule->s_off_hrs_perc,$task->id]);

    }
}
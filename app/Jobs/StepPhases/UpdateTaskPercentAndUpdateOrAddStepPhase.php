<?php

namespace App\Jobs\StepPhases;

use App\Jobs\Job;
use App\Models\Task;
use App\Models\TaskTypeStepPhase;
use App\Rowsys\Tracker\Classes\StepPhases\UpdateTaskStepPercentage;
use App\Rowsys\Tracker\Classes\StepPhases\UpdateTaskStepPhase;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class UpdateTaskPercentAndUpdateOrAddStepPhase
 * @package App\Jobs\StepPhases
 */
class UpdateTaskPercentAndUpdateOrAddStepPhase extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var TaskTypeStepPhase
     */
    protected $taskTypeStepPhase;

    /**
     * Update the tasks Percentage &  Add the step phase for the task if doesnt exisit.
     *
     * @var TaskTypeStepPhase
     *
     */
    public function __construct(TaskTypeStepPhase $taskTypeStepPhase)
    {
        $this->taskTypeStepPhase = $taskTypeStepPhase;
    }

    /**
     * Execute the job.
     * loop through the tasks and update the task percentage and add a task step &
     * add a task step if it does not exist.
     * @return void
     */
    public function handle()
    {
        $stepNumber = $this->taskTypeStepPhase->step_number;

       //stepCount
        $stepCount = $this->taskTypeStepPhase->break_down_count;
        if ($this->taskTypeStepPhase->phase_count > 0) {
            $stepCount = $this->taskTypeStepPhase->phase_count;
        }

        //Update Objects
        $updateTaskStepPercentage = new UpdateTaskStepPercentage();
        $updateTaskStepPhase = new UpdateTaskStepPhase();

        //Tasks that are to be updated
        $tasks = Task::where('task_type_id', $this->taskTypeStepPhase->task_type_id)
            ->where('stage_id', $this->taskTypeStepPhase->stage_id)
            ->get();

        //loop through tasks and update the task percentage and the task step phase details
        foreach ($tasks as $task) {
            //echo "task Number ".$task->number .PHP_EOL;
            $percentage = $updateTaskStepPercentage->updateTaskStepPercentage($task, $stepCount, $stepNumber);
            $updateTaskStepPhase->updateOrAddStepPhase($task, $this->taskTypeStepPhase,$percentage);
        }

    }
}

<?php

namespace App\Jobs\StepPhases;

use App\Jobs\Job;
use App\Models\Task;
use App\Models\TaskStepPhase;
use App\Models\TaskTypeStepPhase;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateTaskStepPhasesAfterPhaseUpdated extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $taskTypeStepPhase;

    /**
     * @param TaskTypeStepPhase $taskTypeStepPhase
     */
    public function __construct(TaskTypeStepPhase $taskTypeStepPhase)
    {
        $this->taskTypeStepPhase = $taskTypeStepPhase;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        TaskStepPhase::where('task_type_step_id',$this->taskTypeStepPhase->id)
            ->update([
                'phase_count' => $this->taskTypeStepPhase->phase_count,
                'break_down_count' => $this->taskTypeStepPhase->break_down_count,
                'p1_owner_id' => $this->taskTypeStepPhase->p1_owner_id,
                'p2_owner_id' => $this->taskTypeStepPhase->p2_owner_id,
                'p3_owner_id' => $this->taskTypeStepPhase->p3_owner_id,
                'p4_owner_id' => $this->taskTypeStepPhase->p4_owner_id,
                'p5_owner_id' => $this->taskTypeStepPhase->p5_owner_id,
            ]);


        $tasks = Task::where('task_type_id', $this->taskTypeStepPhase->task_type_id)
            ->where('stage_id', $this->taskTypeStepPhase->stage_id)
            ->get();
        foreach ($tasks as $task) {
            $task->applyTaskRule();
        }
    }
}

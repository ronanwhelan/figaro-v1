<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Task;
use App\Models\TaskRule;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PhpParser\Node\Stmt\Foreach_;

class JobCompileTasksAfterRuleUpdated extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $startId;
    protected $endId;
    protected $ruleId;

    /**
     * Create a new job instance.
     * @param $startId
     * @param $endId
     * @param $ruleId
     * @internal param TaskRule $rule
     */
    public function __construct($startId,$endId,$ruleId)
    {
        $this->startId = $startId;
        $this->endId = $endId;
        $this->ruleId = $ruleId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applyRule = new ApplyRuleToTasks();
        $rule = TaskRule::find($this->ruleId);
        $now = Carbon::now()->subHour(26);

        $tasks = Task::where('task_type_id', '=', $rule->task_type_id)
            ->where('stage_id', '=', $rule->stage_id)
            ->where('complete', '=', 0)
            ->where('id','>=',$this->startId)
            ->where('id','<=',$this->endId)
            ->orderBy('id','ASC')
            ->get();

        $count = 0;

        foreach ($tasks as $task) {
            $applyRule->compileThisTask($task);
            $count ++;
            echo $task->number . ' ID'.$task->id.''.' : count '.$count. ' : Start id = '. $this->startId. ': End id = '. $this->endId.  PHP_EOL;
        }
    }
}

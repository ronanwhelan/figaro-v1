<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Task;
use App\Models\TaskStepPhase;
use App\Models\TaskTypeStepPhase;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddTaskStepPhases extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $taskTypeStepPhase;


    /**
     * @param TaskTypeStepPhase $taskTypeStepPhase
     */public function __construct(TaskTypeStepPhase $taskTypeStepPhase)
    {
        $this->taskTypeStepPhase = $taskTypeStepPhase;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stageId = $this->taskTypeStepPhase->stage_id;
        $taskTypeId = $this->taskTypeStepPhase->task_type_id;
        $stepNumber = $this->taskTypeStepPhase->step_number;

        $tasks = Task::where('task_type_id', $taskTypeId)->where('stage_id', $stageId)->get();
        $stepPhase = $this->taskTypeStepPhase;

        //---------------------------
        //   New Task Step Phase each effected task
        //---------------------------
        foreach($tasks as $task){

           $stepExists = TaskStepPhase::where('task_id', $task->id)
               ->where('task_type_step_id', $stepPhase->id)
               ->where('step_number',$stepNumber)
                ->exists();
            if(!$stepExists){
                $taskStepPhase = new TaskStepPhase();

                $taskStepPhase->task_id = $task->id;

                $taskStepPhase->task_type_step_id = $stepPhase->id;

                $taskStepPhase->step_number = $stepNumber;

                $taskStepPhase->phase_count = $stepPhase->phase_count;

                $taskStepPhase->p1_owner_id = $stepPhase->p1_owner_id;
                $taskStepPhase->p2_owner_id = $stepPhase->p2_owner_id;
                $taskStepPhase->p3_owner_id = $stepPhase->p3_owner_id;
                $taskStepPhase->p4_owner_id = $stepPhase->p4_owner_id;
                $taskStepPhase->p5_owner_id = $stepPhase->p5_owner_id;
                $taskStepPhase->p6_owner_id = $stepPhase->p6_owner_id;
                $taskStepPhase->p7_owner_id = $stepPhase->p7_owner_id;
                $taskStepPhase->p8_owner_id = $stepPhase->p8_owner_id;
                $taskStepPhase->p9_owner_id = $stepPhase->p9_owner_id;
                $taskStepPhase->p10_owner_id = $stepPhase->p10_owner_id;

                //Status - get the step status e.g gen, review
                $currentId = 1;
                switch ($stepNumber) {
                    case 1:
                        $status = $task->gen_perc;
                        break;
                    case 2:
                        $status = $task->rev_perc;
                        break;
                    case 3:
                        $status = $task->re_issu_perc;
                        break;
                    case 4:
                        $status = $task->s_off_perc;
                        break;
                    default:
                        $status  = 0;

                }
                $taskStepPhase->status = $status;

                //Current Id
                if($status === 100){
                    $currentId = 1;
                }
                $taskStepPhase->current_owner_id = $currentId;

                //Save
                $taskStepPhase->save();

                $task->applyTaskRule();
                //compile the task??
            }

        }

    }


}

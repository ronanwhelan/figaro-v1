<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Task;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompileTasksAfterScheduleDateChange extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $scheduleNumber;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($scheduleNumber)
    {
        $this->scheduleNumber = $scheduleNumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tasks = Task::where('schedule_number', $this->scheduleNumber)->get();

        $applyRule = new ApplyRuleToTasks();
        foreach ($tasks as $task) {
            $applyRule->compileThisTask($task);
            //echo $task->number . ' compiled' .PHP_EOL;

        }
    }
}

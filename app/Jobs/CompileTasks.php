<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Task;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PhpParser\Node\Stmt\Foreach_;

class CompileTasks extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $startId;
    protected $endId;

    /**
     * Create a new job instance.
     * @param $startId
     * @param $endId
     */
    public function __construct($startId,$endId)
    {
        $this->startId = $startId;
        $this->endId = $endId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applyRule = new ApplyRuleToTasks();

        $now = Carbon::now()->subHour(26);

        $tasks = Task::
        //where('complete', '=', 0)->where('complete', '=', 1)
            where('id','>=',$this->startId)
            ->where('id','<=',$this->endId)
            //->where('compiled_at','<' ,$now)
            ->orderBy('id','ASC')
            ->get();

        $count = 0;

        foreach ($tasks as $task) {
            $applyRule->compileThisTask($task);
            $count ++;
           echo $task->number . ' ID'.$task->id.''.' : count '.$count. ' : Start id = '. $this->startId. ': End id = '. $this->endId.  PHP_EOL;
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;

class TaskRule extends Model
{
    //
    protected $table = 'task_rules';


    //******  Task Type  *********
    public function taskType()
    {
        return $this->belongsTo(TaskType::class);
    }

    //******  Stage *********
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    //******  Group -Task Type *********
    public function taskGroup()
    {
        return $this->belongsTo(Group::class);
    }
    /**
     * ---------------------------------
     *    Apply Rule To All Tasks
     * ---------------------------------
     */
    public function applyRuleToAllTasks()
    {
       // (new ApplyRuleToTasks())->applyThisRuleToAllRelatedTasks($this);
        $applyRule = new ApplyRuleToTasks();
        $applyRule->applyThisRuleToAllRelatedTasks($this);
        return $applyRule->getMessage();

    }

}

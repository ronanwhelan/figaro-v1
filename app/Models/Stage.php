<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model {

	//
    protected $table = 'stages';

    //******  Task Rule  *********
    public function taskRule()
    {
        return $this->hasOne(TaskRule::class);
    }

}

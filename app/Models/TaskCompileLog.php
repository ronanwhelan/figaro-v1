<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 2/2/17
 * Time: 11:36 AM
 */

namespace app\Models;

use Illuminate\Database\Eloquent\Model;


class TaskCompileLog extends Model
{

    protected $table = 'task_compile_logs';
    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
            'start_time',
            'start_time'
        ];
    }



}
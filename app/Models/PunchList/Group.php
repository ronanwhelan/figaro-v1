<?php

namespace App\Models\PunchList;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'punch_list_groups';
}

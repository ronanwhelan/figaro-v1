<?php

namespace App\Models\PunchList;

use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{

    protected $fillable = [
        'item_id',
        'assigned_by_user_id',
        'assigned_company_id',
    ];
    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
            'last_updated_at',
            'complete_date',
        ];
    }

    protected $table = 'punch_list_actions';

    //******  assigned Owner User  *********
    public function assignedCompany()
    {
        return $this->belongsTo(Company::class);
    }

    //******  assigned Role *********
    public function assignedRole()
    {
        return $this->belongsTo(Role::class);
    }
    //******  assigned  User  *********
    public function assignedUser()
    {
        return $this->belongsTo(User::class);
    }

    //******  Last Update By User  *********
    public function lastUpdatedBy()
    {
        return $this->belongsTo(User::class);
    }


    public function items()
    {
        return $this->hasMany(Item::class);
    }
}

<?php

namespace App\Models\PunchList;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    protected $table = 'punch_list_attachments';


    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function uploadedBy(){

        return $this->belongsTo(User::class);
    }

}

<?php

namespace App\Models\PunchList;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'punch_list_categories';

    public function items()
    {
        return $this->hasMany(Item::class);
    }

}

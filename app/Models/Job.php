<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return ['available_at', 'created_at'];

    }
}

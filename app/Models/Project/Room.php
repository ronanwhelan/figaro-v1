<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    protected $connection = 'mysql2';

    //******  Floor *********
    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }
}

<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class SystemMilestone extends Model
{
    protected $table = 'system_milestones';
}

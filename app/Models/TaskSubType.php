<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskSubType extends Model
{
    //
    protected $table = 'task_sub_types';

    protected $fillable = ['name','description','task_type_id'];

    public function taskType()
    {
        return $this->belongsTo(TaskType::class);
    }



}

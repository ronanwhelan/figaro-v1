<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskEarnedValue extends Model {

	//
    protected $table = 'task_earned_values';


    protected $guarded = [];


    public function task()
    {
        return $this->hasOne(Task::class);
    }

    //******  Assigned Team *********
    public function groupOwner()
    {
        return $this->belongsTo(Area::class);
    }

}

/* -------------------------------- 
  Fullscreen
-------------------------------- */
function requestFullScreen() {

  var el = document.body;

  // Supports most browsers and their versions.
  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen 
  || el.mozRequestFullScreen || el.msRequestFullScreen;

  if (requestMethod) {

    // Native full screen.
    requestMethod.call(el);

  } else if (typeof window.ActiveXObject !== "undefined") {

    // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");

    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
}

jQuery(document).ready(function($){

	//open/close lateral slide
	$('.nav-slide-trigger').on('click', function(){
		triggerslide(true);
	});
	$('.nav-slide .nav-close').on('click', function(){
		triggerslide(false);
	});

	function triggerslide($bool) {
		var elementsToTrigger = $([$('.nav-slide-trigger'), $('.nav-slide'), $('.nav-tab-slide'), $('.nav-gallery')]);
		elementsToTrigger.each(function(){
			$(this).toggleClass('slide-is-visible', $bool);
		});
	}

	//mobile version - detect click event on slides tab
	var slide_tab_placeholder = $('.nav-tab-slide .placeholder a'),
		slide_tab_placeholder_default_value = 'Select',
		slide_tab_placeholder_text = slide_tab_placeholder.text();
	
	$('.nav-tab-slide li').on('click', function(event){
		//detect which tab slide item was selected
		var selected_slide = $(event.target).data('type');
			
		//check if user has clicked the placeholder item
		if( $(event.target).is(slide_tab_placeholder) ) {
			(slide_tab_placeholder_default_value == slide_tab_placeholder.text()) ? slide_tab_placeholder.text(slide_tab_placeholder_text) : slide_tab_placeholder.text(slide_tab_placeholder_default_value) ;
			$('.nav-tab-slide').toggleClass('is-open');

		//check if user has clicked a slide already selected 
		} else if( slide_tab_placeholder.data('type') == selected_slide ) {
			slide_tab_placeholder.text($(event.target).text());
			$('.nav-tab-slide').removeClass('is-open');	

		} else {
			//close the dropdown and change placeholder text/data-type value
			$('.nav-tab-slide').removeClass('is-open');
			slide_tab_placeholder.text($(event.target).text()).data('type', selected_slide);
			slide_tab_placeholder_text = $(event.target).text();
			
			//add class selected to the selected slide item
			$('.nav-tab-slide .selected').removeClass('selected');
			$(event.target).addClass('selected');
		}
	});

	//close slide dropdown inside lateral .nav-slide 
	$('.nav-slide-block h4').on('click', function(){
		$(this).toggleClass('closed').siblings('.nav-slide-content').slideToggle(300);
	});

	//fix side nav when scrolling
	$(window).on('scroll', function(){
		(!window.requestAnimationFrame) ? fixNav() : window.requestAnimationFrame(fixNav);
	});

	function fixNav() {
		var offsetTop = $('.nav-slide').offset().top,
			scrollTop = $(window).scrollTop();
		( scrollTop >= offsetTop ) ? $('.nav-slide').addClass('is-fixed') : $('.nav-slide').removeClass('is-fixed');
	}

/* -------------------------------- 
  Gallery Pop
-------------------------------- */

    /*$('.img-popup').magnificPopup({
        type: 'image'
    });*/

/* -------------------------------- 
  Boostrap
-------------------------------- */

$('#myTabs a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

/* -------------------------------- 
  Back to top
-------------------------------- */

$(window).scroll(function() {
	if ($(this).scrollTop() > 200) {
		$('.go-top').fadeIn(200);
	} else {
		$('.go-top').fadeOut(200);
	}
});
	
// Animate the scroll to top
$('.go-top').click(function(event) {
	event.preventDefault();
		
	$('html, body').animate({scrollTop: 0}, 300);

	});
});

/* --------------------------------
 Zoom
 -------------------------------- */
if(true){
    setBodyZoomLevel(9);
}
function setBodyZoomLevel(level){
    console.log('zoom function called');
    level = level * .1;
    $('body').css('zoom',level);
}
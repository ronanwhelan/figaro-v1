/**
 * Created by ronanwhelan on 15/1/16.
 *
 * File contains the JS script that handle the tasks. e.g populate the table, update task.
 */


/**
 * This function gets the Task Table Json Data and updates the table rows
 *
 */
var tasksForTable;
var taskTypes;

function populateTaskTable(num) {
    //console.log('tracker/tasks/general/poulateTaskTable function called');
    pleaseWait('loading table');
    var request = $.ajax({
        url: "/tasks-json/" + num,
        success: function (data) {
            tasksForTable = data;
            appendRowsToMissionListtable(tasksForTable);
            waitDone();
            refreshTaskTable();
        }
    });
}

function populateTaskTableFromSearch(tasks) {
    pleaseWait('loading table');
    appendRowsToMissionListTableNew(tasks);
    $('#loading-box-section').remove();
    // appendRowsToMissionListtable(taskData.results);
    waitDone();
    refreshTaskTable();
}
/** ===============================================================
 *
 *  ! NEW - Append Rows to Mission List Table - to see if it works
 *
 * ===============================================================
 */

function appendRowsToMissionListTableNew(tasks) {
    var rowStart = '<tr>';
    var rowEnd = '</tr>';
    var row = '';
    var tdData = '';
    var rowId = '';
    var genData = [];
    var revData = [];
    var reIssuData = [];
    var sOffData = [];

    var taskPerc = 0;
    var numberBkgColor = 'white';
    var nextTargetDate;
    var nextTargetDateArray = '';
    var baseDate;
    var baseDateArray = '';
    var revisedProvDateArray = '';
    var revisedProvDate = ''


    $.each(tasks, function (i, value) {
        genData = dateBackGroundColors(2, value.gen_perc, value.gen_td, value.complete, value.compile_error, value.gen_applicable);
        revData = dateBackGroundColors(2, value.rev_perc, value.rev_td, value.complete, value.compile_error, value.rev_applicable);
        reIssuData = dateBackGroundColors(2, value.re_issu_perc, value.re_issu_td, value.complete, value.compile_error, value.re_issu_applicable);
        sOffData = dateBackGroundColors(2, value.s_off_perc, value.s_off_td, value.complete, value.compile_error, value.s_off_applicable);

        //var dateForHumanArray = date.split(/[- :]/);
        //var dateOut = new Date(dateForHumanArray[0], dateForHumanArray[1]-1, dateForHumanArray[2]).toDateString();
        //Next Target Date
        nextTargetDate = value.next_td;
        if (value.complete === 1) {
            nextTargetDate = 'Complete';
        } else {
            nextTargetDateArray = nextTargetDate.split(/[- :]/);
            nextTargetDate = new Date(nextTargetDateArray[0], nextTargetDateArray[1] - 1, nextTargetDateArray[2]).toDateString();
        }

        //Base Date For Humans
        baseDateArray = value.base_date.split(/[- :]/);
        baseDate = new Date(baseDateArray[0], baseDateArray[1] - 1, baseDateArray[2]).toDateString();

        //Base Date For Humans
        revisedProvDateArray = value.revised_projected_date.split(/[- :]/);
        revisedProvDate = new Date(revisedProvDateArray[0], revisedProvDateArray[1] - 1, revisedProvDateArray[2]).toDateString();

        rowId = value.id;

        numberBkgColor = 'white';
        taskPerc = Math.round(value.earned_val / value.target_val * 100);
        if (value.complete === 1) {
            numberBkgColor = 'lightgreen'
        }


        row = $('<tr onclick="showEditTaskModal(' + rowId + ')" class=" task-table-row"></tr>');


        tdData = tdData + '' +

        '<td id="row-' + rowId + '"  style="background-color:' + numberBkgColor + '"><strong>' + value.description + '<br>' + value.number +

        '<br></strong><p class="font-xs" id="perc-' + rowId + '">( ' + taskPerc + '% of hours earned)</p></td> ' +

        '<td id="' + rowId + '-next-td">' + nextTargetDate + '</td> ' +

        '<td class="rowToHide">' + value.area.description + '</td> ' +
        '<td >' + value.system.tag + '<br> ' + value.system.description + '</td> ' +

        '<td >' + value.group.description + '</td> ' +
        '<td >' + value.task_type.name + '</td> ' +
        '<td id="' + rowId + '-stage">' + value.stage.description + '</td> ' +


        '<td id="' + rowId + '-gen-perc"   style="background-color:' + genData[2] + '">' + genData[0] + '<br><br> ' + genData[1] + '</td> ' +


        '<td id="' + rowId + '-rev-perc"  style="background-color:' + revData[2] + '">' + revData[0] + '<br><br>  ' + revData[1] + '</td> ' +


        '<td id="' + rowId + '-re-issu-perc"  style="background-color:' + reIssuData[2] + '">' + reIssuData[0] + '<br><br>  ' + reIssuData[1] + '</td> ' +


        '<td id="' + rowId + '-s-off-perc"  style="background-color:' + sOffData[2] + '">' + sOffData[0] + '<br><br>  ' + sOffData[1] + '</td> ' +


        '<td id="' + rowId + '-target-val">' + value.target_val + '</td> ' +

        '<td id="' + rowId + '-earned-val">' + value.earned_val + '</td> ' +

        '<td id="' + rowId + '-base-td">' + value.revised_projected_date + '</td> ' +

        '<td id="' + rowId + '-base-td">' + baseDate + '</td> ' +

        '<td id="' + rowId + '-base-dev-days">' + value.base_dev_days + ' days</td> ' +

        rowEnd;

        row.append(tdData);

        $('#mission-list-table-body').append(row);


        tdData = "";

    });
}

/** ===============================================================
 *
 * Append Rows to Mission List Table
 *
 * ===============================================================
 */

function appendRowsToMissionListtable(tasks) {
    var rowStart = '<tr>';
    var rowEnd = '</tr>';
    var row = '';
    var tdData = '';
    var rowId = '';
    var genData = [];
    var revData = [];
    var reIssuData = [];
    var sOffData = [];

    var taskPerc = 0;
    var numberBkgColor = 'white';
    var nextTargetDate;
    var nextTargetDateArray = '';
    var baseDate;
    var baseDateArray = '';


    $.each(tasks, function (i, value) {
        genData = dateBackGroundColors(2, value.gen_perc, value.gen_td, value.complete, value.compile_error, value.gen_applicable);
        revData = dateBackGroundColors(2, value.rev_perc, value.rev_td, value.complete, value.compile_error, value.rev_applicable);
        reIssuData = dateBackGroundColors(2, value.re_issu_perc, value.re_issu_td, value.complete, value.compile_error, value.re_issu_applicable);
        sOffData = dateBackGroundColors(2, value.s_off_perc, value.s_off_td, value.complete, value.compile_error, value.s_off_applicable);

        //var dateForHumanArray = date.split(/[- :]/);
        //var dateOut = new Date(dateForHumanArray[0], dateForHumanArray[1]-1, dateForHumanArray[2]).toDateString();
        //Next Target Date
        nextTargetDate = value.next_td;
        if (value.complete === 1) {
            nextTargetDate = 'Complete';
        } else {
            nextTargetDateArray = nextTargetDate.split(/[- :]/);
            nextTargetDate = new Date(nextTargetDateArray[0], nextTargetDateArray[1] - 1, nextTargetDateArray[2]).toDateString();
        }

        //Base Date For Humans
        baseDateArray = value.base_date.split(/[- :]/);
        baseDate = new Date(baseDateArray[0], baseDateArray[1] - 1, baseDateArray[2]).toDateString();

        rowId = value.id;

        numberBkgColor = 'white';
        taskPerc = Math.round(value.earned_val / value.target_val * 100);
        if (value.complete === 1) {
            numberBkgColor = 'lightgreen'
        }

        row = $('<tr onclick="showEditTaskModal(' + rowId + ')" class="font-xs task-table-row"></tr>');
        tdData = tdData + '' +
        '<td id="row-' + rowId + '"  style="background-color:' + numberBkgColor + '"><strong>' + value.number + value.description +
        '</strong><p class="font-xs text-align-center" id="perc-' + rowId + '">( ' + taskPerc + '% of hours earned)</p></td> ' +

        '<td class="rowToHide">' + value.area.name + '</td> ' +
        '<td >' + value.system.tag + '</td> ' +
        '<td >' + value.system.description + '</td> ' +
        '<td >' + value.group.name + '</td> ' +
        '<td >' + value.task_type.short_name + '</td> ' +
        '<td id="' + rowId + '-stage">' + value.stage.name + '</td> ' +

        '<td id="' + rowId + '-next-td">' + nextTargetDate + '</td> ' +

        '<td id="' + rowId + '-gen-perc"   style="background-color:' + genData[2] + '">' + genData[0] + '</td> ' +
        '<td id="' + rowId + '-gen-td"  style="background-color:' + genData[2] + '">' + genData[1] + '</td> ' +

        '<td id="' + rowId + '-rev-perc"  style="background-color:' + revData[2] + '">' + revData[0] + '</td> ' +
        '<td id="' + rowId + '-rev-td"  style="background-color:' + revData[2] + '">' + revData[1] + '</td> ' +

        '<td id="' + rowId + '-re-issu-perc"  style="background-color:' + reIssuData[2] + '">' + reIssuData[0] + '</td> ' +
        '<td id="' + rowId + '-re-issu-td"  style="background-color:' + reIssuData[2] + '">' + reIssuData[1] + '</td> ' +

        '<td id="' + rowId + '-s-off-perc"  style="background-color:' + sOffData[2] + '">' + sOffData[0] + '</td> ' +
        '<td id="' + rowId + '-s-off-td"  style="background-color:' + sOffData[2] + '">' + sOffData[1] + '</td> ' +

        '<td id="' + rowId + '-target-val">' + value.target_val + '</td> ' +

        '<td id="' + rowId + '-earned-val">' + value.earned_val + '</td> ' +

        '<td id="' + rowId + '-base-td">' + baseDate + '</td> ' +

        '<td id="' + rowId + '-base-dev-days">' + value.base_dev_days + ' days</td> ' +

        rowEnd;

        row.append(tdData);

        $('#mission-list-table-body').append(row);


        tdData = "";

    });
}


/** ===============================================================
 *
 * DATE COLOR FOR COLUMN
 *
 *  Gets the Background color for the Date columns.
 *  If the Date is within 2 weeks of today's date it is coloured Orange OR
 *  if the date is before today then it is late and so coloured RED.
 *  Otherwise it is coloured the same as the Percent Background.
 *
 * ===============================================================
 */
function dateBackGroundColors(orangeWeeks, perc, date, complete, error, applicable) {

    var bkgColor = "white";
    // Show Date in Human Format
    var dateForHumanArray = date.split(/[- :]/);
    var dateOut = new Date(dateForHumanArray[0], dateForHumanArray[1] - 1, dateForHumanArray[2]).toDateString();

    if (applicable === 1) {
        var today = new Date();
        var orangeWeeksToDays = (orangeWeeks * 7);

        var calcdate = String(date).replace(/\-/g, '-');
        var dateArray = calcdate.split(/[- :]/);
        var targetDate = new Date();
        targetDate.setFullYear(dateArray[0], dateArray[1] - 1, dateArray[2]);

        //Calculates the date for date nearly reached. The backgrouod colour is Orange.
        //orange weeks must must be multiplied 7 to give weeks.
        var dateNearlyAtTargetDate = new Date();
        dateNearlyAtTargetDate.setDate(dateNearlyAtTargetDate.getDate() + orangeWeeksToDays);

        if (targetDate < dateNearlyAtTargetDate) {
            bkgColor = "orange";
        }
        if (today > targetDate) {
            bkgColor = "LightSalmon";
            //console.log('today is greater');
        }
        if (complete || perc === 100) {
            bkgColor = "lightGreen";
        }
        perc = Math.ceil(perc) + '%';

    } else {
        perc = 'N/A';
        dateOut = "N/A";
        bkgColor = "LightGrey";
    }

    if (error > 0) {
        bkgColor = "red"
    }

    return [perc, dateOut, bkgColor];
}

/** ===============================================================
 *
 * REFRESH TASK TABLE
 *
 * Refresh the Task Table after it has been built
 * This activates the search menus on the columns at the top of the table
 *
 * ===============================================================
 */

function refreshTaskTable() {
    var responsiveHelper_mission_list_column = undefined;
    var breakpointDefinition = {
        computer: 2000,
        tablet: 1024,
        phone: 480
    };

    var screenHeight = $(document).height();
    screenHeight = (screenHeight - 400) + "px";
    /* COLUMN FILTER  */
    var otable = $('#mission_list').DataTable({
        // "bFilter": true,
        //"bInfo": true,
        //"bLengthChange": true,
        //"bAutoWidth": true,
        fixedHeader: true,
        "pageLength": 25,
        "bPaginate": true,
        "aaSorting": [[2, "ASC"]],
        //fixedHeader: true,
        //"scrollY": screenHeight,
        //"paging": false,
        //"bStateSave": true, // saves sort state using localStorage
        /*            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
         "t"+
         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",*/
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_mission_list_column) {
                responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#mission_list'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_mission_list_column.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_mission_list_column.respond();
        }

    });


    // Apply the filter
    $("#mission_list thead th input[type=text]").on('keyup change', function () {
        otable
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();
    });

    //$('.task-table-row').addClass('font-xs');


    $('a.toggle-vis').on('click', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = otable.column($(this).attr('data-column'));
        // Toggle the visibility
        column.visible(!column.visible());
    });
}


function updateTaskStatus(id) {
    $.ajax({
        type: "GET",
        url: "/update-task-status-popup/" + id, success: function (result) {
            //console.log(result);
            $("#update-status-popup").html(result);
            //pageSetUp();
        }
    });
}

/** ===============================================================
 *
 * TASK EDIT - POP UP Modal
 *
 * Show the Edit Task Pop Up Modal
 *
 * ===============================================================
 */
function showEditTaskModal(id) {
    getTaskDetailsView(id);
    getTaskUpdateStatusForm(id);
    getTaskUpdateOwnerForm(id);
    $('#single-edit-modal').modal('show');

}

/** ===============================================================
 *
 *  Task Details - for Display
 *  Get the Task Details and Update Status Form

 * ===============================================================
 */
function getTaskDetailsView(id) {
    $("#task-details-section").html('<i class="fa fa-2x fa-spinner fa-spin"></i> ...updating');

    $.ajax({
        url: "/get-task-details-view/" + id, success: function (result) {
            $("#task-details-section").html(result);
            $('#title-task-name').text($('#task-number').val());
            $('#title-task-description').text($('#task-description').val());
            // pageSetUp();
        }
    });
}
/** ===============================================================
 *
 *  Task Details - for Display
 *  Get the Task Details and Update Status Form

 * ===============================================================
 */
function getTaskUpdateStatusForm(id) {
    $("#update-task-form-section").html('<i class="fa fa-2x fa-spinner fa-spin"></i> ...updating');
    $.ajax({
        url: "/get-update-task-status-form/" + id, success: function (result) {
            $("#update-task-form-section").html(result);
            //pageSetUp();
            $('.progress-bar').progressbar({
                display_text: 'fill'
            });
        }
    });
}
/** ===============================================================
 *
 *  Task Owner Details - for Display
 *
 * ===============================================================
 */
function getTaskUpdateOwnerForm(id) {
    $("#task-owner-section").html('<i class="fa fa-2x fa-spinner fa-spin"></i> ...updating');
    $.ajax({
        url: "/task/role-details/" + id, success: function (result) {
            $("#task-owner-section").html(result);
            $('.selectpicker').selectpicker();
        }
    });
}

/** ===============================================================
 *
 *  Reload the Task Status Form
 *
 *
 * ===============================================================
 */
function reloadStatusForm(id) {
    getTaskUpdateStatusForm(id);
}


/** ===============================================================
 *
 *  update or save a task
 *
 *  Updates the task - from the pop up modal over Ajax
 *
 * ===============================================================
 */

function updateTask() {
    //a token is needed to prevent cross site
    //$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var task_id = $('#edit-task-id').val();
    var form = $('#update-task-form');
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var url = '/post-update-task/' + task_id;

    $.ajax({
        type: method,
        url: url,
        data: datastring,
        success: function (data) {

            //var r = JSON.stringify(data);
            //var f = JSON.parse(data);
            console.log(data);

            $.notify({
                title: '<strong>Success!</strong><br>',
                message: data.message + ""
            }, {
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay: 1000
            });


            $('#single-edit-modal').modal('hide');
            updateTaskTableRowAfterStatusUpdate(data.task, data.stepOwners);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            alert("Opps, there was an error\n\n" + error );
        }
    });
}

/** ===============================================================
 *   Update Table Row with after Task Updated
 *   update the individual row rather than loading the table
 *   as the table may be filtered
 * ===============================================================
 */
function updateTaskTableRowAfterStatusUpdate(task, stepOwners) {

    //console.log(task);
    genData = dateBackGroundColors(2, task.gen_perc, task.gen_td, task.complete, task.compile_error, task.gen_applicable);
    revData = dateBackGroundColors(2, task.rev_perc, task.rev_td, task.complete, task.compile_error, task.rev_applicable);
    reIssuData = dateBackGroundColors(2, task.re_issu_perc, task.re_issu_td, task.complete, task.compile_error, task.re_issu_applicable);
    sOffData = dateBackGroundColors(2, task.s_off_perc, task.s_off_td, task.complete, task.compile_error, task.s_off_applicable);

    //console.log('update the task table row ');
    console.log(stepOwners[0]);

    // -------------------------------
    //      GEN / EX Columns
    // --------------------------------
    //---------   Percent  -----------
    var genPercRowId = '#' + task.id + '-gen-perc';
    var genPercCol = $(genPercRowId);
    var date = task.gen_td.split(/[- :]/);
    var step1Owner = '<br> with ' + stepOwners[0];
    if (stepOwners[0] === 'N/A') {
        step1Owner = '';
    }
    step1Owner = '';//no need to see the next owner for no - leave blank
    genPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + genData[0] + step1Owner).css("background-color", genData[2]);
    //---------   Target Date  -----------
    var genTdRowId = '#' + task.id + '-gen-td';
    var genTdCol = $(genTdRowId);
    genTdCol.text(genData[1]).css("background-color", genData[2]);

    // -------------------------------
    //      REVIEW Columns
    // --------------------------------
    //---------   Percent  -----------
    var revPercRowId = '#' + task.id + '-rev-perc';
    var revPercCol = $(revPercRowId);
    date = task.rev_td.split(/[- :]/);
    var step2Owner = '<br> with ' + stepOwners[1];
    if (stepOwners[1] === 'N/A') {
        step2Owner = '';
    }

    //revPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + revData[0] + step2Owner).css("background-color", revData[2]);
    revPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + revData[0] ).css("background-color", revData[2]);
    //---------   Target Date  -----------
    var revTdRowId = '#' + task.id + '-rev-td';
    var revTdCol = $(revTdRowId);
    revTdCol.text(revData[1]).css("background-color", revData[2]);

    // ------------------------------
    //      RE-ISSUE Columns
    //---------   Percent  -----------
    var reIssuPercRowId = '#' + task.id + '-re-issu-perc';
    var reIssuPercCol = $(reIssuPercRowId);
    date = task.re_issu_td.split(/[- :]/);
    var step3Owner = '<br> with ' + stepOwners[2];
    if (stepOwners[2] === 'N/A') {
        step3Owner = '';
    }

    //reIssuPercCol.text(reIssuData[0]).css("background-color", reIssuData[2]);
    //reIssuPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + reIssuData[0] + step3Owner).css("background-color", reIssuData[2]);
    reIssuPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + reIssuData[0]).css("background-color", reIssuData[2]);
    //---------   Target Date  -----------
    var reIssuTdRowId = '#' + task.id + '-re-issu-td';
    var reIssuTdCol = $(reIssuTdRowId);
    reIssuTdCol.text(reIssuData[1]).css("background-color", reIssuData[2]);

    // ------------------------------
    //      SIGN OFF  Columns
    //---------   Percent  -----------
    var sOffPercRowId = '#' + task.id + '-s-off-perc';
    var sOffPercCol = $(sOffPercRowId);
    date = task.s_off_td.split(/[- :]/);
    var step4Owner = '<br> with ' + stepOwners[3];
    if (stepOwners[3] === 'N/A') {
        step4Owner = '';
    }

    //sOffPercCol.text(sOffData[0]).css("background-color", sOffData[2]);
    //sOffPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + sOffData[0] + step4Owner).css("background-color", sOffData[2]);
    sOffPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + sOffData[0]).css("background-color", sOffData[2]);
    //---------   Target Date  -----------
    var sOffTdRowId = '#' + task.id + '-s-off-td';
    var sOffTdCol = $(sOffTdRowId);
    sOffTdCol.text(sOffData[1]).css("background-color", sOffData[2]);

    // ------------------------------
    //      Next Target Date
    // ------------------------------
    var nextTdId = '#' + task.id + '-next-td';
    var dateForHumanArray = task.next_td.split(/[- :]/);
    var dateOut = new Date(dateForHumanArray[0], dateForHumanArray[1] - 1, dateForHumanArray[2]).toDateString();
    //$(nextTdId).text(dateOut);
    date = task.next_td.split(/[- :]/);
    $(nextTdId).html('<br>' + date[2] + '-' + date[1] + '-' + date[0]);
    if (task.complete === 1) {
        $(nextTdId).html('<br>' + 'Complete');
    }
    sOffTdCol.text(sOffData[1]).css("background-color", sOffData[2]);

    // ------------------------------
    //      Earned Value
    // ------------------------------
    var earnedValueId = '#' + task.id + '-earned-val';
    $(earnedValueId).text(task.earned_val);

    // ------------------------------
    //    Complete - Number background colour
    // ------------------------------
    var taskNumberId = '#row-' + task.id;
    if (task.complete === 1) {
        $(taskNumberId).css("background-color", "lightgreen");
    } else {
        $(taskNumberId).css("background-color", "white");
    }
    var percId = '#perc-' + task.id;
    var taskPerc = (task.earned_val / task.target_val) * 100;
    $(percId).text('( ' + taskPerc + '% of hours earned)');


    var rowx = '#row-'+ task.id;
    $(rowx).css("background-color", "yellow");
    $(rowx).append('<h6 class="text-right"><i class="fa fa-info-circle"></i> You just updated this task</h6>');

}

/** ===============================================================
 *
 *  update or save a task wner details - Role and Individual
 *
 *  Updates the task - from the pop up modal over Ajax
 *
 * ===============================================================
 */

function updateTaskRoleAndIndividual() {
    //a token is needed to prevent cross site
    //$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var task_id = $('#edit-task-role-id').val();
    var form = $('#task-update-role-and-user-form');
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var url = '/task/update-role/' + task_id;

    $.ajax({
        type: method,
        url: url,
        data: datastring,
        success: function (data) {

            //var r = JSON.stringify(data);
            //var f = JSON.parse(data);
            console.log(data);

            $.notify({
                title: '<strong>Success!</strong><br>',
                message: data.message + ""
            }, {
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay: 1000
            });

            updateTaskRowDetails(data.task);


            $('#single-edit-modal').modal('hide');

        },
        error: function (xhr, status, error) {
            alert("Opps, there was an error\n\n" + error);
        }
    });
}

function updateTaskRowDetails(task){
    //console.log(task);
    genData = dateBackGroundColors(2, task.gen_perc, task.gen_td, task.complete, task.compile_error, task.gen_applicable);
    revData = dateBackGroundColors(2, task.rev_perc, task.rev_td, task.complete, task.compile_error, task.rev_applicable);
    reIssuData = dateBackGroundColors(2, task.re_issu_perc, task.re_issu_td, task.complete, task.compile_error, task.re_issu_applicable);
    sOffData = dateBackGroundColors(2, task.s_off_perc, task.s_off_td, task.complete, task.compile_error, task.s_off_applicable);

    // -------------------------------
    //      GEN / EX Columns
    // --------------------------------
    //---------   Percent  -----------
    var genPercRowId = '#' + task.id + '-gen-perc';
    var genPercCol = $(genPercRowId);
    var date = task.gen_td.split(/[- :]/);
    genPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + genData[0]).css("background-color", genData[2]);
    //---------   Target Date  -----------
    var genTdRowId = '#' + task.id + '-gen-td';
    var genTdCol = $(genTdRowId);
    genTdCol.text(genData[1]).css("background-color", genData[2]);

    // -------------------------------
    //      REVIEW Columns
    // --------------------------------
    //---------   Percent  -----------
    var revPercRowId = '#' + task.id + '-rev-perc';
    var revPercCol = $(revPercRowId);
    date = task.rev_td.split(/[- :]/);
    revPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + revData[0]).css("background-color", revData[2]);
    //---------   Target Date  -----------
    var revTdRowId = '#' + task.id + '-rev-td';
    var revTdCol = $(revTdRowId);
    revTdCol.text(revData[1]).css("background-color", revData[2]);

    // ------------------------------
    //      RE-ISSUE Columns
    //---------   Percent  -----------
    var reIssuPercRowId = '#' + task.id + '-re-issu-perc';
    var reIssuPercCol = $(reIssuPercRowId);
    date = task.re_issu_td.split(/[- :]/);

    //reIssuPercCol.text(reIssuData[0]).css("background-color", reIssuData[2]);
    reIssuPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + reIssuData[0]).css("background-color", reIssuData[2]);
    //---------   Target Date  -----------
    var reIssuTdRowId = '#' + task.id + '-re-issu-td';
    var reIssuTdCol = $(reIssuTdRowId);
    reIssuTdCol.text(reIssuData[1]).css("background-color", reIssuData[2]);

    // ------------------------------
    //      SIGN OFF  Columns
    //---------   Percent  -----------
    var sOffPercRowId = '#' + task.id + '-s-off-perc';
    var sOffPercCol = $(sOffPercRowId);
    date = task.s_off_td.split(/[- :]/);

    //sOffPercCol.text(sOffData[0]).css("background-color", sOffData[2]);
    sOffPercCol.html('' + date[2] + '-' + date[1] + '-' + date[0] + '<br>' + sOffData[0]).css("background-color", sOffData[2]);
    //---------   Target Date  -----------
    var sOffTdRowId = '#' + task.id + '-s-off-td';
    var sOffTdCol = $(sOffTdRowId);
    sOffTdCol.text(sOffData[1]).css("background-color", sOffData[2]);

    // ------------------------------
    //      Next Target Date
    // ------------------------------
    var nextTdId = '#' + task.id + '-next-td';
    var dateForHumanArray = task.next_td.split(/[- :]/);
    var dateOut = new Date(dateForHumanArray[0], dateForHumanArray[1] - 1, dateForHumanArray[2]).toDateString();
    //$(nextTdId).text(dateOut);
    date = task.next_td.split(/[- :]/);
    $(nextTdId).html('<br>' + date[2] + '-' + date[1] + '-' + date[0]);
    if (task.complete === 1) {
        $(nextTdId).html('<br>' + 'Complete');
    }
    sOffTdCol.text(sOffData[1]).css("background-color", sOffData[2]);

    // ------------------------------
    //      Earned Value
    // ------------------------------
    var earnedValueId = '#' + task.id + '-earned-val';
    $(earnedValueId).text(task.earned_val);

    // ------------------------------
    //    Complete - Number background colour
    // ------------------------------
    var taskNumberId = '#row-' + task.id;
    if (task.complete === 1) {
        $(taskNumberId).css("background-color", "lightgreen");
    } else {
        $(taskNumberId).css("background-color", "white");
    }
    var percId = '#perc-' + task.id;
    var taskPerc = (task.earned_val / task.target_val) * 100;
    $(percId).text('( ' + taskPerc + '% of hours earned)');


    var rowx = '#row-'+ task.id;
    $(rowx).css("background-color", "yellow");
    $(rowx).append('<h6 class="text-right"><i class="fa fa-info-circle"></i> You just updated this task</h6>');


}
/** ===============================================================
 * Hide Update Task Modal
 * ===============================================================
 */
function hideUpdateTaskModal() {
    $('#single-edit-modal').modal('hide');
}
/** ===============================================================
 *
 *  Update the Colour for the Panels on the Status Modal
 *  If the Value is 100% or Complete then Colour the Background Green
 *
 * ===============================================================
 */
function updateStatusBkgColorForStatusModal(statusPerc, panleObj) {
    var completeColor = "lightGreen";
    var notCompleteColor = "lightGrey";
    if (statusPerc.indexOf('100') >= 0) {
        panleObj.css("background-color", completeColor);
    } else {
        panleObj.css("background-color", notCompleteColor);
    }
}

/** ===============================================================
 *
 *  Update Next Status Dropdown if the
 *  previous Step has been changed to 100%
 *
 * When a dropdown is change check to see if its been changed to 100%
 * if so then un hide the next steps Dropdown
 * ===============================================================
 */

$('#taskGenStatusUpdate').change(function () {
    var dropDownId = '#' + this.id + ' :selected';
    showNextStatusDropdOnStatusModal($(dropDownId).text(), $('#taskRevStatusUpdate'));
});
$('#taskRevStatusUpdate').change(function () {
    var dropDownId = '#' + this.id + ' :selected';
    showNextStatusDropdOnStatusModal($(dropDownId).text(), $('#taskReIssueStatusUpdate'));
});
$('#taskReIssueStatusUpdate').change(function () {
    var dropDownId = '#' + this.id + ' :selected';
    showNextStatusDropdOnStatusModal($(dropDownId).text(), $('#taskSignOffStatusUpdate'));
});

function showNextStatusDropdOnStatusModal(percVal, nextDropDownObj) {
    console.log(' updating next status dropdown on status modal');
    if (percVal.indexOf('100') >= 0) {
        nextDropDownObj.removeClass('hide');
    } else {
        nextDropDownObj.addClass('hide');
    }
}

/** ===============================================================
 *
 *  GET ID for the Update Task Drop Down
 *
 *  Uses a JSON search function to search through the Task Colors JSON object.
 *  It looks for the value (%), returns the ID of the value in the Object
 *  to populate the choice drop down
 *
 * ===============================================================
 */
function returnIdForTaskColor(value) {
    var taskColorsJson = JSON.parse(taskColors);

    //JSON search functions to search - found in App/app.functions.js
    return jsonSearchGetObjects(taskColorsJson, 'value', value)[0].id;
}


/** ===============================================================
 *
 *  add new task
 *
 *  Adds a new task over Ajax and return and process any errors
 *
 * ===============================================================
 */

function addNewTask() {
    console.log('Add new task function called  in Task General jS File');
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $('#add-task-form');
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#validation-errors');
    var noTaskRuleExistsErrorSection = $('#no-task-type-error');

    $.ajax({
        type: 'GET',
        url: '/post-add-task',
        data: datastring,
        success: function (result) {

            console.log(result);

            if (result.taskTypeRuleExists === false) {
                console.log('no task rule exists');
                noTaskRuleExistsErrorSection.removeClass('hide');
                validationSection.removeClass('hide')
                    .html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>').append('No Task Rule Exists for this type of Task and Stage');
                return;

            }
            noTaskRuleExistsErrorSection.addClass('hide');
            //console.log(result);
            // console.log(result.taskTypeRuleExists);
            $.notify({
                title: '<strong>Success!</strong>',
                message: result.message
            }, {
                type: 'success',
                offset: {
                    x: 50,
                    y: 100
                },
                showProgressbar: false,
                delay: 1000
            });
            validationSection.addClass('hide');

        },
        beforeSend: function () {
            pleaseWait('Adding Tasks');
            // Handle the beforeSend event

        },
        complete: function () {
            // Handle the complete event
            waitDone();
            //location.reload();

        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {

            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
            var validationErrorText = ' Validation Errors<br>';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                });
            } else {
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);
            //console.log(xhr);

        }
    });

}
$("#schedule-number").change(function () {
    //$("#target-dates-section").slideDown().fadeOut("slow");
    //getNewTaskDatesAndPopulateTaskNumber();

});

/** ===============================================================
 *  Check if task number already exisits
 * ===============================================================
 */
function checkIfTaskNumberExists(number,stage) {
    data = {'number':number,'stage':stage};
    $.ajax({
        type: 'GET',
        data: {data: data},
        url: "/check-if-task-number-exists", success: function (result) {

            if (result > 0) {
                $('#task-exists-section').html('<i class="fa fa-info-circle"></i>  ' + result + ' Task(s) already exists with this Number').removeClass('hide');
            } else {
                $('#task-exists-section').addClass('hide');
            }
            console.log('number exists = ' + result);
        }
    });
}

/** ===============================================================
 *
 *  get new task Dates and populate the Task Number
 *
 *  Gets the dates related to the new task type for the system
 *  from the Scheduler Dates table
 *
 *  Fill in the Task Number based on the System and Task Type
 *
 * ===============================================================
 */
function getNewTaskDatesAndPopulateTaskNumber() {
    var scheduleNumber = $('#schedule-number').val();
    var system = $('#system-dropdown').val();
    getTaskSchedulerDates(scheduleNumber, system);
}
/** ===============================================================
 *
 *  Get The Schedule Dates From the Server
 *
 *  gets the date view partial over AJAX
 *
 * ===============================================================
 */

function getTaskSchedulerDates(scheduleId) {
    console.log('get schedule dates');
    $.ajax({
        type: "GET",
        data: {scheduleId: scheduleId},
        url: "/get-task-scheduler-dates", success: function (result) {
            console.log(result);
            $("#target-dates-section").html(result).fadeIn(1500);
        }
    });
}
/** ===============================================================
 *
 *  Get The Task Search Ribbon
 *
 *  gets the date view partial over AJAX
 *
 * ===============================================================
 */

function getTaskSearchRibbon() {
    $.ajax({
        type: "GET",
        url: "/get-task-search-ribbon", success: function (result) {
            $("#task-search-ribbon").html(result);
            //pageSetUp();
        }
    });
}

/** ===============================================================
 *
 *  POPULATE THE TASK TYPE DROP DOWN
 *
 *  Populates the task type drop down when a user selects the
 *  task group - filter
 *
 * ===============================================================
 */
function populateTaskTypeDropDown(groupId) {
    var dropDown = $('#task-type-dropdown');
    dropDown.empty();
    $(dropDown).append('<option value=' + 0 + '>' + '</option>');
    var taskTypesSearch = JSON.parse(taskTypes);

    $.each(taskTypesSearch, function (key, value) {
        if (groupId != 0) {
            if (value.group_id == groupId) {
                //(value.group_id);
                $(dropDown).append('<option value=' + value.id + '>' + value.name + '</option>');
            }
        }
        else {
            $(dropDown).append('<option value=' + value.id + '>' + value.name + '</option>');
        }
    });
}


/** ===============================================================
 *
 * BACKGROUND COLOUR
 *
 *  Updates the task - from the pop up modal over Ajax
 *
 * ===============================================================
 */
function backgroundColor(text) {
    console.log('value = ' + text);
    var notStartedColor = "lightgray";
    var halfWayColor = "lightgreen";
    var completeColor = "darkgreen";
    var nAColor = "white";
    var updateColor = "lightgray";

    if (text === "50%") {
        updateColor = halfWayColor;
    }
    if (text === "100%") {
        updateColor = completeColor;
    }
    if (text === "N/A") {
        updateColor = nAColor;
    }
    return updateColor;
}


// ===============================================================
//                     UPDATE MODELS - SECTION END
// ===============================================================


// Returns an AJAX request with the origin Terminal Details
function showTask(id) {
    $.ajax({
        url: "/get-type-details-ajax/" + id, success: function (result) {
            $("#type_details").html(result);
        }
    });
    runAllForms();
}


function importTaskFromImportTablexx(row) {
    alert(row);
}


function runSparklineGraphsOnPage() {
    //pageSetUp();

    $('.sparkline').sparkline();

    /* Inline sparklines take their values from the contents of the tag */
    $('.inlinesparkline').sparkline({type: 'pie', barColor: 'green'});

    /* Sparklines can also take their values from the first argument
     passed to the sparkline() function */
    var myvalues = [10, 8, 5, 7, 4, 4, 1];
    $('.dynamicsparkline').sparkline(myvalues);

    /* The second argument gives options such as chart type */
    var genPer = [10, 90];
    $('.dynamicbar').sparkline(genPer, {type: 'pie', barColor: 'green'});

    /* Use 'html' instead of an array of values to pass options
     to a sparkline with data in the tag */
    $('.inlinebar').sparkline('html', {type: 'bar', barColor: 'red'});
}

// ===============================================================
//          UPDATE TASK PROGRESS BARS AND STATUS
// ===============================================================
/** ===============================================================
 *
 *  OverRide Status
 *
 *  Flag that the status can of a step that has been completed i.e 100%
 *  can be changed i,e go back to 0% or 50%
 *
 * ===============================================================
 */
var overRide = false;
function overRideStatus() {
    if(overRide === false){
        console.log('over ride activated');
        overRide = true;
        $('#over-ride-button').html('Over Ride ON');
    }else{
        overRide = false;
        $('#over-ride-button').html('Over Ride OFF');
    }
}
/** ===============================================================
 *
 *  Fill  Progress Bar with Status - Checks to see if the Step has phases
 *
 * ===============================================================
 */
function FillTaskStatusStep(tag) {
    console.log('function: FillTaskStatusStep');
    var stepNumber = getStepNumberFromTag(tag);
    var stepBreakDownCount = '#step' + stepNumber + '-break-down-count';
    console.log('count = ' + $(stepBreakDownCount).length);
    if ($(stepBreakDownCount).length) {
        var count = parseInt($(stepBreakDownCount).val());

        if(count > 0){
            var canUpdate = checkIfThisStepCanBeUpdated(tag);
            console.log('can update = ' +canUpdate);
            if(canUpdate){
                updateProgressBarByStepBreakDown(tag,stepNumber);
            }
        }
    }
    else {
        fillStatusProgressBarWithPercentage(tag);

    }
}
function getStepNumberFromTag(tag){
    console.log('function: getStepNumberFromTag');
    var genPer = $('#gen-percent').val();
    var revPer = $('#rev-percent').val();
    var reIssuPer = $('#re-issu-percent').val();
    var sOffPer = $('#s-off-percent').val();

    var genApplicable = $('#gen-applicable').val();
    var revApplicable = $('#rev-applicable').val();
    var reIssuApplicable = $('#re-issu-applicable').val();
    var sOffApplicable = $('#s-off-applicable').val();

    var stepNumber = 0;

    switch (tag) {
        case 'gen':
            if (genApplicable === '1' && genPer !== '100') {
            }
            stepNumber = 1;
            break;
        case 'rev':
            if ((revApplicable === '1' && revPer !== '100') &&
                (genPer === '100' || genApplicable === '0')
            ) {
            }
            stepNumber = 2;
            break;
        case 're-issu':
            if ((reIssuApplicable === '1' && reIssuPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0')
            ) {
            }
            stepNumber = 3;
            break;
        case 's-off':
            if ((sOffApplicable === '1' && sOffPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0') &&
                (reIssuPer === '100' || reIssuApplicable === '0')
            ) {
            }
            stepNumber = 4;
            break;
    }

    return stepNumber;


}
/** ===============================================================
 *
 *  Fill  Progress Bar with Status - No Step Phases

 * ===============================================================
 */
var genProgressBarValue = 0;
function fillStatusProgressBarWithPercentage(tag) {
     console.log(tag + ' fillStatusProgressBarWithPercentage');
    var hash = "#";

    var inputId = hash + tag + '-percent';
    var progresBarId = hash + tag + '-progress-bar';
    var percentTextId = hash + tag + '-percent-text';

    var inputObj = $(inputId);
    var prgressBarObj = $(progresBarId);
    var textObj = $(percentTextId);

    var currentVal = inputObj.val();

    var genPer = $('#gen-percent').val();
    var revPer = $('#rev-percent').val();
    var reIssuPer = $('#re-issu-percent').val();
    var sOffPer = $('#s-off-percent').val();

    var genApplicable = $('#gen-applicable').val();
    var revApplicable = $('#rev-applicable').val();
    var reIssuApplicable = $('#re-issu-applicable').val();
    var sOffApplicable = $('#s-off-applicable').val();

    //Can the Step Be updated i.e  is the previous step at 100% or N/A
    var canUpdate = false;

    switch (tag) {
        case 'gen':
            if (genApplicable === '1' && genPer !== '100') {
                canUpdate = true;
            }
            break;
        case 'rev':
            if ((revApplicable === '1' && revPer !== '100') &&
                (genPer === '100' || genApplicable === '0')
            ) {
                canUpdate = true;
            }
            break;
        case 're-issu':
            if ((reIssuApplicable === '1' && reIssuPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0')
            ) {
                canUpdate = true;
            }
            break;
        case 's-off':
            if ((sOffApplicable === '1' && sOffPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0') &&
                (reIssuPer === '100' || reIssuApplicable === '0')
            ) {
                canUpdate = true;
            }
            break;
    }


    // If the value needs to be changed after is it set 100 it can be - by the PM or delegate
    if (overRide) {
        canUpdate = true;
    }
    currentVal = parseInt(currentVal);
    var oneStepOn = true;
    if(oneStepOn){
        if (canUpdate) {
            if (currentVal < 100) {
                prgressBarObj.css('width', 100 + '%').attr('aria-valuetransitiongoal', 100).attr('data-original-title', 100);
                genProgressBarValue = 100;
                textObj.text('100%');
                prgressBarObj.text('100%');
                inputObj.val(100);
            }
            if (currentVal === 100) {
                prgressBarObj.css('width', 0 + '%').attr('aria-valuetransitiongoal', 0).attr('data-original-title', 0);
                genProgressBarValue = 0;
                prgressBarObj.text('0%');
                textObj.text('0%');
                inputObj.val(0);
            }
        }
    }else{
        if (canUpdate) {
            if (currentVal < 50 ) {
                prgressBarObj.css('width', 50 + '%').attr('aria-valuetransitiongoal', 50).attr('data-original-title', 50);
                genProgressBarValue = 50;
                prgressBarObj.text('50%');
                textObj.text('50%');
                inputObj.val(50);
            }
            if (currentVal > 49 && currentVal < 100) {
                prgressBarObj.css('width', 100 + '%').attr('aria-valuetransitiongoal', 100).attr('data-original-title', 100);
                genProgressBarValue = 100;
                textObj.text('100%');
                prgressBarObj.text('100%');
                inputObj.val(100);
            }
            if (currentVal === 100) {
                prgressBarObj.css('width', 0 + '%').attr('aria-valuetransitiongoal', 0).attr('data-original-title', 0);
                genProgressBarValue = 0;
                prgressBarObj.text('0%');
                textObj.text('0%');
                inputObj.val(0);
            }
        }
    }

    $('.progress-bar').progressbar({
        display_text: 'fill'
    });

}
/** ===============================================================
 *
 *  Fill  Progress Bar with Status - With Step Phases

 * ===============================================================
 */
function fillStepStatusProgressBarWithPercentage(tag) {
    console.log(tag);
    var hash = "#";

    var inputId = hash + tag + '-percent';
    var progresBarId = hash + tag + '-progress-bar';
    var percentTextId = hash + tag + '-percent-text';

    var inputObj = $(inputId);
    var prgressBarObj = $(progresBarId);
    var textObj = $(percentTextId);

    var currentVal = inputObj.val();

    var genPer = $('#gen-percent').val();
    var revPer = $('#rev-percent').val();
    var reIssuPer = $('#re-issu-percent').val();
    var sOffPer = $('#s-off-percent').val();

    var genApplicable = $('#gen-applicable').val();
    var revApplicable = $('#rev-applicable').val();
    var reIssuApplicable = $('#re-issu-applicable').val();
    var sOffApplicable = $('#s-off-applicable').val();

    //Can the Step Be updated i.e  is the previous step at 100% or N/A
    var canUpdate = false;
    var step = 0;

    switch (tag) {
        case 'gen':
            if (genApplicable === '1' && genPer !== '100') {
                canUpdate = true;
            }
            step = 1;
            break;
        case 'rev':
            if ((revApplicable === '1' && revPer !== '100') &&
                (genPer === '100' || genApplicable === '0')
            ) {
                canUpdate = true;
            }
            step = 2;
            break;
        case 're-issu':
            if ((reIssuApplicable === '1' && reIssuPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0')
            ) {
                canUpdate = true;
            }
            step = 3;
            break;
        case 's-off':
            if ((sOffApplicable === '1' && sOffPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0') &&
                (reIssuPer === '100' || reIssuApplicable === '0')
            ) {
                canUpdate = true;
            }
            step = 4;
            break;
    }


    var stepStatusObj = $('#step' + step + '-status');
    var stepStatus = stepStatusObj.val();
    var phaseCount = $('#step' + step + '-phase-count').val();
    var StepIncrement = (100 / phaseCount).toFixed(1);
    //console.log(StepIncrement);

    // If the value needs to be changed after is it set 100 it can be - by the PM or delegate
    if (overRide) {
        canUpdate = true;
        if (stepStatus >= 100) {
            stepStatus = 0;
            inputObj.val(stepStatus);
            StepIncrement = 0;
        }
    }

    if (canUpdate) {

        //update the progress bar and text
        stepStatus = Number(stepStatus) + Number(StepIncrement);
        if (stepStatus > 99)stepStatus = 100;
        prgressBarObj.css('width', stepStatus + '%').attr('aria-valuetransitiongoal', stepStatus).attr('data-original-title', stepStatus);
        prgressBarObj.text(stepStatus + '%');
        textObj.text('' + stepStatus + '%');

        //update the Step Status - for the task and step phase status
        inputObj.val(stepStatus);
        stepStatusObj.val(stepStatus);

        //update the Owner list
        var ownerItem = '';
        var ownerItemText = '';
        var stepDone = '<i class=" text-success fa fa-check"></i>';
        var stepNotDone = '<i class=" text-primary fa fa-times"></i>';
        var stepCurrentOwner = $('#step' + step + '-current-owner');
        var stepPhaseStatusInput = '';
        stepCurrentOwner.val(1);

        for (var i = 1; i <= phaseCount; i++) {
            console.log(' in for loop');
            ownerItem = $('#step' + step + '-phase' + i + '-owner');
            stepPhaseStatusInput = $('#step' + step + '-phase' + i + '-status');
            ownerItemText = ownerItem.text();

            if (stepStatus >= (i * StepIncrement) - 1) {
                ownerItem.html(ownerItemText + stepDone);
                ownerItem.addClass('text-success');
                stepPhaseStatusInput.val(1);
            } else {
                ownerItem.html(ownerItemText + stepNotDone);
                ownerItem.addClass('text-primary');
                stepPhaseStatusInput.val(0);
            }

            //If Override - and at 100% - set all back to Zero and start again
            if (overRide && stepStatus === 0 && StepIncrement === 0) {
                ownerItem.html(ownerItemText + stepNotDone);
                ownerItem.addClass('text-primary');
                stepPhaseStatusInput.val(0);
            }
        }
        $('#step' + step + '-updated').val(1);

    }
    $('.progress-bar').progressbar({
        display_text: 'fill'
    });

}
//update the Task Step Phase Buttons, Status and Progress Bar
function updateButtonTaskStepPhase(tag, phaseCount ,stepNumber, phaseNumber) {
    console.log('function: updateButtonTaskStepPhase');

    if(checkIfThisStepCanBeUpdated(tag) === true){
        var buttonId = '#step' + stepNumber + '-phase' + phaseNumber + '-owner';
        var statusInputId = '#step' + stepNumber + '-phase' + phaseNumber + '-status';
        if($(statusInputId).val() === '1'){
            $(statusInputId).val(0);
            $(buttonId).toggleClass('btn-success btn-default').find('i').toggleClass('fa-check fa-times');

        }else{
            $(statusInputId).val(1);
            $(buttonId).toggleClass('btn-default btn-success').find('i').toggleClass('fa-times fa-check');
        }
        var status = 0;
        var StepIncrement = (100 / phaseCount).toFixed(1);
        for (var i = 1; i <= phaseCount; i++) {
            status = status + (parseInt($('#step' + stepNumber + '-phase' + i + '-status').val()) * StepIncrement) ;
        }
        if (status > 97)status = 100;
        updateTaskProgressBarAndInputStatus(tag,status);
    }

}
//Checks if the step can be updates
// looks at previous step to see if its applicable or 100%
function checkIfThisStepCanBeUpdated(tag){
    console.log('function: checkIfThisStepCanBeUpdated');
    var genPer = $('#gen-percent').val();
    var revPer = $('#rev-percent').val();
    var reIssuPer = $('#re-issu-percent').val();
    var sOffPer = $('#s-off-percent').val();

    var genApplicable = $('#gen-applicable').val();
    var revApplicable = $('#rev-applicable').val();
    var reIssuApplicable = $('#re-issu-applicable').val();
    var sOffApplicable = $('#s-off-applicable').val();

    //Can the Step Be updated i.e  is the previous step at 100% or N/A
    var canUpdate = false;
    var step = 0;

    switch (tag) {
        case 'gen':
            if (genApplicable === '1' && genPer !== '100') {
                canUpdate = true;
            }
            step = 1;
            break;
        case 'rev':
            if ((revApplicable === '1' && revPer !== '100') && (genPer === '100' || genApplicable === '0')
            ) {
                canUpdate = true;
            }
            step = 2;
            break;
        case 're-issu':
            if ((reIssuApplicable === '1' && reIssuPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0')
            ) {
                canUpdate = true;
            }
            step = 3;
            break;
        case 's-off':
            if ((sOffApplicable === '1' && sOffPer !== '100') &&
                (genPer === '100' || genApplicable === '0') &&
                (revPer === '100' || revApplicable === '0') &&
                (reIssuPer === '100' || reIssuApplicable === '0')
            ) {
                canUpdate = true;
            }
            step = 4;
            break;
    }
    // If the value needs to be changed after is it set 100 it can be - by the PM or delegate
    if (overRide) {
        canUpdate = true;
    }
    return canUpdate;
}
function updateProgressBarByStepBreakDown(tag,stepNumber){
    console.log('function: updateProgressBarByStepBreakDown');
    var status = 0;
    var stepCount = 1;//Math.round(100/parseInt($('#step' + stepNumber  + '-break-down-count').val()));
    stepCount = parseInt($('#step' + stepNumber  + '-break-down-count').val());
    //Step Increment to be added to the step
    var stepIncrement = Math.round(100/stepCount);
    //THe update Object
    var stepStatusObj = $('#step' + stepNumber + '-status');
    var stepStatus = stepStatusObj.val();
    var updateStatus = parseInt(stepStatus) + parseInt(stepIncrement);
    if(updateStatus > 100 && overRide === true){
        updateStatus = 0;
    }
    console.log(updateStatus);
    if(updateStatus < 100 || updateStatus === 100){
        if(updateStatus > 97){
            updateStatus = 100;
        }

        updateTaskProgressBarAndInputStatus(tag,updateStatus);
        stepStatusObj.val(updateStatus);
    }
}


function updateTaskProgressBarAndInputStatus(tag,status){
    console.log('function: updateTaskProgressBarAndInputStatus');
    var hash = "#";
    var inputId = hash + tag + '-percent';
    var progresBarId = hash + tag + '-progress-bar';
    var percentTextId = hash + tag + '-percent-text';

    var inputObj = $(inputId);
    var prgressBarObj = $(progresBarId);
    var textObj = $(percentTextId);

    console.log('status = ' + status);
    console.log('--------------------');

    prgressBarObj.css('width', status + '%').attr('aria-valuetransitiongoal', status).attr('data-original-title', status);
    prgressBarObj.text(status + '%');
    textObj.text('' + status + '%');
    inputObj.val(status);
}





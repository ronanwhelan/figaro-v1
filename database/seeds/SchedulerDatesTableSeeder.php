<?php

use Illuminate\Database\Seeder;


class SchedulerDatesTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('scheduler_dates')->delete();

        $data = array(
            array(
                'project_id' => 1,
                'scheduler_task_id' => '54',
                'scheduler_task_name' => 'System and Task Name',
                'system_id' => 1,
                'task_type_id' => 1,
                'gen_date' => new DateTime("+1 months"),
                'ex_date' => new DateTime("+3 months"),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

            array(
                'project_id' => 1,
                'scheduler_task_id' => '55',
                'scheduler_task_name' => 'System and Task Name',
                'system_id' => 2,
                'task_type_id' => 11,
                'gen_date' => new DateTime("+2 months"),
                'ex_date' => new DateTime("+4 months"),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );


        DB::table('scheduler_dates')->insert($data);
    }
}

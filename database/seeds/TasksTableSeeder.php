<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class TasksTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tasks')->delete();

        $date =date_create("2016-05-09");
        date_add($date,date_interval_create_from_date_string("-40 days"));
        $todayMinusOneWeek = date_format($date,"Y-m-d");


        $data = array(
            array(

                'number' => '50.M1.R01.IVP.001',
                'description' => 'Task Description',
                'project_id' => 1,

                'area_id' => 1,
                'system_id' => 1,
                'group_id' => 6,
                'task_type_id' => 14,
                'stage_id' => 1,

                'gen_applicable' => 1,
                'gen_perc' => '0%',
                'gen_td' => new DateTime('2017-05-09'),

                'rev_applicable' => 1,
                'rev_perc' => 0,
                'rev_td' => new DateTime('2017-04-09'),

                're_issu_applicable' => 1,
                're_issu_perc' => 0,
                're_issu_td' => new DateTime('2017-03-20'),

                's_off_applicable' => 1,
                's_off_perc' => 0,
                's_off_td' => new DateTime('2017-03-10'),

                'target_val' => 60,
                'earned_val' => 0,

                'group_owner_id' => 1,

                'next_td' => new DateTime,
                'last_td' => new DateTime('2017-03-10'),

                'prim_date' => new DateTime,
                'base_date' => new DateTime,

                'complete' => false,
                'complete_date' => new DateTime,

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(

                'number' => '100.M1.R01.IVP.002',
                'description' => 'Task Description',
                'project_id' => 1,
                'area_id' => 1,
                'system_id' => 1,
                'group_id' => 5,
                'task_type_id' => 10,
                'stage_id' => 2,

                'gen_applicable' => 0,
                'gen_perc' => 0,
                'gen_td' => new DateTime,

                'rev_applicable' => 1,
                'rev_perc' => 50,
                'rev_td' => new DateTime("-1 day"),

                're_issu_applicable' => 1,
                're_issu_perc' => 0,
                're_issu_td' => new DateTime("+1 week"),

                's_off_applicable' => 1,
                's_off_perc' => 0,
                's_off_td' => new DateTime("+2 months"),

                'target_val' => 60,
                'earned_val' => 25,

                'group_owner_id' => 1,

                'next_td' => new DateTime("-1 day"),
                'last_td' => new DateTime("+2 months"),

                'prim_date' => new DateTime("+3 months"),
                'base_date' => new DateTime("+3 months"),

                'complete' => false,
                'complete_date' => new DateTime,

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(

                'number' => '50.M1.R02.CC.001',
                'description' => 'Task Description',
                'project_id' => 1,

                'area_id' => 1,
                'system_id' => 1,
                'group_id' => 3,
                'task_type_id' => 1,
                'stage_id' => 1,

                'gen_applicable' => 1,
                'gen_perc' => 0,
                'gen_td' => new DateTime('2017-04-15'),

                'rev_applicable' => 1,
                'rev_perc' => 0,
                'rev_td' => new DateTime('2017-05-05'),

                're_issu_applicable' => 1,
                're_issu_perc' => 0,
                're_issu_td' => new DateTime('2017-05-15'),

                's_off_applicable' => 1,
                's_off_perc' => 0,
                's_off_td' => new DateTime('2017-05-25'),

                'target_val' => 60,
                'earned_val' => 0,

                'group_owner_id' => 1,

                'next_td' => new DateTime('2017-04-15'),
                'last_td' => new DateTime('2017-05-25'),

                'prim_date' => new DateTime('2017-05-09'),
                'base_date' => new DateTime('2017-05-09'),

                'complete' => false,
                'complete_date' => new DateTime('0000-00-00'),


                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

        );


        DB::table('tasks')->insert($data);


    }

}
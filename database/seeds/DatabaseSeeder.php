<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->call('AreasTableSeeder');
        //$this->call('GroupsTableSeeder');
        $this->call('StagesTableSeeder');
        //$this->call('TaskTypeTableSeeder');
        $this->call('SystemsTableSeeder');
        //$this->call('TaskColorsTableSeeder');

        $this->call('ProjectsTableSeeder');
        $this->call('CompanyListTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('PunchListSetup');
        $this->call('TrackerSetup');
        //TASKS
        //$this->call('TasksTableSeeder');

        //$this->call('TaskExtensionsTableSeeder');

        //$this->call('TaskRulesSeeder');

        //$this->call('SchedulerDatesTableSeeder');
    }
}

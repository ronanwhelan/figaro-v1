<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class TaskRulesSeeder extends Seeder {

    public function run()
    {
        DB::table('task_rules')->delete();

        $data = array(
            array(

                'project_id' => 1,
                'task_group_id' => 3,
                'task_type_id' => 1,
                'stage_id' => 1,

                'gen_ex_buffer_days' => 30,
                'gen_ex_applicable' => 1,
                'gen_ex_hrs_perc' => 60,

                'rev_buffer_days' => 20,
                'rev_applicable' => 1,
                'rev_hrs_perc' => 20,

                're_issu_buffer_days' => 10,
                're_issu_applicable' => 1,
                're_issu_hrs_perc' => 10,

                's_off_buffer_days' => 0,
                's_off_applicable' => 1,
                's_off_hrs_perc' => 10,

                'default_hrs' => 40,

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(

                'project_id' => 1,
                'task_group_id' => 3,
                'task_type_id' => 1,
                'stage_id' => 2,

                'gen_ex_buffer_days' => 30,
                'gen_ex_applicable' => 1,
                'gen_ex_hrs_perc' => 65,

                'rev_buffer_days' => 20,
                'rev_applicable' => 1,
                'rev_hrs_perc' => 15,

                're_issu_buffer_days' => 10,
                're_issu_applicable' => 1,
                're_issu_hrs_perc' => 10,

                's_off_buffer_days' => 0,
                's_off_applicable' => 1,
                's_off_hrs_perc' => 10,

                'default_hrs' => 40,

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),



        );


        DB::table('task_rules')->insert($data);


    }

}
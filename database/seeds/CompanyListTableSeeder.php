<?php

use Illuminate\Database\Seeder;

class CompanyListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_list')->delete();

        $data = array(
            array(

                'name' => 'PES International',
                'short_name' => '',
                'description' => '',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ),
        );

        DB::table('company_list')->insert($data);
    }
}

<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class TaskExtensionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('task_extensions')->delete();

        $data = array(
            array(
                'task_id' => 1,
                'error_code' => 0,
                'doc_id' => 0,
                'who_completed_id' => 0,
                'who_created_id' => 0,
                'note' => '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

            array(
                'task_id' => 2,
                'error_code' => 0,
                'doc_id' => 0,
                'who_completed_id' => 0,
                'who_created_id' => 0,
                'note' => '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'task_id' => 3,
                'error_code' => 0,
                'doc_id' => 0,
                'who_completed_id' => 0,
                'who_created_id' => 0,
                'note' => '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),


        );


        DB::table('task_extensions')->insert($data);


    }

}

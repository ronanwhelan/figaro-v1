<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class AreasTableSeeder extends Seeder {

    public function run()
    {
        DB::table('areas')->delete();

        $data = array(
            array(
                'project_id' => 1,
                'name' => 'N/A',
                'description' => 'Not Applicable',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
/*            array(
                'project_id' => 1,
                'name' => 'GEN',
                'description' => 'General Update',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'TBD',
                'description' => 'To be determined',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'ELEC',
                'description' => 'Electrical',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'CLZ',
                'description' => 'Clean Rooms',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'CIVIL',
                'description' => 'CLZ Update',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

            array(
                'project_id' => 1,
                'name' => 'USP',
                'description' => 'Up Stream Process',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

            array(
                'project_id' => 1,
                'name' => 'DSP',
                'description' => 'Downstream Process',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'SHF',
                'description' => 'Shared Function',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'PSF',
                'description' => 'Process Function Support',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'CU',
                'description' => 'Clean Utilities',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'BU',
                'description' => 'Black Utilities',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'HVAC',
                'description' => 'HVAC',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'AUTO',
                'description' => 'Automation',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),*/

        );


        DB::table('areas')->insert($data);


    }

}
<?php
use Illuminate\Database\Seeder;
class TrackerSetup extends Seeder {

    public function run()
    {
        //Groups
        DB::table('groups')->delete();
        $data = array(
            array(
                'project_id' => 1,
                'name'      => 'N/A',
                'description'   => 'not applicable',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('groups')->insert( $data );


        //Types
        DB::table('task_types')->delete();
        $data = array(
            array(
                'name'    => 'N/A',
                'short_name' => 'N/A',
                'description'=> 'not applicable',
                'group_id' => 1,
                'rule_type' => 0,
                'project_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('task_types')->insert($data);


        //Sub Types
        DB::table('task_sub_types')->delete();
        $data = array(
            array(
                'name'    => 'N/A',
                'description'=> 'not applicable',
                'task_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('task_sub_types')->insert($data);



    }


}

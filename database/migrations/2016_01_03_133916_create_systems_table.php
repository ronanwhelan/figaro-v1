<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('systems', function(Blueprint $table)
		{
			$table->increments('id');
            $table->Integer('project_id')->unsigned();// Link to the Projects table
            $table->string('tag');
            $table->string('description');
            $table->Integer('area_id')->unsigned();
            $table->Integer('size')->unsigned()->default(1);//low,medium,large - for setting hrs against
            $table->Integer('l1_id')->unsigned()->default(1);
            $table->Integer('l2_id')->unsigned()->default(2);
            $table->Integer('l3_id')->unsigned()->default(3);
            $table->Integer('l4_id')->unsigned()->default(4);
            $table->string('tag_divider')->default('-');
			$table->timestamps();
            $table->foreign('area_id')->references('id')->on('areas');
            $table->index('tag');
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('systems');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}

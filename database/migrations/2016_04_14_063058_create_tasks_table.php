<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function(Blueprint $table)
        {
            $table->increments('id');//the table index

            $table->String('number',50)->index();//the Task Number
            //$table->index('number');
            $table->String('description');//an extra text add-on to further describe the task
            $table->String('add_on',50)->default('');//allows thr user to add a little tag to the number
            $table->String('add_on_desc',80)->default('');//The addon description
            $table->String('tag',50)->default('');//a more easier tag to reference the task by e.g PREP.CC.B456
            $table->String('document_number',80)->default('');//linked document number

            $table->Integer('project_id')->unsigned()->default(1);// Link to the Projects table

            $table->Integer('area_id')->unsigned();// Link to an Area in the Areas table
            $table->Integer('system_id')->unsigned(); // Link to a System in the Systems table
            $table->Integer('group_id')->unsigned(); // Link to an Group in the Groups table
            $table->Integer('task_type_id')->unsigned(); // Link to a task type in the Task_Types table
            $table->Integer('stage_id')->unsigned(); // Link to an stage in the Stages table
            $table->Integer('task_sub_type_id'); // Link to a test spec in the task test spec table

            $table->tinyInteger('gen_applicable')->default(1);//is this step applicable
            $table->tinyInteger('gen_perc')->default(0);// the status of the step e.g 50% , Complete , Due
            $table->dateTime('gen_td')->default(\Carbon\Carbon::createFromDate(2016,01,01)); // The Target date for the step - calculated by the rules that are applied tot he task target Date.
            $table->decimal('gen_target_hrs',5,1)->default(0);// Calculated Target Hours dedicated to the step
            $table->decimal('gen_hrs',5,1)->default(0);// Calculated Hours dedicated tot he step
            $table->dateTime('gen_complete_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));
            $table->Integer('gen_completed_by_id')->unsigned()->default(1);

            $table->tinyInteger('rev_applicable')->default(1);//is this step applicable
            $table->tinyInteger('rev_perc')->default(0);// the status of the step e.g 50% , Complete , Due
            $table->dateTime('rev_td')->default(\Carbon\Carbon::createFromDate(2016,01,01));// The Target date for the step - calculated by the rules that are applied tot he task target Date.
            $table->decimal('rev_target_hrs',5,1)->default(0);// Calculated Target Hours dedicated tot he step
            $table->decimal('rev_hrs',5,1)->default(0);// Calculated Hours dedicated tot he step
            $table->dateTime('rev_complete_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));
            $table->Integer('rev_completed_by_id')->unsigned()->default(1);

            $table->tinyInteger('re_issu_applicable')->default(1);//is this step applicable
            $table->tinyInteger('re_issu_perc')->default(0);// the status of the step e.g 50% , Complete , Due
            $table->dateTime('re_issu_td')->default(\Carbon\Carbon::createFromDate(2016,01,01));// The Target date for the step - calculated by the rules that are applied tot he task target Date.
            $table->decimal('re_issu_taget_hrs',5,1)->default(0);// Calculated Hours dedicated tot he step
            $table->decimal('re_issu_hrs',5,1)->default(0);// Calculated Hours dedicated tot he step
            $table->dateTime('re_issu_complete_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));
            $table->Integer('re_issu_completed_by_id')->unsigned()->default(1);

            $table->tinyInteger('s_off_applicable')->default(1);//is this step applicable
            $table->tinyInteger('s_off_perc')->default(0);// the status of the step e.g 50% , Complete , Due
            $table->dateTime('s_off_td')->default(\Carbon\Carbon::createFromDate(2016,01,01));// The Target date for the step - calculated by the rules that are applied tot he task target Date.
            $table->decimal('s_off_target_hrs',5,1)->default(0);// Calculated Target Hours dedicated to the step
            $table->decimal('s_off_hrs',5,1)->default(0);// Calculated Hours dedicated tot he step
            $table->dateTime('s_off_complete_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));
            $table->Integer('s_off_completed_by_id')->unsigned()->default(1);

            $table->Integer('target_val')->default(0);// the Value of hours set for the task
            $table->decimal('earned_val',5,1)->default(0);// the calculated earned value

            $table->Integer('step_1_owner_id')->unsigned()->default(1);//a link to the user group assigned  - who is responsible now for the step (its phase)
            $table->Integer('step_2_owner_id')->unsigned()->default(1);//a link to the user group assigned  - who is responsible now for the step (its phase)
            $table->Integer('step_3_owner_id')->unsigned()->default(1);//a link to the user group assigned  - who is responsible now for the step (its phase)
            $table->Integer('step_4_owner_id')->unsigned()->default(1);//a link to the user group assigned  - who is responsible now for the step (its phase)

            $table->Integer('group_owner_id')->unsigned();//a link to the user group assigned  - who owns this task

            $table->Integer('assigned_role_id')->unsigned();//a link to the Roles
            $table->Integer('assigned_user_id')->unsigned();//a link to the User

            $table->Integer('status')->default(0);// Automation link - to show all automation tasks associated with this task

            $table->Integer('flag')->default(0);// Flag the task to be watched

            $table->tinyInteger('lag_days')->default(0);// the number of days to lag the from the target date(scheduled date)
            $table->tinyInteger('lock_date')->default(0);// set to 1 if the target date is locked in and does not change

            $table->dateTime('next_td')->default(\Carbon\Carbon::createFromDate(2016,01,01)); // calculated date for the nearest applicable date that is due
            $table->dateTime('last_td')->default(\Carbon\Carbon::createFromDate(2016,01,01)); // calculated date for the latest applicable date that is due
            $table->dateTime('prim_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//The Scheduled Date
            $table->dateTime('projected_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//The Projected Dates - used for Base line Graphs
            $table->dateTime('revised_projected_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//The Projected Dates - used for Base line Graphs
            $table->dateTime('base_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//base line date - this is set by the PM and should not change - the true predicted date the task was due to finish
            $table->integer('base_dev_days')->default(0);// The number of days plus or minus from the last target date to the baseline date

            $table->tinyInteger('complete')->unsigned()->default(0);//its complete status - 1 = complete, 0 = not complete yet
            $table->dateTime('complete_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//what date the task was completed.

            //After creating or updating the date for the task ensure there is no Compile error.
            $table->tinyInteger('compile_error')->unsigned()->default(1);

           // $table->integer('extension_id')->unsigned();// A connection to the Task extensions table (more information about the task)

            //SCHEDULE LINK
            $table->String('schedule_number',30);//The Schedule number linked to the Task
            $table->String('schedule_date_choice',15);//Use the start or Finish Date when getting the schedule date
            $table->dateTime('schedule_start_date')->default(\Carbon\Carbon::createFromDate(2016,01,01)); // The schedule Start Date
            $table->dateTime('schedule_finish_date')->default(\Carbon\Carbon::createFromDate(2016,01,01)); //The schedule Finish Date
            $table->tinyInteger('schedule_lag_days')->default(0);// the number of days to lag the from the target date(scheduled date)

            $table->timestamps();

            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('system_id')->references('id')->on('systems');
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('task_type_id')->references('id')->on('task_types');
            $table->foreign('stage_id')->references('id')->on('stages');
           // $table->foreign('test_spec_id')->references('id')->on('task_test_specs');
            $table->foreign('project_id')->references('id')->on('projects');
            //todo add foreign key restraint to the user groups table
            //$table->foreign('group_owner_id')->references('id')->on('users');
            //$table->foreign('doc_id')->references('id')->on('documents');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Schema::dropIfExists('tableName');
        Schema::drop('tasks');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

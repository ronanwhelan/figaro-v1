<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_rules', function (Blueprint $table) {

            $table->increments('id');

            $table->Integer('project_id')->unsigned();// Link to the stage table

            $table->Integer('task_group_id')->unsigned();// Link to the task type table
            $table->Integer('task_type_id')->unsigned();// Link to the task type table
            $table->Integer('stage_id')->unsigned();// Link to the stage table

            $table->tinyInteger('multi_date');// Is this task multi date e.g for Execution the start date is used and for the others the end date is used

            $table->tinyInteger('gen_ex_applicable');// GenEx  - If the rule is applicable
            $table->tinyInteger('gen_ex_buffer_days');// GenEx  - Number of days from  Task Target Date
            $table->tinyInteger('gen_ex_hrs_perc');// GenEx  - target Hours percentage breakdown
            //$table->decimal('gen_ex_hrs_perc', 3, 2);// GenEx  - target Hours percentage breakdown

            $table->tinyInteger('rev_applicable');// Review  - If the rule is applicable
            $table->tinyInteger('rev_buffer_days');// Review - Number of days from  Task Target Date
            $table->tinyInteger('rev_hrs_perc');// Review  - target Hours percentage breakdown
            //$table->decimal('rev_hrs_perc', 3, 2);// Review  - target Hours percentage breakdown

            $table->tinyInteger('re_issu_applicable');// Re-Issue  - If the rule is applicable
            $table->tinyInteger('re_issu_buffer_days');// Re-Issue Number of days from  Task Target Date
            $table->tinyInteger('re_issu_hrs_perc');// Re-Issue  - target Hours percentage breakdown
            //$table->decimal('re_issu_hrs_perc', 3, 2);// Re-Issue  - target Hours percentage breakdown

            $table->tinyInteger('s_off_applicable');// Sign-Off  - If the rule is applicable
            $table->tinyInteger('s_off_buffer_days');// Sign-Off Number of days from  Task Target Date
            $table->tinyInteger('s_off_hrs_perc');// Sign-Off  - target Hours percentage breakdown
            //$table->decimal('sign_off_hrs_perc', 3, 2);// Sign-Off  - target Hours percentage breakdown

            $table->tinyInteger('default_hrs');// Default Hours for the Task Type

            $table->timestamps();

            $table->foreign('task_type_id')->references('id')->on('task_types');
            $table->foreign('task_group_id')->references('id')->on('groups');
            $table->foreign('stage_id')->references('id')->on('stages');
            $table->foreign('project_id')->references('id')->on('projects');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Schema::dropIfExists('tableName');
        Schema::drop('task_rules');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}

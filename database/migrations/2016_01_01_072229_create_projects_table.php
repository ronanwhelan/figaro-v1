<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');//must be unique until there is a parent tag like building-level-room etc
            $table->string('description')->nullable();
            $table->string('address')->nullable();
            $table->string('location')->nullable();
            $table->string('pm')->nullable();
            $table->string('customer')->nullable();
            $table->string('customer_contact')->nullable();
            $table->tinyInteger('status');
            $table->text('note')->nullable();
			$table->timestamps();
		});
/*
        Schema::create('project_user', function($table)
        {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('user_id');
            $table->timestamps();
        });*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('projects');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        //Schema::drop('project_user');
	}

}

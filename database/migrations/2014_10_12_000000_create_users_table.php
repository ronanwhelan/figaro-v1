<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->string('password');
            $table->Integer('company_id')->unsigned();//Link to Company
            $table->tinyInteger('role')->unsigned();
            $table->tinyInteger('punchlist_role')->default(0);
            $table->tinyInteger('punchlist_filter')->default(0);//0 = see all, 1 = see responsible only, 2= see assigned company only
            $table->tinyInteger('access_to_tracker')->default(0);//user can access tracker
            $table->tinyInteger('access_to_punchlist')->default(0);//user can access Punch list
            $table->tinyInteger('access_to_change_control')->default(0);//user can access Change Control
            $table->tinyInteger('access_to_schedule')->default(0);//user can access project section
            $table->tinyInteger('access_to_roster')->default(0);//user can access Time Sheet
            $table->tinyInteger('access_to_project')->default(0);//user can access project section
            $table->string('area',50)->default('');
            $table->string('group',50)->default('');
            $table->string('type',100)->default('');
            $table->string('stage',20)->default('');
            $table->boolean('confirmed')->default(false);
            $table->tinyInteger('password_changed');//user changed their password i.e does not need to change 0 = needs to change
            $table->dateTime('password_changed_date')->nullable();//the date the user last changed their password
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('company_list')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('users');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

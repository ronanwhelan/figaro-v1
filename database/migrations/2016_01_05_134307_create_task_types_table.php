<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_types', function(Blueprint $table)
		{
			$table->increments('id');
            $table->Integer('project_id')->unsigned();// Link to the Projects table
            $table->string('name',60);
            $table->string('short_name',25);
            $table->string('description',100);
            $table->Integer('group_id')->unsigned();
            $table->Integer('rule_type');
            $table->string('stage_add_rule',100);//when a new task is to be added this will tell the system which stage to add the task for
			$table->timestamps();
            $table->index('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('task_types');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}

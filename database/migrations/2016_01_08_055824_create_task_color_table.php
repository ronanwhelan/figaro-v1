<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskColorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_colors', function(Blueprint $table)
		{
			$table->increments('id');
            $table->String('value',50);
            $table->String('description',100);
            $table->String('bkg_color',50);
            $table->String('text_color',50);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('task_colors');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}

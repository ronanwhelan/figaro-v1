<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('areas', function(Blueprint $table)
		{
			$table->increments('id');
            $table->Integer('project_id')->unsigned();// Link to the Projects table
            $table->string('name')->default('');
            $table->string('short_name')->default('');
            $table->string('description');
			$table->timestamps();
            $table->index('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('areas');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

	}

}

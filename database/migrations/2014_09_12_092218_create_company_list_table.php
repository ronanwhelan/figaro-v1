<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_list', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('short_name')->default('');
            $table->string('description')->default('');
            $table->Integer('customer')->unsigned();// 0 = no, 1 = yes
            $table->timestamps();
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_list');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_filters', function (Blueprint $table) {

            $table->increments('id');

            $table->Integer('user_id')->unsigned();//associated user
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //Tasks
            $table->string('filter_area_ids',100)->default('');
            $table->string('filter_group_ids',100)->default('');
            $table->string('filter_type_ids',100)->default('');
            $table->string('filter_stage_ids',100)->default('');
            $table->string('filter_role_ids',100)->default('');
            $table->tinyInteger('filter_complete')->default(0);

            $table->string('search_text',100)->default('');
            $table->string('search_area_ids',100)->default('');
            $table->string('search_system_ids',200)->default('');
            $table->string('search_group_ids',100)->default('');
            $table->string('search_type_ids',100)->default('');
            $table->string('search_stage_ids',100)->default('');
            $table->string('search_times',10)->default('');
            $table->tinyInteger('search_complete')->default(0);

            $table->string('task_flag_list',500)->default('');

            //Items
            $table->string('item_filter_area_ids',100)->default('');
            $table->string('item_filter_group_ids',100)->default('');
            $table->string('item_filter_type_ids',100)->default('');
            $table->string('item_filter_stage_ids',100)->default('');
            $table->string('item_filter_company_ids',100)->default('');
            $table->tinyInteger('item_filter_complete')->default(0);

            $table->string('item_search_text',100)->default('');
            $table->string('item_search_area_ids',100)->default('');
            $table->string('item_search_system_ids',200)->default('');
            $table->string('item_search_group_ids',100)->default('');
            $table->string('item_search_type_ids',100)->default('');
            $table->string('item_search_company_ids',100)->default('');
            $table->string('item_search_times',10)->default('');
            $table->tinyInteger('item_search_complete')->default(0);

            $table->string('system_report_config',500)->default('');

            $table->string('flags',500)->default('');

            $table->smallInteger('filter_on')->default(0);// is the filter on

            $table->smallInteger('cards_default')->default(0);//if on then user wants to be direct to the cards view

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('user_filters');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}

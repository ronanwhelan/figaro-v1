<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('project_id')->unsigned();// Link to the Projects table
            $table->string('name',60);
            $table->string('short_name',10);
            $table->string('description',100);
            $table->Integer('group_id')->unsigned();
            $table->timestamps();
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_types');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskSubTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_sub_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->index('name');
            $table->string('description');
            $table->integer('task_type_id')->unsigned();//relates to a task type
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('task_sub_types');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

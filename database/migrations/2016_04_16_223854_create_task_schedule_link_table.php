<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskScheduleLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_schedule_links', function (Blueprint $table) {

            $table->increments('id');//Incremental Primary key
            $table->integer('schedule_id')->unsigned();// Link to the Schedule Dates Table -  to link up the rule to the dates
            $table->integer('task_id')->unsigned();// Link to the task type table
            $table->String('date_choice');// Which date to look at the start or finish i.e 0 or 1
            $table->integer('lag');// Lag days  + of - days
            $table->dateTime('target_date')->nullable();//the task target date
            $table->dateTime('base_date')->nullable();//the start date in the schedule
            $table->dateTime('schedule_start_date')->nullable();//the task target date
            $table->dateTime('schedule_finish_date')->nullable();//the start date in the schedule
            $table->timestamps();
            $table->unique(array('schedule_id', 'task_id'));

            // Foreign Keys
            $table->foreign('schedule_id')->references('id')->on('schedule_dates')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('task_schedule_links');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskExtensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_extensions', function (Blueprint $table) {
            $table->increments('id');

            $table->Integer('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');

            $table->dateTime('gen_complete_date');
            $table->Integer('gen_completed_by_id')->unsigned();
            $table->dateTime('rev_complete_date');
            $table->Integer('rev_completed_by_id')->unsigned();
            $table->dateTime('re_issu_complete_date');
            $table->Integer('re_issu_completed_by_id')->unsigned();
            $table->dateTime('s_off_complete_date');
            $table->Integer('s_off_completed_by_id')->unsigned();

            $table->Integer('error_code');// used to store the binary of the code errors e.g 2 = 010, 3 = 011
            $table->Integer('doc_id')->unsigned();//a link to the document table - which documents are associated with this task

            $table->Integer('who_updated_last_id')->unsigned();//Which User Updated the Task last

            $table->Integer('who_completed_id')->unsigned();//Which User Completed the task
            $table->Integer('who_created_id')->unsigned();//Which User Created the task

            $table->String('note',200)->default('');//A simple note for the task

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_extensions');
    }
}

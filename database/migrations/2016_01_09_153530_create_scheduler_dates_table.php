<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_dates', function (Blueprint $table) {

            $table->increments('id');//Incremental Primary key
            $table->String('number',80);// The Scheduler ID
            $table->index('number');//$table->unique('number');
            $table->String('description',300);// The Scheduler System Name
            $table->dateTime('start_date')->nullable();//the start date in the schedule
            $table->dateTime('finish_date')->nullable();//the start date in the schedule
            $table->String('string_start_date',50);// A place holder for the date so to parse the date correctly
            $table->String('string_finish_date',50);// A place holder for the date so to parse the date correctly
            $table->Integer('imported_data')->unsigned();// Is the data local to the app or (imported 0 = local)
            $table->timestamps();

            //$table->primary('id');
            //TODO Is this correct? Does this work as a composite key ?
            //$table->primary(['system_id','task_type_id']);//Composite Primary Key

            // Foreign Keys
            //$table->foreign('system_id')->references('id')->on('systems');
            //$table->foreign('task_type_id')->references('id')->on('task_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('schedule_dates');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}

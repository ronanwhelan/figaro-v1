<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_categories', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',60);
            $table->string('short_name',10);
            $table->string('description',100);
            $table->timestamps();
            $table->index('short_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_categories');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

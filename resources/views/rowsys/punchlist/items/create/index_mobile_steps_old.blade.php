@extends ('rowsys._app.layouts.punchlist')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">



            <div class="row">
                <div class="col-lg-12">
                    <h3>New Item <i class="fa fa-pencil-square-o"></i></h3>
                    <article class="hide">
                        <div aria-label="Justified button group" role="group" class="btn-group btn-group-justified">
                            <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Step 1&nbsp;<i class="fa fa-check"></i></a>
                            <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Step 2&nbsp;<i class="fa fa-times"></i></a>
                            <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Step 3&nbsp;<i class="fa fa-times"></i></a>
                            <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Step 4&nbsp;<i class="fa fa-times"></i></a>
                            <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Step 5&nbsp;<i class="fa fa-times"></i></a>
                        </div>
                    </article>
                </div>
            </div>

            <form action="/item/create" id="add-new-punchlist-item-form" method="post" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    {{--   LOCATION  --}}
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-parent="#accordion" data-toggle="collapse" href="#location" aria-expanded="false"
                                   aria-controls="location">
                                    <label class="label label-info">Step 1</label><i class="fa fa-map-pin"></i>&nbsp; Step & Location
                                </a>
                            </h4>
                        </div>

                        <div id="location" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body animated fadeInUp">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Stage</label>
                                            <select class=" form-control selectpicker" onchange="" data-style="btn-primary"
                                                    name="area">
                                                <option value=""></option>
                                                <option value="1">QOR</option>
                                                <option value="2">OWL</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                            <div class="panel panel-info">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" href="#area-accordion" aria-expanded="true" aria-controls="area-accordion">
                                                            System
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="area-accordion" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label for="inputEmail3" class=" control-label">Area:</label>
                                                                    <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                                            name="area">
                                                                        <option value=""></option>
                                                                        @foreach($areas as $area)
                                                                            <option value="{{$area->id}}">{{$area->name}}, {{$area->description}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label for="inputEmail3" class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> System:</label>
                                                                    <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                                            name="system">
                                                                        <option value=""></option>
                                                                        @foreach($systems as $system)
                                                                            <option value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="panel panel-info">
                                                <div class="panel-heading" role="tab" id="heading-building-accordion">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" href="#building-accordion" aria-expanded="false"
                                                           aria-controls="building-accordion">
                                                            Site
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="building-accordion" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-building-accordion">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class=" control-label">Building:</label>
                                                                    <input type="text" class="form-control" name="building" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class=" control-label">Floor:</label>
                                                                    <input type="text" class="form-control" name="floor" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class=" control-label">Room:</label>
                                                                    <input type="text" class="form-control" name="room" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class=" control-label">Other:</label>
                                                                    <input type="text" class="form-control" name="other_location" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class=" control-label">Drawing Number:</label>
                                            <input type="text" class="form-control" name="drawing_reference" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class=" control-label">Extra Info:</label>
                                            <input type="text" class="form-control" name="extra_info" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--   DETAILS  --}}

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#details" aria-expanded="true" aria-controls="details">
                                    <label class="label label-info">&nbsp;Step 2 &nbsp;</label> <i class="fa fa-pencil"></i>&nbsp; Details
                                    <span class=" hide pull-right text-info" style="font-size: 10px"> R01.T04.M21-ITEM-567</span>
                                </a>
                            </h4>
                        </div>

                        <div id="details" class="panel-collapse collapse in " role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body animated fadeInUp">

                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="form-group ">
                                            <label class=" control-label">Number:</label>
                                            <input disabled type="text" class="form-control input-lg" id="search-task-number" name="item_number" value="">
                                            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group ">
                                            <label class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Description:</label>
                                            <textarea class="longInput form-control" name="description" cols="30" rows="4"></textarea>
                                            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group ">
                                            <label class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Priority:</label>
                                            <select class=" form-control selectpicker" data-style="btn-info" name="priority">
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group ">
                                            <label class=" control-label">Action:</label>
                                            <textarea class="longInput form-control" name="action" cols="30" rows="4"></textarea>
                                            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class=" control-label">Note:</label>
                                            <textarea class="longInput form-control" name="note" cols="30" rows="2"></textarea>
                                            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                    {{--   TYPE AND CATEGORY  --}}


                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-parent="#accordion" data-toggle="collapse" href="#type" aria-expanded="false"
                                   aria-controls="type">
                                    <label class="label label-info">Step 3</label><i class="fa fa-link"></i>&nbsp; Type
                                </a>
                            </h4>
                        </div>

                        <div id="type" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body animated fadeInUp">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label">Group:</label>
                                            <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                    name="group_id">
                                                <option value=""></option>
                                                @foreach($groups as $group)
                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Type:</label>
                                            <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                    name="type">
                                                <option value=""></option>
                                                @foreach($types as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label">Category:</label>
                                            <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                    name="category">
                                                <option value=""></option>
                                                <option value="1">A</option>
                                                <option value="2">B</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--   DUE DATE   --}}

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-parent="#accordion" data-toggle="collapse" href="#due-date" aria-expanded="false"
                                   aria-controls="due-date">
                                    <label class="label label-info">Step 4</label><i class="fa fa-clock-o"></i>&nbsp;Due Date
                                </a>
                            </h4>
                        </div>

                        <div id="due-date" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body animated fadeInUp">
                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label">Schedule Date:</label>
                                            <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary"
                                                    name="schedule_choice">
                                                <option value="0"></option>
                                                <option value="1">MC</option>
                                                <option value="2">Commissioning</option>
                                                <option value="3">OQ</option>
                                                <option value="4">PQ</option>
                                            </select>
                                        </div>
                                        <p class=" text-primary">Schedule Date is associated to a System</p>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Lag (- days):</label>
                                            <input type="text" class="form-control"  name="lag_days"  value="0">
                                        </div>
                                    </div>
                                </div>

                                <h2 class="hide">OR</h2>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class=" control-label">Choose Date:</label>
                                            <div class="input-group date">
                                                <input type="date" id="due_date" name="due_date" class=" datepicker"
                                                       data-date-format="yyyy-mm-dd"
                                                       value="{{Carbon\Carbon::now()->toDateString()}}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    {{--   OWNER AND ASSIGNEE  --}}


                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-parent="#accordion" data-toggle="collapse" href="#owner" aria-expanded="false"
                                   aria-controls="owner">
                                    <label class="label label-info">Step 5</label><i class="fa fa-users"></i>&nbsp;Responsible Party
                                </a>
                            </h4>
                        </div>

                        <div id="owner" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body animated fadeInUp">
                                <h2 class="hide">Responsible</h2>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Company:</label>
                                            <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                    name="responsible_party">
                                                <option value=""></option>
                                                @foreach($companies as $company)
                                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                                @endforeach
              {{--                                  <option value="0"></option>
                                                <option value="0">ABB</option>
                                                <option value="0">ABB/KKC</option>
                                                <option value="0">ABB/Dennis</option>
                                                <option value="0">Abbvie</option>
                                                <option value="0">ALKO</option>
                                                <option value="0">ALKO / TAE</option>
                                                <option value="0"> ALPHA C</option>
                                                <option value="0">BA</option>
                                                <option value="0">BA/KKC</option>
                                                <option value="0">CHUBB</option>
                                                <option value="0">CHUBB/KKC</option>
                                                <option value="0">CHUBB/PURITAS</option>
                                                <option value="0">CHUBB/TAE</option>
                                                <option value="0">DENNI</option>
                                                <option value="0">DORTEK</option>
                                                <option value="0">JACOB/LLS</option>
                                                <option value="0">JEL</option>
                                                <option value="0">KENSTON</option>
                                                <option value="0">KENSTON/KENYON</option>
                                                <option value="0">KENYON</option>
                                                <option value="0">KKC</option>
                                                <option value="0"> KKC/KENSTON</option>
                                                <option value="0">KKC/KENYON</option>
                                                <option value="0">KKC/Lantro vision</option>
                                                <option value="0">KKC/LL</option>
                                                <option value="0">KKC/PURITAS</option>
                                                <option value="0">KKC/TAE</option>
                                                <option value="0">LL</option>
                                                <option value="0">PURITAS</option>
                                                <option value="0">PYROTECH</option>
                                                <option value="0">PYROTECH/PURITAS</option>
                                                <option value="0">TAE</option>
                                                <option value="0">TAE/KENYON</option>
                                                <option value="0">TAE/PURITAS</option>
                                                <option value="0">TAKASAGO</option>
                                                <option value="1">Lend Lease</option>
                                                <option value="2">Electrical</option>--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label"><i style="font-size: 3%" class="fa fa-asterisk text-danger"></i> Attention:</label>
                                            <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                                    name="attention">
                                                <option value=""></option>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>



                                <h2 class="hide">Contractor</h2>

                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label">Contractor:</label>
                                            <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                    name="assigned_doer_group_id">
                                                <option value="0"></option>
                                                <option value="1">Contractor 1</option>
                                                <option value="2">Contractor 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class=" control-label">Individual:</label>
                                            <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                    name="assigned_doer_user_id">
                                                <option value="0"></option>
                                                <option value="1">user 1</option>
                                                <option value="2">user 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{--   ATTACHMENTS  --}}

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-parent="#accordion" data-toggle="collapse" href="#attachment" aria-expanded="false"
                                   aria-controls="attachment">
                                    <label class="label label-info">Step 6</label><i class="fa fa-file-image-o"></i>&nbsp; Attachment
                                </a>
                            </h4>
                        </div>

                        <div id="attachment" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body animated fadeInUp">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2>Attachment</h2>
                                        <p><input type="file"  id="take-picture" name="attachment_file" accept="image/*"><i class="fa fa-camera"></i></p>
                                        <br>
                                        <p><input type="file" multiple="true" id="take-picture" name="attachment_files[]" accept="image/*"><i class="fa fa-camera"></i></p>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class=" control-label">File Name:</label>
                                                    <input type="text" class="form-control" name="attachment_tag" placeholder="Tag the file for reference">
                                                </div>
                                            </div>
                                        </div>

                                        <div id="attachment-preview-section" class="" hidden="true">
                                            <h2>Preview:</h2>
                                            <p><img src="about:blank" style="width: 100%" alt="" id="show-picture"></p>
                                        </div>
                                        <p id="error"></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- SERVER AND VALIDATION ERRORS -->
                    <div class="alert alert-info fadeIn hide" id="validation-errors"></div>


                    <button type="submit"  class=" hide btn btn-primary btn-block"><i class="fa fa-btn fa-plus"></i>&nbsp; Submit</button>

                    <button type="button" onclick="addPunchListItem('add-new-punchlist-item-form','/item/create')" class=" hide btn btn-primary btn-block"><i class="fa fa-btn fa-plus"></i>&nbsp; Submit AJZX</button>
                    <button type="button" onclick="ValidateAndSubmit('add-new-punchlist-item-form','/item/create/validate')" class="  btn btn-primary btn-block"><i class="fa fa-btn fa-plus"></i>&nbsp; Submit</button>
                </div>
            </form>
        </div>
    </div>

    @include('rowsys.punchlist.items.create.feedback_table.table')

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script>
        $(document).ready(function () {
            checkIfOnMobile();
            $('.datepicker').datepicker({
                keyboardNavigation: true,
                calendarWeeks: true,
                autoclose: true
            });

            if (screen.width <= 1300) {
                $('.datepicker').datepicker('destroy');
                //$('.selectpicker').selectpicker('mobile');
                // $('.datepicker').datepicker('destroy');
                //alert('on a mobile');
            }
        });


/*        (function () {
            var takePicture = document.querySelector("#take-picture"),
                    showPicture = document.querySelector("#show-picture");

            if (takePicture && showPicture) {
                // Set events
                takePicture.onchange = function (event) {
                    // Get a reference to the taken picture or chosen file
                    var files = event.target.files,
                            file;
                    if (files && files.length > 0) {
                        file = files[0];
                        try {
                            // Create ObjectURL
                            var imgURL = window.URL.createObjectURL(file);

                            // Set img src to ObjectURL
                            showPicture.src = imgURL;

                            // Revoke ObjectURL
                            URL.revokeObjectURL(imgURL);

                            $('#attachment-preview-section').fadeIn(1500);
                        }
                        catch (e) {
                            try {
                                // Fallback if createObjectURL is not supported
                                var fileReader = new FileReader();
                                fileReader.onload = function (event) {
                                    showPicture.src = event.target.result;
                                };
                                fileReader.readAsDataURL(file);
                            }
                            catch (e) {
                                //
                                var error = document.querySelector("#error");
                                if (error) {
                                    error.innerHTML = "Neither createObjectURL or FileReader are supported";
                                }
                            }
                        }
                    }
                };
            }
        })();*/


        /** ===============================================================
         *
         *  Add Punch List Item
         *
         *  adds the item over AJAX
         *
         * ===============================================================
         */
        function addPunchListItem(formName, urlText) {

            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var formId = '#' + formName;
            var form = $(formId);
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var formData = new FormData($(form));
            var validationSection = $('#validation-errors');
            $.ajax({
                type: 'POST',
                url: '/item/create/validate',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
               // processData: false,  // tell jQuery not to process the data
                //contentType: false,   // tell jQuery not to set contentType
                success: function (result) {
                    console.log(result);
                    $.notify({
                        title: '<strong>Success!</strong><br>',
                        message: result + ""
                    },{
                        animate: {
                            enter: 'animated fadeInLeft',
                            exit: 'animated fadeOutRight'
                        },
                        type: 'success',
                        //offset: {x: 100, y: 100},
                        //placement: {from: "bottom"},
                        showProgressbar: false,
                        delay:1000
                    });
                    setTimeout(function(){
                        //window.location.reload(true);
                    },1500);
                    validationSection.addClass('hide');
                    //getUserDetails(userId);
                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    //waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {
                    waitDone();
                    validationSection.removeClass('hide').html('<br><i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
                    var errorText = '';
                    var validationErrorText = ' Validation Errors';
                    var list = $("<ul class=''></ul>");
                    var contactAdminText = ' - Contact the PES Administrator';

                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li>' + val + '</li>';
                            list.append(validationErrorText);
                            console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });
        }

        function ValidateAndSubmit(formName,urlText){

            var validatePassed = validateForm(formName,urlText);
            if(validatePassed === 'pass'){
                console.log('validation pass');
                $( "#" + formName ).submit();
                console.log('message = ' + validatePassed);

            } else if(validatePassed === 'fail'){
                console.log('validation Fail');

            }else{
                console.log('No Internet Connection');
                alert('you dont seem to be connected to the internet');
            }


/*            var hasConnection = doesConnectionExist();
            if(hasConnection){
            }else{
                alert('you dont seem to be connected to the internet');
            }*/

        }


        function validateForm(formName,urlText){
            var returnMessage = 'fail';
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var formId = '#' + formName;
            var form = $(formId);
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');
            $.ajax({
                type: 'POST',
                url: urlText,
                data: datastring,
                async:false,
                success: function (result) {
                    console.log(result);
                    if(result === 'pass'){
                        returnMessage = 'pass';
                    }
                    validationSection.addClass('hide');
                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    //waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {

                    console.log(xhr);
                    if(xhr.status === 422){

                    }else{
                        returnMessage = '';
                    }

                    waitDone();
                    validationSection.removeClass('hide').html('<i class="fa fa-info-circle"></i><strong> Validation Errors</strong><br>');
                    var errorText = '';
                    var validationErrorText = ' Validation Errors';
                    var list = $('<ul class=""></ul>');
                    var contactAdminText = ' - Contact the PES Administrator';
                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li class="">' + val + '</li>';
                            list.append(validationErrorText);
                            //console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });
            return returnMessage;
        }




        function doesConnectionExist() {
            var xhr = new XMLHttpRequest();
            var file = "http://www.rowsys.co";
            var randomNum = Math.round(Math.random() * 10000);

            xhr.open('HEAD', file + "?rand=" + randomNum, false);

            try {
                xhr.send();

                if (xhr.status >= 200 && xhr.status < 304) {
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                return false;
            }
        }
    </script>

@stop

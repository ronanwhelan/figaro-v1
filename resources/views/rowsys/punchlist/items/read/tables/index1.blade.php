@extends ('rowsys._app.layouts.punchlist')

@section ('page_related_css')

    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    {{--   <link href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
       <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
       <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>--}}
@stop

@section ('head_js')

@stop

@section('content')
    <!-- ========================== CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Item List</h3></div>
                <div class="panel-body">
                    @include('rowsys.punchlist.items.read.tables.table2')

            </div>
        </div>
    </div>



    @include('rowsys.punchlist.items.read.details.modal')
    <!-- ========================== CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')
    <script src="/js/rowsys/tracker/tasks/general.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>

    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>

    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>



    {{--   Are these really needed????
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
       <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>--}}

    {{--
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>--}}



    <script>

        var table = $('#example');

        $(document).ready(function () {
            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";

            var responsiveHelper_example_column = undefined;

            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            table = $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy',
                    'excel'
                ],
                //"sDom":"flrtip",
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 50,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                // paging: false,
                "bStateSave": true, // saves sort state using localStorage
                //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    if (!responsiveHelper_example_column) {
                        responsiveHelper_example_column = new ResponsiveDatatablesHelper($('#example'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_example_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_example_column.respond();
                }
            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [
                    'excel'
                    //'copyHtml5',
                    //'excelHtml5',
                    //'csvHtml5',
                    //'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));


            table.buttons().container()
                    .appendTo($('.col-sm-6:eq(0)', table.table().container()));


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1500, '1');
            $('#loading-box-section').remove();

            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var exampleFilterSection = $('#example_filter');
            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md');
            exampleFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);

        });

        $('.hover-over-item-number').css( 'cursor', 'pointer' );


    </script>

@stop

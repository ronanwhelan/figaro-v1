@extends ('rowsys._app.layouts.punchlist')

@section ('page_related_css')

    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@stop

@section ('head_js')


@stop

@section('content')
    <div class="row animated fadeIn">
        <div class="col-lg-12">
            <h3 class="pull-left text-primary"><i class="fa fa-user"></i> My Items</h3>
        </div>
    </div>

    <div class="row animated fadeIn ">

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/user?selection=2"><h4 class="header-title"><span> Responsible <i class="fa fa-crosshairs pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount"><span> {{$dashBoardStats['user']['totalOpenResponsible']}} </span></h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/user?selection=3"><h4 class="header-title"><span> Assigned<i class="fa fa-rss pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['user']['totalOpenAssigned']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">

                <a class="" href="/items/dashboard/user?selection=1"><h4 class="header-title"><span> Raised <i class="fa fa-rocket pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['user']['totalOpenRaised']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>

            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/user?selection=4"><h4 class="header-title"><span>Ready to Close <i class="fa fa-hourglass-end pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['user']['totalRaisedWaitingToBeClosed']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/user?selection=5"><h4 class="header-title"><span> Urgent <i class="fa fa-warning pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['user']['totalUrgent']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

    </div><!-- end .section-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#user-more-info" aria-expanded="true" aria-controls="user-more-info">
                                More Information &nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                            </a>
                        </h4>
                    </div>

                    <div id="user-more-info" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">

                            <p><a class="" href="/items/dashboard/user?selection=6">Open Items I raised and ready to close : {{$dashBoardStats['user']['raisedByMeAndReadyToClose']}}</a></p>
                            <p><a class="" href="/items/dashboard/user?selection=7">Open Items Im responsible for and the responsible section is complete
                                : {{$dashBoardStats['user']['imResAndResSectionIsCompleteButNotClosed']}}</a></p>

                            <p><a class="" href="/items/dashboard/user?selection=8">Open Items Im responsible for and the assigned section is complete : {{$dashBoardStats['user']['imResForAndTheAssignedSectionIsComplete']}}</a></p>

                            <p><a class="" href="/items/dashboard/user?selection=9">Open Items Im Assigned are complete but the responsible section is still
                                open: {{$dashBoardStats['user']['imAssignedAndCompletedButResSectionIsOpen']}}</a></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="row animated fadeIn  ">
        <div class="col-lg-12">
            <h3 class="pull-left"><i class="fa fa-users"></i> My Teams Items</h3>
        </div>
    </div>

    <div class="row animated fadeIn  ">

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/role?selection=2"><h4 class="header-title"><span>  Responsible <i class="fa fa-crosshairs pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['role']['totalOpenResponsible']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/role?selection=3"><h4 class="header-title"><span>  Assigned <i class="fa fa-rss pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['role']['totalOpenAssigned']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/role?selection=1"><h4 class="header-title"><span>  Raised <i class="fa fa-rocket pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['role']['totalOpenRaised']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>

            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/role?selection=4"><h4 class="header-title"><span>Ready to Close <i class="fa fa-hourglass-end pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['role']['totalRaisedWaitingToBeClosed']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

        <div class="col-lg-2 col-md-6">
            <div class="card-box">
                <a class="" href="/items/dashboard/role?selection=5"><h4 class="header-title"><span> Urgent <i class="fa fa-warning pull-right"></i></span></h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['role']['totalUrgent']}}</h2>

                        <p class="text-muted"></p>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#roles-more-info" aria-expanded="true" aria-controls="roles-more-info">
                                More Information &nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                            </a>
                        </h4>
                    </div>

                    <div id="roles-more-info" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">

                            <p><a class="" href="/items/dashboard/role?selection=6">Open items the team raised and ready to close : {{$dashBoardStats['role']['raisedByTeamAndReadyToClose']}}</a></p>
                            <p><a class="" href="/items/dashboard/role?selection=7">Open items the team is responsible for and the responsible section is complete
                                : {{$dashBoardStats['role']['teamResAndResSectionIsCompleteButNotClosed']}}</a></p>

                            <p><a class="" href="/items/dashboard/role?selection=8">Open Items the team is responsible for and the assigned section is complete
                                : {{$dashBoardStats['role']['teamResForAndTheAssignedSectionIsComplete']}}</a></p>

                            <p><a class="" href="/items/dashboard/role?selection=9">Open Items the team are assigned and complete but the responsible section is still open
                                    : {{$dashBoardStats['role']['teamAssignedAndCompletedButResSectionIsOpen']}}</a></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <br>
    <div class="row animated fadeIn">
        <div class="col-lg-12">
            <h3 class="pull-left"><i class="fa fa-cubes"></i>Project Overview</h3>
        </div>
    </div>

    <div class="row animated fadeIn">

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right hide">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table">View Tasks</a></li>
                    </ul>
                </div>
                <a class="" href="/items" target="_blank"><h4 class="header-title"> Open Items</h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$dashBoardStats['project']['totalOpen']}}</h2>
                        <ul class="list-group">
                            <li>
                                <hr>
                            </li>
                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['total']}}</span>
                                Total
                            </li>
                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['totalClosed']}}</span>
                                Closed
                            </li>

                        </ul>
                        <p class="text-muted"></p>

                        <div class="text-align-center">

                            <a class="" href="/items?last-seven-days=on"><h4 class="header-title"> Last 7 Days</h4></a>
                        </div>
                        <ul class="list-group txt-color-orange">
                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['sevenTotalOpen']}}</span>
                                Opened
                            </li>
                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['sevenTotalClosed']}}</span>
                                Closed
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>
        <div class="col-md-3">
            <div class="card-box">
                <h2> Quick Links</h2>
                <article>

                    <a class="btn btn-primary " href="/items/dashboard/links?choice=1"><i class="fa fa-calendar-check-o" style="color: white"></i> Today</a><br>
                    <a class="btn btn-info " href="/items/dashboard/links?choice=2"> <i class="fa fa-calendar" style="color: white"></i> Last 7 Days</a><br>

                    <a class="btn btn-info " href="/items/dashboard/links?choice=3"> <i class="fa fa-clock-o" style="color: white"></i> Due Soon</a><br>

                    <a class="btn btn-primary " href="/items/dashboard/links?choice=4"><i class="fa fa-hourglass-end" style="color: white"></i> Ready To Close</a><br>
                    <a class="btn btn-warning " href="/items/dashboard/links?choice=5"><i class="fa fa-warning" style="color: white"></i> Urgent</a>

                </article>
            </div>
        </div>


    </div><!-- end .section-->


    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row animated fadeIn hide">

        <div class="panel panel-warning">
            <div class="panel-body">

                <div id="" class="col-lg-12">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Project</a></li>
                        <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">My Team(s)</a></li>
                        <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">My Items</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                            <div class="row animated fadeIn">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <div class="row animated fadeIn">
                                            <div class="col-md-12">
                                                <div class="card-box">
                                                    <div class="text-align-center">
                                                        <a class="" href="/items"><h4 class="header-title">Open Items</h4></a>
                                                    </div>

                                                    <h1 class="text-align-center" style="font-size: 350%"> {{$dashBoardStats['project']['totalOpen']}}</h1>

                                                    <ul class="list-group">
                                                        <li>
                                                            <hr>
                                                        </li>
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['total']}}</span>
                                                            Total (open and closed)
                                                        </li>
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['totalClosed']}}</span>
                                                            Total Closed
                                                        </li>
                                                        <li role="separator" class="divider">
                                                            <hr>
                                                        </li>
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['totalRaised']}}</span>
                                                            Total Raised
                                                        </li>
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['totalResponsible']}}</span>
                                                            Total Responsible For
                                                        </li>
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['totalAssigned']}}</span>
                                                            Total Assigned
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card-box">
                                                    <div class="text-align-center">
                                                        <i class=" fa fa-calendar-o"></i>
                                                        <a class="" href="/items?last-seven-days=on"><h4 class="header-title"> Last 7 Days</h4></a>
                                                    </div>
                                                    <ul class="list-group txt-color-orange">
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['sevenTotalOpen']}}</span>
                                                            Opened
                                                        </li>
                                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['project']['sevenTotalClosed']}}</span>
                                                            Closed
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 hide">
                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <div class="panel panel-default">
                                                <div class="panel-heading"><h3 class="panel-title">Break Down by Group</h3></div>
                                                <div class="panel-body">
                                                    <div id="myfirstchart" style=""></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel panel-default">
                                                <div class="panel-heading"><h3 class="panel-title">Total Overview</h3></div>
                                                <div class="panel-body">
                                                    <div id="bar-example" style=""></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile">
                            <div class="row animated fadeIn">

                                <div class="col-md-3">
                                    <div class="card-box">
                                        <a class="" href="/items?items-team-resp=on"><h4 class="header-title text-align-center">Raised </h4></a>

                                        <h3 class="text-align-center" style="font-size: 260%"> {{$dashBoardStats['role']['totalOpenRaised']}}</h3>
                                        <hr>
                                        <div class="text-align-center">
                                            <i class=" fa fa-calendar-o"></i>
                                            <h5 class="text-align-center txt-color-blue"> Last 7 Days</h5>
                                        </div>
                                        <ul class="list-group txt-color-orange">
                                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalOpen']}}</span>
                                                Opened
                                            </li>
                                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalClosed']}}</span>
                                                Closed
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card-box">
                                        <a class="" href="/items?items-team-resp=on"><h4 class="header-title text-align-center">Responsible </h4></a>

                                        <h3 class="text-align-center" style="font-size: 260%"> {{$dashBoardStats['role']['totalOpenResponsible']}}</h3>
                                        <hr>
                                        <div class="text-align-center">
                                            <i class=" fa fa-calendar-o"></i>
                                            <h5 class="text-align-center txt-color-blue"> Last 7 Days</h5>
                                        </div>
                                        <ul class="list-group txt-color-orange">
                                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalOpen']}}</span>
                                                Opened
                                            </li>
                                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalClosed']}}</span>
                                                Closed
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card-box">
                                        <a class="" href="/items?items-team-assigned=on"><h4 class="header-title text-align-center">Assigned </h4></a>

                                        <h3 class="text-align-center" style="font-size: 260%"> {{$dashBoardStats['role']['totalOpenResponsible']}}</h3>
                                        <hr>
                                        <div class="text-align-center">
                                            <i class=" fa fa-calendar-o"></i>
                                            <h4 class="text-align-center txt-color-blue"> Last 7 Days</h4>
                                        </div>
                                        <ul class="list-group txt-color-orange">
                                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalOpen']}}</span>
                                                Opened
                                            </li>
                                            <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalClosed']}}</span>
                                                Closed
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card-box">
                                        <a class="" href="/items?items-team-urgent=on"><h4 class="header-title text-align-center">Urgent </h4></a>

                                        <h3 class="text-align-center" style="font-size: 260%"> {{$dashBoardStats['role']['urgentCount']}}</h3>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card-box">
                                        <h4 class="header-title text-align-center">My Team List</h4>
                                        <ul class="">
                                            @foreach ($userRoles as $role)
                                                <li class="text-info"> {{$role}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card-box">
                                        <a class="" href="/items?items-team-urgent=on"><h4 class="header-title text-align-center">Waiting to be Closed: </h4></a>

                                        <h3 class="text-align-center" style="font-size: 260%"></h3>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <div class="col-md-4">
                                <div class="card-box">
                                    <a class="" href="/items?items-my-assigned=on"><h4 class="header-title text-align-center">Raised </h4></a>

                                    <h3 class="text-align-center" style="font-size: 200%"> {{$dashBoardStats['user']['myOpenItemsCount']}}</h3>
                                    <hr>
                                    <div class="text-align-center">
                                        <i class=" fa fa-calendar-o"></i>
                                        <h4 class="text-align-center txt-color-blue"> Last 7 Days</h4>
                                    </div>
                                    <ul class="list-group txt-color-orange">
                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalOpen']}}</span>
                                            Opened
                                        </li>
                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalClosed']}}</span>
                                            Closed
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card-box">
                                    <a class="" href="/items?items-my-assigned=on"><h4 class="header-title text-align-center">Responsible For </h4></a>

                                    <h3 class="text-align-center" style="font-size: 200%"> {{$dashBoardStats['user']['myOpenItemsCount']}}</h3>
                                    <hr>
                                    <div class="text-align-center">
                                        <i class=" fa fa-calendar-o"></i>
                                        <h4 class="text-align-center txt-color-blue"> Last 7 Days</h4>
                                    </div>
                                    <ul class="list-group txt-color-orange">
                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalOpen']}}</span>
                                            Opened
                                        </li>
                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalClosed']}}</span>
                                            Closed
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card-box">
                                    <a class="" href="/items?items-my-assigned=on"><h4 class="header-title text-align-center">Assigned to</h4></a>

                                    <h3 class="text-align-center" style="font-size: 200%"> {{$dashBoardStats['user']['myOpenItemsCount']}}</h3>
                                    <hr>
                                    <div class="text-align-center">
                                        <i class=" fa fa-calendar-o"></i>
                                        <h4 class="text-align-center txt-color-blue"> Last 7 Days</h4>
                                    </div>
                                    <ul class="list-group txt-color-orange">
                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalOpen']}}</span>
                                            Opened
                                        </li>
                                        <li class="list-group-item"><span class="badge">{{$dashBoardStats['role']['sevenTotalClosed']}}</span>
                                            Closed
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row animated fadeIn "></div>

    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Open Item List</div>
                <div class="panel-body">

                    <div class="table-responsive font-xs">
                        <table id="items-export" class="table table-bordered table-striped  " cellspacing="0" width="100%">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Description</th>
                                <th>Location</th>
                                <th>Type</th>
                                <th>Raised By</th>
                                <th>Due Date</th>
                                <th>Status</th>

                            </tr>
                            </thead>
                            <tbody class="font-xs">
                            @foreach($items as $item)
                                <tr>
                                    <td>{{$item->number}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{!!$item->getLocation()!!}</td>
                                    <td>{{$item->type->name}}</td>
                                    <td>{{$item->raisedByUser->name}} {{$item->raisedByUser->surname}}</td>
                                    <td>{{$item->due_date->format('d-m-Y')}}</td>
                                    <td> {!!$item->getResStatus()['label']!!}</td>

                                </tr>
                            @endforeach

                            @for($i = 0;$i < 0;$i++)
                                <tr>
                                    <td>Item {{$i}}</td>
                                    <td>Item {{$i}}</td>
                                    <td>Item {{$i}}</td>
                                    <td>Item {{$i}}</td>
                                    <td>Item {{$i}}</td>
                                    <td>Item {{$i}}</td>
                                    <td>Item {{$i}}</td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30"></h4>

            </div>
            <!-- end .card-box -->
        </div>
        <!-- end .col -->
    </div><!-- end .row -->



    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')


@stop

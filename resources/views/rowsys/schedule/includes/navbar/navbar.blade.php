<!-- Side Navigation -->
<nav class="nav-slide">
    <div class="ns-header">
        <a href="#0" class="nav-close"><i class="fa fa-times" aria-hidden="true"></i></a>
        <a href="/schedule/table" class="nav-close-logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
    </div>

    <ul>
        <li class="dashboard hide ">
            <a href="#" class="">Dashboard</a>
        </li>

        <li class="gantt">
            <a href="/schedule/table" class="">Gantt</a>
        </li>

        <li class="tables">
            <a href="/schedule/activity-summary/home" class="">Activity Summary</a>
        </li>

        <li class="areas">
            <a href="/schedule/system-link" class="">System Milestone Links</a>
        </li>

        <li class="import">
            <a href="/schedule/import" class="">Import</a>
        </li>

        <li class="tables hide">
            <a role="button" data-toggle="collapse" href="#nav1">System Milestone Links</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav1">
                <ul class="sub-nav-dropdown">
                    <li><a href="/schedule/system-link">Table</a></li>
                    <li class=""><a href="/schedule/system-link/edit">Edit</a></li>

                </ul>
            </div>
        </li>

        <li class="user visible-xs">
            <a href="javascript:showUserSettingsModal({{ Auth::user()->id }});" class="">Account Settings</a>
        </li>
        <li class="sign-out visible-xs">
            <a href="/logout" class="">Logout</a>
        </li>

        <li class="main-menu">
            <a href="/welcome" class="">Main Menu</a>
        </li>

    </ul>
</nav> <!-- .nav-slide -->
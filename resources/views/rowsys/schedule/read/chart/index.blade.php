@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
   <h2>Hello from Schedule Charts</h2>
    <div style="height: 900px;width: auto;overflow: scroll;">
        <div id="chart_div"></div>
    </div>

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop

@section ('local_scripts')

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['gantt']});
        google.charts.setOnLoadCallback(drawChart);

        var allStats = '<?php echo $data; ?>';
        var data2 = <?php echo json_encode($data );?>;
        var obj = JSON.parse(data2);
        console.log(obj[0]);

        var newData = [];
        var startDate = '';
        var finishDate = '';

        for(var i = 0;i<obj.length;i++){

            startDate = obj[i].start_date.split(" ")[0];

            console.log(startDate);
            newData.push(
                    [
                        obj[i].id.toString(),
                        obj[i].number.toString() + ' ' + obj[i].description.toString(),
                        '',
                        new Date(obj[i].start_date.split(" ")[0]),
                        new Date(obj[i].finish_date.split(" ")[0]),
                        //new Date("2016-04-08"),
                        // new Date(2016, 5, 20),
                        null,
                        45,
                        null
                    ]
            )

        }

        console.log(newData);


        var chartData = [
            ['1', 'Activity 01', '', new Date("2016-04-08"), new Date(2016, 5, 20), null, 0, null],
            ['2', 'Activity 02', '', new Date(2014, 5, 21), new Date(2014, 8, 20), null, 0, null],
            ['3', 'Activity 03', '', new Date(2014, 8, 21), new Date(2014, 11, 20), null, 0, null]

        ];
        console.log(chartData);
        var x = new Date(2014, 2, 22);
        var n = x.getMilliseconds();
        var d = new Date();
         n = d.getMilliseconds();
        console.log(d);
        console.log(n);

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Task ID');
            data.addColumn('string', 'Task Name');
            data.addColumn('string', 'Resource');
            data.addColumn('date', 'Start Date');
            data.addColumn('date', 'End Date');
            data.addColumn('number', 'Duration');
            data.addColumn('number', 'Percent Complete');
            data.addColumn('string', 'Dependencies');

            data.addRows(newData);

            var options = {
                height:900,
                width:2000,

                gantt: {
                    criticalPathEnabled: false,
                    innerGridHorizLine: {
                       //stroke: '#ffe0b2',
                        strokeWidth: 1
                    },
                    percentEnabled:false,
                    barHeight:10,
                    labelMaxWidth:300,
                    labelStyle: {
                        //fontName: Roboto2,
                        fontSize: 12,
                        color: '#757575'
                    }
                    //innerGridTrack: {fill: '#fff3e0'},
                    //innerGridDarkTrack: {fill: '#ffcc80'}
                }
            };

            var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

            chart.draw(data, options);
        }


/*
         chartData = [
            ['2014Spring', 'Spring 2014', '',
                new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null], ['2014Summer', 'Summer 2014', '',
                new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
            ['2014Autumn', 'Autumn 2014', 'autumn',
                new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
            ['2014Winter', 'Winter 2014', 'winter',
                new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
            ['2015Spring', 'Spring 2015', 'spring',
                new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
            ['2015Summer', 'Summer 2015', 'summer',
                new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
            ['2015Autumn', 'Autumn 2015', 'autumn',
                new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
            ['2015Winter', 'Winter 2015', 'winter',
                new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
            ['Football', 'Football Season', 'sports',
                new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
            ['Baseball', 'Baseball Season', 'sports',
                new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
            ['Basketball', 'Basketball Season', 'sports',
                new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
            ['Hockey', 'Hockey Season', 'sports',
                new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
        ];*/
    </script>

    <script></script>

@stop


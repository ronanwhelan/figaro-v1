@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><h1>Systems</h1></div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Add a System</h3></div>
                    <div class="panel-body">

                        @if (Session::has('message') && old('systemFormActive') === '1')
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <i class="fa fa-check">&nbsp; </i>{{ Session::get('message') }}
                            </div>
                        @endif
                        <form action="/system/new" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="">Tag</label>
                                <input type="text" class="form-control" id="" name="systemTag" value="{{old('systemTag')}}" placeholder="System Tag">
                            </div>

                            <div class="form-group">
                                <label for="">Description</label>
                                <input type="text" class="form-control" id="" name="systemDescription" value="{{old('systemDescription')}}" placeholder="Description">
                            </div>

                            <div class="form-group">
                                <label for="">Area</label>
                                <select class=" form-control selectpicker" data-style="btn-primary" data-live-search="true" name="systemArea">
                                    <option></option>
                                    @foreach ($areas as $area)
                                        <option value="{{$area->id}}">{{$area->name}}
                                            , {{$area->description}}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($errors->has() && old('systemFormActive') === '1' )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            <input type="hidden" name="systemFormActive" value="1">
                            <button type="submit" class="btn btn-success ">Add System</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">System List</div>
                    <div class="panel-body">
                        <!-- Table -->
                        <table width="100%" role="grid" id="table-list"
                               class="table table-striped table-hover table-bordered dataTable no-footer" style="opacity: .5">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Area</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($systems as $system)
                                <tr onclick="showUpdateModelModal('/get-system-details/{{$system->id}}')">

                                    <th scope="row">{{$system->tag}}</th>
                                    <td>{{$system->description}}</td>
                                    <td>{{$system->area->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('rowsys.tracker.includes.update_modal.update_modal')
    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')
    <script src="/js/rowsys/tracker/tasks/general.js"></script>

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


    <script>
        var otable = $('#table-list');
        $(document).ready(function () {
            var responsiveHelper_mission_list_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            /* COLUMN FILTER  */
            otable.DataTable({
                //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 50,
                //"bPaginate": true,
                "aaSorting": [[0, "ASC"]],
                //"aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                //responsive: true,
                // paging: false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_mission_list_column) {
                        responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#table-list'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_mission_list_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_mission_list_column.respond();
                }

            });

            // Apply the filter
            $("#table-list thead th input[type=text]").on('keyup change', function () {
                otable.column($(this).parent().index() + ':visible').search(this.value).draw();
            });


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = otable.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });
            $('#table-list').fadeTo(2000, '1');
            $('#loading-box-section').remove();
        });


    </script>

@stop

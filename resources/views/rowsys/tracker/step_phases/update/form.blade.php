<h3 class="text-align-center">{{$category->short_name}} - {{$category->name}}</h3>
<div class="row">
    <form id="update-task-type-step-phase-form" action="/tasks/step/phases/update"  method="POST">
        <input type="hidden" name="step_id" value="{{$stepPhases->id}}">
        <input type="hidden" name="step_number" value="{{$stepPhases->step_number}}">
        <div class="col-md-12">

            <h4>Step 1 (Review)</h4>
            <p class="hide"> id = {{$stepPhases->id}}</p>

            <div class="form-group">
                <label for="exampleInputEmail1">Phase 1</label>
                <select class=" form-control selectpicker" name="phase_1"
                        id="group" data-style="btn-primary">
                    @foreach ($groups as $group)
                        @if($group->id === $stepPhases->p1_owner_id )
                        <option value="{{$group->id}}" selected>{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Phase 2</label>
                <select class=" form-control selectpicker" name="phase_2"
                        id="group" data-style="btn-primary">
                    @foreach ($groups as $group)
                        @if($group->id === $stepPhases->p2_owner_id )
                            <option value="{{$group->id}}" selected>{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Phase 3</label>
                <select class=" form-control selectpicker" name="phase_3"
                        id="group" data-style="btn-primary">
                    @foreach ($groups as $group)
                        @if($group->id === $stepPhases->p3_owner_id )
                            <option value="{{$group->id}}" selected>{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Phase 4</label>
                <select class=" form-control selectpicker" name="phase_4"
                        id="group" data-style="btn-primary">
                    @foreach ($groups as $group)
                        @if($group->id === $stepPhases->p4_owner_id )
                            <option value="{{$group->id}}" selected>{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Phase 5</label>
                <select class=" form-control selectpicker" name="phase_5"
                        id="group" data-style="btn-primary">
                    @foreach ($groups as $group)
                        @if($group->id === $stepPhases->p5_owner_id )
                            <option value="{{$group->id}}" selected>{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <h2>OR</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <p>Step Broken down to <strong>{{$stepPhases->break_down_count}}</strong> steps</p>
                    <label for="group">Increment Steps </label>
                    <input type="hidden" id="step_count" name="step_count" value="{{$stepPhases->break_down_count}}">

                    <select class=" form-control selectpicker" name="phase_break_down_count"
                            id="group" data-style="btn-primary" onchange="updateStepBreakDownCount(this.value)">
                        <option value="0"></option>
                        <option value="1">1 Phases (increment by 100%)</option>
                        <option value="2">2 Phases (increment by 50%)</option>
                        <option value="4">4 Phases (increment by 25%)</option>
                        <option value="5">5 Phases (increment  by 20%)</option>
                        <option value="10">10 Phases (increment by 10%)</option>
                        <option value="20" class="hide">20 Phases (increment by 5%)</option>

                    </select>
                </div>
            </div>
        </div>
            </div>

        <div class="col-md-12 ">

            <div class="form-group">
                <br>
                <button type="button" class="btn btn-success btn-block" onclick="updateCategoryModel('update-task-type-step-phase-form', '/tasks/step/phases/update','{{$stepPhases->id}}')">Update</button>
            </div>
            <div class="alert alert-info" role="alert"> {{$taskCount}} Tasks are associated to this change</div>
        </div>

    </form>
</div>

<script>
    $( "#update-task-type-step-phase-form" ).change(function() {
        updateForm();
    });
</script>

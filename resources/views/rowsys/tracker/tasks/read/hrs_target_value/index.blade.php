@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')

@stop


@section('content')

    <!-- MAIN PANEL -->
    <div id="main" role="main">
        <!-- ==========================RIBBON ========================== -->
        <!-- Ribbon - clear local content etc -->
        <!-- MAIN CONTENT -->
        <div id="content">
            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row">

                    <!-- end widget edit box -->

                    <button class="btn btn-primary" type="submit">General</button>
                    <button class="btn btn-info" type="submit">Schedule Link</button>



                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
                         data-widget-deletebutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Task Rules </h2>
                        </header>
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>

                            <form action="{{ url('update-task-hrs') }}" method="POST" id="frm-example" name="update-parameters-forms" class="">
                                {{ csrf_field() }}
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th style="width:1%">Select All<input type="checkbox" name="select_all" class="font-xl" value="1"
                                                                                  id="example-select-all"></th>
                                            <th class="hasinput" style="width:7%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:5%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:3%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:3%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>

                                            <th class="hasinput" style="width:3%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>

                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>
                                            <th class="hasinput" style="width:2%"><input type="text" class="form-control" placeholder=""/></th>

                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th data-class="expand" class="font-sm">Task Number</th>
                                            <th data-hide="phone,tablet,computer" class="font-sm">Step info</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Task Type</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Stage</th>

                                            <th data-hide="phone,tablet,computer" class="font-xs">Target Date</th>


                                            <th data-hide="phone,tablet,computer" class="font-sm">Target Hours</th>
                                            <th data-hide="phone,tablet,computer" class="font-sm">Added Lag</th>
                                            <th data-hide="phone,tablet,computer" class="font-sm">Lock Date</th>

                                            <th data-hide="" class="font-sm">Schedule Link</th>
                                            <th data-hide="" class="font-sm">Linked Date</th>
                                            <th data-hide="" class="font-sm">Schedule Import Lag</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tasks as $task)
                                            <tr class="font-xs">
                                                <td><input type="checkbox" class="font-xl" name="id[]" value="{{$task->id}}"></td>
                                                <td id="{{$task->number}}" onclick="showUpdateModal({{$task->id}})"><strong>{{$task->number}}</strong></td>
                                                <td class="font-xs"><strong> {{$task->getTaskNextStep()}}</strong>
                                                    <small> is due by  </small>
                                                    <strong>   {{$task->next_td->toFormattedDateString()}}</strong></td>

                                                <td>{{$task->area->name}}</td>
                                                <td><strong>{{$task->system->tag}}</strong> ({{$task->system->description}})</td>
                                                <td>{{$task->group->description}} ({{$task->group->name}})</td>


                                                <td>{{$task->taskType->name}} ({{$task->taskType->short_name}})</td>
                                                <td>{{$task->stage->name}}</td>
                                                <td>{{$task->prim_date->toFormattedDateString() }}</td>
                                                <td>{{$task->target_val}}</td>
                                                <td>{{$task->lag_days}}</td>
                                                @if($task->lock_date === 1)
                                                    <td>Yes</td>
                                                @else
                                                    <td>No</td>
                                                @endif
                                                <td>{{$task->schedule_number}}</td>
                                                <td>{{$task->schedule_date_choice}}</td>
                                                <td>{{$task->schedule_lag_days}}</td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                                <!-- end widget content -->
                            </form>
                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </div>

            </div>

            <!-- ==========================CONTENT ENDS HERE ========================== -->
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->
    <!-- Head Scripts  -->
    @include('rowsys.tracker.tasks.rules.parameters.update_modal')
@stop


@section ('local_scripts')

    <!-- RULES SCRIPT -->
    <script src="/js/rowsys/tracker/tasks/rules.js"></script>

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



    <script type="text/javascript">

        var otable;
        function showIds() {
            console.log('show ids');
            var form = $('#frm-example');
            // Iterate over all checkboxes in the table
            otable.$('input[type="checkbox"]').each(function () {
                // If checkbox doesn't exist in DOM?? Change to exists
                if ($.contains(document, this)) {
                    // If checkbox is checked
                    if (this.checked) {

                        // Create a hidden element
                        $(form).append(
                                $('<input>')
                                        .attr('type', 'text')
                                        .attr('name', this.name)
                                        .val(this.value)
                        );
                    }
                }
            });
        }


        $(document).ready(function () {
            $('.select2 option').addClass('font-xs');


            $('[data-toggle="tooltip"]').tooltip();

            var responsiveHelper_datatable_fixed_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            /* END BASIC */

            /* COLUMN FILTER  */
            otable = $('#datatable_fixed_column').DataTable({
                "bFilter": true,
                "bInfo": true,
                "bLengthChange": true,
                "bAutoWidth": true,
                "bPaginate": true,
                "bStateSave": true,
                "pageLength": 100,
                'columnDefs': [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center'

                }],
                'order': [[1, 'asc']],

                //"bStateSave": true // saves sort state using localStorage
                /*            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                 "t"+
                 "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",*/
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_fixed_column) {
                        responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_datatable_fixed_column.respond();
                }

            });

            // custom toolbar
            //$("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

            // Apply the filter
            $("#datatable_fixed_column thead th input[type=text]").on('keyup change', function () {
                otable
                        .column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
            });

            // Handle click on "Select all" control
            $('#example-select-all').on('click', function () {
                // Get all rows with search applied
                var rows = otable.rows({'search': 'applied'}).nodes();
                // Check/uncheck checkboxes for all rows in the table
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });


            // Handle click on checkbox to set state of "Select all" control
            $('#datatable_fixed_column tbody').on('change', 'input[type="checkbox"]', function () {
                // If checkbox is not checked
                if (!this.checked) {
                    var el = $('#example-select-all').get(0);
                    // If "Select all" control is checked and has 'indeterminate' property
                    if (el && el.checked && ('indeterminate' in el)) {
                        // Set visual state of "Select all" control
                        // as 'indeterminate'
                        el.indeterminate = true;
                    }
                }
            });


            // Handle form submission event
            $('#frm-example').on('submit', function (e) {
                var form = this;
                // Iterate over all checkboxes in the table
                otable.$('input[type="checkbox"]').each(function () {
                    // If checkbox doesn't exist in DOM
                    if (!$.contains(document, this)) {
                        // If checkbox is checked
                        if (this.checked) {
                            // Create a hidden element
                            $(form).append(
                                    $('<input>')
                                            .attr('type', 'hidden')
                                            .attr('name', this.name)
                                            .val(this.value)
                            );
                        }
                    }
                });
            });

        });


        function showUpdateModal(id){
            $('#update-parameter-modal').modal('show');
            console.log(id);
        }
    </script>

    <!-- Your GOOGLE ANALYTICS CODE Below -->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
@stop

@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')

@stop

@section('content')
    <!-- ========================== CONTENT STARTS HERE ========================== -->
    @include('rowsys.tracker.tasks.read.details.details_modal')
    @include('rowsys.tracker.tasks.read.tables.js_table')
    <!-- ========================== CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')
    <!-- APP RELATED FUNCTIONS -->
    <script src="/js/rowsys/tracker/tasks/general.js"></script>
    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <script>

        $(document).ready(function () {

            var containerWraper = $('.main-content, .content-wrapper');
            containerWraper.attr('style', 'margin-left:5px !important');
            var taskData = '<?php echo $taskData; ?>';
            //console.log(taskData);

            console.log(JSON.parse(taskData));

            taskData = JSON.parse(taskData);
            populateTaskTableFromSearch(taskData);

            $(".main-content, .content-wrapper ").css("padding-top", "300");
            //containerWraper.attr('style', 'padding-top:5px !important');
            //$(".main-content, .content-wrapper ").css("margin-left", "300");
            $('.side-nav').addClass('hide');

            //$('.nav-trigger').addClass('hide');
            $(".nav-trigger").click(function () {
                $('.side-nav').removeClass('hide');
            });


            // $( "div, span, p.myClass" ).css( "margin-left", "10");
            //.main-content .content-wrapper
            $('.selectpicker').selectpicker({
                iconBase: 'fontawesome',
                tickIcon: 'fa fa-check'
            });
            //google.charts.setOnLoadCallback(drawChart);
            $('body').off('click.dropdown touchstart.dropdown.data-api', '.dropdown')
                    .on('click.dropdown touchstart.dropdown.data-api', '.dropdown form', function (e) {
                        e.stopPropagation()
                    })
        });

    </script>

@stop
@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')

@stop

@section('content')
    <!-- ========================== CONTENT STARTS HERE ========================== -->
    @include('rowsys.tracker.tasks.read.details.details_modal')
    @include('rowsys.tracker.tasks.read.tables.search_results.search_stats')
    @include('rowsys.tracker.tasks.read.tables.server_table')
    <!-- ========================== CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')
    <!-- APP RELATED FUNCTIONS -->
    <script src="/js/rowsys/tracker/tasks/general.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    {{-- replaced by above  <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>--}}
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <script>

        var otable = $('#mission_list');
        $(document).ready(function () {

            //refreshTaskTable();
            var responsiveHelper_mission_list_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            /* COLUMN FILTER  */
            otable =$('#mission_list').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 50,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                //responsive: true,
               // paging: false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_mission_list_column) {
                        responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#mission_list'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_mission_list_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_mission_list_column.respond();
                }

            });

            //Hiding Columns
            //otable.column( 2 ).visible( false );


            // Apply the filter
            $("#mission_list thead th input[type=text]").on('keyup change', function () {
                otable
                        .column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
            });

            //$('.task-table-row').addClass('font-xs');


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = otable.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });
            $('#table-section').fadeTo(1500,'1');
            $('#loading-box-section').remove();
        });

    </script>

@stop
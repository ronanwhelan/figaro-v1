
<!-- row -->
<div class="row" id="table-section" style="opacity: .08" >
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
        <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
             data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                <h2><strong>Mission </strong> <i>List</i></h2>

                <div class="widget-toolbar">
                    <a href="javascript:void(0);" class="btn btn-primary hide"><i class="fa fa-refresh"></i></a>
                    <a href="javascript:void(0);" class="btn btn-primary hide">Stats <i class="fa fa-bar-chart-o"></i></a>

                    <div class="btn-group">
                        <button class="btn dropdown-toggle btn-xs btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="1">Next Target Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Area</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">System</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Owner</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Category</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="18">Test Specs</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Stage</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="11">Target Hrs</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="12">Earned Hrs</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="13">Revised Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="14">Base Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="15">Base Dev Days</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="16">Schedule Link</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="17">Document Number</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <!-- widget div-->
            <div>
                <div class="widget-body no-padding">
                    <div class=" " >
                        <div id="mission_list_wrapper" class="dataTables_wrapper form-inline no-footer">
                            <table width="100%" role="grid" id="mission_list"
                                   class="display nowrap table table-striped table-hover table-bordered dataTable no-footer has-columns-hidden" cellspacing="0" width="100%">
                                <thead class="">
                                <tr class="hide">
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=" "/></th>

                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=" "/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                </tr>
                                <tr class="font-xs">
                                    <th data-class="expand" class="font-xs">Number</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Next<br>Target<br>Date</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Owner</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Category</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Stage</th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Prep/Ex<br>Status &  Date</span></th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Review<br>Status &  Date</span></th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-orange">ReIssue<br>Status &  Date</span></th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-blue">SignOff<br>Status &  Date</span></th>

                                    <th data-hide="phone,tablet,computer" class="font-xs">Target<br>Val (hrs)</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Earned<br>Val (hrs)</th>

                                    <th data-hide="phone,tablet,computer" class="font-xs">Revised<br>Date</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Date</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Dev Days</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Schedule<br>Link</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Document<br>Number</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Test<br>Spec</th>
                                </tr>
                                </thead>

                                <tbody id="mission-list-table-body" class="" >
                                @foreach( $tasks as $task)

                                    <tr id="" onclick="showEditTaskModal({{$task->id}})" class=" task-table-row" style="background-color: {{$task->getTaskNextStepBackGroundColour()['rowColour']}}">

                                        <td id="row-{{$task->id}}"><strong>{{$task->number}}</strong>
                                            <br>
                                            <span class=" text-primary">
                                                <i class="fa fa-star"></i>&nbsp;{{$task->description}}
                                            </span>
                                            <br>
                                               <span class=" text-warning">
                                                <i class="fa fa-key"></i>&nbsp; {{$task->stage->name}}
                                            </span>

                                        </td>

                                        <td id="{{$task->id}}-next-td" class="">
                                            <br>
                                            {{$task->next_td->format('d-m-Y')}}

                                        <td id="{{$task->id}}-area" class="font-xs">{{$task->area->name}}</td>

                                        <td id="{{$task->id}}-system" class="font-xs"><strong>{{$task->system->tag}}</strong> ({{$task->system->description}})</td>

                                        <td id="{{$task->id}}-group" class="font-xs">{{$task->group->name}}</td>

                                        <td id="{{$task->id}}-type" class="font-xs">{{$task->taskType->name}} ({{$task->taskType->short_name}})</td>

                                        <td id="{{$task->id}}-stage" class="font-xs">{{$task->stage->name}}</td>


                                        <td id="{{$task->id}}-gen-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['genColour']}}">
                                           <br><span class=""> {{$task->gen_td->format('d-m-Y')}} </span>
                                            <br><br><span class="">{{$task->gen_perc}} %</span> </td>


                                        <td id="{{$task->id}}-rev-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['revColour']}}">
                                            <br>{{$task->rev_td->format('d-m-Y')}} <br><br>{{$task->rev_perc}} %  </td>


                                        <td id="{{$task->id}}-re-issu-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['reIssuColour']}}">
                                            <br>{{$task->re_issu_td->format('d-m-Y')}} <br><br>{{$task->re_issu_perc}} % </td>


                                        <td id="{{$task->id}}-s-off-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['sOffColour']}}">
                                            <br>{{$task->s_off_td->format('d-m-Y')}} <br><br>{{$task->s_off_perc}} %</td>

                                        <td id="{{$task->id}}-target-val">{{$task->target_val}}</td>
                                        <td id="{{$task->id}}-earned-val">{{$task->earned_val}}</td>

                                        <td id="{{$task->id}}-rev-proj-td">{{$task->revised_projected_date->format('d-m-Y')}}</td>
                                        <td id="{{$task->id}}-base-td">{{$task->base_date->format('d-m-Y')}}</td>
                                        <td id="{{$task->id}}-base-dev-days">{{$task->base_dev_days}}</td>
                                        <td id="{{$task->id}}-schedule-link">{{$task->scheduleNumber->number}}<div class="font-xs font-xs">{{$task->scheduleNumber->description}}</div></td>
                                        <td id="{{$task->id}}-doc-num">{{$task->document_number}}</td>
                                        <td id="{{$task->id}}-doc-num">{{$task->taskSubType->name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-align-center txt-color-blue" id="loading-box-section">
    <br><br>
    <span><i class="fa fa-spinner  fa-spin fa-5x"></i><br><br>Updating Table....</span>

</div>
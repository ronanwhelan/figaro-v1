@extends ('rowsys._app.layouts.app_master')

@section ('head_js')

@stop

@section('content')
    <!-- ========================== CONTENT STARTS HERE ========================== -->
    @include('rowsys.tracker.tasks.read.details.details_modal')
    @include('rowsys.tracker.tasks.read.tables.search_results.search_stats')
    @include('rowsys.tracker.tasks.read.tables.search_results.js_table')
    <!-- ========================== CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')
    <!-- APP RELATED FUNCTIONS -->
    <script src="/js/rowsys/tracker/tasks/general.js"></script>
    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <script>

        $(document).ready(function () {
            //Get the Tasks and Pass to JS function
            var tasks = '<?php echo $tasks; ?>';
            tasks = JSON.parse(tasks);
            populateTaskTableFromSearch(tasks);

            //console.log(taskData);
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(function () {
                $('[data-toggle="popover"]').popover()
            });



            $(".main-content, .content-wrapper ").css("padding-top", "300");
            //containerWraper.attr('style', 'padding-top:5px !important');
            //$(".main-content, .content-wrapper ").css("margin-left", "300");
            $('.side-nav').addClass('hide');


            $('.selectpicker').selectpicker({
                iconBase: 'fontawesome',
                tickIcon: 'fa fa-check'
            });

            //google.charts.setOnLoadCallback(drawChart);
            $('body').off('click.dropdown touchstart.dropdown.data-api', '.dropdown')
                    .on('click.dropdown touchstart.dropdown.data-api', '.dropdown form', function (e) {
                        e.stopPropagation()
                    })
        });
    </script>

@stop
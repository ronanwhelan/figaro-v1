@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
@stop

@section ('head_js')

@stop

@section('content')
    <!-- ========================== CONTENT STARTS HERE ========================== -->
    @include('rowsys.tracker.tasks.read.details.details_modal')
    @include('rowsys.tracker.tasks.read.tables.search_results.search_stats')

    <div id="task-table-section">

    </div>
  @include('rowsys.tracker.tasks.read.tables.server_table1')
    <!-- ========================== CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')


    <script src="/js/rowsys/tracker/tasks/general.js"></script>

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>

    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>


    <script>
     /*   var listOfTaskIds = php //echo json_encode($listOfTaskIds );>;*/
    </script>

    <script>

        pleaseWait('populating table');

        var table = $('#data_table');
        $(document).ready(function () {
            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            var responsiveHelper_example_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            table = $('#data_table').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'excel'],
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 100,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                "bStateSave": true, // saves sort state using localStorage
                //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    if (!responsiveHelper_example_column) {
                        responsiveHelper_example_column = new ResponsiveDatatablesHelper($('#data_table'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_example_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_example_column.respond();
                }
            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [
                   //'excel'
                    //'copyHtml5',
                    //'excelHtml5',
                    //'csvHtml5',
                    //'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));


            table.buttons().container()
                    .appendTo($('.col-sm-6:eq(0)', table.table().container()));


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1100, '1');
            $('#loading-box-section').remove();
            setTimeout(waitDone, 1100);

            //--------------------------------------------------------
            //      TOGGLE BUTTONS
            //--------------------------------------------------------
            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var tableFilterSection = $('#data_table_filter');

            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
            //tableFilterSection.append('<div class="">Task List</div>');
            tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);
            tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
            'role="button" data-toggle="collapse" data-parent="#accordion" ' +
            'href="#search-stats" aria-expanded="true" aria-controls="search-stats">Stats ' +
            '<i class="fa fa-bar-chart-o"></i> </button>').append('&nbsp;&nbsp;');

            //--------------------------------------------------------
            //      EXPORT BUTTONS
            //--------------------------------------------------------
            tableFilterSection.append('&nbsp;&nbsp;').append(
                    '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                    'class="btn btn-primary dropdown-toggle" ' +
                    'data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false"> Export Table <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                    '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                    //'<li><a href="#">Excel</a></li> ' +
                    //'<li><a href="#">Something else here</a>' +
                    '</li> </ul> </div>').append('&nbsp;&nbsp;');

            //Gantt Chart Button
            tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs hidden" ' +
            'onclick="showTaskGanttChart()"> <i class="fa fa-tasks"></i> Gantt</button>');

            tableFilterSection.append('&nbsp;&nbsp;').append('' +
                '<a class="btn btn-primary hidden-sm hidden-xs" href="/export/csv/tasks"><i class="fa fa-download"></i> Export All</a>');

            var exportButton1 = $('#export-button-1');
            var exportButtons = $('#export-buttons');
            var copyButton = $('.buttons-copy');
            var excelButton = $('.buttons-excel');
            exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
            //exportButton2.append(excelButton);

            var tablePaginateSection = $('#data_table_paginate');
            //tablePaginateSection.css("background-color", "navy");

        });

        //Get the Owner Step Phase Stats for the Task Table Stats section
        function getOwnerStepPhaseStats() {
           $.ajax({
                type: "POST",
                data: {listOfTaskIds: listOfTaskIds},
                url: "/tasks/step/phases/owner-stats", success: function (result) {
                   // $("#step-phase-stats-owners-section").html(result);
                   console.log(result);
                }
            });
        }


        function getTaskTable(){
            console.log('get task table');
            $.ajax({
                type: "GET",
                url: "/tasks/get-table", success: function (result) {
                    $("#task-table-section").html(result).fadeIn(300);
                }
            });
        }

        $('.hover-over-item-number').css( 'cursor', 'pointer' );

    </script>

@stop

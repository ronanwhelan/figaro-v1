@extends ('rowsys._app.layouts.attachment')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

    <div class=" alert alert-success" role="alert">Task Gantt Chart

        <button type="button" class="btn btn-primary btn-sm" id="refresh-button" onclick="drawTaskGanttChart('chart_div')"><i class="fa fa-refresh"></i></button>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Task Gantt Chart</h3>

        </div>
        <div class="panel-body" id="chart-section" style="overflow: scroll">

                <div id="chart_div">

                    <h1 class="text-align-center"> Building Graph..
                        <br><i class="fa fa-spinner fa-5x fa-spin"></i></h1><div class="font-lg"></div>


                </div>

        </div>
    </div>



@stop

@section ('local_scripts')




    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        var rawTaskData = <?php echo json_encode($tasks );?>;

        google.charts.load('current', {'packages': ['gantt']});
        google.charts.setOnLoadCallback(drawChart);

        var options = {};
        var screenWidth = 0;
        var screenHeight = 0;
        var chartHeight = 0;
        var serverChartData = [];
        var data = [];
        var activityCount = 0;

        function drawTaskGanttChart(chartDiv) {
            var serverDataParsed = '';
            var step1Applicable = 0;
            var step2Applicable = 0;
            var step3Applicable = 0;
            var step4Applicable = 0;
            var startDate = '';
            serverDataParsed = JSON.parse(window.sessionStorage.getItem('chartData'));
            //build the array with the data from the server
            console.log(serverDataParsed);
            var dependOn = '';
            for (var i = 0; i < serverDataParsed.length; i++) {
                if (i === 200) {
                    break;
                }
                step1Applicable = serverDataParsed[i].gen_applicable;
                step2Applicable = serverDataParsed[i].gen_applicable;
                step3Applicable = serverDataParsed[i].gen_applicable;
                step4Applicable = serverDataParsed[i].gen_applicable;

                if (step1Applicable === 1) {
                    startDate = serverDataParsed[i].gen_td.split(" ")[0];
                }
                else if (step2Applicable === 1) {
                    startDate = serverDataParsed[i].rev_td.split(" ")[0];
                }
                else if (step3Applicable === 1) {
                    startDate = serverDataParsed[i].re_issu_td.split(" ")[0];
                }
                else if (step4Applicable === 1) {
                    startDate = serverDataParsed[i].s_off_td.split(" ")[0];
                }
                dependOn = serverDataParsed[i].group.name.toString();
                serverChartData.push(
                        [
                            serverDataParsed[i].id.toString(),
                            serverDataParsed[i].number.toString(),
                            //serverDataParsed[i].system.tag.toString() + ', ' + serverDataParsed[i].stage.name.toString() + ', ' + serverDataParsed[i].task_type.short_name.toString(),
                            dependOn,//serverDataParsed[i].stage_id.toString(),
                            new Date(startDate),
                            //null,
                            new Date(serverDataParsed[i].last_td.split(" ")[0]),
                            hoursToDays(serverDataParsed[i].target_val),
                            serverDataParsed[i].status,
                            null
                        ]
                );
                activityCount++;
            }

            //console.log(serverChartData);

            //activityCount = activityCount + serverChartData.length;
            screenWidth = $(document).width() - 100;
            screenHeight = $(document).height() + 200;
            //console.log(activityCount);
            chartHeight = (activityCount * 35) + 300;//each activity row is about 35 pixels high
            $('#chart-section').height(chartHeight + 150).width(screenWidth + 50);
            options = {
                height: chartHeight,
                width: screenWidth,
                gantt: {
                    'legend': 'left',
                    //defaultStartDateMillis: new Date(2015, 3, 28),
                    percentEnabled: true,
                    barHeight: 9,
                    labelMaxWidth: 400,
                    labelStyle: {
                        //fontName: Roboto2,
                        fontSize: 12,
                        color: '#757575'
                    },
                    percentStyle: {fill: '#fff3e0'},
                    criticalPathEnabled: false,
                    criticalPathStyle: {
                        stroke: '#e64a19',
                        strokeWidth: 5
                    }
                    //innerGridDarkTrack: {fill: '#ffcc80'}
                }
            };
            drawChart();
            var chart = new google.visualization.Gantt(document.getElementById(chartDiv));
            chart.clearChart();
            chart.draw(data, options);

        }
        function drawChart() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Task ID');
            data.addColumn('string', 'Task Name');
            data.addColumn('string', 'Owner');
            data.addColumn('date', 'Start Date');
            data.addColumn('date', 'End Date');
            data.addColumn('number', 'Duration');
            data.addColumn('number', 'Percent Complete');
            data.addColumn('string', 'Dependencies');
            data.addRows(serverChartData);
        }


        //Functions to set the date
        function toMilliseconds(minutes) {
            return minutes * 60 * 1000;
        }
        function daysToMilliseconds(days) {
            return days * 24 * 60 * 60 * 1000;
        }
        function hoursToDays(hours) {
            var days = hours / 8;
            if (days < 0) {
                return daysToMilliseconds(1);
            } else {
                return daysToMilliseconds(days);
            }
        }

    </script>

    <script type="text/javascript">

        $(window).load(function () {
            var chartData = window.sessionStorage.getItem('chartData');
            console.log('window loaded');

            setTimeout(function () {
                drawTaskGanttChart('chart_div');
            }, 1500);


        });
    </script>

@stop


@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    @include('rowsys.tracker.tasks.read.details.details_modal')

    <div class="row">
        <div class="col-lg-12">
            @include('rowsys.tracker.tasks.read.tables.search_results.search_stats')
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon3">Search</span>
            <input id="search" type="text" class="form-control input-lg" aria-describedby="basic-addon3">
        </div>
    </div>
    <div class="row">
        <div id="col-lg-12">
            @foreach($tasks as $task)
                <div class="col-lg-12">
                    <div class="task-type-{{$task->taskType->id}}" data-myorder="{{$task->id}}">
                        <div class="">
                            <div class="card-box card-search-class">
                                <div class="progress progress-bar-danger-alt progress-sm">
                                    <div style="width: 100%; background-color: {{$task->getTaskNextStepBackGroundColour()['nextTdColour']}}"
                                         aria-valuemax="100"
                                         aria-valuemin="0"
                                         aria-valuenow="50"
                                         role="progressbar"
                                         class="progress-bar">
                                        @if($task->complete === 1)
                                            Complete
                                        @else
                                            {{$task->getTaskNextStep()}} is due {{$task->next_td->toFormattedDateString()}}
                                        @endif
                                        <span class="sr-only"></span>
                                    </div>
                                </div>
                                <a onclick="showEditTaskModal({{$task->id}})">

                                    <div class="task-search-class strong font-lg "><i class="fa fa-star"></i>&nbsp; {{$task->number}}</div></a>


                                <span class=" text-primary">
                                                <i class="fa fa-star-half-o"></i>&nbsp;{{$task->stage->description}} of {{$task->taskType->name}} for the
                                                {{$task->system->description}} system
                                            </span>

                                    @if($task->complete === 1)
                                        <div class="txt-color-green "><strong> COMPLETE <i class="fa fa-check"></i></strong></div>
                                    @else
                                        <div class=""><i class="fa fa-key"></i>
                                            <strong> {{$task->getTaskNextStep()}}</strong>
                                            <small> is due on</small>
                                            <strong>   {{$task->next_td->toFormattedDateString()}}</strong></div>
                                    @endif

                                    <div class="txt-color-blue task-search-class"><i class="fa fa-bookmark"></i>&nbsp;
                                        {{$task->system->description}}  in the <span class="text-info"><i class="fa fa-cubes"></i>&nbsp; {{$task->area->description}} Area</span>
                                    </div>
                                    <h5>
                                        <i class="fa fa-thumb-tack"></i>&nbsp; <span class="task-search-class label label-primary">{{$task->stage->description}} Stage</span>&nbsp;&nbsp;&nbsp;
                                    </h5>

                                    <h6>
                                        <i class="fa fa-tint"></i>&nbsp;<span class=" task-search-class txt-color-orange">{{$task->group->description}}
                                            : {{$task->taskType->name}}</span>
                                    </h6>

                                    <h5>
                                        <i class="fa fa-tag"></i>&nbsp; <span class="task-search-class  ">{{$task->tag}}</span>
                                    </h5>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                               href="#panel-collapse-1-{{$task->id}}"
                                                               aria-expanded="true"
                                                               aria-controls="panel-collapse-1-{{$task->id}}">
                                                                <i class="fa fa-calendar-o"></i> Step Status and Target Dates
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="panel-collapse-1-{{$task->id}}" class="panel-collapse collapse in " role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <div class="row">

                                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                                    <div class="well well-sm text-align-center"
                                                                         style="background-color: {{$task->getTaskNextStepBackGroundColour()['genColour']}}">
                                                                        <div class="txt-color-white font-xs">Prep/Exe Step<span class="semi-bold"></span></div>
                                                                        <div class="font-xs">{{$task->gen_perc}} %</div>
                                                                        <div class="font-xs">{{$task->gen_td->format('d-m-Y')}}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                                    <div class="well well-sm text-align-center"
                                                                         style="background-color: {{$task->getTaskNextStepBackGroundColour()['revColour']}}">
                                                                        <div class="txt-color-white font-xs">Review Step<span class="semi-bold"></span></div>
                                                                        <div class="font-xs">{{$task->rev_perc}} %</div>
                                                                        <div class="font-xs">{{$task->rev_td->format('d-m-Y')}}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                                    <div class="well well-sm text-align-center"
                                                                         style="background-color: {{$task->getTaskNextStepBackGroundColour()['reIssuColour']}}">
                                                                        <div class="txt-color-white font-xs">Re-Issue Step<span class="semi-bold"></span></div>
                                                                        <div class="font-xs">{{$task->re_issu_perc}} %</div>
                                                                        <div class="font-xs">{{$task->re_issu_td->format('d-m-Y')}}</div>

                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                                    <div class="well well-sm text-align-center"
                                                                         style="background-color: {{$task->getTaskNextStepBackGroundColour()['sOffColour']}}">
                                                                        <div class="txt-color-white font-xs">Sign Off Step<span class="semi-bold"></span></div>
                                                                        <div class="font-xs"> {{$task->s_off_perc}} %</div>
                                                                        <div class="font-xs">{{$task->s_off_td->format('d-m-Y')}}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title">
                                                            <a class="collapsed"
                                                               role="button"
                                                               data-toggle="collapse"
                                                               data-parent="#accordion"
                                                               href="#panel-collapse-2-{{$task->id}}"
                                                               aria-expanded="false"
                                                               aria-controls="panel-collapse-2-{{$task->id}}">
                                                                <i class="fa fa-info-circle"></i> More...
                                                            </a>
                                                        </h4>
                                                    </div>

                                                    <div id="panel-collapse-2-{{$task->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                            <h2>Hours Data</h2>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>



@endsection


@section ('local_scripts')
    <script src="/js/rowsys/tracker/tasks/general.js"></script>
    <script>

        $(document).ready(function () {
            //$('#Container').mixItUp();
        });

        // FILTER ORIGIN
        function filterOriginBoxes(tagName) {
            var filter_array = new Array();
            var filter = tagName.toLowerCase();  // no need to call jQuery here

            filter_array = filter.split(' '); // split the user input at the spaces

            var arrayLength = filter_array.length; // Get the length of the filter array

            $('.card-search-class').each(function () {
                /* cache a reference to the current .media (you're using it twice) */
                var _this = $(this);
                var tag = _this.find('a').first().text().toLowerCase();
                var number = _this.find('.task-search-class').text().toLowerCase();
                tag = tag + ' ' + number;

                var hidden = 0; // Set a flag to see if a div was hidden

                // Loop through all the words in the array and hide the div if found
                for (var i = 0; i < arrayLength; i++) {
                    if (tag.indexOf(filter_array[i]) < 0) {
                        _this.hide();
                        hidden = 1;
                    }
                }
                // If the flag hasn't been tripped show the div
                if (hidden == 0) {
                    _this.show();
                }
            });
        }


        $('#search').keyup(function () {
            var filter = this.value.toLowerCase();  // no need to call jQuery here
            filterOriginBoxes(filter);
        });

    </script>

@stop

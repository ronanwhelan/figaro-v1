@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')
@stop

@section('content')

            <!-- ==========================CONTENT STARTS HERE ========================== -->
            @include('rowsys.tracker.tasks.read.details.details_modal')


            <div class="row">
                <div class="col-sm-12">
                    <div class="well-white">
                        <table class="table table-striped table-forum">
                            <thead>
                            <tr>
                                <th colspan="2">Due Within A Month</th>
                                <th class="text-center hidden-xs hidden-sm" style="width: 150px;">Due Date</th>
                                <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Target Hrs</th>
                                <th class="hidden-xs hidden-sm" style="width: 200px;">Target Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tbody>

                            @foreach($tasks as $task)

                                <!-- TR -->
                                <tr>

                                    <!-- Badge and Label Column -->
                                    <td class="text-center" style="width: 100px;">
                                        <span class="label label-primary font-sm">{{$task->stage->name}}</span>
                                        <br><br>
                                        <span class="badge bg-color-orange font-md">{{$task->taskType->short_name}}</span>
                                        <br><br>

                                        <div class="font-xs">
                                            @if($task->complete === 1)
                                                <span class="badge bg-color-greenLight"><i class="fa fa-check-square-o fa-2x"></i></span>
                                            @else
                                                <span class="badge font-md ">{{$task->getTaskCompletePercentage()}}</span>

                                            @endif
                                        </div>
                                    </td>

                                    <!-- Task Details Column -->
                                    <td onclick="showEditTaskModal({{$task->id}})">

                                        <div class="txt-color-blue font-lg "><strong>{{$task->number}}</strong>
                                        </div>
                                        <div class="txt-color-blue font-md ">
                                            <small>{{$task->system->description}}</small>
                                        </div>

                                        @if($task->complete === 1)
                                            <h6 class="txt-color-green "><strong> COMPLETE</strong></h6>
                                        @else
                                            <h6 class="txt-color-green "><strong> {{$task->getTaskNextStep()}}</strong>
                                                <small> is due by  </small>
                                                <strong>   {{$task->next_td->toFormattedDateString()}}</strong></h6>
                                        @endif

                                        <p class=" hide font-xs txt-color-orange">{{$task->taskType->name}}</p>

                                        <p class="font-xs txt-color-orange">Hours Used : {{$task->earned_val}} ({{$task->getTaskHoursPercentage()}})</p>

                                        <div class="">
                                            <table class=" hide table table-condensed no-border font-xs">

                                                <tbody class="text-align-center ">
                                                <tr>
                                                    @if($task->stage_id === 1)
                                                        <td><span class="label label-info font-xs">Gen</span></td>
                                                    @else
                                                        <td><span class="label label-info font-xs">Exe</span></td>
                                                    @endif

                                                    <td><span class="label label-info font-xs">Rev</span></td>
                                                    <td><span class="label label-info font-xs">Re Iss</span></td>
                                                    <td><span class="label label-info font-xs">S Off</span></td>

                                                </tr>
                                                <tr>
                                                    <td>{{$task->gen_perc}}%<br>{{$task->gen_td->toFormattedDateString()}}</td>
                                                    <td>{{$task->rev_perc}}%<br> {{$task->rev_td->toFormattedDateString()}}</td>
                                                    <td>{{$task->re_issu_perc}}%<br>{{$task->re_issu_td->toFormattedDateString()}}</td>
                                                    <td>{{$task->s_off_perc}}%<br>{{$task->s_off_td->toFormattedDateString()}}</td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>

                                    </td>

                                    <td class="text-center hidden-xs hidden-sm">
                                        <i>{{$task->prim_date->toFormattedDateString()}}</i>
                                    </td>

                                    <td class="text-center hidden-xs hidden-sm">
                                        <small><i>{{$task->target_val}}</i></small>
                                    </td>

                                    <td class="hidden-xs hidden-sm">by
                                        <a href="javascript:void(0);">John Doe</a>
                                        <br>
                                        <small><i>January 1, 2014</i></small>
                                    </td>

                                </tr>
                                <!-- end TR -->
                            @endforeach
                            </tbody>
                        </table>


                        <table class="table table-striped table-forum"></table>


                        <table class="table table-striped table-forum"></table>


                    </div>
                </div>

            </div>

            <!-- end row -->


            <!-- ==========================CONTENT ENDS HERE ========================== -->


@stop


@section ('local_scripts')
    <!-- APP RELATED FUNCTIONS -->
    <script src="/js/tracker/app/app.functions.js"></script>
    <script src="/js/tracker/tasks/general.js"></script>
    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="/smartadmin/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <script>

        $(document).ajaxStart(function () {
            // pleaseWait();
        });
        $(document).ajaxStop(function () {
            // waitDone();
            //refreshTaskTable();

        });

        $(document).ready(function () {
            pageSetUp();
            runAllForms();

        })

    </script>


@stop
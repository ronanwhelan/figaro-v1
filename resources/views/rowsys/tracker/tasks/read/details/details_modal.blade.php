<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="single-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close btn-lg" aria-hidden="true" onclick="hideUpdateTaskModal()"><i class="fa fa-times">&nbsp;</i></button>
            <div class="modal-header">
                <h4 class=" hide modal-title text-align-center" id="title-task-name">Updating...</h4>
                <h5 class="modal-title text-align-center hidden-xs hide" id="title-task-description"></h5>
                <div class="modal-body">

                    <div class="row">
                        <div id="myTabs" class="col-lg-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-tasks"></i> <span class="hidden-xs"> Status</span></a></li>
                                <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-newspaper-o "></i> <span class="hidden-xs"> Overview</span></a></li>
                                <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i><span class="hidden-xs"> Update</span></a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div id ="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <div class="panel panel-default">

                                        <div id="collapseThree" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div id="update-task-form-section">
                                                    <i class="fa fa-lg fa-spinner fa-spin fa-3x text-align-center"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">

                                    <div class="panel panel-default">
                                            <div class="panel-body no-padding">
                                                <div id="task-details-section">
                                                    <i class="fa fa-lg fa-spinner fa-spin fa-3x"></i>
                                                </div>
                                            </div>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <div class="panel panel-default">
                                        <div class="panel-body no-padding">
                                            <div id="task-owner-section">
                                                <i class="fa fa-lg fa-spinner fa-spin fa-3x"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="accordion" class="panel-group accordion-default">

                    </div>
                </div>



                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="hideUpdateTaskModal()">Close</button>
                </div>

            </div><!-- /.modal-header -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


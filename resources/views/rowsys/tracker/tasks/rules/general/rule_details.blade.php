<div class="row">


    <div class="col-md-12">
        <form class="form" role="form" id="update-task-rule-form" method="POST">


            <div class="">Update the <strong>{{$taskRule->taskType->name}}</strong> Rule for the <strong>{{$taskRule->stage->description}}</strong> Stage</div>
            <br>

            <legend>
                Status
            </legend>
            <input type="hidden" id="edit-task-rule-id" name="edit-task-rule-id" value="{{$taskRule->id}}">
            <fieldset>
                <div class="font-xs txt-color-blue"><strong>Yes</strong> (Applicable) OR <strong>N/A </strong>(Not Applicable)</div>
                <br>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Gen/Ex</label>
                            <select class="form-control" id="gen-applicable" name="gen_ex_applicable">
                                <option value="0">N/A</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>

                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Review</label>
                            <select class="form-control" id="rev-applicable" name="review_applicable">
                                <option value="0">N/A</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>

                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Update</label>
                            <select class="form-control" id="re-issu-applicable" name="re_issue_applicable">
                                <option value="0">N/A</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Sign-Off</label>
                            <select class="form-control" id="s-off-applicable" name="sign_off_applicable">
                                <option value="0">N/A</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>
            <br>
            <legend>
                Target Date Buffer Days
            </legend>

            <fieldset>
                <div class="font-xs txt-color-blue">Number of Days from Target Date</div>
                <br>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Generation</label>
                            <input type="number" class="form-control" id="gen-days-buffer"
                                   name="gen_ex_buffer_days" value="{{$taskRule->gen_ex_buffer_days}}"/>
                        </div>

                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Review</label>
                            <input type="number" class="form-control" id="rev-days-buffer"
                                   name="review_buffer_days" value="{{$taskRule->rev_buffer_days}}"/>
                        </div>

                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Update</label>
                            <input type="number" class="form-control" id="re-issu-days-buffer"
                                   name="re_issue_buffer_days"
                                   value="{{$taskRule->re_issu_buffer_days}}"/>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Sign-Off</label>
                            <input type="number" class="form-control" id="s-off-days-buffer"
                                   name="s_off_buffer_days"
                                   value="{{$taskRule->s_off_buffer_days}}"/>
                        </div>
                    </div>

                    <!-- Days Buffer Error Message -->
                    <div class="alert alert-info fadeIn hide " id="hrs-error-message">
                        Value Errors! Please Check the values
                    </div>
                </div>
            </fieldset>

            <br>
            <legend>
                Hours
            </legend>

            <fieldset>
                <div class="font-xs txt-color-blue">% Value OR N/A</div>
                <br>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Generation</label>
                            <select class="form-control" id="gen-hrs" name="gen_hrs">
                                @for ($i = 0; $i < 105; $i=$i+5)
                                    <option value="{{$i}}">{{$i}}%</option>
                                @endfor
                            </select>
                        </div>

                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Review</label>
                            <select class="form-control" id="rev-hrs" name="rev_hrs">
                                @for ($i = 0; $i < 105; $i=$i+5)
                                    <option value="{{$i}}">{{$i}}%</option>
                                @endfor
                            </select>
                        </div>

                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Update</label>
                            <select class="form-control" id="re-issu-hrs" name="re_issu_hrs">
                                @for ($i = 0; $i < 105; $i=$i+5)
                                    <option value="{{$i}}">{{$i}}%</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Sign-Off</label>
                            <select class="form-control" id="s-off-hrs" name="s_off_hrs">
                                @for ($i = 0; $i < 105; $i=$i+5)
                                    <option value="{{$i}}">{{$i}}%</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <br>

                    <div id="totalPercentWarning" class="alert alert-info fade in hide">
                        <i class="fa-fw fa fa-info"></i>
                        <strong>Info!</strong> All Applicable Steps must add up to make 100%
                        <br><br>

                        <div class="alert-info font-md" id="totalPercValue"></div>
                    </div>
                </div>
            </fieldset>


            @if($showMultiDate)
                <fieldset>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label class="control-label">Multi Date Selection is {{$multiDateText}}</label>
                                <select class="form-control" id="multi-date" name="multi_date">
                                    <option value="0">Off</option>
                                    @if($taskRule->multi_date)
                                        <option value="1" selected>On</option>
                                    @else
                                        <option value="1" >On</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <br>
            @endif

            <input type="hidden" id="task-rule-count" name="task_rule_count" value="{{$taskCount}}">

            <h3 class="text-align-right">
                <small>Updating this rule will effect</small>
                <strong>{{$taskCount}}</strong>
                <small> Tasks</small>
            </h3>

        </form>
    </div>
</div>
@if($taskCount === 0 && Auth::user()->role > 7 )

    <div class="row">
        <div class="col-md-12">

            <form class="form" action="/delete-rule/{{$taskRule->id}}" method="GET">

                <input type="hidden" name="delete-rule-id" value="{{$taskRule->id}}">

                <button type="submit" class="btn btn-danger btn-sm">Delete Rule</button>

            </form>
        </div>
    </div>


@endif
<script>

    // Run Validate Function when the form is updated
    $('#update-task-rule-form').on('keyup change', 'input, select, textarea', function () {
        $('#save-task-rule-btn').attr('disabled', true);
        // validateUpdateRulesForm();
        var validationPassed = validateUpdateRulesForm();
        if (validationPassed === true) {
            $('#save-task-rule-btn').attr('disabled', false);
        }
    });

    var taskRule = '<?php echo $taskRule; ?>';
</script>
@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')
@stop

@section('content')



        <!-- ==========================CONTENT STARTS HERE ========================== -->
        @include('rowsys.tracker.tasks.rules.general.modals.edit_gen_rule_modal')
        @include('rowsys.tracker.tasks.rules.general.table')
        <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <!-- RULES SCRIPT -->
    <script src="/js/rowsys/tracker/tasks/rules.js"></script>

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

    <script>

        $(document).ready(function() {

            $('.select2 option').addClass('font-xs');


            $('[data-toggle="tooltip"]').tooltip();

            var responsiveHelper_datatable_fixed_column = undefined;
            var breakpointDefinition = {
                computer : 2000,
                tablet : 1024,
                phone : 480
            };

            /* END BASIC */

            /* COLUMN FILTER  */
            var otable = $('#datatable_fixed_column').DataTable({
                //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                "aaSorting": [[ 3, "asc" ]],
                fixedHeader: true,
                "pageLength": 25,
                "bPaginate": true,
                "bStateSave": true, // saves sort state using localStorage
                /*            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                 "t"+
                 "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",*/
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_fixed_column) {
                        responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_fixed_column.respond();
                }

            });

            // custom toolbar
            //$("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

            // Apply the filter
            $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
                otable
                        .column( $(this).parent().index()+':visible' )
                        .search( this.value )
                        .draw();
            } );

        })

    </script>

@stop

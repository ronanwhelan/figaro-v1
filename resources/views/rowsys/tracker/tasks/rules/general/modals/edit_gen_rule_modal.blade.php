<!-- Modal -->
<div class="modal fade " id="task-rule-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>

                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">


                <div id="task-rule-details">

                    <div class="text-align-center">
                        <i class="fa fa-spinner fa-spin fa-4x"></i>Updating...
                    </div>
                    <!--  this section is populated by AJAX - imports a View-->

                </div>

                <!-- SERVER AND VALIDATION ERRORS -->
                <div  class="alert alert-info fadeIn hide "  id="validation-errors">
                    Form has Validation Errors! Please Check again
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
                <button type="button" id="save-task-rule-btn" class="btn btn-success" disabled onclick="updateTaskRule()">
                    Save
                </button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

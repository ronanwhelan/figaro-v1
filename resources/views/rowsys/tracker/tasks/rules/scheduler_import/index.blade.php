@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group hide">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Search</span>
                            <input id="search" type="text" class="form-control input-lg" aria-describedby="basic-addon3">
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3>Schedule Dates</h3></div>
                        <div class="panel-body"></div>

                        <!-- Table -->
                        <table width="100%" role="grid" id="table-list"
                               class="table table-striped table-hover table-bordered dataTable no-footer" style="opacity: .5" >
                            <thead>
                            <tr class="">
                                <th>Activity</th>
                                <th>Description</th>
                                <th>Start Date</th>
                                <th>Finish Date</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($scheduleDates as $scheduleDate)
                            <tr>
                                <th scope="row">{{$scheduleDate->number}}</th>
                                <td>{{$scheduleDate->description}}</td>
                                <td>{{$scheduleDate->start_date->format('d-m-Y')}}</td>
                                <td>{{$scheduleDate->finish_date->format('d-m-Y')}}</td>
                            </tr>

                            @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')
    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


    <script>
        var otable = $('#table-list');
        $(document).ready(function () {
            var responsiveHelper_mission_list_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            /* COLUMN FILTER  */
            otable.DataTable({
                //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 50,
                //"bPaginate": true,
                "aaSorting": [[ 0, "ASC" ]],
                //"aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                //responsive: true,
                // paging: false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_mission_list_column) {
                        responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#table-list'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_mission_list_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_mission_list_column.respond();
                }

            });

            // Apply the filter
            $("#table-list thead th input[type=text]").on('keyup change', function () {
                otable.column($(this).parent().index() + ':visible').search(this.value).draw();
            });


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = otable.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });
            $('#table-list').fadeTo(2000,'1');
            $('#loading-box-section').remove();
        });


    </script>


@stop

@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->


    <div class="panel panel-info">
        <div class="panel-body">

            <form action="/tasks/charts/area/weekly/search" id="get-area-charts-form" method="GET">

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Responsible Team</label>
                        <select class="form-control selectpicker"
                                id="role" name="teams[]" multiple
                                data-style="btn-primary" title="All Teams">
                            <optgroup label="All Teams">
                                @foreach ($areas as $area)
                                    <option value="{{$area->id}}">{{$area->name}}</option>
                                @endforeach
                            </optgroup>
                        </select>

                    </div>
                </div>

                <div class=" col-md-2">
                    <div class="form-group">
                        <label class=" control-label">From Month:</label>
                        <input type="hidden" id="old_search_date" value="">
                        <div class="input-group date">
                            <input type="text" id="from_date" onchange="" name="from_date" class=" datepicker"
                                   data-type="date" data-date-format="yyyy-mm-dd" value="">
                        </div>
                    </div>
                </div>

                <div class=" col-md-2">
                    <div class="form-group">
                        <label class=" control-label">To Month:</label>
                        <input type="hidden" id="old_search_date" value="">
                        <div class="input-group date">
                            <input type="text" id="to_date" onchange="" name="to_date" class=" datepicker"
                                   data-type="date" data-date-format="yyyy-mm-dd" value="">
                        </div>
                    </div>
                </div>

                <div class=" col-md-2">
                    <div class="form-group"><br>
                        <button type="button" onclick="getChartData()" class="  btn btn-success btn-block"><i class="fa fa-bar-chart "></i> Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="col-md-12">
        <div class=" alert alert-info" role="alert">
            <p id="reportDetails"><i class="fa fa-info"></i> Report for All teams from Jun 2016 to Feb 2017</p>
        </div>
    </div>

    <br>

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"> Accumulative</h3></div>
            <div class="panel-body">

                <div style="width: 100%;">
                    <canvas id="chart-2"></canvas>
                </div>
                <div class="row ">
                    <div class=" col-md-1">
                        <div class="form-group">
                            <label class=" control-label" for="chart-axis-selection">Chart Axis</label>
                            <select class="form-control selectpicker btn-block"
                                    id="chart-axis-selection"
                                    name="chart_axis_selection"
                                    data-style="btn-primary"
                                    onchange="switchSChartAxis(this.value)">
                                <option value="1">Hours</option>
                                <option value="2">%</option>
                            </select>

                        </div>
                    </div>


                </div>

                <div class="hide alert alert-info" role="alert">
                    <p>
                        <small><i class="fa fa-info"></i> This is a lovely Chart</small>
                    </p>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Chart Data Table</div>
                            <!-- Table -->
                            <div class="table-responsive">
                                <table id="report-table" class="table table-bordered table-responsive">
                                    <tbody style="font-size: 10px">
                                    <tr id="col-1">
                                        <td>Date:</td>
                                    </tr>
                                    <tr id="col-2">
                                        <td>Prep:</td>
                                    </tr>
                                    <tr id="col-3">
                                        <td>Prep:</td>
                                    </tr>
                                    <tr id="col-4">
                                        <td>Exec:</td>
                                    </tr>
                                    <tr id="col-5">
                                        <td>Roster:</td>
                                    </tr>
                                    <tr id="col-6">
                                        <td>Earned:</td>
                                    </tr>
                                    <tr id="col-7">
                                        <td>Spent:</td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>

                            <div class="row">
                                <br>
                                <div class=" col-md-2">
                                    <div class="form-group">
                                        <label class=" control-label" for="table-selection">Table Type</label>
                                        <select class="form-control selectpicker btn-block"
                                                id="table-selection"
                                                name="table_selection"
                                                data-style="btn-primary"
                                                onchange="switchTableType()">
                                            <option value="1">Accumulative</option>
                                            <option value="2">Monthly</option>
                                        </select>

                                    </div>
                                </div>

                                <div class=" col-md-2">
                                    <div class="form-group">
                                        <label class=" control-label" for="table_column_selection">Table Values</label>
                                        <select class="form-control selectpicker btn-block"
                                                id="table_column_selection"
                                                name="table_column_selection"
                                                data-style="btn-primary"
                                                onchange="switchTableType()">
                                            <option value="1">Hours</option>
                                            <option value="2">%</option>
                                        </select>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"> Projected</h3></div>
            <div class="panel-body">
                <div style="width: 100%">
                    <canvas id="chart-1"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"> Stacked Chart</h3></div>
            <div class="panel-body">
                <div style="width: 100%">
                    <canvas id="chart-3"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"> Hours</h3></div>
            <div class="panel-body">
                <div style="width: 100%">
                    <canvas id="chart-4"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- end row -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->


@stop


@section ('local_scripts')
    <!-- Morris Chart Dependencies -->
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/js/plugin/chartjs/Chart.min.js"></script>


    <script type="text/javascript">

        var serverBarChartData = '<?php echo json_encode($projectedChartData);?>';
        //console.log(serverBarChartData);
        serverBarChartData = JSON.parse(serverBarChartData);
        //console.log(serverBarChartData);

        var chart1Instance;
        var chart2Instance;
        var chart3Instance;
        var chart4Instance;

        //Setup the date for display
        var displayDate = new Date();
        var oldDate = $('#old_search_date').val();
        if (oldDate !== '') {
            displayDate = new Date(oldDate);
        }
        $('.datepicker').datepicker({
                format: "mm-yyyy",
                viewMode: "months",
                minViewMode: "months",
                startDate: '-14m',
                endDate: '+16m'
                // 'setDate', displayDate
            }
        );

        function getAreaCharts(area_id) {
            console.log(area_id);
            //window.location
            window.location.href = "/tasks/charts/test1/" + area_id;
        }

        var chartType = 1;var tableType = 1;
        function populateDataTable(data) {
            console.log(data);
            var buffer1 = 0;var buffer2 = 0;var buffer3 = 0;var buffer4 = 0;var buffer5 = 0;var buffer6 = 0;var buffer7 = 0;
            var col1 = $('#col-1');var col2 = $('#col-2');var col3 = $('#col-3');var col4 = $('#col-4');var col5 = $('#col-5');
            var col6 = $('#col-6');var col7 = $('#col-7');

            col1.empty().append('<td><strong></strong></td>');
            col2.empty().append('<td><strong>Planned Prep+Exe:</strong></td>');
            col3.empty().append('<td><strong>Planned Prep:</strong></td>');
            col4.empty().append('<td><strong>Planned Exec:</strong></td>');
            col5.empty().append('<td><strong>Prep+Exec Roster:</strong></td>');
            col6.empty().append('<td><strong>Prep+Exec Earned:</strong></td>');
            col7.empty().append('<td><strong>Prep+Exec Spent:</strong></td>');

            var all = true;var pos1 = 0;var pos2 = 6;var pos3 = 0;var pos4 = 7;var pos5 = 8;var pos6 = 9;var pos7 = 10;
            var total = 0;var prep = 0;var exec = 0;var roster = 0;var earned = 0;var spent = 0;
            if (!all) {pos1 = 0;pos2 = 1;pos3 = 2;pos4 = 3;pos5 = 4;pos6 = 5;}


            console.log(data[0]);
            var arrayLength = data[0].length;
           // var totalHrsAssigned = sumTargetValue(parseInt(data[pos2][arrayLength-1]));// parseInt(data[pos2][arrayLength-1]);
           // var totalHrsPrep = sumTargetValue(parseInt(data[pos2][arrayLength-1]) - parseInt(data[pos4][arrayLength-1]));//parseInt(data[pos2][arrayLength-1]) - parseInt(data[pos4][arrayLength-1]);
           // var totalHrsExec = sumTargetValue(parseInt(data[pos4][arrayLength-1]));//parseInt(data[pos4][arrayLength-1]);
            //var totalRoster = sumTargetValue(parseInt(data[pos5][arrayLength-1]));//parseInt(data[pos5][arrayLength-1]);//roster
           // var totalEarned = sumTargetValue(parseInt(data[pos6][arrayLength-1]));//parseInt(data[pos6][arrayLength-1]);
            var totalSpent = sumTargetValue(data[23]['spentTotal']);//parseInt(data[pos7][arrayLength-1]);
            var totalRoster =  sumTargetValue(data[24]['rosterTotal']);
            var  totalHrsAssigned =  sumTargetValue(data[25]['projectTotalTarget']);
            var  totalHrsPrep = sumTargetValue(data[26]['projectTotalPrepTarget']);
            var  totalHrsExec = sumTargetValue(data[27]['projectTotalExecTarget']);
            var  totalEarned =  sumTargetValue(data[28]['projectTotalEarned']);

            col1.append('<td>Total (hrs)</td>');
            col2.append('<td>' + totalHrsAssigned + '</td>');
            col3.append('<td>' + totalHrsPrep + '</td>');
            col4.append('<td>' + totalHrsExec + '</td>');
            col5.append('<td>' + totalRoster + '</td>');
            col6.append('<td>' + totalEarned + '</td>');
            col7.append('<td>' + data[23]['spentTotal'] + '</td>');

            var i = 0;
            if (tableType === 1) {
                //Hours Table
                for (i = 0; i < data[0].length; i++) {
                    col1.append('<td>' + data[pos1][i] + '</td>');
                    if (chartType === 2) {
                        //Monthly
                        total = (parseInt(data[pos2][i]) - buffer2);//total
                        col2.append('<td>' + total + '</td>');
                        buffer2 = parseInt(data[pos2][i]);

                        exec = (parseInt(data[pos4][i]) - buffer4);//xec
                        col4.append('<td>' + exec + '</td>');
                        buffer4 = parseInt(data[pos4][i]);

                        prep = total - exec;
                        col3.append('<td>' + (total - exec ) + '</td>');//Prep

                        roster = (parseInt(data[pos5][i]) - buffer5);
                        col5.append('<td>' + roster + '</td>');
                        buffer5 = parseInt(data[pos5][i]);//Roster

                        earned = (parseInt(data[pos6][i]) - buffer6);
                        col6.append('<td>' + earned + ' </td>');
                        buffer6 = parseInt(data[pos6][i]);//Earned

                        spent = (parseInt(data[pos7][i]) - buffer7);
                        col7.append('<td>' + spent + '</td>');
                        buffer7 = parseInt(data[pos7][i]);//Spent

                    } else {
                        //Accumulative
                        col2.append('<td>' + parseInt(data[pos2][i]) + '</td>');//total
                        col3.append('<td>' + (parseInt(data[pos2][i]) - parseInt(data[pos4][i])) + '</td>'); //Prep
                        col4.append('<td>' + parseInt(data[pos4][i]) + '</td>'); //Exe
                        col5.append('<td>' + parseInt(data[pos5][i]) + '</td>'); //Roster
                        col6.append('<td>' + parseInt(data[pos6][i]) + '</td>');
                        col7.append('<td>' + parseInt(data[pos7][i]) + '</td>');
                    }
                }
            }else{
                //Percentage Table
                for ( i = 0; i < data[0].length; i++) {
                    col1.append('<td>' + data[pos1][i] + '</td>');

                    if (chartType === 2) {

                        total = roundUp(((parseInt(data[pos2][i]) - buffer2)/totalHrsAssigned * 100),100);//total
                        col2.append('<td>' + total + ' %</td>');
                        buffer2 = parseInt(data[pos2][i]);

                        exec = roundUp(((parseInt(data[pos4][i]) - buffer4)/totalHrsAssigned * 100),100);//(parseInt(data[pos4][i]) - buffer4);//xec
                        col4.append('<td>' + exec + ' %</td>');
                        buffer4 = parseInt(data[pos4][i]);

                        col3.append('<td>' + roundUp(((((parseInt(data[pos2][i]) - parseInt(data[pos4][i])) -  buffer3) / totalHrsAssigned) * 100 ),100) + ' %</td>');//Prep// col3.append('<td>' + (total - exec ) + '</td>');//Prep
                        buffer3 = parseInt(data[pos2][i]) - parseInt(data[pos4][i]);

                        col5.append('<td>' + (roundUp(((parseInt(data[pos5][i]) - buffer5)/totalRoster * 100),100)) + ' %</td>');//col5.append('<td>' + (parseInt(data[pos5][i]) - buffer5) + '</td>');
                        buffer5 = parseInt(data[pos5][i]);//Roster

                        col6.append('<td>' + (roundUp(((parseInt(data[pos6][i]) - buffer6)/totalHrsAssigned * 100),100)) + '%</td>');//col6.append('<td>' + (parseInt(data[pos6][i]) - buffer6) + '</td>');
                        buffer6 = parseInt(data[pos6][i]);//Earned

                        col7.append('<td>' + (roundUp(((parseInt(data[pos7][i]) - buffer7)/totalSpent * 100),100)) + '%</td>');// col7.append('<td>' + (parseInt(data[pos7][i]) - buffer7) + '</td>');
                        buffer7 = parseInt(data[pos7][i]);//Spent

                    } else {

                        //Accumulative

                        col2.append('<td>' + roundUp((((parseInt(data[pos2][i]))/totalHrsAssigned) * 100),100) + ' %</td>');//total//col2.append('<td>' + parseInt(data[pos2][i]) + '</td>');//total

                        col3.append('<td>' + roundUp((((parseInt(data[pos2][i]) - parseInt(data[pos4][i])) / totalHrsPrep) * 100 ),100) + ' %</td>'); //Prep//col3.append('<td>' + (parseInt(data[pos2][i]) - parseInt(data[pos4][i])) + '</td>'); //Prep

                        col4.append('<td>' + (roundUp(((parseInt(data[pos4][i])/totalHrsExec) * 100),100))  + ' %</td>'); //Exe// col4.append('<td>' + parseInt(data[pos4][i]) + '</td>'); //Exe

                        col5.append('<td>' + (roundUp(((parseInt(data[pos5][i])/totalRoster) * 100),100))  + ' %</td>'); //Roster//col5.append('<td>' + parseInt(data[pos5][i]) + '</td>'); //Roster

                        col6.append('<td>' + (roundUp(((parseInt(data[9][i])/totalHrsAssigned) * 100),100))  + '%</td>');//col6.append('<td>' + parseInt(data[pos6][i]) + '</td>');
                       // col6.append('<td>' + (roundUp(((parseInt(data[6][i])/totalEarned) * 100),100))  + '%</td>');
                        console.log(data[9][i]);

                        col7.append('<td>' + (roundUp(((parseInt(data[pos7][i])/totalSpent) * 100),100)) + ' %</td>');// col7.append('<td>' + parseInt(data[pos7][i]) + '</td>');
                    }
                }
            }


        }

        function roundUp(num, precision) {
            return Math.ceil(num * precision) / precision
        }

        function sumTargetValue(value){
            if(value === 0){
                value = 1;
            }
            return value;
        }

        function switchTableType() {
            chartType = parseInt($('#table-selection').val());
            tableType = parseInt($('#table_column_selection').val());
            populateDataTable(serverBarChartData);
        }

        /*
         *   Get the Chart Data and populate the Charts
         */
        function getChartData() {
            clearCharts();
            var form = $('#get-area-charts-form');
            var url = form.attr('action');
            var method = form.find('input[name="_method"]').val() || 'GET';
            var datastring = $(form).serialize();
            var button = $('#' + event.target.id);
            $.ajax({
                type: method,
                url: url,
                data: datastring,
                success: function (result) {
                    waitDone();
                   console.log(result);
                     console.log(result.chartData);
                    buildCharts(JSON.parse(result.chartData));
                    serverBarChartData = JSON.parse(result.chartData);
                    populateDataTable(JSON.parse(result.chartData));

                    //refreshTheModelTable();
                    /*                    hideModelBaseModal();
                     notifyUserSuccessOfChange(result.message);
                     form.find(".validation-errors-section-update").addClass('hide');
                     refreshModelDropDownAfterModelChangedOverAjax(result);
                     refreshTheModelTable(result);
                     removeSpinnerAndReEnableButton(button);*/
                },
                beforeSend: function () {
                    pleaseWait('updating ...');
                },
                complete: function () {
                    waitDone();
                },

                error: function (xhr, status, error) {
                    alert('there is an error\n' + error);
                    waitDone();
                }
            });

        }


        function buildCharts(serverAllChartsData) {
            if (serverAllChartsData.length === 0) {
                return alert('No Data for Chosen Team');
            }
            $('#chart-1').fadeIn(2000);
            $('#chart-2').fadeIn(2000);
            $('#chart-3').fadeIn(2000);
            $('#chart-4').fadeIn(2000);
            buildChart1('chart-1', serverAllChartsData);
            buildChart2('chart-2', serverAllChartsData);
            buildChart3('chart-3', serverAllChartsData);
            buildChart4('chart-4', serverAllChartsData);

            updateReportInfoDetails(serverAllChartsData);
        }

        function updateReportInfoDetails(data) {
            var reportText = 'Report : From ' + data[18] + ' To ' + data[19];
            $('#reportDetails').text(reportText);
        }

        function clearCharts() {
            console.log('clear charts called');
            chart1Instance.destroy();
            chart2Instance.destroy();
            chart3Instance.destroy();
            chart4Instance.destroy();

            $('#chart-1').hide();
            $('#chart-2').hide();
            $('#chart-3').hide();
            $('#chart-4').hide();
        }

        $(document).ready(function () {
            buildCharts(serverBarChartData);
            populateDataTable(serverBarChartData);
        });


        /* ------------------------------------------
         CHART 1 , PROJECTED  BAR CHART - NON STACKED
         ---------------------------------------------*/
        function buildChart1(chartId, serverData) {

            labels = serverData[0];
            var line1Data = serverData[1];
            var line2Data = serverData[3];
            var line3Data = serverData[4];
            var line4Data = serverData[5];

            var chart1Data = {
                labels: labels,
                datasets: [{
                    type: 'bar',
                    label: 'Prep + Exec',
                    backgroundColor: "rgba(30,144,255,0.5)",
                    data: line1Data,
                }/*, {
                 type: 'line',
                 label: 'Exec',
                 backgroundColor: "rgba(60,179,113,0.7)",
                 data: serverBarChartData[2],
                 fill: false,
                 pointStyle:'square',
                 borderDash: [5, 5],
                 }*/, {
                    type: 'line',
                    label: 'Roster',
                    backgroundColor: "rgba(0,0,0,1.0)",
                    data: line2Data,
                    borderColor: 'black',
                    borderWidth: 1,
                    pointStyle: 'square',
                    borderDash: [5, 5],
                    borderDashOffset: 0.5,
                    fill: false,
                }, {
                    type: 'line',
                    label: 'Earned',
                    backgroundColor: "rgba(0,128,0,1.0)",
                    borderColor: 'green',
                    data: line3Data,
                    fill: false,
                }, {
                    type: 'line',
                    label: 'Spent',
                    backgroundColor: "rgba(255,215,0,1.0)",
                    data: line4Data,
                    borderColor: 'orange',
                    borderWidth: 1,
                    fill: false,
                }
                ]
            };

            var chart1 = document.getElementById(chartId).getContext("2d");
            chart1Instance = new Chart(chart1, {
                type: 'bar',
                data: chart1Data,
                options: {
                    title: {
                        display: true,
                        text: ""
                    },
                    legend: {
                        position: 'bottom'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            //stacked: true,
                        }],
                        yAxes: [{
                            //stacked: true
                        }]
                    }
                }
            });
        }//End Build Chat 1

        /* ------------------------------------------
         CHART 2 CHART = ACCUMULATIVE PROJECTED AREA LINE CHART
         ---------------------------------------------*/
        function buildChart2(chartId, serverData) {

            labels = serverData[0];
            var line1Data = serverData[6];
            var line2Data = serverData[7];
            var line3Data = serverData[8];
            var line4Data = serverData[9];
            var line5Data = serverData[10];

            var chartData = {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            type: 'line',
                            label: "Prep",
                            data: line1Data, //serverBarChartData[6],
                            fill: true,
                            backgroundColor: "rgba(0,0,205,0.2)"
                        },
                        {
                            type: 'line',
                            label: "Exec",
                            data: line2Data,//serverBarChartData[7],
                            fill: true,
                            backgroundColor: "rgba(60,179,113,0.5)"
                        },
                        {
                            type: 'line',
                            label: 'Roster',
                            backgroundColor: "rgba(0,0,0,1.0)",
                            data: line3Data,//serverBarChartData[8],
                            borderColor: 'black',
                            borderWidth: 1,
                            pointStyle: 'square',
                            borderDash: [5, 5],
                            borderDashOffset: 0.5,
                            fill: false,
                        },
                        {
                            type: 'line',
                            label: "Earned",
                            data: line4Data,//serverBarChartData[9],
                            fill: false,
                            borderColor: 'green',
                            backgroundColor: "rgba(0,128,0,0.6)"
                        },
                        {
                            type: 'line',
                            label: "Spent",
                            data: line5Data,//serverBarChartData[10],
                            fill: false,
                            borderColor: 'orange',
                            backgroundColor: "rgba(255,215,0,0.6)"
                        }
                    ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ""
                    },
                    legend: {
                        position: 'bottom'
                    },
                    tooltips: {
                        //mode: 'label',
                    },
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            //stacked: true,
                        }],
                        yAxes: [{
                            // stacked: true
                        }]
                    }
                }
            };

            var chart2 = document.getElementById(chartId).getContext("2d");
            chart2Instance = new Chart(chart2, chartData);

        }//End Build Chat 2


        /* ------------------------------------------
         CHART 3 CHART =  PROJECTED STACKED BAR CHART
         ---------------------------------------------*/
        function buildChart3(chartId, serverData) {

            labels = serverData[0];
            var line1Data = serverData[11];
            var line2Data = serverData[12];
            var line3Data = serverData[13];

            var chartData = {
                labels: labels,//serverBarChartData[0],
                datasets: [{
                    type: 'bar',
                    label: 'Prep',
                    backgroundColor: "rgba(0,0,205,0.5)",
                    borderColor: 'black',
                    data: line1Data,//serverBarChartData[11],
                }, {
                    type: 'bar',
                    label: 'Exec',
                    backgroundColor: "rgba(60,179,113,0.5)",
                    data: line2Data,//serverBarChartData[12],
                    borderColor: 'green',
                }, {
                    type: 'line',
                    label: 'Roster',
                    backgroundColor: "rgba(0,0,0,1.0)",
                    data: line3Data,//serverBarChartData[13],
                    borderColor: 'black',
                    borderWidth: 1,
                    pointStyle: 'square',
                    borderDash: [5, 5],
                    borderDashOffset: 0.5,
                    fill: false,
                }
                ]
            };
            var chart3 = document.getElementById(chartId).getContext("2d");
            chart3Instance = new Chart(chart3, {
                type: 'bar',
                data: chartData,
                options: {
                    title: {
                        display: true,
                        text: ""
                    },
                    legend: {
                        position: 'bottom'
                    },
                    tooltips: {
                        // mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });

        }//End Build Chat 3

        /* ------------------------------------------
         CHART 4 CHART =  PROJECTED STACKED BAR CHART
         ---------------------------------------------*/
        function buildChart4(chartId, serverData) {

            labels = serverData[0];
            var line1Data = serverData[14];
            var line2Data = serverData[15];
            var line3Data = serverData[16];

            var chartData = {
                labels: labels,//serverBarChartData[0],
                datasets: [
                    {
                        type: 'line',
                        label: 'Roster',
                        backgroundColor: "rgba(0,0,0,1.0)",
                        data: line1Data,//serverBarChartData[14],
                        borderColor: 'black',
                        borderWidth: 1,
                        pointStyle: 'square',
                        borderDash: [5, 5],
                        borderDashOffset: 0.5,
                        fill: false,
                    }, {
                        type: 'line',
                        label: 'Earned',
                        backgroundColor: "rgba(0,128,0,1.0)",
                        borderColor: 'green',
                        data: line2Data,//serverBarChartData[15],
                        fill: false,
                    }, {
                        type: 'line',
                        label: 'Spent',
                        backgroundColor: "rgba(255,215,0,1.0)",
                        data: line3Data,//serverBarChartData[16],
                        borderColor: 'orange',
                        borderWidth: 1,
                        fill: false,
                    }
                ]
            };

            var chart4 = document.getElementById(chartId).getContext("2d");
            chart4Instance = new Chart(chart4, {
                type: 'line',
                data: chartData,
                options: {
                    title: {
                        display: true,
                        text: ""
                    },
                    legend: {
                        position: 'bottom'
                    },
                    tooltips: {
                        // mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            //stacked: true,
                        }],
                        yAxes: [{
                            //stacked: true
                        }]
                    }
                }
            });

        }//End Build Chat 4


        function switchSChartAxis(choice) {
            var totalHours = serverBarChartData[17];
            console.log(totalHours);
            var serverData = serverBarChartData;
            var bufferArray = [serverBarChartData[0]];
            var data = [];

            for (var i = 6; i < 12; i++) {
                if(i === 8){
                    var rosterTotal = serverData[i][serverData[i].length -1];
                    data = calculatePercentage(serverData[i], totalHours);
                }else{
                    data = calculatePercentage(serverData[i], totalHours);
                }

                bufferArray.push(data);
            }
            console.log(bufferArray);

            if (parseInt(choice) === 2) {
                chart2Instance.destroy();
                buildPercentChart2('chart-2', bufferArray);
            } else {
                chart2Instance.destroy();
                buildChart2('chart-2', serverBarChartData);
            }

        }


        function calculatePercentage(dataArray, totalHours) {
            var bufferArray = [];
            var p = 0;
            totalHours = parseInt(totalHours);
            console.log(totalHours);
            for (var i = 0; i < dataArray.length; i++) {
                p = 0;
                if (dataArray[i] > 0) {
                    p = parseInt((dataArray[i] / totalHours) * 100);
                }
                bufferArray.push(p);
            }
            return bufferArray
        }
        /* ------------------------------------------
         CHART 2 CHART = ACCUMULATIVE PROJECTED AREA LINE CHART
         ---------------------------------------------*/
        function buildPercentChart2(chartId, data) {

            var labels = data[0];
            var line1Data = data[1];
            var line2Data = data[2];
            var line3Data = data[3];
            var line4Data = data[4];
            var line5Data = data[5];


            var chartData = {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            type: 'line',
                            label: "Prep",
                            data: line1Data, //serverBarChartData[6],
                            fill: true,
                            backgroundColor: "rgba(0,0,205,0.2)"
                        },
                        {
                            type: 'line',
                            label: "Exec",
                            data: line2Data,//serverBarChartData[7],
                            fill: true,
                            backgroundColor: "rgba(60,179,113,0.5)"
                        },
                         {
                         type: 'line',
                         label: 'Roster',
                         backgroundColor: "rgba(0,0,0,1.0)",
                         data: line3Data,//serverBarChartData[8],
                         borderColor: 'black',
                         borderWidth: 1,
                         pointStyle: 'square',
                         borderDash: [5, 5],
                         borderDashOffset: 0.5,
                         fill: false,
                         },
                        {
                            type: 'line',
                            label: "Earned",
                            data: line4Data,//serverBarChartData[9],
                            fill: false,
                            borderColor: 'green',
                            backgroundColor: "rgba(0,128,0,0.6)"
                        },
                        {
                            type: 'line',
                            label: "Spent",
                            data: line5Data,//serverBarChartData[10],
                            fill: false,
                            borderColor: 'orange',
                            backgroundColor: "rgba(255,215,0,0.6)"
                        }
                    ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ""
                    },
                    legend: {
                        position: 'bottom'
                    },
                    tooltips: {
                        //mode: 'label',
                    },
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            //stacked: true,
                        }],
                        yAxes: [{
                            ticks: {
                                max: 100,
                                min: 0,
                                stepSize: 5
                            }
                        }],
                        tooltips: {
                            enabled: true,
                        }
                    }
                }
            };

            var chart2 = document.getElementById(chartId).getContext("2d");
            chart2Instance = new Chart(chart2, chartData);

        }//End Build Chat 2

        /* ------------------------------------------
         CHART 3 CHART =  PROJECTED STACKED BAR CHART
         ---------------------------------------------*/
        /*            var chart3 = document.getElementById("chart-3").getContext("2d");
         window.myBar = new Chart(chart3, {
         type: 'bar',
         data: chart3Data,
         options: {
         title: {
         display: true,
         text: ""
         },
         legend: {
         position: 'bottom'
         },
         tooltips: {
         // mode: 'label'
         },
         responsive: true,
         scales: {
         xAxes: [{
         stacked: true,
         }],
         yAxes: [{
         stacked: true
         }]
         }
         }
         });*/
        /* ------------------------------------------
         CHART 4 CHART =  PROJECTED STACKED BAR CHART
         ---------------------------------------------*/
        /*            var chart4 = document.getElementById("chart-4").getContext("2d");
         window.myBar = new Chart(chart4, {
         type: 'line',
         data: chart4Data,
         options: {
         title:{
         display:true,
         text:""
         },
         legend:{
         position:'bottom'
         },
         tooltips: {
         // mode: 'label'
         },
         responsive: true,
         scales: {
         xAxes: [{
         //stacked: true,
         }],
         yAxes: [{
         //stacked: true
         }]
         }
         }
         });
         });*/

        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         CHART  DATA
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/


        /* /!* ------------------------------------------
         CHART 2 DATA =  ACCUMULATIVE PROJECTED AREA LINE CHART
         ---------------------------------------------*!/
         var chart2Data = {
         type: 'line',
         data: {
         labels: serverBarChartData[0],
         datasets: [
         {
         type: 'line',
         label: "Prep",
         data: serverBarChartData[6],
         fill: true,
         backgroundColor: "rgba(0,0,205,0.2)"
         },
         {
         type: 'line',
         label: "Exec",
         data: serverBarChartData[7],
         fill: true,
         backgroundColor: "rgba(60,179,113,0.5)"
         },
         {
         type: 'line',
         label: 'Roster',
         backgroundColor: "rgba(0,0,0,1.0)",
         data: serverBarChartData[8],
         borderColor: 'black',
         borderWidth: 1,
         pointStyle:'square',
         borderDash: [5, 5],
         borderDashOffset:0.5,
         fill: false,
         },
         {
         type: 'line',
         label: "Earned",
         data: serverBarChartData[9],
         fill: false,
         borderColor: 'green',
         backgroundColor: "rgba(0,128,0,0.6)"
         },
         {
         type: 'line',
         label: "Spent",
         data: serverBarChartData[10],
         fill: false,
         borderColor: 'orange',
         backgroundColor: "rgba(255,215,0,0.6)"
         }
         ]
         },
         options: {
         responsive: true,
         title: {
         display: true,
         text: ""
         },
         legend:{
         position:'bottom'
         },
         tooltips: {
         //mode: 'label',
         },
         hover: {
         mode: 'label'
         },
         scales: {
         xAxes: [{
         //stacked: true,
         }],
         yAxes: [{
         // stacked: true
         }]
         }
         }
         };
         */

        /* ------------------------------------------
         CHART 3 DATA = PROJECTED STACKED BAR CHART
         ---------------------------------------------*/
        /*        var chart3Data = {
         labels: serverBarChartData[0],
         datasets: [{
         type: 'bar',
         label: 'Prep',
         backgroundColor: "rgba(0,0,205,0.5)",
         borderColor: 'black',
         data: serverBarChartData[11],
         }, {
         type: 'bar',
         label: 'Exec',
         backgroundColor: "rgba(60,179,113,0.5)",
         data: serverBarChartData[12],
         borderColor: 'green',
         }, {
         type: 'line',
         label: 'Roster',
         backgroundColor: "rgba(0,0,0,1.0)",
         data: serverBarChartData[13],
         borderColor: 'black',
         borderWidth: 1,
         pointStyle:'square',
         borderDash: [5, 5],
         borderDashOffset:0.5,
         fill: false,
         }
         ]
         };*/
        /* ------------------------------------------
         CHART 4 DATA = PROJECTED STACKED BAR CHART
         ---------------------------------------------*/
        /*        var chart4Data = {
         labels: serverBarChartData[0],
         datasets: [{
         type: 'line',
         label: 'Roster',
         backgroundColor: "rgba(0,0,0,1.0)",
         data: serverBarChartData[14],
         borderColor: 'black',
         borderWidth: 1,
         pointStyle:'square',
         borderDash: [5, 5],
         borderDashOffset:0.5,
         fill: false,
         }, {
         type: 'line',
         label: 'Earned',
         backgroundColor: "rgba(0,128,0,1.0)",
         borderColor: 'green',
         data: serverBarChartData[15],
         fill: false,
         }, {
         type: 'line',
         label: 'Spent',
         backgroundColor: "rgba(255,215,0,1.0)",
         data: serverBarChartData[16],
         borderColor: 'orange',
         borderWidth: 1,
         fill: false,
         }
         ]
         };*/


    </script>




@stop
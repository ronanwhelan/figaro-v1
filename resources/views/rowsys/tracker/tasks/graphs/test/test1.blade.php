@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="col-md-12">
                <label class="control-label">Responsible Team</label>
                <select class="form-control selectpicker" onchange="getAreaBarAndLineGraphData(this.value)" name="area" id="area" data-style="btn-primary">
                    <option value="0"></option>
                    @foreach($areas as $area)
                        <option value="{{$area->id}}" > {{$area->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="col-md-12"></div>
        </div>
    </div>


    <title>Combo Bar-Line Chart</title>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
            <div class="panel-body">
                <div style="width: 100%">
                    <canvas id="pro-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
            <div class="panel-body">
                <div style="width: 100%">
                    <canvas id="accum-chart"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- end row -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->


@stop


@section ('local_scripts')
    <!-- Morris Chart Dependencies -->
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/js/plugin/chartjs/Chart.min.js"></script>

    <script type="text/javascript">

        var barChartData = '';

        var labels = '';

;
        function getAreaBarAndLineGraphData(area_id){
            console.log(area_id);
            console.log('getAreaBarAndLineGraphData');
            $.ajax({
                type: "GET",
                url: "/tasks/charts/test1/" + area_id, success: function (data) {
                    console.log(data);
                    labels = data[0];
                    drawAreaProjectedBarChart(data[0],data[1],data[2],data[3]);
                    drawAreaProjectedAccumLineChart();
                }
            });
        }



        function drawAreaProjectedAccumLineChart(){
            var chart2 = document.getElementById("accum-chart").getContext("2d");
            window.myLine = new Chart(chart2, config);
        }

        function drawAreaProjectedBarChart(labels,bar1,bar2,roster){
            barChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        type: 'bar',
                        label: 'Prep',
                        backgroundColor: "rgba(123,104,238,0.5)",
                        data: bar1,
                        datasetFill: false,
                    },
                    {
                        type: 'bar',
                        label: 'Exec',
                        backgroundColor: "rgba(34,139,34,0.5)",
                        data: bar2,
                    },

                    {
                        type: 'line',
                        label: 'Roster',
                        backgroundColor: "rgba(151,187,205,1.0)",
                        data: [5, 15, 95, 55, 75, 25, 65],
                        borderColor: 'black',
                        borderWidth: 1,
                        fill: false,
                    },
                    /*                {
                     type: 'line',
                     label: 'Dataset 4',
                     backgroundColor: "rgba(151,187,205,1.0)",
                     data: [5, 15, 95, 55, 75, 25, 65],
                     borderColor: 'blue',
                     borderWidth: 1,
                     fill: false,

                     },
                     {
                     type: 'line',
                     label: 'Dataset 5',
                     backgroundColor: "rgba(151,187,205,1.0)",
                     data: [5, 10, 45, 55, 75, 30, 45],
                     borderColor: 'red',
                     borderWidth: 1,
                     fill: false,
                     },*/
                ]

            };
            var chart1 = document.getElementById("pro-chart").getContext("2d");
            window.myBar = new Chart(chart1, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    stacked: true,
                    lineTension: 0.1,
                    title: {
                        display: true,
                        text: 'Chart.js Combo Bar Line Chart'
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }

                }
            });

        }

        </script>





    <script type="text/javascript">
        $(document).ready(function () {


        });

    </script>
    <script>

        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {type: 'line', label: "Prep -Accum", data: [0, 10, 70, 95, 105, 120, 140], fill: true, backgroundColor: "rgba(0,0,255,0.3)"},
                    {type: 'line', label: "Exec-Accum", data: [0, 35, 50, 60, 85, 95, 110], fill: true, backgroundColor: "rgba(255,0,255,0.3)"},
                    {type: 'line', label: "line 1", data: [0, 35, 50, 65, 80, 110, 140], fill: false, borderColor: 'black'},
                    {type: 'line', label: "line 2", data: [0, 40, 55, 60, 75], fill: false, borderColor: 'orange'},
                    {type: 'line', label: "line 3", data: [0, 10, 35, 50], fill: false, borderColor: 'blue'}
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: "Chart.js Line Chart - Stacked Area"
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        //stacked: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };

    </script>



@stop
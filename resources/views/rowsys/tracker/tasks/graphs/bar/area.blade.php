@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Downstream Processes - Generation</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <div id="chart_div1" onclick="alert('where would you like to go next?')"></div>
                            <p class="txt-color-redLight">Past Due: <strong>234</strong></p>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="well well-sm">
                                <dl>
                                    <dt>Status %</dt>
                                    <dd>
                                        <h5>
                                            <small>Target:</small>
                                            <strong>40%</strong>
                                        </h5>
                                    </dd>
                                    <dd>
                                        <h5>
                                            <small>Actual:</small>
                                            <strong>60%</strong>
                                        </h5>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>Tasks</dt>
                                    <dd><h5 class="">Total Tasks: <strong>2345</strong></h5></dd>
                                    <dd><h5 class="">Completed <strong>1267 </strong><i class="fa fa-check"></i>
                                        </h5></dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Downstream Processes - Execution</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <div id="chart_div2" onclick="alert('where would you like to go next?')"></div>
                            <p class="txt-color-redLight">Past Due: <strong>234</strong></p>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="well well-sm">
                                <dl>
                                    <dt>Status %</dt>
                                    <dd>
                                        <h5>
                                            <small>Target:</small>
                                            <strong>40%</strong>
                                        </h5>
                                    </dd>
                                    <dd>
                                        <h5>
                                            <small>Actual:</small>
                                            <strong>60%</strong>
                                        </h5>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>Tasks</dt>
                                    <dd><h5 class="">Total Tasks: <strong>2345</strong></h5></dd>
                                    <dd><h5 class="">Completed <strong>1267 </strong><i class="fa fa-check"></i>
                                        </h5></dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="well">
                <label>Target</label>
                <input type="number" id="target-num" value="40">
                <br>
                <label>Actual</label>
                <input type="number" id="actual-num" value="60">
                <br><br>
                <label>Press
                    <button type="button" id="calc-cahrt-btn" onclick="testChart()"> GO!</button>
                    for the Magic</label>

            </div>
        </div>

    </div>
    <!-- end row -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->


@stop


@section ('local_scripts')
    <!-- Morris Chart Dependencies -->
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        //Call to the Google Charts Library
        google.charts.load('current', {packages: ['corechart', 'bar']});


        $(document).ready(function () {
            testChart();
            DrawChart(50, 65, 'chart_div2', 'this is the title');

        });

        //var chartData = <?php //echo  $typeStats ?>;


        //The page is built by the server for each area dn the div for the chart is added

        //The client requests the data for the graph i.e actual and target value.
        // The other data can follow like number past due etc.

        //loop through each area to get the graph data and build the charts

        //a function works out the values for the chart

        // a function draws the chart

        //function

        //Setup the Variables
        var p1Val = 44;
        var p1PerText = '46%';
        var p1Colour = 'green';

        var p2Val = 1;
        var p2PerText = '';
        var p2Colour = 'black';

        var p3Val = 15;
        var p3PerText = '60%';
        var p3Colour = 'green';

        var p4Val = 40;
        var p4PerText = '';
        var p4Colour = 'lightgrey';

        var chartTitle = 'Upstream Process Generation';

        var chartId = 'chart_div1';

        var targetVal;
        var actualVal;

        var graphData = [];
        var seriesData = [];
        var options = {};

        function calculateTheGraphData(targetVal, actualVal) {
            console.log('in function');

            if (actualVal > targetVal) {

                console.log('in 1');
                p1Val = targetVal - 1;
                p1Colour = 'green';
                p1PerText = '';

                p2Val = 1;
                p2Colour = 'black';
                p2PerText = targetVal + '%';

                p3Val = actualVal - targetVal;
                p3Colour = 'green';
                p3PerText = actualVal + '%';

                p4Val = 100 - (p1Val + p2Val + p3Val);
                p4Colour = 'lightgrey';
                p4PerText = '';

            }

            if (targetVal > actualVal) {
                console.log('in 2');

                console.log(targetVal + ' ' + actualVal);

                p1Val = actualVal;
                p1Colour = 'red';
                p1PerText = actualVal + '%';

                p2Val = (targetVal - actualVal) - 1;
                p2Colour = 'lightgrey';
                p2PerText = '';

                p3Val = 1;
                p3Colour = 'black';
                p3PerText = targetVal + '%';

                p4Val = 100 - (p1Val + p2Val + p3Val);
                p4Colour = 'lightgrey';
                p4PerText = '';
            }

            if (targetVal === actualVal) {
                console.log('in 3');
                p1Val = actualVal - 1;
                p1Colour = 'green';
                p1PerText = actualVal + '%';

                p2Val = 1;
                p2Colour = 'black';
                p2PerText = targetVal + '%';

                p3Val = 100 - actualVal;
                p3Colour = 'lightgrey';
                p3PerText = '';

                p4Val = 100 - (p1Val + p2Val + p3Val);
                p4Colour = 'lightgrey';
                p4PerText = '';

            }
            graphData = [
                ['Area',
                    'p1', {role: 'annotation'},
                    'p2', {role: 'annotation'},
                    'p3', {role: 'annotation'},
                    'p4', {role: 'annotation'}],
                ['', p1Val, p1PerText,
                    p2Val, p2PerText,
                    p3Val, p3PerText,
                    p4Val, p4PerText
                ]
                // ['', 44, '45%', 1, '', 15, '60%', 40, '']
            ];

            seriesData = [
                {color: p1Colour, visibleInLegend: false},
                {color: p2Colour, visibleInLegend: false},
                {color: p3Colour, visibleInLegend: false},
                {color: p4Colour, visibleInLegend: false}
            ];
            options = {
                animation: {"startup": true, duration: 2500, easing: 'inAndOut'},
                chartArea: {left: 10, top: 10, width: "85%", height: "50%"},
                enableInteractivity: false,//this disables interactivity like clicking
                //tooltip: { trigger: 'selection' }, //This enables the click event andshow percentage
                series: seriesData,
                title: chartTitle,
                isStacked: 'percent',
                legend: {position: 'top', maxLines: 3},
                hAxis: {
                    minValue: 0,
                    ticks: [0, .25, .50, .75, 1]
                }
            };
        }


        function testChart() {
            var v1 = $('#target-num').val();
            var v2 = $('#actual-num').val();
            parseInt(v1);
            parseInt(v2);
            if ($.isNumeric(v1) && $.isNumeric(v2)) {
                if (v1 <= 100 && v2 <= 100) {

                    chartId = 'chart_div1';
                    calculateTheGraphData(v1 * 1, v2 * 1);
                    prepareAndDrawBarPercentChart(graphData, chartId);
                }
            }
        }

        function DrawChart(target, actual, id, title) {
            chartId = id;
            chartTitle = title;
            calculateTheGraphData(target, actual);
            prepareAndDrawBarPercentChart(graphData, chartId);


        }


        /*
         *  --------------------------------------------------
         * Draw the Bar Chart WIth Target Percentage
         * --------------------------------------------------
         */
        var chart;
        function prepareAndDrawBarPercentChart(graphdata, chartId) {
            google.charts.setOnLoadCallback(drawStacked);
            function drawStacked() {
                var data = google.visualization.arrayToDataTable(graphdata);
                chart = new google.visualization.BarChart(document.getElementById(chartId));
                chart.draw(data, options);
                // Add our over/out handlers.
                //google.visualization.events.addListener(chart, 'onmouseover', barMouseOver);
                //google.visualization.events.addListener(chart, 'onmouseout', barMouseOut);

            }
        }


        function barMouseOver(e) {
            console.log(e);
            chart.setSelection([e]);
        }

        function barMouseOut(e) {
            chart.setSelection([{'row': null, 'column': null}]);
        }

        function convertJsonToArray(typeStats) {
            var dataOut = [];
            var data = typeStats;

            //var data = $.parseJSON(typeStats);
            for (var i = 0, l = data.length; i < l; i++) {
                var holder = [];
                holder[0] = data[i][0];
                holder[1] = data[i][1];
                //holder[2] = data[i][2];
                dataOut.push(holder);
            }
            return dataOut;
        }


    </script>



@stop
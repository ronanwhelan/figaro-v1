@extends ('layouts.main.master')


@section('content')

    <!-- MAIN PANEL -->
    <div id="main" role="main">
        <!-- ==========================RIBBON ========================== -->
        <!-- Ribbon - clear local content etc -->
        <!-- MAIN CONTENT -->
        <div id="content">
            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <h1> Hours Graphs</h1>

            <div id="sankey_basic" style="width: auto; height: auto;"></div>
            <br>
            <hr>
            <div id="sankey_multiple" style="width: 900px; height: 500px;"></div>
            <!-- ==========================CONTENT ENDS HERE ========================== -->
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

@stop


@section ('local_scripts')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["sankey"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'From');
            data.addColumn('string', 'To');
            data.addColumn('number', 'Weight');
            data.addRows([
                [ 'Brazil', 'Portugal', 5 ],
                [ 'Brazil', 'France', 1 ],
                [ 'Brazil', 'Spain', 1 ],
                [ 'Brazil', 'England', 1 ],
                [ 'Canada', 'Portugal', 1 ],
                [ 'Canada', 'France', 5 ],
                [ 'Canada', 'England', 1 ],
                [ 'Mexico', 'Portugal', 1 ],
                [ 'Mexico', 'France', 1 ],
                [ 'Mexico', 'Spain', 5 ],
                [ 'Mexico', 'England', 1 ],
                [ 'USA', 'Portugal', 1 ],
                [ 'USA', 'France', 1 ],
                [ 'USA', 'Spain', 1 ],
                [ 'USA', 'England', 5 ],
                [ 'Portugal', 'Angola', 2 ],
                [ 'Portugal', 'Senegal', 1 ],
                [ 'Portugal', 'Morocco', 1 ],
                [ 'Portugal', 'South Africa', 3 ],
                [ 'France', 'Angola', 1 ],
                [ 'France', 'Senegal', 3 ],
                [ 'France', 'Mali', 3 ],
                [ 'France', 'Morocco', 3 ],
                [ 'France', 'South Africa', 1 ],
                [ 'Spain', 'Senegal', 1 ],
                [ 'Spain', 'Morocco', 3 ],
                [ 'Spain', 'South Africa', 1 ],
                [ 'England', 'Angola', 1 ],
                [ 'England', 'Senegal', 1 ],
                [ 'England', 'Morocco', 2 ],
                [ 'England', 'South Africa', 7 ],
                [ 'South Africa', 'China', 5 ],
                [ 'South Africa', 'India', 1 ],
                [ 'South Africa', 'Japan', 3 ],
                [ 'Angola', 'China', 5 ],
                [ 'Angola', 'India', 1 ],
                [ 'Angola', 'Japan', 3 ],
                [ 'Senegal', 'China', 5 ],
                [ 'Senegal', 'India', 1 ],
                [ 'Senegal', 'Japan', 3 ],
                [ 'Mali', 'China', 5 ],
                [ 'Mali', 'India', 1 ],
                [ 'Mali', 'Japan', 3 ],
                [ 'Morocco', 'China', 5 ],
                [ 'Morocco', 'India', 1 ],
                [ 'Morocco', 'Japan', 3 ]
            ]);

            // Set chart options
            var options = {
                width: 600,
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.Sankey(document.getElementById('sankey_multiple'));
            chart.draw(data, options);
        }


    </script>

    <script type="text/javascript">

        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'From');
            data.addColumn('string', 'To');
            data.addColumn('number', 'Weight');
            data.addRows([
                [ 'Project', 'Design', 15 ],
                [ 'Project', 'Construction', 20 ],
                [ 'Design', 'System Urs', 5 ],
                [ 'Design', 'ERES', 7 ],
                [ 'Design', 'FAT', 2 ],
                [ 'Design', 'DataSheets', 8 ],
                [ 'Design', 'SRA', 1 ],
                [ 'Design', 'Design Rev', 5 ],
                [ 'Construction', 'Cons Comp', 2 ],
                [ 'Construction', 'IVP/IVR', 9 ],
                [ 'Construction', 'Context Menu', 4 ],
                [ 'Construction', '10% Spot Checks', 4 ]
            ]);

            // Sets chart options.
            var options = {
                width: 600,
            };

            // Instantiates and draws our chart, passing in some options.
            var chart = new google.visualization.Sankey(document.getElementById('sankey_basic'));
            chart.draw(data, options);
        }
    </script>
@stop

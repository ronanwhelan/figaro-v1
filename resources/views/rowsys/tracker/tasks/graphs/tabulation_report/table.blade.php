<div class="">
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="0">Col 1</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="1">Col 2</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Col 3</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Col 4</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Col 5</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Col 6</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Col 7</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Col 8</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Col 9</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Col 10</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Tabulation Report</div>
        <div class="panel-body">
            <div class="row" id="table-section" style="opacity: .08">
                <div class="col-lg-12 font-xs">

                    <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="font-xs">
                            <th data-class="expand" class="font-xs"></th>
                            <th data-hide="" class="font-xs"></th>
                            <th data-hide="" rowspan="" colspan="3" class="font-xs text-align-center" style="background-color: #add8e6">Prep/Exec</th>
                            <th data-hide="" colspan="3" class="font-xs text-align-center" style="background-color: #f5f5f5">Review</th>
                            <th data-hide="" colspan="3" class="font-xs text-align-center" style="background-color: #add8e6">Update</th>
                            <th data-hide="" colspan="3" class="font-xs text-align-center" style="background-color: #f5f5f5">Sign Off</th>
                            <th data-hide="" class="font-xs"></th>
                            <th data-hide="" class="font-xs"></th>
                        </tr>
                        <tr class="font-xs">
                            <th data-class="expand" class="font-xs">Category</th>
                            <th data-hide="" class="font-xs">Stage</th>

                            <th data-hide="" class="font-xs" style="background-color: #add8e6">Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #add8e6">Past Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #add8e6">Done</th>

                            <th data-hide="" class="font-xs" style="background-color: #f5f5f5">Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #f5f5f5">Past Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #f5f5f5">Done</th>

                            <th data-hide="" class="font-xs" style="background-color: #add8e6">Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #add8e6">Past Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #add8e6">Done</th>

                            <th data-hide="" class="font-xs" style="background-color: #f5f5f5">Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #f5f5f5">Past Due</th>
                            <th data-hide="" class="font-xs" style="background-color: #f5f5f5">Done</th>
                            <th data-hide="" class="font-xs">Total</th>
                            <th data-hide="" class="font-xs">Baseline Total</th>
                        </tr>
                        </thead>
                        <tbody>

                        {{-- Insert For each to populate table--}}
                        @for($i=0;$i<sizeof($tableData);$i++)

                            <tr onclick="showUpdateModelModal('/get-design-doc-details/1')" class="text-align-center">
                                <td class="text-left">{{$tableData[$i][0]}}</td>
                                <td>{{$tableData[$i][1]}}</td>

                                {{-- Step 1--}}
                                @if((int)$tableData[$i][16] === 1)

                                    <td style="background-color: #add8e6">{{$tableData[$i][2]}}</td>
                                    <td style="background-color: #add8e6">{{$tableData[$i][3]}}</td>
                                    <td style="background-color: #add8e6">{{$tableData[$i][4]}}

                                {{--@if($tableData[$i][2] > 0)<span
                                            style="font-size: 50%">&nbsp; ({{round($tableData[$i][3] / $tableData[$i][2] * 100,1)}}%)</span>@endif--}}
                                </td>
                                @else
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                @endif

                                {{-- Step 2--}}
                                @if((int)$tableData[$i][17] === 1)
                                    <td style="background-color: #f5f5f5">{{$tableData[$i][5]}}</td>
                                    <td style="background-color: #f5f5f5">{{$tableData[$i][6]}}</td>
                                    <td style="background-color: #f5f5f5">{{$tableData[$i][7]}}
                                    {{--@if($tableData[$i][4] > 0)<span
                                                style="font-size: 50%">&nbsp; ({{round($tableData[$i][5] / $tableData[$i][4] * 100,1)}}%)</span>@endif--}}</td>
                                @else
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                @endif

                                {{-- Step 3--}}
                                @if((int)$tableData[$i][18] === 1)
                                <td style="background-color: #add8e6">{{$tableData[$i][8]}}</td>
                                    <td style="background-color: #add8e6">{{$tableData[$i][9]}}</td>
                                <td style="background-color: #add8e6">{{$tableData[$i][10]}}
                                {{--@if($tableData[$i][6] > 0)<span
                                            style="font-size: 50%">&nbsp; ({{round($tableData[$i][7] / $tableData[$i][6] * 100,1)}}%)</span>@endif--}}</td>
                                @else
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                @endif


                                {{-- Step 4--}}
                                @if((int)$tableData[$i][19] === 1)
                                    <td style="background-color: #f5f5f5">{{$tableData[$i][11]}}</td>
                                <td style="background-color: #f5f5f5">{{$tableData[$i][12]}}</td>
                                <td style="background-color: #f5f5f5">{{$tableData[$i][13]}}
                                {{--@if($tableData[$i][8] > 0)<span
                                            style="font-size: 50%">&nbsp; ({{round($tableData[$i][9] / $tableData[$i][8] * 100,1)}}%)</span>@endif--}}</td>
                                @else
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                    <td style="background-color: lightgray">N/A</td>
                                @endif


                                <td style="background-color: lightcyan">{{$tableData[$i][14]}}</td>
                                <td style="background-color: lightcyan">{{$tableData[$i][15]}}</td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




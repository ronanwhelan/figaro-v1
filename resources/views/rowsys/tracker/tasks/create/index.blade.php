@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 class="panel-title">Add Task</h2>
                    </div>
                    <div class="panel-body">

                        <form id="add-task-form" method="post">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Area</label>
                                        <select class="form-control selectpicker" name="area" id="area" data-style="btn-primary"
                                                onchange="getFilteredSystemsByArea()">
                                            <option value="0"></option>
                                            @foreach($areas as $area)
                                                <option value="{{$area->id}}"> {{$area->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pad-20">
                                        <label class="control-label">System</label>
                                        <select class="form-control selectpicker systems"  data-live-search="true" multiple name="system[]" id="system" data-style="btn-primary">
                                            <option value="0"></option>
                                            @foreach($systems as $system)
                                                <option value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <p class=" hide lead"> Select Task Type and Template</p>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Owner
                                        </label>
                                        <select class=" form-control selectpicker" name="taskGroup"
                                                id="group" data-style="btn-primary"
                                                onchange="getAddTaskFilteredTypesByGroup()">
                                            <option></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}
                                                   </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pad-20">
                                        <label class="control-label">Category</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="taskType"
                                                id="type" data-style="btn-primary"
                                                onchange="getAddTaskFilteredSubTypeByType()">
                                            <option></option>
                                            @foreach ($taskTypes as $taskType)
                                                <option value="{{$taskType->id}}">{{$taskType->short_name}}, {{$taskType->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pad-20">
                                        <label class="control-label">Sub Category</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="taskSubType"
                                                id="sub-type" data-style="btn-primary">
                                            <option value="1"></option>
                                            @foreach ($taskSubTypes as $taskSubType)
                                                <option value="{{$taskSubType->id}}">{{$taskSubType->name}}, {{$taskSubType->description}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label>Add On Text</label>
                                        <input type="text" class="form-control" name="numberAddOn" placeholder="text is added to the task number" id="number-add-on"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pad-20">
                                        <label>Add On Text Description</label>
                                        <input type="text" class="form-control"  placeholder="text is added to the task description" name="numberAddOnDescription" id="number-add-on-description"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Document Number</label>
                                        <input type="text" class="form-control" name="documentNumber" id="document-number" placeholder="is there a linked document number?"/>
                                    </div>
                                </div>
                                <span id="helpBlock" class=" hide txt-color-orange font-xs help-block">Add text to the end of the task number</span>
                            </div>

                            <p class="lead hide"> Stage</p>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Stage
                                        </label>
                                        <select class=" form-control selectpicker" name="stage"
                                                id="stage-dropdown" data-style="btn-primary"
                                                onchange="">
                                            @foreach ($stages as $stage)
                                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pad-20">
                                        <label class="control-label">Target Hours</label>
                                        <input type="number" class="form-control" name="assignedHours" id="assigned-hours" value="0"/>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Responsible Team</label>
                                        <select class="form-control selectpicker" name="responsible_team" id="responsible_team" data-style="btn-primary">
                                            <option value=""></option>
                                            @foreach($areas as $area)
                                                <option value="{{$area->id}}"> {{$area->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pad-20">
                                        <label class="control-label">Schedule Link Number
                                        </label>

                                        <input type="text" class="form-control" id="schedule-number-input" name="scheduleNumberInput" value=""
                                               placeholder="Start typing for suggestions">
                                        <p class="txt-color-orange font-xs"></p>
                                        <span id="helpBlock" class="txt-color-orange font-xs help-block">Choose a link to a milestone in the schedule to set the task target date</span>

                                        <input type="hidden" value="" name="scheduleNumber" id="schedule-number">

                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Date Choice
                                        </label>
                                        <select class=" form-control selectpicker" name="scheduleDateChoice"
                                                id="schedule-date-choice" data-style="btn-primary"
                                                onchange="">
                                            <option value="start">Use Start date</option>
                                            <option value="finish">Use Finish Date</option>

                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                        <label class="control-label">Lead/Lag (+/- days)</label>
                                        <input type="text" class="form-control" id="schedule-date-lag" name="scheduleDateLag" value="0">

                                    </div>

                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="" id="target-dates-section">
                                            <div class="" id="target-dates-sub-section">
                                                <!-- Section is updated with AJAX call - to get Dates -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="">

                            <p class="lead"> Task Number and Description</p>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4 id="display-task-number" class="text-primary">Task Number</h4>
                                        <h4 id="display-task-description" class="text-info">Task Description</h4>
                                        <input type="hidden" class="form-control" name="taskNumber" id="task-number"/>
                                        <input type="hidden" class="form-control" name="taskDescription" id="task-description"/>

                                        <div class="alert alert-warning hide" id="task-exists-section" role="alert"><i class="fa fa-info-circle"></i> Task Number Already Exists
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </div>
                            <div class="form-group hide">
                                <div class="form-group">
                                    <label class="control-label">Add a Note:</label>
                                    <textarea class="form-control" name="note" id="note" rows="3"></textarea>
                                </div>
                            </div>

                            <!-- SERVER AND VALIDATION ERRORS -->
                            <div class="alert alert-info fadeIn hide " id="validation-errors"></div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <button class="btn btn-success btn-lg btn-block" type="button" name="btn-add-new-task"
                                                id="btn-add-new-task"
                                                onclick="addNewTask()">
                                            <i class="fa fa-plus "></i>
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- .col-lg-12 -->
        </div>
        <!-- .row -->
    </div>
@stop

@section ('local_scripts')
    <script src="/js/rowsys/tracker/tasks/general.js"></script>

    <!-- variables need to populate dropdowns
     ****  used to filter the dropdowns when an Area or Task Group is selected **** -->
    <script>

        $(document).ready(function () {
            $('#target-dates-section').hide();
            $('#enter-target-dates-section').hide();
        });

        $('#add-task-form').change(function () {
            console.log('formed changed');
            //Populate the Number Field
            var systemNumber = $('#system option:selected').text();
            var systemTag = systemNumber.split(",");

            var stageNum = $('#stage-dropdown option:selected').text();
            var stageName = stageNum.split(",");

            var taskTypeNum = $('#type option:selected').text();
            var typeName = taskTypeNum.split(",");
            var taskTemplateNum = $('#sub-type option:selected').text();
            var typeTemplateName = taskTemplateNum.split(",");
            var addonText = $('#number-add-on').val();
            var addonDescription = $('#number-add-on-description').val();
            var scheduleNumber = $('#schedule-number-input').val();
            if(scheduleNumber === ''){
                $('#schedule-number').val('');
                //$("#target-dates-section").html(result).fadeIn(1500);
                $("#target-dates-section").fadeOut(1500);
            }
            //getTaskSchedulerDates(scheduleNumber);

            //var taskTypeNum = jsonSearchGetObjects(JSON.parse(taskTypes), 'id', type.val())[0].short_name;

            var type = $('#task-type-dropdown');

            //Populating the Number for display and Hidden Input
            var number = $('#task-number');
            var numberText = $('#display-task-number');
            var numberDescription = $('#task-description');
            var numberDescriptionDisplay = $('#display-task-description');
            var dot1 = '';
            var dot2 = '';
            var dot3 = '';
            if (typeName[0] !== '' && systemTag[0] !== '')dot1 = '.';
            if (typeTemplateName[0] !== '')dot2 = '.';
            if (addonText !== '')dot3 = '.';

            var completeNumber = '';
            var completeNumberDescription = '';


/*            if (systemTag[0] !== '') {
                completeNumber = systemTag[0];
                completeNumberDescription = systemTag[1];
            }*/


            if (stageName[0] !== '') {
                var stageShortName = "PREP";
                if(stageName[0] === 'Execution'){
                    stageShortName = "EXEC";
                }
                completeNumber = stageShortName;
                completeNumberDescription = stageName[0];
               // completeNumber = completeNumber + dot1 + stageShortName;
               // completeNumberDescription = completeNumberDescription + ' ' + stageName[0];
            }

            if (systemTag[0] !== '') {
               // completeNumber = systemTag[0];
                //completeNumberDescription = systemTag[1];
                completeNumber = completeNumber + dot1 + systemTag[0];
                completeNumberDescription = completeNumberDescription + ' ' + systemTag[1];
            }

            if (typeName[0] !== '') {
                completeNumber = completeNumber + dot1 + typeName[0];
                completeNumberDescription = completeNumberDescription + ' ' + typeName[1];
            }
            if (typeTemplateName[0] !== '') {
                completeNumber = completeNumber + dot2 + typeTemplateName[0];
                completeNumberDescription = completeNumberDescription + ' ' + typeTemplateName[1];
            }
            if (addonText !== '') {
                completeNumber = completeNumber + dot3 + addonText;
                completeNumberDescription = completeNumberDescription + ' ' + addonDescription;
            }
            numberText.text(completeNumber);
            number.val(completeNumber);
            numberDescriptionDisplay.text(completeNumberDescription.trim());
            numberDescription.val(completeNumberDescription.trim());

            //Check if the Number already exists
            var stageId = $('#stage-dropdown option:selected').val();
            checkIfTaskNumberExists(completeNumber,stageId);

        });

        //The Design Data to be populated on the Form
        var systems = <?php echo json_encode($systems); ?>;
        systems = JSON.stringify(systems);
        systems = JSON.parse(systems);


        //The Design Data to be populated on the Form
        var taskTypes = <?php echo json_encode($taskTypes); ?>;
        taskTypes = JSON.stringify(taskTypes);
        taskTypes = JSON.parse(taskTypes);

        //var systems = '<?php  //echo json_encode($systems) ; ?>';
        //var taskTypes = '<?php //echo $taskTypes; ?>';





    </script>


@stop

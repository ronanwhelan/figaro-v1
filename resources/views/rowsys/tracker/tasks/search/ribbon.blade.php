<div id="search-ribbon" class="">

    <div class="panel-group smart-accordion-default" id="search-accordion">

        <div class="panel panel-default">
        
            <div class="panel-heading">
                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#search-accordion" href="#collapseSearch">Search <i class="fa fa-lg fa-angle-down"></i> <i class="fa fa-lg fa-angle-up "></i> </a></h4>
            </div>
            <div id="collapseSearch" class="panel-collapse collapse">
                <div class="panel-body no-padding">

                    <div class="well no-padding font-xs" style="background-color:#f5f5f5">

                        <div id="myTabContent1" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s100">
                                <div class="row">

                                    <div class="col-lg-12">
                                        <p class="text-align-center font-xs"><strong>Area</strong></p>
                                        <div class="form-group">
                                            <select multiple style="width:100%" class="select2">
                                                @foreach ($areas as $area)
                                                    <option value="{{$area->id}}">{{$area->name}}, {{$area->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <p class="text-align-center font-xs"><strong>System</strong></p>
                                        <select multiple style="width:100%" class="select2">
                                            @foreach ($systems as $system)
                                                <option value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class=" col-xs-offset-2 col-xs-8  col-sm-offset-2 col-sm-8   col-md-offset-0  col-md-1  col-lg-offset-0 col-lg-1 ">
                                        <p class="text-align-center font-xs"><strong>Group</strong></p>
                                        <select class=" form-control" data-live-search="true" name="task-group-dropdown" id="task-group-dropdown"
                                                onchange="populateTaskTypeDropDown(this.value)">
                                            <option></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}, {{$group->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class=" col-xs-offset-2 col-xs-8  col-sm-offset-2 col-sm-8   col-md-offset-0  col-md-2  col-lg-offset-0 col-lg-2 ">
                                        <p class="text-align-center font-xs"><strong>Task Type</strong></p>
                                        <select class="form-control" data-live-search="true" name="task-type-dropdown" id="task-type-dropdown" style="">
                                            <option></option>
                                            @foreach ($taskTypes as $taskType)
                                                <option value="{{$taskType->id}}">{{$taskType->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class=" col-xs-offset-2 col-xs-8  col-sm-offset-2 col-sm-8   col-md-offset-0  col-md-1  col-lg-offset-0 col-lg-1 ">
                                        <p class="text-align-center font-xs"><strong>Stage</strong></p>
                                        <select class=" form-control" data-live-search="true" name="system_id" id="system_id">
                                            <option></option>
                                            @foreach ($stages as $stage)
                                                <option value="{{$stage->id}}">{{$stage->name}}, {{$stage->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class=" col-xs-offset-2 col-xs-8  col-sm-offset-2 col-sm-8   col-md-offset-0  col-md-1  col-lg-offset-0 col-lg-1 ">
                                        <p class="text-align-center font-xs"><strong>Date</strong></p>
                                        <input onclick="showStageTaskDateFilter()" id="task-search-date" class="form-control input-sm font-xs">
                                    </div>


                                    <div class=" col-xs-offset-2 col-xs-8  col-sm-off set-2 col-sm-8   col-md-offset-0  col-md-1  col-lg-offset-0 col-lg-1 ">
                                        <br>
                                        <button class="btn btn-success  fa fa-search  "> Search</button>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="s101"></div>
                            <div class="tab-pane fade" id="s102"></div>
                            <div class="tab-pane fade" id="s103"></div>
                        </div>

                    </div>

                    <div class="panel panel-default hide">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                                    Stats&nbsp;&nbsp;&nbsp; <i class="fa fa-lg fa-angle-down "></i> <i class="fa fa-lg fa-angle-up "></i> </a></h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non
                                cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
                                aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel panel-default ">
            <div class="panel-heading">
                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed">Search Result Stats&nbsp;&nbsp;&nbsp; <i class="fa fa-lg fa-angle-down "></i> <i class="fa fa-lg fa-angle-up"></i> </a></h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <div id="search-stats">
                        <table class="table">
                            <tr>
                                <th>Total</th>
                                <th>Complete</th>
                                <th>Past Due</th>
                                <th>Due This Week</th>
                                <th>Due Within 2 Weeks</th>
                            </tr>

                            <tr>
                                <td><h2><small></small> <span class="label label-primary font-sm">23446</span></h2></td>
                                <td><h3><small> </small><span class="label label-success font-sm">23446</span></h3></td>
                                <td><h3><small></small> <span class="label label-danger font-sm">234</span></h3></td>
                                <td> <h3><small></small> <span class="label label-info font-sm">23446</span></h3></td>
                                <td> <h3><small></small> <span class="label label-info font-sm">23446</span></h3></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>


<!-- END RIBBON -->

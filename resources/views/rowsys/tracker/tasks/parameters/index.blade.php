@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')

@stop


@section('content')

    <!-- MAIN PANEL -->
    <div id="main" role="main">
        <!-- ==========================RIBBON ========================== -->
        <!-- Ribbon - clear local content etc -->
        <!-- MAIN CONTENT -->
        <div id="content">
            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row">

                    <div class="alert alert-info alert-dismissible " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-info-circle"></i> <strong>Info:</strong>
                        Use the check buttons to  select which tasks are to be updated.
                        Select the <strong>'Show Update Form '</strong> button to view the update parameters form.

                    </div>
                    <button class="btn btn-info "  type="button" id="" onclick="showSearchModal('parameters')"><i class="fa fa-search"></i> Search</button>
                    <button class="btn btn-primary" disabled type="button" id="show-update-modal-btn" onclick="showUpdateModal()"> No Tasks Selected</button>

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
                         data-widget-deletebutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Task Rules </h2>
                        </header>
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>

                            <form action="{{ url('update-task-hrs') }}" method="POST" id="update-selected-tasks-form" name="update-parameters-forms" class="">
                                {{ csrf_field() }}
                                <!-- widget content -->
                                <div  id="table-section" class="widget-body no-padding" style="opacity:.05;">
                                        <table id="parameter_list_table" class="table table-striped table-bordered" width="100%" >
                                            <thead>

                                            <tr>
                                                <th><input type="checkbox" name="select_all" class="font-xl" value="1"
                                                           id="select-all"></th>
                                                <th data-class="expand" class="font-sm">Task Number</th>
                                                <th data-hide="phone,tablet,computer" class="font-sm">Step info</th>
                                                <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                                <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                                <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                                <th data-hide="phone,tablet,computer" class="font-xs">Task Type</th>
                                                <th data-hide="phone,tablet,computer" class="font-xs">Stage</th>

                                                <th data-hide="phone,tablet,computer" class="font-xs">Target Date</th>


                                                <th data-hide="phone" class="font-sm">Target Hours</th>
                            {{--                    <th data-hide="phone" class="font-sm">Added Days</th>--}}
                                                <th data-hide="phone" class="font-sm">Lock Date</th>

                                                <th data-hide="phone" class="font-sm">Schedule Link</th>
                                                <th data-hide="phone" class="font-sm">Linked Date</th>
                                                <th data-hide="phone" class="font-sm">Schedule Import Lag</th>
                                                <th data-hide="phone" class="font-sm">Team</th>
                                                <th data-hide="phone" class="font-sm">User Res</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($tasks as $task)
                                                <tr class="font-xs">
                                                    <td class="text-align-center" ><input type="checkbox" class="font-xl" name="id[]" value="{{$task->id}}"></td>


                                                    <td id="{{$task->number}}"><strong>{{$task->number}}</strong>
                                                    <br>{{$task->description}}
                                                        <br><i class="fa fa-key"> {{$task->stage->name}}</i>
                                                        <br><strong>{{$task->system->tag}}</strong> ({{$task->system->description}})
                                                        <br>Target Date: {{$task->prim_date->toFormattedDateString()}}
                                                    </td>


                                                    <td class="font-xs"><strong> {{$task->getTaskNextStep()}}</strong>
                                                        <small> is due by  </small>
                                                        <strong>   {{$task->next_td->toFormattedDateString()}}</strong></td>


                                                    <td>{{$task->area->name}}</td>

                                                    <td><strong>{{$task->system->tag}}</strong> ({{$task->system->description}})</td>

                                                    <td>{{$task->group->description}} ({{$task->group->name}})</td>


                                                    <td>{{$task->taskType->name}} ({{$task->taskType->short_name}})</td>

                                                    <td>{{$task->stage->name}}</td>

                                                    <td>{{$task->prim_date->toFormattedDateString()}}</td>

                                                    <td>{{$task->target_val}}</td>

                                                 {{--   <td>{{$task->lag_days}}</td>--}}

                                                    @if($task->lock_date === 1)
                                                        <td>Yes</td>
                                                    @else
                                                        <td>No</td>
                                                    @endif

                                                    <td id="{{$task->id}}-schedule-link">
                                                        @if(isset($task->scheduleNumber->number))
                                                            {{$task->scheduleNumber->number}}
                                                            <div class="font-xs font-xs">{{$task->scheduleNumber->description}}</div>
                                                        @else
                                                            <div style="color: red">
                                                                ERROR <i class="fa fa-exclamation-circle"></i><br>
                                                                {{$task->schedule_number}}
                                                            </div>
                                                        @endif
                                                    </td>

                                                    <td>{{$task->schedule_date_choice}}</td>
                                                    <td>{{$task->schedule_lag_days}}</td>

                                                    <td>   @if(isset($task->groupOwner->name))
                                                            {{$task->groupOwner->name}}
                                                        @else
                                                            Unknown!
                                                        @endif
                                                    </td>

                                                    <td id="{{$task->id}}-doc-num">
                                                        @if( ! is_null($task->assigned_user_id ) && $task->assigned_user_id !== "" && $task->assigned_user_id !== 0 )
                                                            {{$task->assignedUser['name']}}  {{$task->assignedUser['surname']}}
                                                        @else
                                                            NOT ASSIGNED
                                                        @endif
                                                        {{--  {{$task->getAssignedUserDetails()}}--}}
                                                    </td>



                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                </div>
                                <!-- end widget content -->
                                @include('rowsys.tracker.tasks.parameters.forms.update_modal')

                            </form>
                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </div>

            </div>

            <!-- ==========================CONTENT ENDS HERE ========================== -->
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->
    <!-- Head Scripts  -->

@stop


@section ('local_scripts')
    <!-- RULES SCRIPT -->
    <script src="/js/rowsys/tracker/tasks/rules.js"></script>
    <script src="/js/rowsys/tracker/tasks/general.js"></script>

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



    <script type="text/javascript">
        pleaseWait('loading table..');

        $(document).ready(function () {
            waitDone();
            $('.select2 option').addClass('font-xs');

            $('[data-toggle="tooltip"]').tooltip();

            var responsiveHelper_parameter_list_table = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            var otable = $('#parameter_list_table').DataTable({
                //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                //fixedHeader: true,
               // "pageLength": 250,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                "scrollX": "500px",
                //"paging": false,
                responsive: true,
                //"bStateSave": true // saves sort state using localStorage
               // "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                "bPaginate": false,
                //"bStateSave": true,
               // 'columnDefs': [{'targets': 0, 'searchable': false, 'orderable': false, 'className': 'dt-body-center'}],
               // 'order': [[1, 'asc']],

                //"bStateSave": true // saves sort state using localStorage
                /*            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                 "t"+
                 "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",*/
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_parameter_list_table) {
                        responsiveHelper_parameter_list_table = new ResponsiveDatatablesHelper($('#parameter_list_table'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_parameter_list_table.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_parameter_list_table.respond();
                }

            });

            // Apply the filter
            $("#parameter_list_table thead th input[type=text]").on('keyup change', function () {
                otable.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            // Handle click on "Select all" control
            $('#select-all').on('click', function () {
                // Get all rows with search applied
                var rows = otable.rows({'search': 'applied'}).nodes();
                // Check/uncheck checkboxes for all rows in the table
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });


            // Handle click on checkbox to set state of "Select all" control
            $('#parameter_list_table tbody').on('change', 'input[type="checkbox"]', function () {
                // If checkbox is not checked
                if (!this.checked) {
                    //console.log('NOT checked');
                    var el = $('#select-all').get(0);
                    // If "Select all" control is checked and has 'indeterminate' property
                    if (el && el.checked && ('indeterminate' in el)) {
                        // Set visual state of "Select all" control
                        // as 'indeterminate'
                        el.indeterminate = true;
                    }
                }else{
                    //console.log('checked');
                }
            });

            $('#table-section').fadeTo(1500,'1');

        });//End document Ready function



        //====== ENABLE OR  DISABLE SHOW FORM BUTTON ======
        var selectAllIsSelected = false;
        $('#select-all').on('change', function () {
            if($('#select-all').is(":checked")){
                //enableShowUpdateFormButton();
                selectAllIsSelected = true;
                console.log('select-all is ON' );
            }else{
                selectAllIsSelected = false;
                console.log('select-all is OFF' + $("#update-selected-tasks-form input:checkbox:checked").length);
            }
        });
        $('#update-selected-tasks-form').on('change', function () {
            if ($("#update-selected-tasks-form input:checkbox:checked").length > 0 || $('#select-all').is(":checked") === true)
            {
                enableShowUpdateFormButton();
            }
            else{
                if(selectAllIsSelected){
                }else{
                    disableShowUpdateFormButton();
                }
            }
        });
        var checkBoxChed = false;
        function checkAllCheckBoxStatus(){

        }
        var showUpdateModalButton = $('#show-update-modal-btn');
        function enableShowUpdateFormButton(){
            showUpdateModalButton.removeAttr('disabled').html('<i class="fa fa-pencil-square-o"></i>').text('Show Update Form ');
            showUpdateModalButton.append(' <i class="fa fa-pencil-square-o"></i>');
        }
        function disableShowUpdateFormButton(){
            showUpdateModalButton.text('No Tasks Selected ');
            showUpdateModalButton.attr('disabled' , 'disable');
        }


        function showIds() {
            console.log('show ids');
            var form = $('#frm-example');
            var table = $('#parameter_list_table');
            // Iterate over all checkboxes in the table
            table.$('input[type="checkbox"]').each(function () {
                // If checkbox doesn't exist in DOM?? Change to exists
                if ($.contains(document, this)) {

                    // If checkbox is checked
                    if (this.checked) {
                        console.log('checked');
                        // Create a hidden element
                        //$(form).append($('<input>').attr('type', 'text').attr('name', this.name).val(this.value));
                    }
                }else{
                    console.log('None checked');
                }
            });
        }


        // ====== OLD TEST FUNCTIONS =======
        // Handle form submission event
        $('#frm-example').on('submit', function (e) {
            var form = this;
            // Iterate over all checkboxes in the table
            $('#parameter_list_table').$('input[type="checkbox"]').each(function () {
                // If checkbox doesn't exist in DOM
                if (!$.contains(document, this)) {
                    // If checkbox is checked
                    if (this.checked) {
                        // Create a hidden element
                        $(form).append(
                                $('<input>').attr('type', 'hidden').attr('name', this.name).val(this.value)
                        );
                    }
                }
            });
        });

        function showUpdateModal(){
            if ($("#update-selected-tasks-form input:checkbox:checked").length > 0) {
                console.log('checked');
            }
            else {
                console.log('None checked');
                return;// none is checked
            }
            $.ajax({
                type: "GET",
                url: "/get-bulk-task-update-form", success: function (result) {
                    //console.log(result);
                    $('#bulk-update-form-section').html(result);
                    $(".selectpicker").selectpicker();
                    setSelectPickerFonts();
                    $('#schedule-link-section').fadeIn(1500);
                }
            });
            $('#update-parameter-modal').modal('show');
        }

        /** ===============================================================
         *
         *  Update the Task Parameters - from Update Modal
         *
         * @param formName
         * @param urlText
         * ===============================================================
         */
        function updateSelectedTasks(formName, urlText) {
            $('#update-selected-tasks-button').append('<i class="fa fa-spinner fa-spin"></i>').attr('disabled','disabled');
            var formId = '#' + formName;
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var form = $(formId);
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');
            $.ajax({
                type: 'POST',
                url: urlText,
                data: datastring,
                success: function (result) {
                    console.log(result);
                    $('#update-parameter-modal').modal('hide');
                    //check if any tasks are selected
                    if (result.indexOf("No") > -1){
                        alert('No Tasks Selected');
                    }else{
                        $.notify({
                            title: '<strong>Success!</strong><br>',
                            message: result + ""
                        },{
                            animate: {
                                enter: 'animated fadeInLeft',
                                exit: 'animated fadeOutRight'
                            },
                            type: 'success',
                            //offset: {x: 100, y: 100},
                            //placement: {from: "bottom"},
                            showProgressbar: false,
                            delay:1000
                        });

                        setTimeout(function(){
                            window.location.reload(true);
                        },1500);

                        validationSection.addClass('hide');
                    }
                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    //waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {
                    waitDone();

                    validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
                    var errorText = '';
                    var validationErrorText = ' Validation Errors';
                    var list = $("<ul class=''></ul>");
                    var contactAdminText = ' - Contact the PES Administrator';

                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li>' + val + '</li>';
                            list.append(validationErrorText);
                            console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });
        }

        function showDeleteTaskSection(){
            $('#are-you-sure-delete-task-section').removeClass('hide')

        }
        function deleteTasks(){
            var formId = '#update-selected-tasks-form';
            var urlText = '/bulk-delete-tasks';
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var form = $(formId);
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');
            $.ajax({
                type: 'POST',
                url: urlText,
                data: datastring,
                success: function (result) {
                    console.log(result);
                    $('#update-parameter-modal').modal('hide');
                    //check if any tasks are selected
                    if (result.indexOf("No") > -1){
                        alert('No Tasks Selected');
                    }else{
                        $.notify({
                            title: '<strong>Success!</strong><br>',
                            message: result + ""
                        },{
                            animate: {
                                enter: 'animated fadeInLeft',
                                exit: 'animated fadeOutRight'
                            },
                            type: 'success',
                            //offset: {x: 100, y: 100},
                            //placement: {from: "bottom"},
                            showProgressbar: false,
                            delay:1000
                        });

                        setTimeout(function(){
                            window.location.reload(true);
                        },1500);

                        validationSection.addClass('hide');
                    }
                }
            });
        }
    </script>

@stop

@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    @if($group != null)
        <h2>{{$group->description}}  Group Systems</h2>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">System Information Table ( {{$groupSystemStats['system_count']}} Systems)</div>
                <div class="panel-body">

                </div>

                    <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                        <thead>
                        <tr>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>
                            <th class="hasinput"><input type="text" class="form-control" placeholder=""/></th>


                        </tr>
                        <tr>
                            <th data-class="expand">System</th>
                            <th data-hide="">% Target</th>
                            <th data-hide="">% Actual</th>
                            <th data-hide="">Task Count</th>
                            <th data-hide="">% Complete</th>
                            <th data-hide="">Complete</th>
                            <th data-hide="">Late</th>


                        </tr>
                        </thead>

                        <tbody>
                        @foreach($groupSystemStats['data'] as $key => $value)
                            <tr>
                                <th scope="row" ><a href="/tasks/table">{{$value[0]['system_tag']}} , {{$value[1]['system_desc']}}</a></th>
                                <th scope="row">{{$value[2]['complete_percent']}}</th>
                                <th scope="row">{{$value[2]['complete_percent']}}</th>
                                <th scope="row">{{$value[3]['task_count']}}</th>
                                <th scope="row">{{$value[2]['complete_percent']}}</th>
                                <th scope="row"> {{$value[4]['task_complete']}}</th>
                                <th scope="row"> {{$value[5]['late_task_count']}}</th>    <th> </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

            </div>
        </div>



    @foreach($systems as $model)
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="panel panel-primary" style="height: 450px">
                    <div class="panel-heading"><h3 class="panel-title">System</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="myTabs" class="col-lg-12">
                                <h3>{{$model->description}} System</h3>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active"><a href="#tab-1-{{$model->id}}"
                                                          aria-controls="tab-1-{{$model->id}}" role="tab"
                                                          data-toggle="tab">Graph</a>
                                    </li>
                                    @if($model->stats['past_due_count'] > 0 )
                                        <li><a href="#tab-2-{{$model->id}}" aria-controls="tab-2-{{$model->id}}" role="tab" data-toggle="tab">Past Due <span
                                                        class="badge">{{$model->stats['past_due_count']}}</span></a>
                                        </li>
                                    @endif
                                    <li><a href="#tab-3-{{$model->id}}" aria-controls="tab-3-{{$model->id}}" role="tab"
                                           data-toggle="tab">Task Info</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="tab-1-{{$model->id}}">

                                        <div class="row">
                                            <div id="chart-{{$model->id}}" onclick="selectArea({{$model->id}})"></div>
                                            <div class="text-align-center" id="loading-spinner-section-{{$model->id}}">
                                                <br>
                                                <i class="fa fa-spinner fa-spin fa-4x"> </i>
                                                <br>

                                                <p> Loading Graph......</p><br><br><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <h4 class="txt-color-red text-align-center">Past
                                                Due: {{$model->stats['past_due_count']}}</h4>
                                        </div>
                                    </div>


                                    <div role="tabpanel" class="tab-pane fade" id="tab-2-{{$model->id}}">

                                        @if(!empty( $model->stats['past_due_group'] ))
                                            <!-- Table -->
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Task Type</th>
                                                    <th>Count</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($model->stats['past_due_group'] as $key => $value)
                                                    <tr>
                                                        <th>{{$key}}</th>
                                                        <td>{{$value}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>

                                            </table>
                                        @else
                                            <h4>No Late Items</h4>
                                        @endif
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="tab-3-{{$model->id}}">
                                        <ul class="list-group">
                                            <li class="list-group-item">Task Count<span
                                                        class="badge">{{$model->stats['total']}}</span></li>
                                            <li class="list-group-item">Completed<span
                                                        class="badge">{{$model->stats['completed']}}</span></li>
                                            <li class="list-group-item">Left To Do<span
                                                        class="badge">{{$model->stats['left_to_do']}}</span></li>
                                            <li class="list-group-item">Due in Two Weeks<span
                                                        class="badge">{{$model->stats['due_in_two_weeks']}}</span></li>
                                        </ul>

                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar"
                                                 aria-valuenow="{{$model->stats['done_percent']}}"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 12%;">
                                                {{$model->stats['done_percent']}}%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

                <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


    <!-- Load HIGHCHARTS -->
    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>

    <!-- Load GOOGLE CHARTS-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Load APP scritps -->
    <script src="/js/rowsys/tracker/tasks/graphs.js"></script>


    <script type="text/javascript">
        /*
         *  --------------------------------------------------
         * Load the Areas JSON - used to get the Graph Data
         * --------------------------------------------------
         */
        var systems = <?php echo  $systems ?>;
        /*
         *  --------------------------------------------------
         * Load GOOGLE Charts
         * --------------------------------------------------
         */
        //Call to the Google Charts Library
        google.charts.load('current', {packages: ['corechart', 'bar']});

        /*
         *  --------------------------------------------------
         * Document Ready
         * --------------------------------------------------
         */
        $(document).ready(function () {

            $.each(systems, function (i, item) {
                getSystemGraphData(item.id);
            });


            var responsiveHelper_datatable_fixed_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            /* END BASIC */

            /* COLUMN FILTER  */
            otable = $('#datatable_fixed_column').DataTable({
                "bFilter": true,
                "bInfo": true,
                "bLengthChange": true,
                "bAutoWidth": true,
                "bPaginate": true,
                "bStateSave": true,
                "pageLength": 100,
                'columnDefs': [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center'

                }],
                'order': [[3, 'asc']],

                //"bStateSave": true // saves sort state using localStorage
                /*            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                 "t"+
                 "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",*/
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_fixed_column) {
                        responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_datatable_fixed_column.respond();
                }

            });

            // custom toolbar
            //$("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

            // Apply the filter
            $("#datatable_fixed_column thead th input[type=text]").on('keyup change', function () {
                otable
                        .column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
            });

            // Handle click on "Select all" control
            $('#example-select-all').on('click', function () {
                // Get all rows with search applied
                var rows = otable.rows({'search': 'applied'}).nodes();
                // Check/uncheck checkboxes for all rows in the table
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });


            // Handle click on checkbox to set state of "Select all" control
            $('#datatable_fixed_column tbody').on('change', 'input[type="checkbox"]', function () {
                // If checkbox is not checked
                if (!this.checked) {
                    var el = $('#example-select-all').get(0);
                    // If "Select all" control is checked and has 'indeterminate' property
                    if (el && el.checked && ('indeterminate' in el)) {
                        // Set visual state of "Select all" control
                        // as 'indeterminate'
                        el.indeterminate = true;
                    }
                }
            });


            // Handle form submission event
            $('#frm-example').on('submit', function (e) {
                var form = this;
                // Iterate over all checkboxes in the table
                otable.$('input[type="checkbox"]').each(function () {
                    // If checkbox doesn't exist in DOM
                    if (!$.contains(document, this)) {
                        // If checkbox is checked
                        if (this.checked) {
                            // Create a hidden element
                            $(form).append(
                                    $('<input>')
                                            .attr('type', 'hidden')
                                            .attr('name', this.name)
                                            .val(this.value)
                            );
                        }
                    }
                });
            });

        });


        $(document).ajaxStop(function () {
            waitDone();
            //$( "#loading" ).hide();
        });

        /*
         *  --------------------------------------------------
         * Draw the Bar Chart
         * --------------------------------------------------
         */
        function DrawChart(target, actual, id, title) {
            chartId = id;
            chartTitle = title;
            calculateTheGraphData(target, actual);
            prepareAndDrawBarPercentChart(graphData, chartId);
        }

        /*
         *  --------------------------------------------------
         * Draw the Bar Chart WIth Target Percentage
         * --------------------------------------------------
         */
        var chart;
        function prepareAndDrawBarPercentChart(graphdata, chartId) {
            google.charts.setOnLoadCallback(drawStacked);
            function drawStacked() {
                var data = google.visualization.arrayToDataTable(graphdata);
                chart = new google.visualization.BarChart(document.getElementById(chartId));
                chart.draw(data, options);
            }
        }
        /*
         *  --------------------------------------------------
         * Select the Group by Selecting the Area Bar Chart
         * --------------------------------------------------
         */
        function selectArea(id) {
            var url = "/tasks/stats/areas/bar-graph?area=" + id;
            window.location = url;
        }
    </script>

@stop
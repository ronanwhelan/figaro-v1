@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <h2> Monthly Area Bar Hours Breakdown</h2>
            <div class="form-group">
                <select class=" form-control selectpicker" title="Area"
                        onchange="getAreaChart(this.value)" data-style="btn-primary" data-live-search="true" name="systemArea">
                    <option></option>
                    @foreach ($areas as $area)
                        <option value="{{$area->id}}">{{$area->name}}</option>
                    @endforeach
                </select>
            </div>

            @if(isset($selectedArea))
            <div class="row">
                <div class="col-lg-12">
                    <div id="barChartPanel" class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title">{{$selectedArea->name}}</h3></div>
                        <div class="panel-body">
                            <div id="bar-example"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row hide">
                <div class="panel panel-info">
                    <div class="panel-heading"><h3 class="panel-title">Hours</h3></div>
                    <div class="panel-body">

                        <table class="highchart"
                               data-graph-container-before="1"
                               data-graph-type="column"
                               style="display:none"
                               data-graph-legend-disabled="0"
                               data-graph-yaxis-1-stacklabels-enabled="1"
                               data-graph-color-1="#456"
                               data-graph-legend-layout="horizontal">
                            <caption> Hours Break Down by Group</caption>
                            <thead>
                            <tr>
                                <th>Automation</th>
                                <th data-graph-stack-group="1">Target</th>
                                <th data-graph-stack-group="1">Earned</th>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>Rr</td>
                                    <td data-graph-name=" 20">20</td>
                                    <td data-graph-name=" 25"> 25</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="row">
                    <div class="col-lg-12">
                        <h2> No Data Available for the Area</h2>
                        </div>
                    </div>
            @endif



            <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>

    <!-- Load HIGHCHARTS -->
    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>

    <!-- Load GOOGLE CHARTS-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Load APP scritps -->
    <script src="/js/rowsys/tracker/tasks/graphs.js"></script>

    <script>
        $(document).ready(function () {

            Morris.Bar({
                element: 'bar-example',
                stacked:true,
                data: <?php echo json_encode($data);?>,
                //data: [{ y: '2006', a: 100, b: 90 }, { y: '2007', a: 75,  b: 65 }, { y: '2008', a: 50,  b: 40 }, { y: '2009', a: 75,  b: 65 }, { y: '2010', a: 50,  b: 40 }, { y: '2011', a: 75,  b: 65 }, { y: '2012', a: 100, b: 90 }],
                xkey: 'm',
                ykeys: ['p', 'e'],
                labels: ['PREP', 'EXEC']
            });
        });

        function getAreaChart(area_id){
            console.log(area_id);
            //window.location
            window.location.href = "/tasks/stats/areas/monthly-bar/" + area_id;
        }
    </script>

@stop

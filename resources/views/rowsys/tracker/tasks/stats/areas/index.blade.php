@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

    <style>

        @media print {
            @page {
                margin: 15px;
            }

            body {
                margin: 20px;
            }

            .noPrint {
                display: none;
            }

            .main-header {
                display: none;
            }
        }

        .page-break {
            page-break-after: always;
        }
    </style>

@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <h2>Area Status Overview </h2>

    <?php  $loopCount = 1; ?>
    <?php  $pageBreakCount = 1; ?>

    @foreach($areas as $model)
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="panel panel-primary" style="height: 500px">
                    <div class="panel-heading"><h3> {{$model->name}} Area Status Overview</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="myTabs" class="col-lg-12">
                                <h3>Preparation Stage</h3>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active"><a href="#tab-1-{{$model->id}}-1"
                                                          aria-controls="tab-1-{{$model->id}}" role="tab"
                                                          data-toggle="tab">Graph</a>
                                    </li>
                                    @if($model->stats_gen['past_due_count'] > 0)
                                        <li><a href="#tab-2-{{$model->id}}-1" aria-controls="tab-2-{{$model->id}}-1" role="tab" data-toggle="tab">Past Due <span
                                                        class="badge">{{$model->stats_gen['past_due_count']}}</span></a>
                                        </li>
                                    @endif
                                    <li><a href="#tab-3-{{$model->id}}-1" aria-controls="tab-3-{{$model->id}}-1" role="tab"
                                           data-toggle="tab">Task Info</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="tab-1-{{$model->id}}-1">

                                        <div class="col-md-12">
                                            <br>

                                            <div class="text-align-center loading-section" id="loading-spinner-section-{{$model->id}}">
                                                <br>
                                                <i class="fa fa-spinner fa-spin fa-4x"> </i>
                                                <br>

                                                <p> Loading Graph......</p><br><br><br>
                                            </div>

                                            <div id="chart-section-prep-{{$model->id}}" onclick="viewTaskGroup({{$model->id}})" class="" style="">
                                                <canvas id="percentage-chart-canvas-prep-{{$model->id}}" width="1002" height="250"
                                                        class="percentage-chart"></canvas>
                                            </div>

                                        </div>
                                        <br>
                                        @if($model->stats_gen['past_due_count'] > 0)

                                            <div class="col-md-12">
                                                <h4 class="txt-color-red  ">Past
                                                    Due: {{$model->stats_gen['past_due_count']}}</h4>
                                            </div>
                                        @endif


                                    </div>


                                    <div role="tabpanel" class="tab-pane fade" id="tab-2-{{$model->id}}-1">

                                    @if(!empty( $model->stats_gen['past_due_group'] ))
                                        <!-- Table -->
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Task Group</th>
                                                    <th>Count</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($model->stats_gen['past_due_group'] as $key => $value)
                                                    <tr>
                                                        <th><a onclick="gotoTaskTable()">{{$key}}</a></th>
                                                        <td>{{$value}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>

                                            </table>
                                        @else
                                            <h4>No Late Items</h4>
                                        @endif
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="tab-3-{{$model->id}}-1">
                                        <ul class="list-group">
                                            <li class="list-group-item">Task Count<span
                                                        class="badge">{{$model->stats_gen['total']}}</span></li>
                                            <li class="list-group-item">Completed<span
                                                        class="badge">{{$model->stats_gen['completed']}}</span></li>
                                            <li class="list-group-item">Left To Do<span
                                                        class="badge">{{$model->stats_gen['left_to_do']}}</span></li>
                                            <li class="list-group-item">Due in Two Weeks<span
                                                        class="badge">{{$model->stats_gen['due_in_two_weeks']}}</span></li>
                                        </ul>

                                        <button type="button" onclick="gotoTaskTable()" class=" hide btn btn-info">Task Table</button>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="panel panel-primary" style="height: 500px">
                    <div class="panel-heading"><h3 class=""> {{$model->name}} Area Status Overview</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="myTabs" class="col-lg-12">
                                <h3>Execution Stage</h3>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active"><a href="#tab-1-{{$model->id}}"
                                                          aria-controls="tab-1-{{$model->id}}" role="tab"
                                                          data-toggle="tab">Graph</a>
                                    </li>
                                    @if($model->stats_exe['past_due_count'] > 0)
                                        <li><a href="#tab-2-{{$model->id}}" aria-controls="tab-2-{{$model->id}}" role="tab" data-toggle="tab">Past Due <span
                                                        class="badge">{{$model->stats_exe['past_due_count']}}</span></a>
                                        </li>
                                    @endif
                                    <li><a href="#tab-3-{{$model->id}}" aria-controls="tab-3-{{$model->id}}" role="tab"
                                           data-toggle="tab">Task Info</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="tab-1-{{$model->id}}">

                                        <div class="col-md-12">
                                            <br>
                                            <div class="text-align-center loading-section" id="loading-spinner-section-{{$model->id}}">
                                                <br>
                                                <i class="fa fa-spinner fa-spin fa-4x"> </i>
                                                <br>

                                                <p> Loading Graph......</p><br><br><br>
                                            </div>


                                            <div id="chart-section-exe-{{$model->id}}" onclick="viewTaskGroup({{$model->id}})" class="" style="">
                                                <canvas id="percentage-chart-canvas-exe-{{$model->id}}" width="1002" height="250"
                                                        class="percentage-chart"></canvas>
                                            </div>

                                        </div>
                                        @if($model->stats_exe['past_due_count'] > 0)

                                            <div class="col-md-12">
                                                <h4 class="txt-color-red ">Past
                                                    Due: {{$model->stats_exe['past_due_count']}}</h4>
                                            </div>
                                        @endif
                                    </div>


                                    <div role="tabpanel" class="tab-pane fade" id="tab-2-{{$model->id}}">

                                    @if(!empty( $model->stats_exe['past_due_group'] ))
                                        <!-- Table -->
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Task Group</th>
                                                    <th>Count</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($model->stats_exe['past_due_group'] as $key => $value)
                                                    <tr>
                                                        <th><a onclick="gotoTaskTable()">{{$key}}</a></th>
                                                        <td>{{$value}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>

                                            </table>
                                        @else
                                            <h4>No Late Items</h4>
                                        @endif
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="tab-3-{{$model->id}}">
                                        <ul class="list-group">
                                            <li class="list-group-item">Task Count<span
                                                        class="badge">{{$model->stats_exe['total']}}</span></li>
                                            <li class="list-group-item">Completed<span
                                                        class="badge">{{$model->stats_exe['completed']}}</span></li>
                                            <li class="list-group-item">Left To Do<span
                                                        class="badge">{{$model->stats_exe['left_to_do']}}</span></li>
                                            <li class="list-group-item">Due in Two Weeks<span
                                                        class="badge">{{$model->stats_exe['due_in_two_weeks']}}</span></li>
                                        </ul>

                                        <button type="button" onclick="gotoTaskTable()" class="hide btn btn-info">Task Table</button>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        @if($pageBreakCount === 2)
            @if($loopCount != sizeof($areas) )
                <div class="page-break"></div>
            @endif
            <?php  $pageBreakCount = 0; ?>
        @endif
        <?php  $pageBreakCount++; ?>
        <?php  $loopCount++; ?>
    @endforeach

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <!-- Load HIGHCHARTS -->
    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>

    <!-- Load GOOGLE CHARTS-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Load APP scritps -->
    <script src="/js/rowsys/tracker/tasks/graphs.js"></script>


    <script type="text/javascript">
        /*
         *  --------------------------------------------------
         * Load the Areas JSON - used to get the Graph Data
         * --------------------------------------------------
         */
        var areas = <?php echo $areas ?>;

        /*
         *  --------------------------------------------------
         * Document Ready
         * --------------------------------------------------
         */
        $(document).ready(function () {
            //pleaseWait('loading graph data');
            $.each(areas, function (i, item) {
                drawAreaBarCharts(item.id);
            });
        });

        $(document).ajaxStop(function () {
            waitDone();
            //$( "#loading" ).hide();
        });

        /*
         *  --------------------------------------------------
         * Select the Group by Selecting the Area Bar Chart
         * --------------------------------------------------
         */
        function viewTaskGroup(id) {
            var url = "/tasks/stats/groups/bar-graph?area=" + id;
            window.location = url;
        }

        /*
         *  --------------------------------------------------
         * GoTo Task Table
         * --------------------------------------------------
         */
        function gotoTaskTable() {
            var url = "/tasks/table";
            window.location = url;
        }

    </script>

@stop

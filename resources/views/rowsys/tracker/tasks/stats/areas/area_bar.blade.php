<div class="col-lg-4">
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"> {{$area->description}}</h3></div>
        <div class="panel-body">
            <div class="row">
                <div id="myTabs" class="col-lg-12">
                    <h3>{{$area->description}}</h3>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab"
                                              data-toggle="tab">Graph</a>
                        </li>
                        <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Past
                                Due</a>
                        </li>
                        <li><a href="#messages" aria-controls="messages" role="tab"
                               data-toggle="tab">Info</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home">


                            <div id="chart-{{$area->id}}" onclick="alert('where would you like to go next?')">
                            </div>

                            <h4 class="txt-color-red">Past Due: 345</h4>
                        </div>


                        <div role="tabpanel" class="tab-pane fade" id="profile">
                            <div class="col-lg-12"></div>

                        </div>


                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <h3>Tab 3</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

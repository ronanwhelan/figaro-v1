
@if(isset($stepStats['ownerPhaseData']))
    @if(sizeof($stepStats['ownerPhaseData']) > 0)
        <div class="card-box">
        <p> Review and Approval Stats</p>
        <table class="table table-responsive ">
            <thead class="text-primary" style="background-color: ghostwhite">
            <tr>
                <th>Owner</th>
                <th>Review</th>
                <th>Approval</th>
            </tr>
            </thead>
            <tbody>
            @for($i=0;$i < sizeof($stepStats['ownerPhaseData']);$i++)

                <tr>
                    <td><span class="">{{$stepStats['ownerPhaseData'][$i][0]}}</span></td>
                    <td><span class=" font-sm" >{{$stepStats['ownerPhaseData'][$i][1]}}</span></td>
                    <td>{{$stepStats['ownerPhaseData'][$i][2]}}</td>
                </tr>
            @endfor

            </tbody>
        </table>
            </div>
    @else
        <div class="alert alert-info" role="alert"> <p> No Review and Approval Stats for this Search</p></div>
    @endif
@endif
<form id="update-model-form" action="/update-sub-type-details" method="POST">
    {{ csrf_field() }}

        <input type="hidden" id="task_id" name="update_task_id" value="{{$sub_type->id}}">

        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name="update_name" value="{{$sub_type->name}}">
        </div>
        <div class="form-group">
            <label for="">Description</label>
            <textarea class="form-control" name="update_description" id="update_description" rows="4" cols="6">{{$sub_type->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="">Category</label>
            <select class="form-control selectpicker" id="update_parent" name="update_parent"
                    data-style="btn-primary" title="Task Category">
                <optgroup label="Task Types">
                    @foreach ($types as $type)
                        @if($type->id === $sub_type->task_type_id)
                            <option selected value="{{$type->id}}">{{$type->name}}</option>
                        @else
                            <option value="{{$type->id}}">{{$type->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updateCategoryModel('update-model-form', '/update-sub-type-details','{{$sub_type->id}}')" class="btn btn-success">Update</button>

        @if($taskCount === 0 && Auth::user()->role > 7)
            <button type="button" onclick="showModelDeleteSection({{$taskCount}})" class="btn btn-danger pull-left">Delete</button>
        @endif
    </div>
    <div id="are-you-sure-delete-section" class="hide">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
            <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                <button type="button" onclick="deleteSubCategory({{$sub_type->id}})" class="btn btn-danger"> Yes</button>
            </p>
        </div>
    </div>
</form>
<div class="alert alert-warning" role="alert">{{$taskCount}} Tasks are associated to this Test Specification</div>
<script>
    $(document).ready(function () {

    });

    function deleteSubCategory(id){
        $.ajax({
            type: 'GET',
            url: '/delete-task-sub-type/' + id, success: function (result) {
                $('#update-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });

                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

</script>



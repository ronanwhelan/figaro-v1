<form id="update-model-form" action="/update-type-details" method="POST">
    {{ csrf_field() }}

    <input type="hidden" id="task_id" name="update_task_id" value="{{$type->id}}">

    <div class="form-group">
        <label for="">Name</label>
        <input type="text" class="form-control" id="name" name="update_name" value="{{$type->name}}">
    </div>
    <div class="form-group">
        <label for="">Shot Name</label>
        <input type="text" class="form-control" id="shortName" name="update_short_name" value="{{$type->short_name}}">
    </div>
    <div class="form-group">
        <label for="">Description</label>
        <textarea class="form-control" name="update_description" id="update_description" rows="4" cols="6">{{$type->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="">Owner</label>
        <select class="form-control selectpicker" id="update_parent" name="update_parent"
                data-style="btn-primary" title="Task Owner">
            <optgroup label="Owner">
                @foreach ($groups as $group)
                    @if($group->id === $type->group_id)
                        <option selected value="{{$group->id}}">{{$group->name}}</option>
                    @else
                        <option value="{{$group->id}}">{{$group->name}}</option>
                    @endif
                @endforeach
            </optgroup>
        </select>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updateCategoryModel('update-model-form', '/update-type-details','{{$type->id}}')" class="btn btn-success">Update</button>

        @if($taskCount === 0 && Auth::user()->role > 7 )
            <button type="button" onclick="showModelDeleteSection({{$taskCount}})" class="btn btn-danger pull-left">Delete</button>
        @endif
    </div>

    <div id="are-you-sure-delete-section" class="hide">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
            <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                <button type="button" onclick="deleteCategory({{$type->id}})" class="btn btn-danger"> Yes</button>
            </p>
        </div>
    </div>

</form>

<div class="alert alert-warning font-xs" role="alert">{{$taskCount}} Tasks and {{$subCount}} Sub Categories are associated to this Category</div>

<div class="alert alert-info font-xs" role="alert">
    @if(sizeof($rules) > 0)
        Rule exist for:
        @foreach($rules as $rule)
            {{$rule->stage->name}} Stage,
        @endforeach
    @else
        No Rules Exist for this Category
    @endif</div>
<div class="row">
    <div class="col-lg-12">

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">

                        <a role="button" data-toggle="collapse" href="#rulesSection" aria-expanded="true" aria-controls="rulesSection">
                            <strong>Add a Rule <i class="fa fa-plus-circle"></i> </strong>
                            <i class="fa fa-caret-down pull-right"></i>
                        </a>
                    </h4>
                </div>

                <div id="rulesSection" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div id="add-rule-section" class="font-xs">

                            @if(sizeof($rules) !== 2)
                                <form id="add-rule-for-stage-form" action="/add-rule-for-stage" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="taskTypeId" value="{{$type->id}}">

                                    <div class="form-group">
                                        <label for="">Add a Rule for a Stage</label>
                                        <select class="form-control selectpicker" id="stage_rule" name="rule_choice"
                                                data-style="btn-primary" title="Stage">
                                            <option value="1">Preparation Stage only</option>
                                            <option value="2" >Execution Stage only</option>
                                            <option value="3" selected>Both Stages</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Add Rule</button>
                                    </div>
                                </form>
                            @else
                                <p> No Rule to add as Rules exist for both stages</p>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<p class="txt-color-orange font-xs"> Note: if you wish to delete this category, first delete all associated tasks and Sub Categories.</p>
<script>
    $(document).ready(function () {

    });

    function deleteCategory(id) {
        $.ajax({
            type: 'GET',
            url: '/delete-task-type/' + id, success: function (result) {
                $('#update-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });

                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

    function showRulesSection() {
        $('#add-rule-section').fadeIn(1500);
    }


</script>




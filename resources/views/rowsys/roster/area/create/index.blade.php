@extends ('rowsys.roster.includes.layout.layout')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="container">
        @include('rowsys.roster.area.create.form.form')
        @include('rowsys.roster.area.read.table.table1')
    </div>
    <!-- Modals-->
    @include('rowsys.roster.user.update.modal')
    <!-- ==========================CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')

    <script src="/js/plugin/bootstrap-editable/bootstrap-editable.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="/js/plugin/select2/select2.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharttable.org/master/jquery.highchartTable-min.js"></script>
    <script src="/js/plugin/chartjs/Chart.js"></script>
    <script>

        var table = $('#data_table');

        // ==== DOCUMENT READY =====
        $(document).ready(function () {


            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";

            var responsiveHelper;

            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };


            table = $('#data_table').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'excel'],
                //"sDom":"flrtip",
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 100,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                // paging: false,
                "bStateSave": true, // saves sort state using localStorage
                autoWidth: false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(table, breakpointDefinition);
                    }
                },
                rowCallback: function (nRow) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                drawCallback: function (oSettings) {
                    //responsiveHelper.respond();
                }

            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [//'excel'//'copyHtml5',//'excelHtml5',//'csvHtml5',//'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));
            table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1500, '1');
            $('#loading-box-section').remove();

            //--------------------------------------------------------
            //      TOGGLE BUTTONS
            //--------------------------------------------------------
            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var tableFilterSection = $('#data_table_filter');

            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
            //tableFilterSection.append('<div class="">Task List</div>');
            tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);

            /*
             tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
             'role="button" data-toggle="collapse" data-parent="#accordion" ' +
             'href="#search-stats" aria-expanded="true" aria-controls="search-stats">Stats ' +
             '<i class="fa fa-bar-chart-o"></i> </button>').append('&nbsp;&nbsp;');*/

            //--------------------------------------------------------
            //      EXPORT BUTTONS
            //--------------------------------------------------------
            tableFilterSection.append('&nbsp;&nbsp;').append(
                    '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                    'class="btn btn-primary dropdown-toggle" ' +
                    'data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false"> Export <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                    '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                        //'<li><a href="#">Excel</a></li> ' +
                        //'<li><a href="#">Something else here</a>' +
                    '</li> </ul> </div>').append('&nbsp;&nbsp;');

            var exportButton1 = $('#export-button-1');
            var exportButtons = $('#export-buttons');
            var copyButton = $('.buttons-copy');
            var excelButton = $('.buttons-excel');
            exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
            //exportButton2.append(excelButton);


            var tablePaginateSection = $('#data_table_paginate');
            //tablePaginateSection.css("background-color", "navy");


        });


        //Update the hours
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $('#data_table a').editable({
            validate: function (value) {
                if ($.trim(value) == '')
                    return 'Value is required.';
            },
            type: 'textarea',
            title: 'Enter Hours',
            placement: 'left',
            send: 'always',
            ajaxOptions: {
                dataType: 'json',
                type: 'post'
            }, success: function (response, newValue) {
                console.log(response);
                console.log(response.status);
                if (response.status == 'error') {
                    return response.msg; //msg will be shown in editable form
                }
            }
        });

        //==== DELETE THE USER AREA DATA ======
        var deleteId = 0;
        function deleteAreaHoursConfirmation(id) {
            deleteId = id;

            var r = confirm("Are you sure?");
            if (r == true) {
                deleteAreaHours();
            }
        }
        function deleteAreaHours() {
            $.ajax({
                type: 'GET',
                url: '/roster/area-hours/delete/' + deleteId, success: function (result) {
                    $.notify({
                        title: '<strong>Success!</strong><br>',
                        message: result.message + ""
                    }, {
                        animate: {
                            enter: 'animated fadeInLeft',
                            exit: 'animated fadeOutRight'
                        },
                        type: 'success',
                        //offset: {x: 100, y: 100},
                        //placement: {from: "bottom"},
                        showProgressbar: false,
                        delay: 900
                    });

                    setTimeout(function () {
                        location.reload();
                    }, 900);
                }
            });
        }


        //==== Draw the User Chart ======
        function drawUserHoursChart(id) {
            console.log(id);

            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                        label: '# of Votes',
                        data: [12, 19, 3, 5, 2, 3],
                        borderWidth: 1
                    }]
                },
                backgroundColor: [
                    "#FF6384",
                    "#4BC0C0",
                    "#FFCE56",
                    "#E7E9ED",
                    "#36A2EB"
                ],
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }


    </script>

@stop
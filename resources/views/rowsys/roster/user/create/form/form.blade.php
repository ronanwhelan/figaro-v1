<div class=" hide row">
    <div class="col-lg-12"><h3>User Hours by Area</h3></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">User Hours </h3></div>
            <div class="panel-body">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-check"> </i>{{ Session::get('message') }}
                    </div>
                @endif
                <form action="/roster" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="selectpicker form-control show-tick" id="user" name="user" data-style="btn-primary" data-live-search="true" data-max-options="1"
                                    title="User">
                                <optgroup label="Users">
                                    @foreach ($users as $user)
                                        @if ((int)old('user') === $user->id)
                                            <option value="{{$user->id}}" selected>{{$user->name}}  {{$user->surname}}</option>
                                        @else
                                            <option value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="form-control selectpicker"
                                    id="area" name="area"
                                    data-style="btn-primary" title="Area">
                                <optgroup label="Areas">
                                    @foreach ($areas as $area)
                                        @if ((int)old('area') === $area->id)
                                            <option value="{{$area->id}}" selected>{{$area->name}}</option>
                                        @else
                                            <option value="{{$area->id}}">{{$area->name}}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="selectpicker form-control show-tick" id="year" name="year" data-style="btn-primary" data-max-options="1" title="Year">
                                <optgroup label="years">
                                    <option value="2016" selected>2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                </optgroup>

                            </select>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="selectpicker form-control show-tick" id="month" name="month" data-style="btn-primary" data-max-options="1" title="Month">
                                <optgroup label="Months">
                                    @for($i=0;$i<sizeof($months);$i++)
                                        @if ((int)old('month') === ($i+1))
                                            <option value="{{$i + 1}}" selected>{{$months[$i]}}</option>
                                        @else
                                            <option value="{{$i + 1}}">{{$months[$i]}}</option>
                                        @endif

                                    @endfor
                                </optgroup>

                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Hrs</span>
                                <input type="number" class="form-control" id="hours" name="hours" value="10" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block"><i class="fa fa-plus"></i> Add</button>
                        </div>
                    </div>
                    @if ($errors->has())

                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>

@extends ('rowsys.roster.includes.layout.layout')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Import Schedule Dates</h3></div>
                <div class="panel-body">

                    <h2>Import Area Roster Data</h2>
                    <br>
                    <h4 class="text-primary">Choose a file to be uploaded</h4>
                    <form action="/roster/import/area" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="file" name="file" required>

                            <p class="help-block">(only csv files can be imported).</p>
                            @if (Session::has('message'))
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Info!</strong>  {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-success btn-block">Import <i class="fa fa-upload"></i></button>
                                <br>
                            </div>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Something went wrong!.<br><br>

                                <ul class="list-group">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')



@stop

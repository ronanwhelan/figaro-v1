<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <title>Rowsys</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Rowsys">
    <meta name="keywords" content="Rowsys">
    <meta name="author" content="Rowsys">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>

    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href=' https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/smartadmin-production-plugins.min.css">

</head>
<body>
<div class="container">
    <br><br>
    <table id="example" class="table table-striped table-hover table-bordered" style="font-size: 9px" cellspacing="0" width="100%">
        <thead class="font-xs">
        <tr class="font-xs">
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=" "/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=" "/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
            <th class="hasinput" ><input type="text" class="form-control font-xs" placeholder=""/></th>
        </tr>
        <tr class="font-xs">

            <th data-class="expand"  style="width: 13%" class="font-xs">Number</th>
            <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
            <th data-hide="phone,tablet,computer" class="font-xs">System</th>
            <th data-hide="phone,tablet,computer" class="font-xs">System Description</th>
            <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
            <th data-hide="phone,tablet" class="font-xs">Task Type</th>
            <th data-hide="phone,tablet" class="font-xs">Stage</th>
            <th data-hide="phone,tablet" class="font-xs">Next<br>Target<br>Date</th>
            <th data-hide="" class="font-xs"><span class="txt-color-orange">Gen/Ex<br>Status %</span></th>
            <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">Gen/Ex<br>Target<br>Date</span></th>
            <th data-hide="" class="font-xs"><span class="txt-color-blue">Review<br>Status %</span></th>
            <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-blue">Review<br>Target<br>Date</span></th>
            <th data-hide="" class="font-xs"><span class="txt-color-orange">ReIssue<br>Status %</span></th>
            <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">ReIssue<br>Target<br>Date</span></th>
            <th data-hide="" class="font-xs"><span class="txt-color-blue">SignOff<br>Status %</span></th>
            <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-blue">SignOff<br>Target<br>Date</span></th>
            <th data-hide="phone,tablet" class="font-xs">Target<br>Val (hrs)</th>
            <th data-hide="phone,tablet" class="font-xs">Earned<br>Val (hrs)</th>
            <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Date</th>
            <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Dev Days</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Office</th>
            <th>Age</th>
            <th>Start date</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
            <th>Salary</th>
        </tr>
        </tfoot>
        <tbody>
        <tr>
            <td>Tiger Nixon</td>
            <td>System Architect</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>2011/04/25</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
        </tr>
        <tr>
            <td>Garrett Winters</td>
            <td>Accountant</td>
            <td>Tokyo</td>
            <td>63</td>
            <td>2011/07/25</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
            <td>$320,800</td>
        </tr>

        </tbody>
    </table>
</div>

<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>


<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>


<script src="/js/vendor/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="/js/vendor/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="/js/vendor/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="/js/vendor/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="/js/vendor/plugin/datatable-responsive/datatables.responsive.min.js"></script>


<script>


        $(document).ready(function() {
            var responsiveHelper_mission_list_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };
            var otable = $('#example').DataTable({
                        "bFilter": true,
                        "bInfo": true,
                        "bLengthChange": true,
                        "bAutoWidth": true,
                        "bPaginate": false,
                        fixedHeader: true,
                        // "scrollY": screenHeight,
                        "paging": true,
                        //"bStateSave": true // saves sort state using localStorage
                        /*            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                         "t"+
                         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",*/
                        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                        "autoWidth": true,
                        "preDrawCallback": function () {
                            // Initialize the responsive datatables helper once.
                            if (!responsiveHelper_mission_list_column) {
                                responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#example'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_mission_list_column.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_mission_list_column.respond();
                        }
                    }
            );

            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                otable
                        .column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
            });
        } );

</script>

</body>
</html>
@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/smartadmin/smartadmin-production-plugins.min.css">
@stop

@section ('head_js')
@stop

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

        <div id="mission_list_wrapper" class="dataTables_wrapper form-inline no-footer">

            <table width="100%" role="grid" id="mission_list" class="table table-striped table-hover table-bordered dataTable no-footer has-columns-hidden" style="width="100%">

            <thead class="font-xs">
            <tr class="font-xs">
                <th class="hasinput" style="width:4%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:4%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:4%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:4%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:4%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:2%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:1%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:1%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:1%"><input type="text" class="form-control font-xs" placeholder=""/></th>
                <th class="hasinput" style="width:1%"><input type="text" class="form-control font-xs" placeholder=""/></th>
            </tr>

            <tr class="font-xs">
                <th data-class="expand" class="font-xs">Number</th>
                <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                <th data-hide="phone,tablet,computer" class="font-xs">System Description</th>
                <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                <th data-hide="phone,tablet" class="font-xs">Task Type</th>
                <th data-hide="phone,tablet" class="font-xs">Stage</th>
                <th data-hide="phone,tablet" class="font-xs">Next Target Date</th>
                <th data-hide="" class="font-xs"><span class="txt-color-orange">Gen/Ex Status %</span></th>
                <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Gen/Ex Target Date</span></th>
                <th data-hide="" class="font-xs"><span class="txt-color-blue">Review Status %</span></th>
                <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Review Target Date</span></th>
                <th data-hide="" class="font-xs"><span class="txt-color-orange">Reissue Status %</span></th>
                <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Reissue Target Date</span></th>
                <th data-hide="" class="font-xs"><span class="txt-color-blue">Sign-off Status %</span></th>
                <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Sign-off Target Date</span></th>
                <th data-hide="phone,tablet" class="font-xs">Target Val (hrs)</th>
                <th data-hide="phone,tablet" class="font-xs">Earned Val (hrs)</th>
                <th data-hide="phone,tablet,computer" class="font-xs">Base Date</th>
                <th data-hide="phone,tablet,computer" class="font-xs">Base Dev Days</th>
            </tr>
            </thead>

            <tbody id="mission-list-table-body" class="font-sm">
            <!-- table is populated by AJAX and js in js/tasks/general -->
            </tbody>

            </table>

        </div>
    </div>


    <div class="modal fade" id="single-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true" onclick="hideUpdateTaskModal()"> &times;</button>
                    <h4 class="modal-title text-align-center" id="myModalLabel">....Updating</h4>

                    <div class="modal-body">
                        <div class="row">
                            <div class=" col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xl-offset-1  col-xs-12 col-sm-12 col-md-9 col-lg-10 col-xl-10">

                                <div class="panel-group smart-accordion-default" id="accordion">

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#task-details-panel" class="collapsed">
                                                    <i class="fa fa-lg fa-angle-down pull-right"></i>
                                                    <i class="fa fa-lg fa-angle-up pull-right"></i> Task Details</a></h4>
                                        </div>

                                        <div id="task-details-panel" class="panel-collapse collapse  ">
                                            <div class="panel-body no-padding">
                                                <div id="task-details-section">
                                                    <i class="fa fa-lg fa-cog fa-spin fa-2x"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default padding-5">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
                                                    <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>Update Task Status </a></h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div id="update-task-form-section">
                                                    <i class="fa fa-lg fa-cog fa-spin fa-2x text-align-center"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="hideUpdateTaskModal()">
                            Cancel
                        </button>
                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



@stop

@section ('local_scripts')

    <script src="/js/rowsys/tracker/app/app.functions.js"></script>
    <script src="/js/rowsys/tracker/tasks/general.js"></script>

    <script src="/js/vendor/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/vendor/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/vendor/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/vendor/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/vendor/plugin/datatable-responsive/datatables.responsive.min.js"></script>



    <script>
        $(document).ready(function () {
            populateTaskTable(0);
            //refreshTaskTable();

        })
    </script>

@stop
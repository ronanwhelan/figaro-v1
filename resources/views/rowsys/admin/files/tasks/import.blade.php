@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')
@stop

@section ('head_js')
@stop


@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Import Tasks Into Database</h3>
                </div>
                <div class="panel-body">
                    <form action="import-file" method="post" enctype="multipart/form-data">

                        {!! csrf_field() !!}


                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="file" name="file" required>

                            <p class="help-block">(only csv files can be imported).</p>
                            @if (Session::has('message') && sizeof($importErrors) === 0)
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Info!</strong> {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-info">Upload File to Database <i class="fa fa-upload"></i>
                        </button>
                        <br> <br>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Something went wrong!.<br><br>

                                <ul class="list-group">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>

                        @endif
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div class="row hide" id="import-feedback-section">
        <div class="col-sm-6">
            <div class="alert alert-success" role="alert" id="">
                <h3>Compiling of all Tasks has started</h3>
                <h5>This make take some time as there are <strong>...</strong> Tasks to be updated</h5>
            </div>
        </div>
    </div>


    @if(sizeof($importErrors) > 0)
        <div class="panel panel-danger">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-exclamation-triangle"></i> oops - seems to be some errors.</h3></div>
            <div class="panel-body">
                <h2> Somethings are not quite right</h2>
                <p>Please correct the following rows in the import file</p>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Stage</th>
                        <th>Area</th>
                        <th>System</th>
                        <th>Category Group</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Team</th>
                        <th>Schedule Link</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($importErrors as $importError)
                        <tr>
                            <td>Row: <span class="label label-primary"> {{$importError->id + 1}}</span></td>
                            <td>@if((int)$importError->stage_id === 0)<span class="label label-danger"> <i class="fa fa-times"></i> Stage</span> @else <i
                                        class="fa fa-check"></i> @endif</td>
                            <td>@if((int)$importError->area_id === 0)<span class="label label-danger"> <i class="fa fa-times"></i> Area</span> @else <i
                                        class="fa fa-check"></i> @endif</td>
                            <td>@if((int)$importError->system_id === 0)<span class="label label-danger"> <i class="fa fa-times"></i> System</span> @else <i
                                        class="fa fa-check"></i> @endif</td>
                            <td>@if((int)$importError->group_id === 0)<span class="label label-danger"> <i class="fa fa-times"></i> Group</span> @else <i
                                        class="fa fa-check"></i> @endif</td>
                            <td>@if((int)$importError->task_type_id === 0)<span class="label label-danger"> <i class="fa fa-times"></i> Category</span> @else <i
                                        class="fa fa-check"></i> @endif</td>
                            <td>@if((int)$importError->task_sub_type_id === 0)<span class="label label-danger"> <i
                                            class="fa fa-times"></i> Sub Category</span> @else <i class="fa fa-check"></i> @endif</td>
                            <td>@if((int)$importError->group_owner_id === 0)<span class="label label-danger"> <i class="fa fa-times"></i> Team</span> @else <i
                                        class="fa fa-check"></i> @endif</td>
                            <td>@if($importError->schedule_link === 'no')<span class="label label-danger"> <i
                                            class="fa fa-times"></i> {{$importError->schedule_number}}</span> @else <i class="fa fa-check"></i> @endif</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @endif

    @if (Session::has('message') && sizeof($importErrors) === 0)
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6" id="report-panel">
                    <div class="row ">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Import Report <i class="fa fa-table"></i></h3>
                            </div>
                            <div class="panel-body">
                                <h3 class="text-info"> View Tasks to be imported in table below</h3>

                                <table class="table " id="report-table" style="text-align: left ">

                                    <tbody class="txt-color-blue">
                                    <tr>
                                        <td>Total Number of tasks that will be added</td>
                                        <td><span class="label label-success">{{$totalNumOfNewTasks}}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Number of unique tasks (i.e unique task number)</td>
                                        <td><span class="label label-success">{{$numOfNewTasks}}</span></td>
                                    </tr>
                                    <tr class="hide">
                                        <td>Num of tasks in Import table with no date</td>
                                        <td><span class="label label-danger">{{$numOfTasksWithNoDate}}</span></td>
                                    </tr>
                                    <tr class="hide">
                                        <td>Number of new Areas to be added</td>
                                        <td><span class="label label-primary">{{$numOfNewAreas}}</span></td>
                                    </tr>
                                    <tr class="hide">
                                        <td>Number of new Systems to be added</td>
                                        <td><span class="label label-primary">{{$numOfNewSystems}}</span></td>
                                    </tr>
                                    <tr class="hide">
                                        <td>Number of new Task Groups to be added</td>
                                        <td><span class="label label-danger">{{$numOfNewGroups}}</span></td>
                                    </tr>
                                    <tr class="hide">
                                        <td>Number of new Task Types to be added</td>
                                        <td><span class="label label-danger">{{$numOfNewTaskTypes}}</span></td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <br>
                                            <button type="button" class="btn btn-success btn-block" id="btn-add-everything"
                                                    onclick="addEverything()">Import New Tasks <i
                                                        id="import-spinner"
                                                        class="fa  "></i>
                                            </button>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <small class=" ">Note: Importing and Compiling of all tasks may take a short time to complete.</small>

                                <div class="alert alert-success hide" role="alert" id="import-all-alert"></div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="col-md-12" id="">
                    <div class="row ">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Overview of Tasks to be imported <i class="fa fa-table"></i></h3>
                            </div>
                            <div class="panel-body">
                                <!-- Table -->
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                    <tr style="font-size: 40%">
                                        <th>#</th>
                                        <th>Number</th>
                                        <th>Add On Details</th>
                                        <th>Stage</th>
                                        <th>Area</th>
                                        <th>System</th>
                                        <th>Category Group</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th>Team</th>
                                        <th>Schedule Link</th>
                                        <th>Choice</th>
                                        <th>Lead/Lag</th>
                                        <th>Target Hrs</th>
                                        <th>Document</th>

                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if( isset($importLists) && sizeof($importLists) > 0)
                                        @foreach($importLists as $importList)
                                            <tr style="font-size: 50%">
                                                <td>{{$importList->id}}</td>
                                                <td>{{$importList->number}}</td>
                                                <td>{{$importList->add_on}}<br>
                                                    {{$importList->add_on_desc}}</td>
                                                <td>{{$importList->stage->name}}</td>
                                                <td>{{$importList->area->name}}</td>
                                                <td>{{$importList->system->tag}}</td>
                                                <td>{{$importList->group->name}}</td>
                                                <td>{{$importList->taskType->name}}</td>
                                                <td>{{$importList->taskSubType->name}}</td>
                                                <td>{{$importList->groupOwner->name}}</td>
                                                <td>
                                                    @if( isset($importList->scheduleNumber))
                                                    {{$importList->scheduleNumber->number}}
                                                    <div class="font-xs font-xs">{{$importList->scheduleNumber->description}}</div>
                                                        @else
                                                        <span class="label label-danger"> <i class="fa fa-times"></i>  ERROR - CANT FIND {{$importList->schedule_number}}</span>

                                                    @endif
                                                </td>
                                                <td>{{$importList->schedule_date_choice}}</td>
                                                <td>{{$importList->schedule_lag_days}}</td>
                                                <td>{{$importList->target_val}}</td>
                                                <td>{{$importList->document_number}}</td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            @endif
            @stop

            @section ('local_scripts')

                <script>
                    function addEverything() {
                        //alert('im here');
                        $('#import-spinner').addClass('fa-spinner fa-spin');
                        $.ajax({
                            type: "GET",
                            url: "import/all-from-import-table", success: function (result) {
                                console.log(result);
                                $('#import-all-alert').html('<h2></h2>').text(result);
                                $('#import-all-alert').removeClass('hide').fadeIn(1500);
                                $('#import-spinner').removeClass('fa-spinner fa-spin');
                            }
                        });
                    }
                </script>

@stop
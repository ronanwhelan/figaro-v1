@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

<br>
<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-10">
            <h3>Send a Company email</h3>
            <p> emails are sent from the <strong>info{{ '@' }}rowsys.com</strong> email account</p>
        </div>
    </div>
<hr>
<form class="form-horizontal">
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3" placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">CC</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputPassword3" placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Subject</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputPassword3" placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Message</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="10"></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="mail-reminder/1" class="btn btn-default">Send mail</a>

        </div>
    </div>
</form>


@if (Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <strong>Info!</strong>  {{ Session::get('message') }}
    </div>
@endif
</div>

@stop

@section ('local_scripts')


@stop
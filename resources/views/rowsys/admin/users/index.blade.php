@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-sm-3"> <!-- required for floating -->
            <!-- Nav tabs -->
            <div class="panel panel-info">
                <div class="panel-heading"><h4 class="panel-title">Settings</h4></div>
                <div class="panel-body">
                    <div class="settings list-group">
                        <a href="#home-v" data-toggle="tab" class="list-group-item "><i class="fa fa-user-plus"></i>&nbsp;&nbsp;Register User</a>
                        <a href="#profile-v" data-toggle="tab" class="list-group-item"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Update User</a>
                        <a href="#company-v" data-toggle="tab" class="list-group-item"><i class="fa fa-users"></i>&nbsp;&nbsp;Company</a>
                        @if(Auth::user()->access_to_tracker === 1)
                            <a href="#messages-v" data-toggle="tab" class="list-group-item"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Tracker Roles</a>
                        @endif
                        @if(Auth::user()->access_to_punchlist === 1)
                            <a href="#messages-v" data-toggle="tab" class="hide list-group-item"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Punch List Roles</a>
                        @endif
                        <a href="#settings-v" data-toggle="tab" class=" hide list-group-item active"><i class="fa fa-plus"></i>&nbsp;&nbsp;Stats</a>
                    </div>
                    <ul class="nav nav-tabs tabs-left sideways hide">
                        <li class="active"><a href="#home-v" data-toggle="tab">Create</a></li>
                        <li><a href="#profile-v" data-toggle="tab">Update</a></li>
                        <li class="hide"><a href="#messages-v" data-toggle="tab">Roles</a></li>
                        <li class="hide"><a href="#settings-v" data-toggle="tab">Settings</a></li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="col-sm-9">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home-v">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Register User</h3></div>
                        <div class="panel-body animated fadeIn">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/add-user') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Name:</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Surname:</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="surname" value="{{ old('surname') }}">

                                        @if ($errors->has('surname'))
                                            <span class="help-block">{{ $errors->first('surname') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Email:</label>

                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Password:</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Confirm Password:</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password_confirmation">

                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Company</label>
                                    <div class="col-md-6">
                                        <select class=" form-control selectpicker" data-style="btn-primary" name="company" data-live-search="true">
                                            <option value=""></option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('company'))
                                            <span class="help-block">{{ $errors->first('company') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Role/Team</label>
                                    <div class="col-md-6">
                                        <select class=" form-control selectpicker" data-style="btn-primary" name="roles[]" multiple>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('roles'))
                                            <span class="help-block">{{ $errors->first('roles') }}</span>
                                        @endif
                                    </div>
                                </div>


                                @if(Auth::user()->access_to_tracker === 1)
                                    <hr>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label"> Tracker Access</label>

                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="access_to_tracker">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tracker Access Level</label>

                                        <div class="col-md-6">
                                            <select class=" form-control selectpicker" data-style="btn-primary" name="role">
                                                @for($i = 1;$i < 9;$i++)
                                                    <option value="{{$i}}">Level {{$i}} </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                @endif
                                @if(Auth::user()->access_to_punchlist === 1)
                                    <hr>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Punchlist Access</label>

                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="access_to_punchlist">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Punchlist Access Level</label>

                                        <div class="col-md-6">
                                            <select class=" form-control selectpicker" data-style="btn-primary" name="punchlist_role">
                                                @for($i = 1;$i < 9;$i++)
                                                    <option value="{{$i}}">Level {{$i}} </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Punchlist Filter</label>
                                        <div class="col-md-6">
                                            <select class=" form-control selectpicker" data-style="btn-primary" name="punchlist_filter">
                                                <option value=""> View All</option>
                                                <option value="1"> Responsible Company Only</option>
                                                <option value="2"> Assigned Company Only</option>
                                            </select>
                                        </div>
                                    </div>

                                @endif

                                <hr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Allow Access</label>

                                    <div class="col-md-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="confirmed" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-user"></i> Register</button>

                                        @if (Session::has('message'))
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <i class="fa fa-check">&nbsp; </i>{{ Session::get('message') }}
                                            </div>
                                        @endif


                                        @if ($errors->has() )
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="profile-v">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Update User</h3></div>
                        <div class="panel-body animated fadeIn">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Users</label>

                                    <div class="col-sm-10">
                                        <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                name="systemArea">
                                            <option></option>
                                            @foreach ($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}} {{$user->surname}}
                                                    , {{$user->email}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <form class="form-horizontal" role="form" id="update-user-details-form" method="POST" action="{{ url('/admin/update-user') }}">
                                    {{ csrf_field() }}
                                    <div id="user-details-section"></div>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="tab-pane" id="company-v">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Add New Company</h3></div>
                        <div class="panel-body animated fadeIn">
                            <div class="row pad-20">
                                <div class="col-lg-12">
                                    <form class="form-horizontal" id="add-new-company-form" role="form" method="POST" action="{{ url('/admin/add-company') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Company Name:</label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="name" value="">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="button" onclick="addNewCompany('add-new-company-form', '/admin/add-company')"
                                                        class="btn btn-primary"><i class="fa fa-btn fa-plus-circle"></i> Add
                                                </button>

                                                <!-- SERVER AND VALIDATION ERRORS -->
                                                <div class="alert alert-info fadeIn hide  " id="add-company-validation-errors"></div>

                                                @if (Session::has('company_message'))
                                                    <div class="alert alert-success alert-dismissible" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <i class="fa fa-check">&nbsp; </i>{{ Session::get('company_message') }}
                                                    </div>
                                                @endif


                                                @if ($errors->has() )
                                                    <div class="alert alert-danger">
                                                        @foreach ($errors->all() as $error)
                                                            {{ $error }}<br>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Update Company</h3></div>
                        <div class="panel-body animated fadeIn">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Company</label>

                                <div class="col-md-6">
                                    <select class=" form-control selectpicker" data-style="btn-primary" onchange="getCompanyDetails(this.value)" name="company"
                                            data-live-search="true">
                                        <option value=""></option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('company'))
                                        <span class="help-block">{{ $errors->first('company') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-sm-offset-3 col-sm-8">
                                <form class="form-horizontal" role="form" id="update-company-details-form" method="POST" action="{{ url('/admin/update-user') }}">
                                    {{ csrf_field() }}
                                    <div id="company-details-section"></div>
                                </form>
                            </div>

                        </div>
                    </div>


                </div>


                <div class="tab-pane" id="messages-v">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">User Role Levels</h3></div>
                        <div class="panel-body animated fadeIn">
                            <div class="row pad-20">
                                <div class="col-lg-12">
                                    <table class="table table-responsive table-bordered ">
                                        <tbody>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 1</span>
                                                User has Read Only Access
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 2</span>
                                                Can update Task Status (cant reverse)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 3</span></td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 4</span></td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 5</span>

                                                Can add and update Tasks, Categories, Owners, Test Specs, Areas and Systems. Reverse Task Status
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 6</span>
                                                Update Rules and Task Parameters
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 7</span>
                                                Import Schedule
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 8</span>
                                                Add and Update Users , Delete Models
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><span class="label label-info font-sm" id="search-stat-total">Level 9</span>
                                                Super User - PES Admin
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane hide" id="settings-v">Settings Tab.</div>

            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script src="/js/rowsys/admin/users.js"></script>
    <script>
        $(document).ready(function () {

        });

        function deleteUser(id){
            var txt;
            var r = confirm("Are you sure?");
            if (r == true) {
                window.location.href = '/user/Delete/' + id;
            }
        }
    </script>

@stop

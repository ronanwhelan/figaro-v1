
<!-- Modal -->
<div class="modal fade" id="user-settings-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User Settings</h4>
            </div>
            <div class="modal-body">
                <div id="user-setting-details-section">
                    <!--Search Modal -->
                    @include('rowsys._app.includes.loading.spinner.spinner_simple')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
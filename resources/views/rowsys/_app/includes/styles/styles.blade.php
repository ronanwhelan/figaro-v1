<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<!-- CSS -->
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/font-awesome.min.css">

<link rel="stylesheet" href="/css/vendor/notify/animate.css">
<link rel="stylesheet" href="/css/vendor/wait-me/waitMe.min.css">
<link rel="stylesheet" href="/css/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="/css/vendor/bootstrap-datepicker/bootstrap-datepicker.css">

<link rel="stylesheet" href="/css/style.css">

<!-- Fav & touch icons -->
<link rel="shortcut icon" href="/img/branding/favicon.png" />

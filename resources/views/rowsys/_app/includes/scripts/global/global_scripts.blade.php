<!-- Resource jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="/js/jquery.menu-aim.js"></script>
<!-- Resource Bootstrap -->
<script src="/js/bootstrap.min.js"></script>

<!-- Notifications Plugin -->
<script src="/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>
<!-- Wait Me Plugin -->
<script src="/js/plugin/wait-me/waitMe.min.js"></script>
<script src="/js/plugin/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> <!-- Bootstrap Datepicker-->
<script src="/js/plugin/bootstrap-select/bootstrap-select.min.js"></script> <!-- Bootstrap Select-->
<script src="/js/plugin/select2/select2.min.js"></script> <!-- Bootstrap Select2-->

<!-- Custom App Scripts -->
<script src="/js/rowsys/app/main.js"></script>

<script src="/js/main.js"></script> <!-- Resource jQuery -->


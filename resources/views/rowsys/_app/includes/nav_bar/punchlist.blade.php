<!-- Side Navigation -->
<nav class="nav-slide">
    <div class="ns-header">
        <a href="#0" class="nav-close"><i class="fa fa-times" aria-hidden="true"></i></a>
        <a href="/dashboard/admin" class="nav-close-logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
    </div>

    <ul>
        <li class="dashboard ">
            <a href="/items/dashboard" class="">Dashboard</a>
        </li>

        <li class="new">
            <a href="/items/create/mobile" class="">New</a>
        </li>

        <li class="new hide">
            <a role="button" data-toggle="collapse" href="#nav1">New</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav1">
                <ul class="sub-nav-dropdown">
                    <li><a href="/items/create/mobile">Add (Mobile)</a></li>
                    <li><a href="/items/create">Add (Alternative look)</a></li>

                </ul>
            </div>
        </li>


        <li class="rules">
            <a href="/items" class="">Items</a>
        </li>

        <li class="build hide ">
            <a role="button" data-toggle="collapse" href="#nav4">Models</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav4">
                <ul class="sub-nav-dropdown">
                    <li><a href="/task/create">Types</a></li>
                    <li><a href="/task/type">Groups</a></li>
                    <li><a href="/system">Systems</a></li>
                    <li><a href="/area">Areas</a></li>
                    <li><a href="/area">Categories</a></li>
                    <li><a href="/area">Classes</a></li>
                </ul>
            </div>
        </li>



        <li class="user visible-xs">
            <a href="javascript:showUserSettingsModal({{ Auth::user()->id }});" class="">Account Settings</a>
        </li>
        <li class="sign-out visible-xs">
            <a href="/logout" class="">Logout</a>
        </li>

        <li class="main-menu">
            <a href="/welcome" class="">Main Menu</a>
        </li>

    </ul>
</nav> <!-- .nav-slide -->
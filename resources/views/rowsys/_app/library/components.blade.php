@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop


@section('content')

            <!-- ==========================CONTENT STARTS HERE ========================== -->

            <h1>Library</h1>

            <div class="row">
                <div class="col-lg-12">
                    <h3>UI Elements</h3>
                    <article>
                        <div class="alert alert-warning fade in">
                            <button data-dismiss="alert" class="close">×</button>
                            <i class="fa-fw fa fa-warning"></i>
                            <strong>Warning</strong> Your monthly traffic is reaching limit.
                        </div>
                        <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close">×</button>
                            <i class="fa-fw fa fa-check"></i>
                            <strong>Success</strong> The page has been added.
                        </div>
                        <div class="alert alert-info fade in">
                            <button data-dismiss="alert" class="close">×</button>
                            <i class="fa-fw fa fa-info"></i>
                            <strong>Info!</strong> You have 198 unread messages.
                        </div>
                        <div class="alert alert-danger fade in">
                            <button data-dismiss="alert" class="close">×</button>
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> The daily cronjob has failed.
                        </div>
                    </article>
                </div>
            </div>

                <div class="row">
                    <div id="myTabs" class="col-lg-12">
                        <h3>Example of a tab system</h3>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                            <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                            <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div id ="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                <h3>Tab 1</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                <h3>Tab 2</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="messages">
                                <h3>Tab 3</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Buttons</h3>
                    <article>
                        <p class="alert alert-info">The icons below are the most basic ones, without any icons or additional css applied to it</p>
                        <p>Buttons come in 6 different default color sets
                            <code>.btn .btn-*</code>
                            <code>.btn-default, .btn-primary, .btn-success... etc</code>
                        </p>
                        <a class="btn btn-default" href="javascript:void(0);">Default</a>
                        <a class="btn btn-primary" href="javascript:void(0);">Primary</a>
                        <a class="btn btn-success" href="javascript:void(0);">Success</a>
                        <a class="btn btn-info" href="javascript:void(0);">Info</a>
                        <a class="btn btn-warning" href="javascript:void(0);">Warning</a>
                        <a class="btn btn-danger" href="javascript:void(0);">Danger</a>
                        <a class="btn btn-default disabled" href="javascript:void(0);">Disabled</a>
                        <a class="btn btn-link" href="javascript:void(0);">Link</a>
                    </article>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Button dropdowns</h3>
                    <article>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Deafault <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">Primary <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider" role="separator"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button">Success <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider" role="separator"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">info <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider" role="separator"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" type="button">Warning <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider" role="separator"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">Danger <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider" role="separator"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                    </article>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Justified button groups</h3>
                    <article>
                        <div aria-label="Justified button group" role="group" class="btn-group btn-group-justified"> <a role="button" class="btn btn-default" href="#">Left</a> <a role="button" class="btn btn-default" href="#">Middle</a> <a role="button" class="btn btn-default" href="#">Right</a> </div>
                    </article>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <h3>Collapsible Groups</h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Collapsible Group Item #1
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Collapsible Group Item #2
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Progress bars</h3>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">60%</div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">0%</div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 2%;">2%</div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                            <span class="sr-only">20% Complete</span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                            <span class="sr-only">60% Complete (warning)</span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                            <span class="sr-only">45% Complete</span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-success" style="width: 35%"><span class="sr-only">35% Complete (success)</span></div>
                        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 20%"><span class="sr-only">20% Complete (warning)</span></div>
                        <div class="progress-bar progress-bar-danger" style="width: 10%"><span class="sr-only">10% Complete (danger)</span></div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Contextual alternatives</h3>

                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                        <div class="panel-body">Panel content</div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                        <div class="panel-body">Panel content</div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                        <div class="panel-body">Panel content</div>
                    </div>

                    <div class="panel panel-warning">
                        <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                        <div class="panel-body">Panel content</div>
                    </div>

                    <div class="panel panel-danger">
                        <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                        <div class="panel-body">Panel content</div>
                    </div>

                    <h3>With tables</h3>

                    <div class="panel panel-primary">
                        <div class="panel-heading">Panel heading</div>
                        <div class="panel-body"><p>Panel content</p></div>

                        <!-- Table -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr>

                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td> </tr>
                            <tr> <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop

@section ('local_scripts')


@stop
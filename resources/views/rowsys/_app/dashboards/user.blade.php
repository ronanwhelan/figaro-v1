@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <style>.well {
            background-color: lightcyan !important;
        / / black as a hint
        }</style>

@stop

@section ('head_js')
@stop

@section('content')
    <div class="row animated fadeIn">
        <div class="col-lg-12">
            <h3 class="pull-left">Dashboard</h3>
            @if($filterOn === 0)
                <button class="btn btn-filter pull-right" onclick="showUserFilterModal()">Filter <i class="fa fa-filter"></i></button>
            @else
                <button class="btn btn-success pull-right" onclick="showUserFilterModal()">Filter On <i class="fa fa-filter"></i></button>
            @endif

        </div>
    </div>

    <div class="row animated fadeIn">

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table">View Tasks</a></li>
                    </ul>
                </div>

                <a class="" href="/tasks/table?user_filtered_tasks=on" target="_blank"><h4 class="header-title"> Open Tasks</h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stats['totalNotDone']}}</h2>

                        <p class="text-muted">{{$stats['percDone']}} % Complete ({{$stats['completed']}} out of {{$stats['total']}})</p>
                    </div>

                    <div class="progress progress-bar-success-alt progress-sm">
                        <div style="width: {{$stats['percDone']}}%;" aria-valuemax="100"
                             aria-valuemin="0" aria-valuenow="10" role="progressbar"
                             class="progress-bar progress-bar-success">
                            <span class="">{{$stats['percDone']}}% Complete</span>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>


        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table">View Tasks</a></li>
                    </ul>
                </div>

                <a class="" href="/tasks/table?user_filtered_tasks=on" target="_blank"><h4 class="header-title"> Earned Hrs </h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stats['earnedPerc']}} %</h2>

                        <p class="text-muted">{{$stats['earnedHrs']}} Earned from a total of {{$stats['targetHrs']}}</p>
                    </div>

                    <div class="progress progress-bar-success-alt progress-sm">
                        <div style="width: {{$stats['percDone']}}%;" aria-valuemax="100"
                             aria-valuemin="0" aria-valuenow="10" role="progressbar"
                             class="progress-bar progress-bar-success">
                            <span class="">{{$stats['earnedPerc']}}% Complete</span>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>


        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table?dashboard_past_due_on=on" target="_blank">View Past Due Tasks</a></li>

                    </ul>
                </div>


                <a class="" href="/tasks/table?dashboard_past_due_on=on" target="_blank"><h4 class="header-title">Tasks Overdue</h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stats['pastDue']}}</h2>

                        <p class="text-muted">{{$stats['pastDuePercent']}} % of Total overdue</p>
                    </div>
                    <div class="progress progress-bar-danger-alt progress-sm">
                        <div style="width: {{$stats['pastDuePercent']}}%;"
                             aria-valuemax="100"
                             aria-valuemin="0"
                             aria-valuenow="{{$stats['pastDuePercent']}}"
                             role="progressbar"
                             class="progress-bar progress-bar-danger">
                            <span class="sr-only">{{$stats['pastDuePercent']}}% Complete</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end .card-box -->
        </div>
        <!-- end col -->


        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="/tasks/table">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table">View Tasks</a></li>


                    </ul>
                </div>
                <a class="" href="/tasks/table?dashboard_due_in_weeks={{$stats['numOfWeeksForLink']}}" target="_blank"><h4
                            class="header-title">{{$stats['tasksDueTextForDisplay']}}</h4></a>

                <div class="widget-box">
                    <div class="widget-detail">

                        <h2 class="amount">{{$stats['tasksDueCountForDisplay']}}</h2>

                        <p class="text-muted">{{$stats['duePerc']}}% of all my tasks</p>
                    </div>
                    <div class="progress progress-bar-danger-alt progress-sm">
                        <div style="width: {{$stats['duePerc']}}%;"
                             aria-valuemax="100"
                             aria-valuemin="0"
                             aria-valuenow="{{$stats['duePerc']}}"
                             role="progressbar"
                             class="progress-bar progress-bar-primary">
                            <span class="sr-only">{{$stats['duePerc']}}% Complete</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end .card-box -->

        <!-- end col -->
   <!-- end .card-box -->



    </div><!-- end row -->

    <div class="row animated fadeIn pad-20">

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <a class="" href="/tasks/table?my_tasks=on" target="_blank"><h4 class="header-title">My Tasks</h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stats['myOpenTaskCount']}}</h2>

                        <p class="text-muted">{{$stats['myTaskPerc']}} % Complete ({{$stats['myTaskDone']}} out of {{$stats['myTaskCount']}})</p>
                    </div>
                    <div class="progress progress-bar-success-alt progress-sm">
                        <div style="width: {{$stats['myTaskPerc']}}%;" aria-valuemax="100"
                             aria-valuemin="0" aria-valuenow="10" role="progressbar"
                             class="progress-bar progress-bar-success">
                            <span class="">{{$stats['myTaskPerc']}}% Complete </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6">

            <div class="card-box">
                <a class="" href="/tasks/table?my_flagged_tasks=on" target="_blank"><h4 class="header-title">My Watch List</h4></a>

                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-eraser"></i>
                    </a>

                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/watchlist/my/clear">Clear my watchlist</a></li>
                    </ul>

                </div>
                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stats['myFlagCount']}}</h2>

                        <p class="text-muted">{{$stats['myFlaggedPerc']}} % Complete ({{$stats['myFlaggedDone']}} out of {{$stats['myFlagCount']}})</p>
                    </div>
                    <div class="progress progress-bar-success-alt progress-sm">
                        <div style="width: {{$stats['myFlaggedPerc']}}%;" aria-valuemax="100"
                             aria-valuemin="0" aria-valuenow="10" role="progressbar"
                             class="progress-bar progress-bar-success">
                            <span class="">{{$stats['myFlaggedPerc']}}% Complete </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                @if( Auth::user()->role > 8)
                    <div class="dropdown pull-right">
                        <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                            <i class="fa fa-eraser"></i>
                        </a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="/watchlist/project/clear">Remove All Flags</a></li>
                        </ul>
                    </div>
                @endif

                <a class="" href="/tasks/table?flagged_tasks=on" target="_blank"><h4 class="header-title">Project Watch List</h4></a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stats['flagCount']}}</h2>

                        <p class="text-muted">{{$stats['flaggedPerc']}} % Complete ({{$stats['flaggedDone']}} out of {{$stats['flagCount']}})</p>
                    </div>
                    <div class="progress progress-bar-success-alt progress-sm">
                        <div style="width: {{$stats['flaggedPerc']}}%;" aria-valuemax="100"
                             aria-valuemin="0" aria-valuenow="10" role="progressbar"
                             class="progress-bar progress-bar-success">
                            <span class="">{{$stats['flaggedPerc']}}% Complete </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6">

            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table">View Tasks</a></li>
                    </ul>
                </div>
                <a class="" href="/tasks/table?active_tasks=on" target="_blank">
                    <h4 class="header-title">Active Tasks
                        <small>(filtered)</small>
                    </h4>
                </a>

                <div class="widget-box">
                    <div class="widget-detail">
                        <h2 class="amount">{{$stepStats['activeCount']}}</h2>

                        <p class="text-muted">{{$activePercentage}} % of open tasks</p>
                    </div>
                    <div class="progress progress-bar-success-alt progress-sm">
                        <div style="width: {{$activePercentage}}%;" aria-valuemax="100"
                             aria-valuemin="0" aria-valuenow="10" role="progressbar"
                             class="progress-bar progress-bar-success">
                            <span class="">{{$activePercentage}}% Complete</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>



    <div class="row animated fadeIn pad-20">

        <div class="col-md-6">

            <div class="row">

                <div class="col-md-6">
                    <div class="card-box">
                        <a class="" href="/tasks/table?my_teams_tasks=on" target="_blank">
                            <h4 class="header-title">My Team Tasks
                                <small>(non filtered)</small>
                            </h4>
                        </a>

                        <div class="widget-box">
                            <div class="widget-detail">
                                <h2 class="amount">{{$stats['roleOpenTaskCount']}}</h2>

                                <p class="text-muted">{{$stats['roleTaskPerc']}} % Complete ({{$stats['roleTaskDone']}} out of {{$stats['roleTaskCount']}})</p>
                            </div>
                            <div class="progress progress-bar-success-alt progress-sm">
                                <div style="width: {{$stats['roleTaskPerc']}}%;" aria-valuemax="100"
                                     aria-valuemin="0" aria-valuenow="10" role="progressbar"
                                     class="progress-bar progress-bar-success">
                                    <span class="">{{$stats['roleTaskPerc']}}% Complete </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

               <div class="col-md-6">
                    <div class="card-box">
                        <a class="" href="/tasks/table?my_teams_tasks=on" target="_blank">
                            <h4 class="header-title">My Team Earned Hrs
                            </h4>
                        </a>

                        <div class="widget-box">
                            <div class="widget-detail">
                                <h2 class="amount">{{$stats['teamEarnedPerc']}} %</h2>

                                <p class="text-muted">{{$stats['teamEarnedHrs']}} Earned out of {{$stats['teamTargetHrs']}}</p>
                            </div>
                            <div class="progress progress-bar-success-alt progress-sm">
                                <div style="width: {{$stats['teamEarnedPerc']}}%;" aria-valuemax="100"
                                     aria-valuemin="0" aria-valuenow="10" role="progressbar"
                                     class="progress-bar progress-bar-success">
                                    <span class="">{{$stats['teamEarnedPerc']}}% Complete </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>




            <div class="panel panel-default">
                <div class="panel-heading"><a class="" href="/tasks/table?prep_active_tasks=on">
                        <h4>
                            <small><i class="fa fa-pencil-square-o"></i> Preparation Stage Step Breakdown</small> {{$stepStats['genActiveCount']}} Active
                        </h4>
                    </a></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class=" well text-align-center">
                                    <p class="font-xs">Preparation <i class="fa fa-pencil-square-o"></i></p>
                                    <h4 class="text-align-center">{{$stepStats['inGenCount']}}  </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="well text-align-center ">
                                    <p class="font-xs">Review <i class=" fa fa-search "></i></p>
                                    <h4 class="text-align-center">{{$stepStats['inReviewCount']}}</h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="well text-align-center">
                                    <p class="font-xs">Re-Issue <i class=" fa fa-repeat "></i></p>
                                    <h4 class="text-align-center">{{$stepStats['inReIssueCount']}}</h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="well bg-info text-align-center">
                                    <p class="font-xs"> Sign Off <i class=" fa fa-thumbs-o-up"></i></p>
                                    <h4 class="text-align-center">{{$stepStats['inSignOff']}}</h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-warning">
                <div class="panel-heading"><a class="" href="/tasks/table?exec_active_tasks=on">
                        <h4>
                            <small><i class="fa fa-gears"></i> Execution Stage Step Breakdown</small> {{$stepStats['execActiveCount']}} Active
                        </h4>
                    </a></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="well text-align-center">
                                    <p class="font-xs">Execution <i class="fa fa-gears"></i></p>
                                    <h4 class="text-align-center">{{$stepStats['execInGenCount']}}  </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="well text-align-center">
                                    <p class="font-xs">Review <i class=" fa fa-search "></i></p>
                                    <h4 class="text-align-center">{{$stepStats['execInReviewCount']}}  </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="well text-align-center">
                                    <p class="font-xs">Re-Issue <i class=" fa fa-repeat "></i></p>
                                    <h4 class="text-align-center">{{$stepStats['execInReIssueCount']}}  </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="well text-align-center">
                                    <p class="font-xs">Sign Off <i class=" fa fa-thumbs-o-up"></i></p>
                                    <h4 class="text-align-center">{{$stepStats['execInSignOff']}}  </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

{{--            <div id="view-rev-and-app-button-section" class="">
                <div class="alert alert-info" role="alert">
                    <button onclick=" getStepStats()" class="btn btn-primary"> View</button>
                    Review and Approval Stats Breakdown
                </div>
            </div>

            <div id="step-phase-stats-owners-section" hidden="true">
                <div class="text-align-center">
                    <p>Loading review and approval stats...</p>
                    <i class="fa fa-spinner fa-5x fa-spin"></i>

                    <p>Loading....</p>
                </div>
            </div>--}}
        </div>


        <div class="col-md-6">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="dropdown pull-right">
                        <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="/tasks/table">View Tasks</a></li>

                        </ul>
                    </div>

                    <h4 class="header-title">Task Due over the next weeks</h4>

                    <div class="widget-chart">

                        <table class="highchart"
                               data-graph-legend-disabled="0"
                               data-graph-container-before="1"
                               data-graph-type="line"

                               style="display:none"
                               data-graph-datalabels-enabled="1"
                               data-graph-color-1="#999"
                               data-graph-line-width="6"
                               data-graph-legend-layout="horizontal">

                            <caption></caption>
                            <thead>
                            <tr>
                                <th>Week</th>
                                <th>Task Due</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>One Week</td>
                                <td>{{$stats['dueWithinOneWeek']}}</td>
                            </tr>
                            <tr>
                                <td>Two Weeks</td>
                                <td>{{$stats['dueWithinTwoWeeks']}}</td>
                            </tr>
                            <tr>
                                <td>Three Weeks</td>
                                <td>{{$stats['dueWithinThreeWeeks']}}</td>
                            </tr>
                            <tr>
                                <td>One Month</td>
                                <td>{{$stats['dueWithinOneMonth']}}</td>
                            </tr>
                            <tr>
                                <td>Two Months</td>
                                <td>{{$stats['dueWithinTwoMonths']}}</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="card-box">
                    <div class="dropdown pull-right">
                        <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="tasks/dashboard/past-due">View Past Due Tasks</a></li>
                        </ul>
                    </div>

                    <h4 class="header-title">Past Due Breakdown - By Task Group</h4>

                    @if($stats['pastDue'] !== 0 )
                        <div class="widget-chart">
                            <table class="highchart"
                                   data-graph-legend-disabled="0"
                                   data-graph-container-before="1"
                                   data-graph-type="column"
                                   data-graph-xaxis-labels-font-size="60%"
                                   style="display:none"
                                   data-graph-datalabels-enabled="1"
                                   data-graph-color-1="#999">

                                <thead>
                                <tr>
                                    <th>Week</th>
                                    <th>Task Count</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pastDueByGroup as $key =>$value)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{$value}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <h2>No Over Due Tasks</h2>
                    @endif

                </div>
            </div>
        </div>
        <!-- end .col -->

        <!-- end .col -->
    </div><!-- end .row -->

    <div class="row animated fadeIn hide  pad-20">
        <div class="col-lg-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="/tasks/table">View Tasks</a></li>

                    </ul>
                </div>

                <h4 class="header-title">Task due over the next weeks</h4>

                <div class="widget-chart">

                    <table class="highchart"
                           data-graph-legend-disabled="0"
                           data-graph-container-before="1"
                           data-graph-type="line"
                           style="display:none"
                           data-graph-datalabels-enabled="1"
                           data-graph-color-1="#999"
                           data-graph-line-width="6"
                           data-graph-legend-layout="horizontal">

                        <caption></caption>
                        <thead>
                        <tr>
                            <th>Week</th>
                            <th>Task Due</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>One Week</td>
                            <td>{{$stats['dueWithinOneWeek']}}</td>
                        </tr>
                        <tr>
                            <td>Two Weeks</td>
                            <td>{{$stats['dueWithinTwoWeeks']}}</td>
                        </tr>
                        <tr>
                            <td>Three Weeks</td>
                            <td>{{$stats['dueWithinThreeWeeks']}}</td>
                        </tr>
                        <tr>
                            <td>One Month</td>
                            <td>{{$stats['dueWithinOneMonth']}}</td>
                        </tr>
                        <tr>
                            <td>Two Months</td>
                            <td>{{$stats['dueWithinTwoMonths']}}</td>
                        </tr>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

    <div class="row hide">
        <div class="col-lg-12">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">Filters</h4>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Task Name</th>
                            <th>Start Date</th>
                            <th>Due Date</th>
                            <th>Status</th>
                            <th>Assign</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>26/04/2016</td>
                            <td><span class="label label-danger">Released</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>26/04/2016</td>
                            <td><span class="label label-success">Released</span></td>
                            <td>Adminto admin</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/05/2016</td>
                            <td>10/05/2016</td>
                            <td><span class="label label-info">Pending</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-info">Work in Progress</span>
                            </td>
                            <td>Adminto Joe Blogs</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-warning">Coming soon</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>

                        <tr>
                            <td>6</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-primary">Coming soon</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>

                        <tr>
                            <td>7</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-primary">Coming soon</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- end .card-box -->
        </div>
        <!-- end .col -->
    </div><!-- end .row -->

    <!-- Modal -->
    <div class="modal fade" id="user-filter-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <form id="update-user-filter-form" action="/user/filters/update">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">My Filters &nbsp;<i onclick="resetSearchModalDropDown()" class="fa fa-undo"></i></h4>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group ">
                                    <select class="selectpicker" name="area_filter[]" id="search-areas"
                                            data-style="btn-primary"
                                            multiple title="Areas"
                                            onchange="">
                                        <optgroup label="Areas">
                                            @foreach($areaList as $area)
                                                @if(in_array($area->id,$selectedAreasArray))
                                                    <option selected value="{{$area->id}}">{{$area->name}}</option>
                                                @else
                                                    <option value="{{$area->id}}">{{$area->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group ">
                                    <select class="selectpicker" id="search-groups" name="group_filter[]"
                                            multiple title="Owners"
                                            data-style="btn-primary"
                                            onchange="getFilteredTypesByGroup()">
                                        <optgroup label="Owners">
                                            @foreach($groupList as $group)
                                                @if(in_array($group->id,$selectedGroupsArray))
                                                    <option selected value="{{$group->id}}">{{$group->name}}</option>
                                                @else
                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <select class="selectpicker" id="search-types" name="type_filter[]"
                                            multiple title="Category"
                                            data-style="btn-primary">
                                        <optgroup label="Categories">
                                            @foreach($typesList as $type)
                                                @if(in_array($type->id,$selectedTypesArray))
                                                    <option selected value="{{$type->id}}">{{$type->name}}</option>
                                                @else
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group ">
                                    <select class="selectpicker" name="stage_filter[]"
                                            multiple title="Stages"
                                            data-style="btn-primary">
                                        <optgroup label="Stages">
                                            @foreach($stageList as $stage)
                                                @if(in_array($stage->id,$selectedStagesArray))
                                                    <option selected value="{{$stage->id}}">{{$stage->name}}</option>
                                                @else
                                                    <option value="{{$stage->id}}">{{$stage->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group ">
                                    <select class="selectpicker" id="search-groups" name="role_filter[]"
                                            multiple title="Responsible Team"
                                            data-style="btn-primary">
                                        <optgroup label="Teams">
                                            @foreach($roleList as $role)
                                                @if(in_array($role->id,$selectedRolesArray))
                                                    <option selected value="{{$role->id}}">{{$role->name}}</option>
                                                @else
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success pull-right">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

@section ('local_scripts')
    <!-- Load HIGHCHARTS -->
    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>
    <!-- Load GOOGLE CHARTS-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Load APP scritps -->
    <script src="/js/rowsys/tracker/tasks/graphs.js"></script>

    <script>
        $(document).ready(function () {
            var taskTypes = '<?php echo $typesList; ?>';
            var taskGroups = '<?php echo $groupList; ?>';

            $('table.highchart').highchartTable();
            setSelectPickerFonts();
            $('.progress-bar').progressbar({
                display_text: 'fill'
            });


        });

        //Get the Owner Step Phase Stats for the Task Table Stats section
        function getStepStats() {
            $('#view-rev-and-app-button-section').fadeOut(500);
            $('#step-phase-stats-owners-section').delay(501).fadeIn(2000);

            $.ajax({
                type: "GET",
                url: "/dashboard/step-stats", success: function (result) {
                    $("#step-phase-stats-owners-section").html(result);
                    console.log(result);
                }
            });
        }

        function clearMyWatchList() {
            alert('my watch list cleared');
        }

        function clearProjectWatchList() {
            alert('project watch list cleared');
        }

    </script>
@stop